import 'package:bedo_shop_admin/presentation/screens/add_courier_screen.dart';
import 'package:bedo_shop_admin/presentation/screens/add_discount_screen.dart';
import 'package:bedo_shop_admin/presentation/screens/add_shop.dart';
import 'package:bedo_shop_admin/presentation/screens/edit_shop.dart';
import 'package:bedo_shop_admin/presentation/screens/home.dart';
import 'package:bedo_shop_admin/presentation/screens/homePage.dart';
import 'package:bedo_shop_admin/presentation/screens/login.dart';
import 'package:bedo_shop_admin/presentation/screens/menu_bedo.dart';
import 'package:bedo_shop_admin/presentation/screens/orders_screen.dart';
import 'package:bedo_shop_admin/presentation/screens/prouct_pages/add_new_menu_item_page.dart';
import 'package:bedo_shop_admin/presentation/screens/prouct_pages/products_page.dart';
import 'package:bedo_shop_admin/presentation/screens/settings_pages/settings.dart';
import 'package:bedo_shop_admin/presentation/screens/sign_up.dart';
import 'package:bedo_shop_admin/presentation/screens/splash.dart';
import 'package:bedo_shop_admin/presentation/screens/support_pages.dart';
import 'package:bedo_shop_admin/presentation/widgets/comments_page.dart';
import 'package:bedo_shop_admin/presentation/screens/courier_list_screen.dart';

final routes = {
  Login.routeName: (context) => Login(),
  SignUp.routeName: (context) => SignUp(),
  Debug.routeName: (context) => Debug(),
  Splash.routeName: (context) => Splash(),
  AddShopPage.routeName: (context) => AddShopPage(),
  EditShopInformation.routeName: (context) => EditShopInformation(),
  SettingsMain.routeName: (context) => SettingsMain(),
  CommentsPage.routeName: (context) => CommentsPage(),
  OrdersScreen.routeName: (context) => OrdersScreen(),
  HomePage.routeName: (context) => HomePage(),
  CourierListScreen.routeName: (context) => CourierListScreen(),
  Settings.routeName: (context) => Settings(),
  SupportPages.routeName: (context) => SupportPages(),
  CourierCreateScreen.routeName: (context) => CourierCreateScreen(),
  AddNewMenuItem.routeName: (context) => AddNewMenuItem(),
  AddDiscountScreen.routeName: (context) => AddDiscountScreen(),
  ProductsPage.routeName: (context) => ProductsPage(),
};
