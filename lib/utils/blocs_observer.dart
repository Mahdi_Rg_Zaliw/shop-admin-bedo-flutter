import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:bloc/bloc.dart';

/// {@template counter_observer}
/// [BlocObserver] for the counter application which
/// observes all state changes.
/// {@endtemplate}
// class CounterObserver extends BlocObserver {
//   @override
//   void onChange(BlocBase bloc, Change change) {
//     super.onChange(bloc, change);
//     printYellowBg('${bloc.runtimeType} $change');
//   }
// }

// class GlobalBlocObserver extends BlocObserver {
//   @override
//   void onChange(Cubit cubit, Change change) {
//     printYellowBg(change);
//     super.onChange(cubit, change);
//   }

//   @override
//   void onError(Cubit cubit, Object error, StackTrace stackTrace) {
//     printYellowBg('$error, $stackTrace');
//     super.onError(cubit, error, stackTrace);
//   }
// }

class GlobalBlocObserver extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object event) {
    printYellowBg('>>>>>GlobalBlocObserver::onEvent ');
    printYellowBg(' $event');
    super.onEvent(bloc, event);
  }

  @override
  void onChange(Cubit cubit, Change change) {
    printYellowBg('>>>>>GlobalBlocObserver::onChange ');
    printYellowBg(' $change');
    super.onChange(cubit, change);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    printYellowBg('>>>>>GlobalBlocObserver::onTransition ');
    printYellowBg(' $transition');
    super.onTransition(bloc, transition);
  }

  @override
  void onError(Cubit cubit, Object error, StackTrace stackTrace) {
    printYellowBg('>>>>>GlobalBlocObserver::onError ');
    printYellowBg(' $error, $stackTrace');
    super.onError(cubit, error, stackTrace);
  }
}
