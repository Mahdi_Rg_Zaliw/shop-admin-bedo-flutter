import 'package:flutter/material.dart';

class DeviceDimensions {
  static heightSize(context) {
    return MediaQuery.of(context).size.height;
  }

  static widthSize(context) {
    return MediaQuery.of(context).size.width;
  }
}
