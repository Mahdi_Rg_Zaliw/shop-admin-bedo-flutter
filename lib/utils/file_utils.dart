import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';

Future<File> fileFromImageUrl(String url) async {
  final response = await http.get('$url');

  final documentDirectory = await getApplicationDocumentsDirectory();

  final file = File('${documentDirectory.path}imagetest.png');

  file.writeAsBytesSync(response.bodyBytes);

  return file;
}
