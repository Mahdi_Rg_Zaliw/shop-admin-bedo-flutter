import 'package:ansicolor/ansicolor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

AnsiPen pen = new AnsiPen()
  ..white()
  ..rgb(r: 1.0, g: 0.8, b: 0.2);
void printRedBg(title) {
  print('\x1B[41m \x1B[37m \x1B[1m $title \x1B[1m \x1B[0m\x1B[40m');
  // print('\x1B[31 $title \x1B[0m');
  // print(title);
}

void printMagentBg(title) {
  print('\x1B[45m \x1B[37m \x1B[1m $title \x1B[2m \x1B[30m\x1B[40m');
  // print('\x1B[31 $title \x1B[0m');
  // print(title);
}

void printYellowBg(title) {
  print('\x1B[43m \x1B[30m \x1B[1m $title \x1B[2m \x1B[40m');
  // print('\x1B[31 $title \x1B[0m');
  // print(title);
}
// \x1B[42m

void printGreenBg(title) {
  print('\x1B[42m \x1B[30m \x1B[1m $title \x1B[2m \x1B[40m');
  // print('\x1B[31 $title \x1B[0m');
  // print(title);
}

void printCyanBg(title) {
  print('\x1B[46m \x1B[30m \x1B[1m $title \x1B[2m \x1B[40m');
  // print('\x1B[31 $title \x1B[0m');
  // print(title);
}

void printWhiteBg(title) {
  print('\x1B[47m \x1B[30m \x1B[1m $title \x1B[2m \x1B[40m');
  // print('\x1B[31 $title \x1B[0m');
  // print(title);
}

void printBlueBg(title) {
  print('\x1B[44m \x1B[37m \x1B[1m $title \x1B[2m \x1B[30m\x1B[40m');
  // print('\x1B[31 $title \x1B[0m');
  // print(title);
}

void printOrange(title) {
  print('\x1B[38;2;252;127;0m $title \x1B[48;2;252;127;0m ');
  // print('\x1B[31 $title \x1B[0m');
  // print(title);
}

String makeRGB(int r, int g, int b, String title) {
  return '\x1B[38;2;$r;$g;${b}m	$title \x1B[48;2;$r;$g;${b}m';
}

String makeBGRGB(int r, int g, int b, String title) {
  return '\x1B[38;2;0;0;0m \x1B[48;2;$r;$g;${b}m	$title \x1B[48;2;$r;$g;${b}m';
}

void printPink(String title) {
  print(makeRGB(211, 56, 211, title));
}

void printColor(Color color, Object title) {
  String coloredObject =
      makeRGB(color.red, color.green, color.blue, title.toString());
  print(coloredObject);
}

void printColorBgLight(Color color, Object title) {
  String coloredObject =
      makeBGRGB(color.red, color.green, color.blue, title.toString());
  print(coloredObject);
}

// Any palette color (with V in [0-255]) ==	\x1B[38;5;Vm	\x1B[48;5;Vm
// Any RGB color (with values in [0-255])	== \x1B[38;2;R;G;Bm	\x1B[48;2;R;G;Bm
// The [38; directs command to the foreground. If you want to
// change the background color instead, use [48; instead

AnsiPen someColorPen = AnsiPen()..rgb(r: .5, g: .2, b: .4);
void printSomeColor(title) {
  print(someColorPen('$title'));
  // final c = Colors.red;
}
