import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:flutter/cupertino.dart';

String relativeTime(DateTime dateTime, BuildContext context) {
  final String ago = S.of(context).ago;
  Duration diff = DateTime.now().difference(dateTime);
  if (diff.inDays > 365)
    return "${(diff.inDays / 365).floor()} ${(diff.inDays / 365).floor() == 1 ? S.of(context).year : S.of(context).years} $ago";
  if (diff.inDays > 30)
    return "${(diff.inDays / 30).floor()} ${(diff.inDays / 30).floor() == 1 ? "month" : S.of(context).months} $ago";
  if (diff.inDays > 7)
    return "${(diff.inDays / 7).floor()} ${(diff.inDays / 7).floor() == 1 ? S.of(context).week : S.of(context).weeks} $ago";
  if (diff.inDays > 0)
    return "${diff.inDays} ${diff.inDays == 1 ? S.of(context).day : S.of(context).days} " +
        ago;
  if (diff.inHours > 0)
    return "${diff.inHours} ${diff.inHours == 1 ? S.of(context).hour : S.of(context).hours} $ago";
  if (diff.inMinutes > 0)
    return "${diff.inMinutes} ${diff.inMinutes == 1 ? S.of(context).minute : S.of(context).minutes} $ago";
  return S.of(context).justNow;
}

int getWeekDayIndexAr(String enm) {
  int day;
  switch (enm) {
    case 'SAT':
      {
        day = 0;
      }

      break;
    case 'SUN':
      {
        day = 1;
      }

      break;
    case 'MON':
      {
        day = 2;
      }

      break;
    case 'TUE':
      {
        day = 3;
      }

      break;
    case 'WEN':
      {
        day = 4;
      }

      break;
    case 'THU':
      {
        day = 5;
      }

      break;
    case 'FRI':
      {
        day = 6;
      }

      break;
    default:
      {
        day = 6;
      }
  }
  return day;
}

String makeVarEnums(int d) {
  String day;
  switch (d) {
    case 1:
      {
        day = 'SUN';
      }

      break;
    case 2:
      {
        day = 'MON';
      }

      break;
    case 3:
      {
        day = 'TUE';
      }

      break;
    case 4:
      {
        day = 'WEN';
      }

      break;
    case 5:
      {
        day = 'THU';
      }

      break;
    case 6:
      {
        day = 'FRI';
      }

      break;
    case 0:
      {
        day = 'SAT';
      }

      break;
    default:
      {
        day = 'FRI';
      }
  }
  return day;
}
