import 'package:bedo_shop_admin/app.dart';
import 'package:bedo_shop_admin/bloc/courier_bloc/courier_bloc.dart';
import 'package:bedo_shop_admin/bloc/courier_bloc/courier_state.dart';
import 'package:bedo_shop_admin/bloc/conversations_bloc/conversations_bloc.dart';
import 'package:bedo_shop_admin/bloc/discount_bloc/discount_bloc.dart';
import 'package:bedo_shop_admin/bloc/comments_bloc/approved_comments_fetch_bloc/approvedcomments_bloc.dart';
import 'package:bedo_shop_admin/bloc/comments_bloc/pending_comments_fetch_bloc/pendingcomments_bloc.dart';
import 'package:bedo_shop_admin/bloc/comments_bloc/total_comments_fetch_bloc/total_comments_bloc.dart';
import 'package:bedo_shop_admin/bloc/orders_bloc/orders_bloc.dart';
import 'package:bedo_shop_admin/bloc/menu_bloc/menu_bloc.dart';
import 'package:bedo_shop_admin/bloc/menu_bloc/menu_state.dart';
import 'package:bedo_shop_admin/bloc/reports_bloc/reports_bloc.dart';
import 'package:bedo_shop_admin/bloc/settings_bloc/settings_bloc.dart';
import 'package:bedo_shop_admin/bloc/shop_bloc/shop_bloc.dart';
import 'package:bedo_shop_admin/bloc/transactions_bloc/transactions_bloc.dart';
import 'package:bedo_shop_admin/utils/blocs_observer.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path_provider/path_provider.dart';

import 'bloc/auth_bloc/auth_bloc.dart';
import 'bloc/lang_bloc/lang_bloc.dart';
import 'bloc/notifications_bloc/notifications_bloc.dart';
import 'bloc/router_bloc/signinrouter_bloc.dart';

// import 'package:bloc/bloc.dart';
// import 'package:flutter/material.dart';

// import 'app.dart';
// import 'counter_observer.dart';

// void main() {
//   Bloc.observer = CounterObserver();
void main() async {
  // Bloc.observer = GlobalBlocObserver();

  WidgetsFlutterBinding.ensureInitialized();
  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory: await getApplicationDocumentsDirectory(),
  );

  runApp(MultiBlocProvider(
    providers: [
      BlocProvider<AuthBloc>(create: (context) => AuthBloc()),
      BlocProvider<TransactionsBloc>(create: (context) => TransactionsBloc()),
      BlocProvider<SigninrouterBloc>(create: (context) => SigninrouterBloc()),
      BlocProvider<LangBloc>(create: (context) => LangBloc()),
      BlocProvider<OrdersBloc>(create: (context) => OrdersBloc()),
      BlocProvider<DiscountBloc>(create: (context) => DiscountBloc()),
      BlocProvider<ApprovedcommentsBloc>(
          create: (context) => ApprovedcommentsBloc()),
      BlocProvider<TotalCommentsBloc>(create: (context) => TotalCommentsBloc()),
      BlocProvider<PendingcommentsBloc>(
          create: (context) => PendingcommentsBloc(
              BlocProvider.of<ApprovedcommentsBloc>(context))),
      BlocProvider<MenuBloc>(
        create: (context) => MenuBloc(MenuInitial()),
      ),
      BlocProvider<PendingcommentsBloc>(
          create: (context) => PendingcommentsBloc(
              BlocProvider.of<ApprovedcommentsBloc>(context))),
      BlocProvider<MenuBloc>(create: (context) => MenuBloc(MenuInitial())),
      BlocProvider<ConversationBloc>(create: (context) => ConversationBloc()),
      BlocProvider<CourierBloc>(
          create: (context) => CourierBloc(CourierInitial())),
      BlocProvider<ReportsBloc>(create: (context) => ReportsBloc()),
      BlocProvider(create: (context) => SettingsBloc()),
      BlocProvider(create: (context) => ShopBloc()),
      BlocProvider(create: (context) => NotificationsBloc()),
    ],
    child: App(),
  ));
}
