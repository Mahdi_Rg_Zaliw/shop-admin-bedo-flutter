import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

enum orderPromotionCondition { timely, first_order, percentage }
enum promotionType { fixed, percent }

class Discount extends Equatable {
  String id;
  DateTime startDate;
  DateTime endDate;
  orderPromotionCondition condition;
  promotionType type;
  double percent;
  double maximumPromotion;
  String promotionCode;
  int useLimitCount;
  DateTime createdAt;
  DateTime updatedAt;
  bool isExpired;

  Discount({
    @required this.startDate,
    @required this.endDate,
    @required this.percent,
    @required this.isExpired,
    @required this.promotionCode,
    this.id,
    this.type = promotionType.percent,
    this.condition = orderPromotionCondition.timely,
    this.createdAt,
    this.updatedAt,
    this.maximumPromotion,
    this.useLimitCount,
  });

  @override
  String toString() {
    return this.promotionCode +
        this.endDate.toString() +
        this.isExpired.toString();
  }

  @override
  List<Object> get props =>
      [id, startDate, endDate, percent, promotionCode, isExpired];
}
