import 'package:bedo_shop_admin/constants/constants.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SecureStorage {
  static FlutterSecureStorage _storage = FlutterSecureStorage();

  static Future<void> setId(String id) async {
    await _storage.write(key: userIdKey, value: id);
  }

  static Future<void> setTokens(
      String accessToken, String refreshToken, String fcmToken) async {
    await _storage.write(key: accessTokenKey, value: accessToken);
    await _storage.write(key: refreshTokenKey, value: refreshToken);
    await _storage.write(key: fcmTokenKey, value: fcmToken);
  }

  static Future<String> getToken(String key) async {
    return await _storage.read(key: key);
  }

  static Future<String> getId(String key) async {
    return await _storage.read(key: key);
  }

  static Future<void> removeToken(String key) async {
    return await _storage.delete(key: key);
  }

  static Future<String> getLocale() async {
    return await _storage.read(key: savedLocale);
  }

  static Future<void> setLocale(String value) async {
    return await _storage.write(key: savedLocale, value: value);
  }

  static Future<void> removeId(String key) async {
    return await _storage.delete(key: key);
  }

  static Future<void> deleteAllData() async {
    await _storage.deleteAll();
  }
}
