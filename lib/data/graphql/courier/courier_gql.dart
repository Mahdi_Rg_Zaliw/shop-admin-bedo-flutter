const String getDriversByShopAdminQuery = """
  query getDriversByShopAdmin {
    getDriversByShopAdmin {
      fullName
      _id
      email
      phoneNumber
      gender
      profileImageUrl
    }
  }
  """;

const String createDriverByShopAdminMutation = """
  mutation createDriverByShopAdmin(\$data: createDriverByShopAdminInput!) {
    createDriverByShopAdmin(data: \$data) {
      _id
      fullName
      email
      phoneNumber
      gender
      state
      profileImageUrl
    }
  }
  """;

const String updateDriverInfoByShopAdminMutation = """
  mutation updateDriverInfoByShopAdmin(\$id: ID!,\$input: UpdateDriverProfileInfoByAdminInput!) {
    updateDriverInfoByShopAdmin(input: \$input, driverId: \$id) {
      _id
      fullName
      email
      phoneNumber
      gender
      profileImageUrl
    }
  }
  """;
