import 'package:bedo_shop_admin/constants/constants.dart';
import 'package:bedo_shop_admin/data/graphql/courier/courier_gql.dart';
import 'package:bedo_shop_admin/data/graphql/graphql.dart';
import 'package:bedo_shop_admin/data/secure_storage/secure_storage.dart';
import 'package:graphql/client.dart';

class CourierRequest {
  QueryOptions queryOptions;
  MutationOptions mutationOptions;

  Future<QueryResult> getDriversByShopAdminRequest() async {
    queryOptions = QueryOptions(
      fetchPolicy: FetchPolicy.cacheAndNetwork,
      document: gql(getDriversByShopAdminQuery),
    );
    // String token = await SecureStorage.getToken(accessTokenKey);
    // Map<String, String> header = {"authorization": token};
    // Graphql.addHeader(header);
    return await Graphql.client.query(queryOptions);
  }

  Future<QueryResult> createDriverByShopAdminRequest(
    Map data,
  ) async {
    mutationOptions = MutationOptions(
      document: gql(createDriverByShopAdminMutation),
      variables: {'data': data},
    );
    // String token = await SecureStorage.getToken(accessTokenKey);
    // Map<String, String> header = {"authorization": token};
    // Graphql.addHeader(header);
    return await Graphql.client.mutate(mutationOptions);
  }

  Future<QueryResult> updateDriverInfoByShopAdminRequest(
    String id,
    Map<String, dynamic> updateCourierData,
  ) async {
    mutationOptions = MutationOptions(
      document: gql(updateDriverInfoByShopAdminMutation),
      variables: {
        "id": id,
        "input": updateCourierData,
      },
    );
    // String token = await SecureStorage.getToken(accessTokenKey);
    // Map<String, String> header = {"authorization": token};
    // Graphql.addHeader(header);
    return await Graphql.client.mutate(mutationOptions);
  }
}
