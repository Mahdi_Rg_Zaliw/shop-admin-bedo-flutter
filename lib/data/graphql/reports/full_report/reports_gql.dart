const String getFullReportByShopAdmin = '''
query getFullReportByShopAdmin{
  getFullReportByShopAdmin{
    _id
    createdAt
    tracking{
      trackId
    }
    payment{
      type
    }
    finalPrice
    total
    cart{
      _id
      currency
      productsPrice
      products{
        product{
          _id
          title
          productDetails{
            price
          }
        }
        quantity
      }
    }
    delivery
    HST
  }
}
''';

const String getOrdersStatisticsListByShopAdmin = '''
query getOrdersStatisticsListByShopAdmin(\$filters: PeriodFilters) {
  getOrdersStatisticsListByShopAdmin(filters: \$filters){
    numberOfSales
    receivedOrders
    returnedOrders
    successfulOrders
    unSuccessfulOrders
    cashDaySales
    cardDaySales
    companyCommission
  }
}
''';

const String getOrderStatisticsListCountValuesByShopAdmin = '''
query getOrderStatisticsListCountValuesByShopAdmin(\$filters: PeriodHourFilters){
  getOrderStatisticsListCountValuesByShopAdmin(filters:\$filters){
    numberOfSales{
      num
      date
    }
    receivedOrders{
      num
      date
    }
    successfulOrders{
      num
      date
    }
    unSuccessfulOrders{
      num
      date
    }
    returnedOrders{
      num
      date
    }
    cashDaySales{
      num
      date
    }
    cardDaySales{
      num
      date
    }
    companyCommission{
      num
      date
    }
  }
}
''';


const String getShopTotalOrdersCountAndTotalAmountByShopAdmin = '''
query getShopTotalOrdersCountAndTotalAmountByShopAdmin(\$filters: RangeFilerInput!){
  getShopTotalOrdersCountAndTotalAmountByShopAdmin(filters:\$filters){
    totalOrderAmount
  }
}
''';
