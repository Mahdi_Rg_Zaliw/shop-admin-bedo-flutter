import 'package:bedo_shop_admin/data/graphql/graphql.dart';
import 'package:bedo_shop_admin/data/graphql/reports/full_report/reports_gql.dart';
import 'package:graphql/client.dart';

class ReportsRequest {
  QueryOptions _queryOptions;

  Future<QueryResult> getOrders() async {
    _queryOptions = QueryOptions(
      fetchPolicy: FetchPolicy.cacheAndNetwork,
      document: gql(getFullReportByShopAdmin),
    );
    return await Graphql.client.query(
      _queryOptions,
    );
  }

  Future<QueryResult> getOrdersStatistics(
      DateTime from, DateTime to, bool allTimes) async {
    _queryOptions = QueryOptions(
        fetchPolicy: FetchPolicy.cacheAndNetwork,
        document: gql(getOrdersStatisticsListByShopAdmin),
        variables: {
          'filters': {'from': from.toString(), 'to': to.toString()}
        });
    return await Graphql.client.query(
      _queryOptions,
    );
  }

  Future<QueryResult> getOrdersStatisticsForCharts(
      DateTime from, DateTime to, bool allTimes) async {
    if (allTimes) {
      print('its true');
      _queryOptions = QueryOptions(
          fetchPolicy: FetchPolicy.noCache,
          document: gql(getOrderStatisticsListCountValuesByShopAdmin));
    } else {
      _queryOptions = QueryOptions(
        fetchPolicy: FetchPolicy.noCache,
        document: gql(getOrderStatisticsListCountValuesByShopAdmin),
        variables: {
          'filters': {
            'from': from.toString(),
            'to': to.toString(),
            'hour': from == to ? 6 : 24,
          }
        },
      );
    }
    return await Graphql.client.query(
      _queryOptions,
    );
  }

  Future<QueryResult> getTotalOrdersAmount(from, to, allTimes) async {
    _queryOptions = QueryOptions(
      fetchPolicy: FetchPolicy.cacheAndNetwork,
      document: gql(getShopTotalOrdersCountAndTotalAmountByShopAdmin),
      variables: {
        'filters': {
          'from': from.toString(),
          'to': to.toString(),
        }
      },
    );
    return await Graphql.client.query(
      _queryOptions,
    );
  }
}
