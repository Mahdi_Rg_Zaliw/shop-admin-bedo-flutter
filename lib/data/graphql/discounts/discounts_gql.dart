const String getDiscountsQuery = """
query getPromotions{
  getOrderPromotionsByShopAdmin{
    _id,
    type,
    from,
    to,
    percent,
    promotionCode,
    isDeleted
  }
}
""";

const String createOrderPromotionByShopAdmin ='''
mutation createOrderPromotionByShopAdmin(\$promotionCode: String!,\$percent: Float!, \$from:Date!,\$to: Date!){
  createOrderPromotionByShopAdmin(data:{
    condition:TIMELY
    type:PERCENT
    promotionCode:\$promotionCode
    from:\$from
    to:\$to
    percent:\$percent
  }){
    _id,
    type,
    from,
    to,
    percent,
    promotionCode,
    isDeleted
  }
}
''';

const String expireOrderPromotionByShopAdmin = '''
mutation expireOrderPromotionByShopAdmin(\$orderPromotionId:ID!){
  expireOrderPromotionByShopAdmin(orderPromotionId: \$orderPromotionId){
    _id
  }
}
''';

const String updateOrderPromotionByShopAdmin = '''
mutation updateOrderPromotionByShopAdmin(\$promotionId: ID!,\$promotionCode: String!,\$percent: Float!, \$from:Date!,\$to: Date! ){
  updateOrderPromotionByShopAdmin(promotionId:\$promotionId,data:{
    condition:TIMELY
    type:PERCENT
    promotionCode:\$promotionCode
    from:\$from
    to:\$to
    percent:\$percent
  }){
    _id
  }
}
''';
