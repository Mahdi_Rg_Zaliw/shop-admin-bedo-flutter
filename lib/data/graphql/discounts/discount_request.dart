import 'package:bedo_shop_admin/constants/constants.dart';
import 'package:bedo_shop_admin/data/secure_storage/secure_storage.dart';
import 'package:bedo_shop_admin/models/discount.dart';
import 'package:graphql/client.dart';

import 'package:bedo_shop_admin/data/graphql/discounts/discounts_gql.dart';
import 'package:bedo_shop_admin/data/graphql/graphql.dart';

class DiscountRequest {
  QueryOptions queryOptions;
  // final key = {
  //   "authorization":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDRkZDgwZjA4NzdkYTYxMjBjMDM2ODciLCJzdWIiOiI2MDRkZDgwZjA4NzdkYTYxMjBjMDM2ODciLCJpc3MiOiJzcGFyay5jb20iLCJ0b2tlblR5cGUiOiJBQ0NFU1NfVE9LRU4iLCJyb2xlcyI6IlNIT1BfQURNSU4iLCJmY20iOiIiLCJ0b2tlbktleSI6ImM1Yzk4NDQxLThkNzktNDljMi05YzY3LTA3YjVkMWE0MWJiZCIsInNob3AiOiI2MDRkZDg0MjA4NzdkYWI1MjhjMDM2OGMiLCJpYXQiOjE2MTU3MTU1NjksImV4cCI6MTYxODMwNzU2OX0.L-lGUkEM4lrZZ6T9bwt4XSm68H4nqzeyO8Eexp6lxsM"
  // };

  Future<QueryResult> getDiscountsRequest() async {
    queryOptions = QueryOptions(
      fetchPolicy: FetchPolicy.cacheAndNetwork,
      document: gql(getDiscountsQuery),
    );
    // Graphql.addHeader(key);

    return await Graphql.client.query(queryOptions);
  }

  Future<QueryResult> createDiscount(Discount discount) async {
    final mutationOptions = MutationOptions(
        document: gql(createOrderPromotionByShopAdmin),
        variables: {
          "promotionCode": discount.promotionCode,
          "percent": discount.percent,
          "from": discount.startDate.toString(),
          "to": discount.endDate.toString()
        });
    // Graphql.addHeader(key);

    return await Graphql.client.mutate(mutationOptions);
  }

  Future<QueryResult> updateDiscount(Discount discount) async {
    final mutationOptions = MutationOptions(
        document: gql(updateOrderPromotionByShopAdmin),
        variables: {
          "promotionId": discount.id,
          "promotionCode": discount.promotionCode,
          "percent": discount.percent,
          "from": discount.startDate.toString(),
          "to": discount.endDate.toString()
        });
    // Graphql.addHeader(key);

    return await Graphql.client.mutate(mutationOptions);
  }

  Future<QueryResult> expireDiscount(Discount discount) async {
    final mutationOptions = MutationOptions(
        cacheRereadPolicy: CacheRereadPolicy.mergeOptimistic,
        document: gql(expireOrderPromotionByShopAdmin),
        variables: {
          "orderPromotionId": discount.id,
        });
    // Graphql.addHeader({"authorization": await SecureStorage.getToken(accessTokenKey)});

    return await Graphql.client.mutate(mutationOptions);
  }
}
