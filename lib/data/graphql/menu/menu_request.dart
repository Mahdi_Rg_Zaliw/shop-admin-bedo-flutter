import 'package:bedo_shop_admin/constants/constants.dart';
import 'package:bedo_shop_admin/data/graphql/menu/menu_gql.dart';
import 'package:bedo_shop_admin/data/graphql/graphql.dart';
import 'package:bedo_shop_admin/data/secure_storage/secure_storage.dart';
import 'package:graphql/client.dart';

class MenuRequest {
  QueryOptions queryOptions;
  MutationOptions mutationOptions;

  Future<QueryResult> getShopByShopAdminRequest() async {
    queryOptions = QueryOptions(
      fetchPolicy: FetchPolicy.cacheAndNetwork,
      document: gql(getShopByShopAdminQuery),
    );
    // String token = await SecureStorage.getToken(accessTokenKey);
    // Map<String, String> header = {"authorization": token};
    // Graphql.addHeader(header);
    return await Graphql.client.query(queryOptions);
  }

  Future<QueryResult> createShopMenuByShopAdminRequest(
    Map<String, dynamic> data,
  ) async {
    mutationOptions = MutationOptions(
      // fetchPolicy: FetchPolicy.noCache,
      document: gql(createShopMenuByShopAdminMutation),
      variables: {
        'data': data,
      },
    );
    // String token = await SecureStorage.getToken(accessTokenKey);
    // Map<String, String> header = {"authorization": token};
    // Graphql.addHeader(header);
    return await Graphql.client.mutate(mutationOptions);
  }

  Future<QueryResult> updateShopMenuByShopAdminRequest(
    String id,
    Map<String, dynamic> data,
  ) async {
    mutationOptions = MutationOptions(
      // fetchPolicy: FetchPolicy.noCache,
      document: gql(updateShopMenuByShopAdminMutation),
      variables: {
        'data': data,
        'id': id,
      },
    );
    // String token = await SecureStorage.getToken(accessTokenKey);
    // Map<String, String> header = {"authorization": token};
    // Graphql.addHeader(header);
    return await Graphql.client.mutate(mutationOptions);
  }
}
