const String getShopByShopAdminQuery = """
  query getShopMenuByShopAdmin {
    getShopByShopAdmin {
      shopMenu {
        _id
        subMenus{
          _id
          name{
            value
          }
        }
      }
    }
  }
  """;

const String createShopMenuByShopAdminMutation = """
  mutation createShopMenuByShopAdminMutation(\$data: CreateShopMenuInput!) {
    createShopMenuByShopAdmin(inputs: \$data) {
      _id
      subMenus {
        name {
          lang
          value
        }
      }
    }
  }
""";

const String updateShopMenuByShopAdminMutation = """
  mutation updateShopMenuByShopAdmin(\$data: UpdateShopMenuInput!, \$id: ID!) {
    updateShopMenuByShopAdmin(inputs: \$data, _id: \$id) {
      subMenus {
        name {
          lang
          value
        }
        products {
          _id
        }
      }
    }
  }
""";
