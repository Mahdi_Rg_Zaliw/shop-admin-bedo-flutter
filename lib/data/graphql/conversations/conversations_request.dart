import 'dart:async';

import 'package:bedo_shop_admin/data/graphql/conversations/conversations_gql.dart';
import 'package:bedo_shop_admin/data/graphql/graphql.dart';
import 'package:bedo_shop_admin/model/message_model.dart';
import 'package:graphql/client.dart';
import 'package:http/http.dart';

class ConversationsRequest {
  QueryOptions queryOptions;
  final key = {
    "authorization":
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDRkZDgwZjA4NzdkYTYxMjBjMDM2ODciLCJzdWIiOiI2MDRkZDgwZjA4NzdkYTYxMjBjMDM2ODciLCJpc3MiOiJzcGFyay5jb20iLCJ0b2tlblR5cGUiOiJBQ0NFU1NfVE9LRU4iLCJyb2xlcyI6IlNIT1BfQURNSU4iLCJ0b2tlbktleSI6IjkxNDMxYTdmLTkwZWMtNGUyNi04MjQ4LWM3ODljMDEwNzhhMSIsInNob3AiOiI2MDRkZDg0MjA4NzdkYWI1MjhjMDM2OGMiLCJpYXQiOjE2MTgyMDgxOTYsImV4cCI6MTYyMDgwMDE5Nn0.cWXWeouMMKONsM80w39LnjSBO8nB7yIEWKun1lFovNs"
  };

  Future<QueryResult> getConversations() async {
    queryOptions = QueryOptions(
        fetchPolicy: FetchPolicy.networkOnly,
        document: gql(getConversationsQuery),
        variables: {
          "filters": {
            "closed": false,
          }
        });
    // Graphql.addHeader(key);

    return await Graphql.client.query(queryOptions);
  }

  Future<QueryResult> getConversation(String id) async {
    queryOptions = QueryOptions(
        fetchPolicy: FetchPolicy.networkOnly,
        document: gql(getConversationQuery),
        variables: {"id": id});
    // Graphql.addHeader(key);

    return await Graphql.client.query(queryOptions);
  }

  Future<QueryResult> sendMessage(
      String conversationID, MessageType type, String text) async {
    var mutationOptions =
        MutationOptions(document: gql(sendMessageMutation), variables: {
      'sendMessageInput': {
        'conversation': conversationID,
        'messageType': type.toString().split('.').last,
        'text': text,
      }
    });

    // Graphql.addHeader(key);

    return await Graphql.client.mutate(mutationOptions);
  }

  Future<QueryResult> uploadFile(
      MultipartFile multipartFile, String folderName) async {
    var options = MutationOptions(document: gql(uploadFileRequest), variables: {
      'data': {'file': multipartFile, 'folderName': folderName}
    });
    // Graphql.addHeader(key);

    return await Graphql.client.mutate(options);
  }
}
