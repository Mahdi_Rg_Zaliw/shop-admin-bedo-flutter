const String getConversationsQuery = '''
query getConversations(\$filters: conversationFilterInput){
  getConversations(filters:\$filters){
    _id
    title
    updatedAt
    repliedByAdmin
    lastMessage{
      _id
      text
      senderType
      createdAt
      messageType
    }
    user{
      _id
      fullName
    }
    driver{
      _id
      fullName
    }
  }
}
''';

const String getConversationQuery = '''
query getConversation(\$id: ID!){
  getConversation(id:\$id){
    user{_id}
    driver{_id}
    messages{
      _id
      text
      senderType
      createdAt
      messageType
    }
  }
}
''';

const String sendMessageMutation = '''
mutation sendMessage(\$sendMessageInput : sendMessageInput!){
  sendMessage(sendMessageInput: \$sendMessageInput){
    _id
      text
      senderType
      createdAt
      messageType
  }
}
''';

const String uploadFileRequest = '''
mutation uploadFile(\$data: uploadInput!){
  uploadFile(data: \$data){
    url
  }
}
''';
