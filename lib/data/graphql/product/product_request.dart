import 'package:bedo_shop_admin/constants/constants.dart';
import 'package:bedo_shop_admin/data/graphql/product/product_gql.dart';
import 'package:bedo_shop_admin/data/graphql/graphql.dart';
import 'package:bedo_shop_admin/data/secure_storage/secure_storage.dart';
import 'package:graphql/client.dart';

class ProductRequest {
  QueryOptions queryOptions;

  Future<QueryResult> getProductsByShopAdmin(
    Map pagination,
  ) async {
    queryOptions = QueryOptions(
      document: gql(getProductsByShopAdminQuery),
      variables: {
        'page': pagination,
      },
    );
    // String token = await SecureStorage.getToken(accessTokenKey);
    // Map<String, String> header = {"authorization": token};
    // Graphql.addHeader(header);
    return await Graphql.client.query(queryOptions);
  }
}
