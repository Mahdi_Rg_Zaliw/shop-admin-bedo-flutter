const String getProductsByShopAdminQuery = """
  query getProductsByShopAdmin(\$page: Pagination!) {
    getProductsByShopAdmin(pagination: \$page) {
    	title {
        value
      }
      _id
    	productDetails {
        price
        size
        stock
      }
    }
  }
  """;
