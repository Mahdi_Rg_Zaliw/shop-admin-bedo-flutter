import 'dart:async';

import 'package:bedo_shop_admin/data/graphql/graphql.dart';
import 'package:bedo_shop_admin/model/order.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:graphql/client.dart';
import 'orders_gql.dart';

class OrderRequest {
  QueryOptions _queryOptions;

  Future<QueryResult> getOrders(String status) async {
    var filters = {};
    if (status == 'PENDING') {
      filters = {'status': status};
    } else {
      filters = {'isCurrentOrder': true};
    }
    _queryOptions = QueryOptions(
        fetchPolicy: FetchPolicy.noCache,
        document: gql(getOrdersByShopAdmin),
        variables: {"filters": filters});
    // Graphql.sayHeadr();
    // Graphql.addHeader({"authorization": _key});
    return await Graphql.client.query(
      _queryOptions,
    );
  }

  Future<QueryResult> acceptOrder(AcceptOrderModel order) async {
    final MutationOptions mutationOptions = MutationOptions(
      document: gql(acceptOrderByShopAdmin),
      variables: {
        'preparationTime': order.preparationTime,
        'shipmentModel': order.shipmentModel.toString().split('.').last,
        'orderID': order.id,
        'driver': order.driverID,
      },
    );
    // Graphql.addHeader({"authorization": _key});

    return await Graphql.client.mutate(mutationOptions);
  }

  Future<QueryResult> getTotalOrdersAmount(from, to) async {
    _queryOptions = QueryOptions(
        fetchPolicy: FetchPolicy.cacheAndNetwork,
        document: gql(getShopTotalOrdersCountAndTotalAmountByShopAdmin),
        variables: {
          "range": {"from": from, "to": to}
        });
    // Graphql.addHeader({"authorization": _key});
    return await Graphql.client.query(
      _queryOptions,
    );
  }

  Future<QueryResult> rejectOrder(String order) async {
    final MutationOptions mutationOptions = MutationOptions(
      document: gql(rejectOrderByShopAdmin),
      variables: {
        'orderId': order,
      },
    );
    // Graphql.addHeader({"authorization": _key});

    return await Graphql.client.mutate(mutationOptions);
  }

  Stream<QueryResult> subscribeToOrders() {
    final _options = SubscriptionOptions(
      document: gql(createOrderSub),
      fetchPolicy: FetchPolicy.cacheAndNetwork,
    );
    // Graphql.client.subscribe(_options).listen((event) {
    //   printYellowBg(event);
    // });

    return Graphql.client.subscribe(_options);
  }
}
