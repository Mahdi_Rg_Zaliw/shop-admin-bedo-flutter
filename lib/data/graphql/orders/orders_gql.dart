const String getOrdersByShopAdmin = '''
query getOrdersByShopAdmin(\$filters: GetOrdersByShopAdminQueryInput){
  getOrdersByShopAdmin(filters:\$filters){
    _id
    status
    cart{
      _id
      currency
      products{
        product{
          _id
          title
          productDetails{
            price
          }
        }
        quantity
      }
      productsPrice
      shipmentCost
      finalPrice
      discount
      afterDiscountPrice
    }
    userLocation{
      lat
      long
    }
    tracking{
      trackId
      estimatedDelivery
    }
    description
  }
}
''';

const String acceptOrderByShopAdmin = '''
mutation acceptOrderByShopAdmin(\$preparationTime: Int!, \$shipmentModel: OrderShipmentModel!, \$driver: ID, \$orderID: ID!){
  acceptOrderByShopAdmin(inputs:{
    preparationTime:\$preparationTime
    shipmentModel:\$shipmentModel
    driver:\$driver
  }
  orderId: \$orderID){
    _id
  }
}
''';

const String rejectOrderByShopAdmin = '''
mutation rejectOrderByShopAdmin(\$orderId: ID!, \$rejectedFor: OrderRejectedFor){
  rejectOrderByShopAdmin(orderId:\$orderId
  rejectedFor:\$rejectedFor){
  	_id,
  }
}
''';

const String getShopTotalOrdersCountAndTotalAmountByShopAdmin = '''
query getShopTotalOrdersCountAndTotalAmountByShopAdmin(\$range: RangeFilerInput!){
  getShopTotalOrdersCountAndTotalAmountByShopAdmin(filters:\$range){
    totalOrdersCount
    totalOrderAmount
  }
}
''';

const String createOrderSub = """
subscription listenToOrdersMan{
  createOrder{
    _id
  }
}
""";
