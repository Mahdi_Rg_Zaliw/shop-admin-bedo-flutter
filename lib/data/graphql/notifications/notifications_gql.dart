const String getNotificationsFromServer = """
query getNotificationsFromServer{
  getNotificationsStatus {
    isOrderNotificationActive
    isOrderStatusNotificationActive
    isPaymentNotificationActive
    isDeliveryNotificationActive
    isMessageNotificationActive
  }
}
  """;

const String changeNotif = """
mutation changeNotif(\$input : UpdateNotificationsStatusInput!){
  updateNotificationsStatus(inputs:\$input)
  {
    isOrderNotificationActive
    isOrderStatusNotificationActive
    isPaymentNotificationActive
    isDeliveryNotificationActive
    isMessageNotificationActive
  }
}
""";
