import 'package:bedo_shop_admin/data/graphql/graphql.dart';

import 'package:graphql/client.dart';

import 'notifications_gql.dart';

class NotificationRequest {
  Future<QueryResult> getUserSettings() async {
    QueryOptions queryOptions = QueryOptions(
      fetchPolicy: FetchPolicy.cacheAndNetwork,
      document: gql(getNotificationsFromServer),
    );

    return await Graphql.client
        .query(queryOptions)
        .timeout(Duration(seconds: 15));
  }

  Future<QueryResult> changeNotifications({Map variable}) async {
    MutationOptions mutationOptions = MutationOptions(
        fetchPolicy: FetchPolicy.cacheAndNetwork,
        document: gql(changeNotif),
        variables: {'input': variable});

    return await Graphql.client
        .mutate(mutationOptions)
        .timeout(Duration(seconds: 15));
  }
}
