const String getTotalComments = """
query getTotalComments {
  getCommentsOnShopByShopAdminCount
}
""";

const String getPendingComments = """
query getPendingComments{
  getShopAdminCommentsOnShop(filters: { status: PENDING }) {
    readyComments {
      rate
    }
    _id
    createdAt
    user {
      fullName
      profileImageUrl
    }
    status
    userComment
  }
}
""";

const String getConFirmedComments = """
query getConFirmedComments {
  getShopAdminCommentsOnShop(filters: { status: CONFIRMED }) {
    _id
    createdAt
        readyComments {
      rate
    }
    user {
      fullName
      profileImageUrl
    }
    userComment
    shopAdminReply {
      admin {
        fullName
      }
      comment {
        userComment
      }
    }
  }
}
""";

const String replyToComment = """
mutation reply(\$id: ID!, \$text: String!) {
  setShopAdminReplyOnComment(input: { comment: \$id, text: \$text }) {
    _id
  }
}

""";

const String rejectComment = """
mutation reject(\$id:ID!){
  rejectCommentOnShopByShopAdmin(id:\$id){
    message
  }
}
""";
