import 'package:bedo_shop_admin/constants/constants.dart';
import 'package:bedo_shop_admin/data/graphql/auth/auth_gql.dart';
import 'package:bedo_shop_admin/data/graphql/comments/comments_gql.dart';
import 'package:bedo_shop_admin/data/graphql/graphql.dart';
import 'package:bedo_shop_admin/data/secure_storage/secure_storage.dart';
import 'package:bedo_shop_admin/model/comments_model.dart';
import 'package:graphql/client.dart';

class CommentsRequest {
  Future<QueryResult> getTotalCommentsQuery() async {
    QueryOptions queryOptions = QueryOptions(
      fetchPolicy: FetchPolicy.cacheAndNetwork,
      document: gql(getTotalComments),
    );
    // Graphql.addHeader(
    //     {"authorization": await SecureStorage.getToken(accessTokenKey)});
    return await Graphql.client
        .query(queryOptions)
        .timeout(Duration(seconds: 15));
  }

  Future<QueryResult> getPendingCommentsQuery() async {
    QueryOptions queryOptions = QueryOptions(
      fetchPolicy: FetchPolicy.cacheAndNetwork,
      document: gql(getPendingComments),
    );
    // Graphql.addHeader(
    //     {"authorization": await SecureStorage.getToken(accessTokenKey)});
    return await Graphql.client
        .query(queryOptions)
        .timeout(Duration(seconds: 15));
  }

  Future<QueryResult> getConfirmedCommentsQuery() async {
    QueryOptions queryOptions = QueryOptions(
      fetchPolicy: FetchPolicy.cacheAndNetwork,
      document: gql(getConFirmedComments),
    );
    // Graphql.addHeader(
    //     {"authorization": await SecureStorage.getToken(accessTokenKey)});
    return await Graphql.client
        .query(queryOptions)
        .timeout(Duration(seconds: 15));
  }

  Future<QueryResult> replyToCommentMutation(
      String id, String replyText) async {
    MutationOptions mutationOptions = MutationOptions(
        fetchPolicy: FetchPolicy.cacheAndNetwork,
        document: gql(replyToComment),
        variables: {"id": id.toString(), "text": replyText.toString()});
    // Graphql.addHeader(
    //     {"authorization": await SecureStorage.getToken(accessTokenKey)});
    return await Graphql.client
        .mutate(mutationOptions)
        .timeout(Duration(seconds: 15));
  }

  Future<QueryResult> rejectCommentMutation(String id) async {
    MutationOptions mutationOptions = MutationOptions(
        fetchPolicy: FetchPolicy.cacheAndNetwork,
        document: gql(rejectComment),
        variables: {
          "id": id.toString(),
        });
    // Graphql.addHeader(
    //     {"authorization": await SecureStorage.getToken(accessTokenKey)});
    return await Graphql.client
        .mutate(mutationOptions)
        .timeout(Duration(seconds: 15));
  }
}
