const String editShop = """
mutation upShop(
  \$logoUrl: String = "http://www.mandysam.com/img/random.jpg"
  \$name: String = "zzzz"
  \$coordinates: [Float!]! = [0.0, 0.0]
  \$phoneNumbers: [String!]! = ["9121234567"]
  \$workingHoursInMinutes: [workingHoursInMinutesInput] 
  \$categories:[ID]
  \$preparingTime: Int=60
  \$taxCode: String
  \$cardNumber:String
) {
  updateShopByShopAdmin(
    data: {
      logoUrl: \$logoUrl
      name: \$name
      preparingTime: \$preparingTime
      phoneNumbers: \$phoneNumbers
      location: { type: Point, coordinates: \$coordinates }
      workingHoursInMinutes: \$workingHoursInMinutes
      categories:\$categories
      taxCode: \$taxCode
      cardNumber:\$cardNumber
    }
  ) {
    name
    logoUrl
    _id
    workingHoursInMinutes {
      type
      from
      to
    }
  }
}
  """;

const String getShopData = """

query getshopImportantData {
  getShopByShopAdmin {
    name
    address
    phoneNumbers
    location {
      coordinates
    }
    preparingTime

    categories {
      _id
      
    }
    taxCode
    cardNumber
    address
		categories{
		_id
		}
    workingHoursInMinutes {
      type
      from
      to
    }
    logoUrl
  }
  getTotalTransactionsAmountByShopAdmin
  getCategories{
    title
    _id
  }
}


""";

const String createShop = """

mutation createShop(
  \$phoneNumbers: [String!]! 
  \$coordinates: [Float!]! # = [40.4093, 49.8671]
  \$name: String 
  \$taxCode: String 
  \$cardNumber: String 
  \$categories:[ID]
  \$workingHoursInMinutes: [workingHoursInMinutesInput]
  \$logoUrl: String
  \$preparingTime:Int!
) {
  createShopByShopAdmin(
    shopData: {
      preparingTime: \$preparingTime 
      phoneNumbers: \$phoneNumbers
      location: { type: Point, coordinates: \$coordinates }
      name: \$name
      categories:\$categories
      taxCode: \$taxCode
      cardNumber: \$cardNumber
      workingHoursInMinutes: \$workingHoursInMinutes
      logoUrl: \$logoUrl
    }
  ) {
    name
  }
}

""";

const String getShpCategories = """

query getShpCategories {
  getCategories {
    title
    _id
  }
}
""";

const String getTransactions = """

query getTransactions(\$type : TransactionType! =TOTAL ,\$max : Int =15 ,\$distance: Int =0 ) {
  getTransactionsByShopAdmin(
    filters: { type: \$type}
    pagination: { limit: \$max , skip: \$distance }
  ) {
    amount
    createdAt
    transactionMethod
    status
    type
  }
}

""";

const String getAllTransActionsToGather = """

query getalltx(\$distance: Int =0,\$max : Int =15){
  inputs:  getTransactionsByShopAdmin(
    filters: { type: INPUT}
    pagination: { limit:\$max, skip: \$distance }
  ){
    ...frags
  }
  outputs:  getTransactionsByShopAdmin(
    filters: { type:OUTPUT }
    pagination: { limit:\$max, skip: \$distance }
  ){
    ...frags
  }
    totals:  getTransactionsByShopAdmin(
    filters: { type: TOTAL}
    pagination: { limit:\$max, skip: \$distance }
  ){
    ...frags
  }
}

fragment frags on Transaction {
    amount
    createdAt
    transactionMethod
    status
    type
  }

  """;
