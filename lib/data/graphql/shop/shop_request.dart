import 'package:bedo_shop_admin/data/graphql/graphql.dart';
import 'package:bedo_shop_admin/data/graphql/settings/settings_gql.dart';
import 'package:bedo_shop_admin/data/graphql/shop/shop_gql.dart';

import 'package:graphql/client.dart';

class ShopRequests {
  Future<QueryResult> getCategoriesAlone() async {
    QueryOptions queryOptions = QueryOptions(
      fetchPolicy: FetchPolicy.cacheAndNetwork,
      document: gql(getShpCategories),
    );

    return await Graphql.client
        .query(queryOptions)
        .timeout(Duration(seconds: 15));
  }

  ///
  ///
  ///
  ///
  ///
  Future<QueryResult> getShopDatas() async {
    QueryOptions queryOptions = QueryOptions(
      fetchPolicy: FetchPolicy.cacheAndNetwork,
      document: gql(getShopData),
    );

    return await Graphql.client
        .query(queryOptions)
        .timeout(Duration(seconds: 15));
  }

//
//
//
//
  Future<QueryResult> updateShop(
      {logoUrl,
      name,
      coordinates,
      phoneNumbers,
      taxCode,
      workingHoursInMinutes,
      categories,
      int preparingTime = 60,
      cardNumber}) async {
    MutationOptions mutationOptions = MutationOptions(
        fetchPolicy: FetchPolicy.cacheAndNetwork,
        document: gql(editShop),
        variables: {
          'logoUrl': logoUrl,
          'name': name,
          'coordinates': coordinates,
          'phoneNumbers': phoneNumbers,
          'taxCode': taxCode,
          'workingHoursInMinutes': workingHoursInMinutes,
          'cardNumber': cardNumber,
          'categories': categories,
          'preparingTime': preparingTime,

          ///
          ///
          ///
          ///
          ///
        });

    return await Graphql.client
        .mutate(mutationOptions)
        .timeout(Duration(seconds: 15));
  }

//
//
//
//
  Future<QueryResult> createShopRequest({
    phoneNumbers,
    coordinates,
    name,
    taxCode,
    cardNumber,
    categories,
    workingHoursInMinutes,
    logoUrl,
    preparingTime,
  }) async {
    MutationOptions mutationOptions = MutationOptions(
        fetchPolicy: FetchPolicy.cacheAndNetwork,
        document: gql(createShop),
        variables: {
          'phoneNumbers': phoneNumbers,
          'coordinates': coordinates,
          'name': name,
          'taxCode': taxCode,
          'cardNumber': cardNumber,
          'categories': categories,
          'workingHoursInMinutes': workingHoursInMinutes,
          'logoUrl': logoUrl,
          'preparingTime': preparingTime,
        });

    return await Graphql.client
        .mutate(mutationOptions)
        .timeout(Duration(seconds: 15));
  }

  Future<QueryResult> getAllTransActions(
    type,
    max,
    distance,
  ) async {
    QueryOptions queryOptions = QueryOptions(
        fetchPolicy: FetchPolicy.cacheAndNetwork,
        document: gql(getTransactions),
        variables: {
          'type': type,
          'max': max,
          'distance': distance,
        });

    return await Graphql.client
        .query(queryOptions)
        .timeout(Duration(seconds: 15));
  }

  Future<QueryResult> getAllTransActionsTogather() async {
    QueryOptions queryOptions = QueryOptions(
      fetchPolicy: FetchPolicy.cacheAndNetwork,
      document: gql(getAllTransActionsToGather),
    );

    return await Graphql.client
        .query(queryOptions)
        .timeout(Duration(seconds: 15));
  }
}
