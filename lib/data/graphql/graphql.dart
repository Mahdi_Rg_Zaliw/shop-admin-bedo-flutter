import 'package:bedo_shop_admin/constants/constants.dart';
import 'package:bedo_shop_admin/data/secure_storage/secure_storage.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:graphql/client.dart';
import 'package:bedo_shop_admin/config/config.dart';

class Graphql {
  static HttpLink _httpLink = HttpLink(
    uri,
    defaultHeaders: {'Authorization': '$tkn'},
  );

  static Link link =
      Link.split((request) => request.isSubscription, _webSocketLink, _link);

  static WebSocketLink _webSocketLink = WebSocketLink(
    uri.replaceAll('http', 'ws'),
    // .replaceAll('graphql', ''),
    config: SocketClientConfig(
        inactivityTimeout: Duration(minutes: 15),
        autoReconnect: true,
        initPayload: () {
          return {
            'headers': {'Authorization': '$tkn'}
          };
        }),
  );
  static GraphQLClient client = GraphQLClient(
    link: link,
    cache: GraphQLCache(),
  );

  static AuthLink _authLink = AuthLink(
    getToken: () async {
      return await tkn;
    },
  );
  // SecureStorage.getToken(accessTokenKey).then((value) => v = value);
  // final Link _link = _authLink.concat(_httpLink);

  // static void addHeader(Map<String, String> headers) {
  //   _httpLink = HttpLink(
  //     uri,
  //     defaultHeaders: headers,
  //   );
  //   GraphQLCache c = client.cache;
  //   client = GraphQLClient(
  //     link: _httpLink,
  //     cache: c,
  //   );
  // }

  static void sayHeadr() {
    printCyanBg(_httpLink.headers.toString());
  }

  static Link _link = _authLink.concat(_httpLink);
  // .concat(_webSocketLink);

  Link get lnk {
    return _link;
  }

  static Future<String> get tkn async {
    String t;

    t = await SecureStorage.getToken(accessTokenKey);
    printOrange('call getter in GQL was=> '.padLeft(100, ' '));
    printOrange('tkn getter in GQL was=> '.padLeft(100, ' '));
    printOrange('$t');
    // _httpWauth = HttpLink(
    //   uri,
    //   defaultHeaders: {"authorization": tkn},
    // );

    return t;
  }
}

extension tkn on GraphQLClient {
  Future<String> get calcTkn async {
    String v;
    v = await SecureStorage.getToken(accessTokenKey).then((value) => v = value);
    printYellowBg('we got key v :$v');

    printYellowBg('we got key v :$v');
    return v;
  }

  void readheaders() {
    // this.link.headers;
    printMagentBg(this.link.toString());
  }
}

extension tknH on HttpLink {
  Future<String> get calcTkn async {
    String v;
    v = await SecureStorage.getToken(accessTokenKey).then((value) => v = value);
    printYellowBg('we got key v :$v');
    return v;
  }

  void sayHeaders() {
    printRedBg(this.defaultHeaders);
  }

  Map<String, String> get headers {
    return this.defaultHeaders;
  }
}
