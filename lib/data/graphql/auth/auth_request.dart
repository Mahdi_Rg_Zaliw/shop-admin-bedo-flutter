import 'package:bedo_shop_admin/data/graphql/auth/auth_gql.dart';
import 'package:bedo_shop_admin/data/graphql/graphql.dart';
import 'package:graphql/client.dart';

class AuthRequest {
  MutationOptions mutationOptions;

  Future<QueryResult> adminLoginRequest(
    String emailOrPhoneNumber,
    String password,
    String fcm,
  ) async {
    mutationOptions = MutationOptions(
      document: gql(adminLoginMutation),
      variables: {
        'emailOrPhoneNumber': emailOrPhoneNumber,
        'password': password,
        'fcm': fcm,
      },
    );
    return await Graphql.client.mutate(mutationOptions);
  }

  Future<QueryResult> getShopAdminPhoneVerificationCodeRequest(
    String phoneNumber,
  ) async {
    mutationOptions = MutationOptions(
      document: gql(getShopAdminPhoneVerificationCodeMutation),
      variables: {
        'phoneNumber': phoneNumber,
      },
    );
    return await Graphql.client.mutate(mutationOptions);
  }

  Future<QueryResult> checkShopAdminPhoneVerificationCodeRequest(
    String phoneNumber,
    String phoneVerificationCode,
  ) async {
    mutationOptions = MutationOptions(
      document: gql(checkShopAdminPhoneVerificationCodeQuery),
      variables: {
        'phoneNumber': phoneNumber,
        'phoneVerificationCode': phoneVerificationCode,
      },
    );
    return await Graphql.client.mutate(mutationOptions);
  }

  Future<QueryResult> shopAdminSignUpMutationRequest(
    String phoneNumber,
    String phoneSignUpCode,
    String fullName,
    String email,
    String password,
  ) async {
    mutationOptions = MutationOptions(
      document: gql(shopAdminSignUpMutation),
      variables: {
        'phoneNumber': phoneNumber,
        'phoneSignUpCode': phoneSignUpCode,
        'fullName': fullName,
        'email': email,
        'password': password,
      },
    );
    return await Graphql.client.mutate(mutationOptions);
  }

  Future<QueryResult> testingVerification(
    String phoneNumber,
  ) async {
    mutationOptions = MutationOptions(
      document: gql(testingSMS),
      variables: {
        'phoneNumber': phoneNumber,
      },
    );
    return await Graphql.client.mutate(mutationOptions);
  }
}
