const String adminLoginMutation = """
mutation adminLogin(\$emailOrPhoneNumber: String!, \$password: String!, \$fcm: String!) {
  adminLogin(input: {emailOrPhoneNumber: \$emailOrPhoneNumber,password: \$password, fcm: \$fcm}) {
    _id
    fullName
    email
    phoneNumber
    phoneNumberVerified
    token {
      accessToken
      refreshToken
    }
  }
}
  """;

const String getShopAdminPhoneVerificationCodeMutation = """
  mutation getShopAdminPhoneVerificationCode(\$phoneNumber: String!) {
    getShopAdminPhoneVerificationCode(phoneNumber: \$phoneNumber) {
      phoneVerificationCode
      phoneVerificationCodeExpireTime
      __typename
    }
  }
""";

const String checkShopAdminPhoneVerificationCodeQuery = """
  query checkShopAdminPhoneVerificationCode(\$phoneNumber: String!, \$phoneVerificationCode: String!) {
    checkShopAdminPhoneVerificationCode(input: {phoneNumber: \$phoneNumber, phoneVerificationCode: \$phoneVerificationCode}) {
      phoneSignUpCode
      __typename
    }
  }
""";

const String shopAdminSignUpMutation = """
mutation shopAdminSignUp(\$fullName: String!, \$email: String!, \$password: String!, \$phoneNumber: String!, \$phoneSignUpCode: String!) {
  shopAdminSignUp(input: {fullName: \$fullName, email: \$email, password: \$password, phoneNumber: \$phoneNumber, phoneSignUpCode: \$phoneSignUpCode}) {
    _id
    fullName
    email
    token {
      accessToken
      refreshToken
      __typename
    }
    shop {
      _id
      __typename
    }
    __typename
  }
}
""";

const String testingSMS = """
mutation getShopAdminVerificationCode(\$phoneNumber:String!){
  getShopAdminPhoneVerificationCode(
    phoneNumber:\$phoneNumber){
    phoneVerificationCode
    
  }
}
""";
