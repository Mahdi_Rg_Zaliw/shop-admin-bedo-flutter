const String getShopAdminData = """
query getShopAdminData {
  getShopByShopAdmin {
    name
    cardNumber
    address
    location {
      coordinates
    }
    preparingTime
    phoneNumbers
    averageRate
    shopAdmin {
      fullName
      email
      phoneNumber
    }
  }
}
  """;

const String changePass = """
mutation chnagepass(\$newPassword:String!,\$currentPassword:String!){
  changeShopAdminPassword(
   input:{
    newPassword:\$newPassword,
    currentPassword:\$currentPassword,
  }
  ) {
    fullName
    phoneNumber
  }
}

""";

const String changeCardNumber = """

mutation updateShop(\$cardNumber:String!,\$phoneNumbers:[String!]!,\$preparingTime:Int!,\$coordinates:[Float!]!) {
  updateShopByShopAdmin(
    data: {
      phoneNumbers: \$phoneNumbers 
      cardNumber: \$cardNumber
      preparingTime: \$preparingTime
      location: {
        type:Point
        coordinates:\$coordinates
      } 
    }
  ) {
    cardNumber
  }
}

""";

const String logOut = """

mutation logOut{
  signOut{
    message
  }
}

""";

const String getHowItWorks = """

query getHowItWorkss{
  getShopHowItWorks{
    title
    description
  }
}

""";
