import 'package:bedo_shop_admin/data/graphql/graphql.dart';
import 'package:bedo_shop_admin/data/graphql/settings/settings_gql.dart';

import 'package:graphql/client.dart';

class SettingsRequests {
  Future<QueryResult> getUserSettings() async {
    //
    //
    //
    //
    QueryOptions queryOptions = QueryOptions(
      fetchPolicy: FetchPolicy.cacheAndNetwork,
      document: gql(getShopAdminData),
    );
    //
    //
    //
    //
    return await Graphql.client
        .query(queryOptions)
        .timeout(Duration(seconds: 15));
  }

  Future<QueryResult> changePassword(
      {String newPassword, String currentPassword}) async {
    //
    //
    //
    //
    MutationOptions mutationOptions = MutationOptions(
        fetchPolicy: FetchPolicy.cacheAndNetwork,
        document: gql(changePass),
        variables: {
          "newPassword": newPassword,
          "currentPassword": currentPassword
        });
    //
    //
    //
    //
    return await Graphql.client
        .mutate(mutationOptions)
        .timeout(Duration(seconds: 15));
  }

  Future<QueryResult> changeCard({
    String cardNumber,
    List phoneNumbers,
    int preparingTime,
    List coordinates,
  }) async {
    //
    //
    //
    //
    MutationOptions mutationOptions = MutationOptions(
        fetchPolicy: FetchPolicy.cacheAndNetwork,
        document: gql(changeCardNumber),
        variables: {
          'cardNumber': cardNumber,
          'phoneNumbers': phoneNumbers,
          'preparingTime': preparingTime,
          'coordinates': coordinates,
        });
    //
    //
    //
    //
    return await Graphql.client
        .mutate(mutationOptions)
        .timeout(Duration(seconds: 15));
  }

  Future<QueryResult> logout() async {
    //
    //
    //
    //
    MutationOptions mutationOptions = MutationOptions(
      fetchPolicy: FetchPolicy.cacheAndNetwork,
      document: gql(logOut),
    );
    //
    //
    //
    //
    return await Graphql.client
        .mutate(mutationOptions)
        .timeout(Duration(seconds: 15));
  }

  Future<QueryResult> getHowitWorks() async {
    //
    //
    //
    //
    QueryOptions queryOptions = QueryOptions(
      fetchPolicy: FetchPolicy.cacheAndNetwork,
      document: gql(getHowItWorks),
    );
    //
    //
    //
    //
    return await Graphql.client
        .query(queryOptions)
        .timeout(Duration(seconds: 15));
  }
}
