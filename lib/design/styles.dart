import 'package:bedo_shop_admin/design/theme.dart';
import 'package:flutter/material.dart';

const grey12regular =
    TextStyle(color: titleGrey, fontSize: 12, fontWeight: FontWeight.w400);

const grey14semiBold =
    TextStyle(color: titleGrey, fontSize: 14, fontWeight: FontWeight.w600);
const black12regular =
    TextStyle(color: textColor, fontSize: 12, fontWeight: FontWeight.w400);
const black24medium =
    TextStyle(color: textColor, fontSize: 24, fontWeight: FontWeight.w500);
const grey14regular =
    TextStyle(color: titleGrey, fontSize: 14, fontWeight: FontWeight.w400);
const black14regular =
    TextStyle(color: textColor, fontSize: 14, fontWeight: FontWeight.w400);
const grey14medium =
    TextStyle(color: titleGrey, fontSize: 14, fontWeight: FontWeight.w500);
const black14medium =
    TextStyle(color: textColor, fontSize: 14, fontWeight: FontWeight.w500);
const black18medium =
    TextStyle(color: textColor, fontSize: 18, fontWeight: FontWeight.w500);
const red12semibold =
    TextStyle(color: red, fontSize: 12, fontWeight: FontWeight.w600);
const green12semiBold = TextStyle(
    color: primaryMaterialColor, fontSize: 12, fontWeight: FontWeight.w600);
const green14medium = TextStyle(
    color: primaryMaterialColor, fontSize: 14, fontWeight: FontWeight.w500);
const green18medium = TextStyle(
    color: primaryMaterialColor, fontSize: 18, fontWeight: FontWeight.w500);
const red14medium =
    TextStyle(color: red, fontSize: 14, fontWeight: FontWeight.w500);
const black35medium =
    TextStyle(color: textColor, fontSize: 35, fontWeight: FontWeight.w500);
const grey22medium =
    TextStyle(color: hintTextColor, fontSize: 22, fontWeight: FontWeight.w500);
const black16semibold =
    TextStyle(color: textColor, fontSize: 16, fontWeight: FontWeight.w600);
const semibold14 =
    TextStyle(color: red, fontSize: 14, fontWeight: FontWeight.w600);
const black24regular =
    TextStyle(color: textColor, fontSize: 24, fontWeight: FontWeight.w400);
const black24semibold =
    TextStyle(color: textColor, fontSize: 24, fontWeight: FontWeight.w600);
const green14regular = TextStyle(
    color: primaryMaterialColor, fontSize: 14, fontWeight: FontWeight.w400);
const green12regular = TextStyle(
    color: primaryMaterialColor, fontSize: 12, fontWeight: FontWeight.w400);
const red14regular =
    TextStyle(color: red, fontSize: 14, fontWeight: FontWeight.w400);
const grey35medium =
    TextStyle(color: titleGrey, fontSize: 35, fontWeight: FontWeight.w500);
const grey18semibold =
    TextStyle(color: titleGrey, fontSize: 18, fontWeight: FontWeight.w600);
const black18regular =
    TextStyle(color: textColor, fontSize: 18, fontWeight: FontWeight.w400);
const black20regular =
    TextStyle(color: textColor, fontSize: 20, fontWeight: FontWeight.w400);
const white14medium =
    TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w500);

