// Place fonts/iconsf.ttf in your fonts/ directory and
// add the following to your pubspec.yaml
// flutter:
//   fonts:
//    - family: iconsf
//      fonts:
//       - asset: fonts/iconsf.ttf
import 'package:flutter/widgets.dart';

class Iconsf {
  Iconsf._();

  static const String _fontFamily = 'iconsf';

  static const IconData i01_add_to_menu =
      IconData(0xe900, fontFamily: _fontFamily);
  static const IconData i03_brifcase =
      IconData(0xe901, fontFamily: _fontFamily);
  static const IconData i04_bike = IconData(0xe902, fontFamily: _fontFamily);
  static const IconData i05_box = IconData(0xe903, fontFamily: _fontFamily);
  static const IconData i06_brifcase =
      IconData(0xe904, fontFamily: _fontFamily);
  static const IconData i07_calendar_checked =
      IconData(0xe905, fontFamily: _fontFamily);
  static const IconData i08_calendar_unchecked =
      IconData(0xe906, fontFamily: _fontFamily);
  static const IconData i09_camera = IconData(0xe907, fontFamily: _fontFamily);
  static const IconData i10_chat = IconData(0xe908, fontFamily: _fontFamily);
  static const IconData i11_chat_selected =
      IconData(0xe909, fontFamily: _fontFamily);
  static const IconData i18_delete = IconData(0xe90a, fontFamily: _fontFamily);
  static const IconData i19_discount =
      IconData(0xe90b, fontFamily: _fontFamily);
  static const IconData i23_fast_food =
      IconData(0xe90c, fontFamily: _fontFamily);
  static const IconData i24_hamburger_menu =
      IconData(0xe90d, fontFamily: _fontFamily);
  static const IconData i25_hamburger_menu_2 =
      IconData(0xe90e, fontFamily: _fontFamily);
  static const IconData i26_home = IconData(0xe90f, fontFamily: _fontFamily);
  static const IconData i28_home_selected =
      IconData(0xe910, fontFamily: _fontFamily);
  static const IconData i31_location_marker =
      IconData(0xe911, fontFamily: _fontFamily);
  static const IconData i32_logo = IconData(0xe912, fontFamily: _fontFamily);
  static const IconData i36_paper_plane =
      IconData(0xe913, fontFamily: _fontFamily);
  static const IconData i37_peincil = IconData(0xe914, fontFamily: _fontFamily);
  static const IconData i38_pencil_drawing_line =
      IconData(0xe915, fontFamily: _fontFamily);
  static const IconData i40_resturant_location_marker =
      IconData(0xe916, fontFamily: _fontFamily);
  static const IconData i41_settings_gear =
      IconData(0xe917, fontFamily: _fontFamily);
  static const IconData i42_settings_outline =
      IconData(0xe918, fontFamily: _fontFamily);
  static const IconData i43_settings_outline_2 =
      IconData(0xe919, fontFamily: _fontFamily);
  static const IconData i44_square_location_marker =
      IconData(0xe91a, fontFamily: _fontFamily);
  static const IconData i45_star = IconData(0xe91b, fontFamily: _fontFamily);
  static const IconData i47_star_location_marker =
      IconData(0xe91c, fontFamily: _fontFamily);
  static const IconData i48_stats = IconData(0xe91d, fontFamily: _fontFamily);
  static const IconData i49_stats_selected =
      IconData(0xe91e, fontFamily: _fontFamily);
  static const IconData i50_timer = IconData(0xe91f, fontFamily: _fontFamily);
  static const IconData i51_view_produxt_menue =
      IconData(0xe920, fontFamily: _fontFamily);
  static const IconData i52_target = IconData(0xe921, fontFamily: _fontFamily);
  static const IconData i53_coupon = IconData(0xe922, fontFamily: _fontFamily);
  static const IconData i54_message_to =
      IconData(0xe923, fontFamily: _fontFamily);
}
