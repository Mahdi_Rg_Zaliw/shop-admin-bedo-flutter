import 'package:bedo_shop_admin/design/styles.dart';
import 'package:flutter/material.dart';

final theme = ThemeData(
    dividerTheme: DividerThemeData(thickness: .7),
    dialogTheme: DialogTheme(
        shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(14)),
    )),
    bottomSheetTheme: BottomSheetThemeData(
        shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(24),
        topRight: Radius.circular(24),
      ),
    )),
    primarySwatch: primaryMaterialColor,
    errorColor: red,
    // fontFamily: 'Gilroy',
    scaffoldBackgroundColor: Colors.white,
    unselectedWidgetColor: unselectedColor,
    appBarTheme: AppBarTheme(
      elevation: 0,
      color: Colors.white,
    ),
    buttonTheme: ButtonThemeData(
      // padding: EdgeInsets.all(20),
      height: 50,
      // materialTapTargetSize: MaterialTapTargetSize.padded,
      textTheme: ButtonTextTheme.normal,

      colorScheme: ThemeData.light()
          .colorScheme
          .copyWith(secondary: Colors.white, primary: primaryMaterialColor),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(100),
        ),
      ),
    ).copyWith(),
    textTheme: TextTheme(
      subtitle1: grey12regular,
      bodyText2: black12regular,
      button: semibold14.copyWith(color: Colors.white),
    ),
    inputDecorationTheme: InputDecorationTheme(
      errorBorder: OutlineInputBorder(
        borderSide: BorderSide(
          width: 1,
          color: red,
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(12),
        ),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderSide: BorderSide(
          width: 1,
          color: red,
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(12),
        ),
      ),
      hintStyle: TextStyle(
        color: hintTextColor,
        fontSize: 14,
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
          width: 1,
          color: primaryMaterialColor,
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(12),
        ),
      ),
      fillColor: lightGreyBg,
      filled: true,
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          width: 1,
          color: lightGreyBg,
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(12),
        ),
      ),
    ));

const primaryMaterialColor = MaterialColor(0xFF2AD8BD, color);
const hintTextColor = Color(0xff979797);
const lightGreen = Color(0xff99F91B);
const orange = Color(0xffFF9900);
const yellow = Color(0xffFFD825);
const lightBlue = Color(0xff36DEFF);

const lightGreyBg = Color(0xFFF3F3F3);
const textColor = Color(0xff161616);
const titleGrey = Color(0xff707070);
const gold = Color(0xfff0c11b);
const red = Color(0xffFF5252);
const highlightWhite = Color(0xFFF1F1F1);
const colorIcon = Color(0xFF454545);
const unselectedColor = Color(0xffE2E2E2);
const switchBg = Color(0xFFEDEFF3);
const iconColor = Color(0xFFC3C3C3);

const Map<int, Color> color = {
  50: Color.fromRGBO(42, 216, 189, .1),
  100: Color.fromRGBO(42, 216, 189, .2),
  200: Color.fromRGBO(42, 216, 189, .3),
  300: Color.fromRGBO(42, 216, 189, .4),
  400: Color.fromRGBO(42, 216, 189, .5),
  500: Color.fromRGBO(42, 216, 189, .6),
  600: Color.fromRGBO(42, 216, 189, .7),
  700: Color.fromRGBO(42, 216, 189, .8),
  800: Color.fromRGBO(42, 216, 189, .9),
  900: Color.fromRGBO(42, 216, 189, 1),
};

final whiteAllButtonsColor = ThemeData(
  buttonTheme: ButtonThemeData(
    // padding: EdgeInsets.all(20),
    height: 50,
    // materialTapTargetSize: MaterialTapTargetSize.padded,
    textTheme: ButtonTextTheme.normal,

    colorScheme: ThemeData.dark()
        .colorScheme
        .copyWith(secondary: Colors.white, primary: primaryMaterialColor),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(100),
      ),
    ),
  ).copyWith(),
);
