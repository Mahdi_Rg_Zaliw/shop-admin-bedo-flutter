abstract class ReportsEvent {}

class LoadReportsEvent extends ReportsEvent{}
class LoadOrdersStatisticsEvent extends ReportsEvent{
  final DateTime from;
  final DateTime to;
  final bool allTimes;
  LoadOrdersStatisticsEvent(this.from, this.to, this.allTimes);
}
class LoadOrdersStatisticsForChartsEvent extends ReportsEvent{
  final DateTime from;
  final DateTime to;
  final bool allTimes;
  LoadOrdersStatisticsForChartsEvent(this.from, this.to, this.allTimes);
}
