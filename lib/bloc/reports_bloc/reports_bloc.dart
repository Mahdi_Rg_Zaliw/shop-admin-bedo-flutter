import 'package:bedo_shop_admin/bloc/reports_bloc/reports_event.dart';
import 'package:bedo_shop_admin/bloc/reports_bloc/reports_state.dart';
import 'package:bedo_shop_admin/data/graphql/reports/full_report/reports_request.dart';
import 'package:bedo_shop_admin/model/cart.dart';
import 'package:bedo_shop_admin/model/cart_product.dart';
import 'package:bedo_shop_admin/model/chart_data_model.dart';
import 'package:bedo_shop_admin/model/order.dart';
import 'package:bedo_shop_admin/model/orders_statistics.dart';
import 'package:bedo_shop_admin/model/product.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ReportsBloc extends Bloc<ReportsEvent, ReportsState> {
  List<Order> orders;
  OrdersStatistics statistics;
  ChartDataModel chartData;
  double totalAmount = 0;
  ReportsRequest _request = ReportsRequest();

  ReportsBloc() : super(ReportsInitial());

  @override
  Stream<ReportsState> mapEventToState(ReportsEvent event) async* {
    if (event is LoadReportsEvent) {
      yield* loadReportsToState();
    } else if (event is LoadOrdersStatisticsEvent) {
      yield* loadOrdersStatisticsToState(event.from, event.to, event.allTimes);
    } else if (event is LoadOrdersStatisticsForChartsEvent) {
      yield* loadOrdersStatisticsForChartsToState(
          event.from, event.to, event.allTimes);
    }
  }

  bool parsePaymentType(String type) {
    if (type == 'PAY_FROM_USER_TO_SHOP' || type == 'PAY_FROM_BEDO_TO_SHOP') {
      return true;
    }
    if (type == 'PAY_FROM_SHOP_TO_DRIVER' || type == 'PAY_FROM_SHOP_TO_BEDO') {
      return false;
    }
    return false;
  }

  Stream<ReportsState> loadReportsToState() async* {
    yield ReportsLoading(orders, statistics, chartData, totalAmount);
    var queryResults = await this._request.getOrders();

    if (queryResults.hasException) {
      print(queryResults.exception);
      yield ReportsLoadingFailed(orders, statistics, chartData, totalAmount,
          queryResults.exception.graphqlErrors);
      return;
    }
    List<dynamic> rawOrders =
        queryResults.data['getFullReportByShopAdmin'] as List<dynamic>;
    List<Order> results = rawOrders.map((element) {
      final cart = element['cart'];
      final rawProducts = cart['products'] as List<dynamic>;
      final products = rawProducts
          .map((e) => CartProduct(
              Product(
                  id: e['product']['_id'],
                  title: e['product']['title'],
                  price: (e['product']['productDetails'][0]['price'] as int)
                      .toDouble()),
              e['quantity']))
          .toList();

      return Order(
        id: element['_id'],
        cart: Cart(id: cart['_id'] as String, products: products),
        trackID: (element['tracking']['trackId'] as String).toUpperCase(),
        createdAt: DateTime.parse(element['createdAt']),
        totalPrice: element['total'],
        finalPrice: element['finalPrice'],
        tax: double.parse(element['HST'].toString()),
        courierFee: element['delivery'],
        inputOrder: element['payment'] != null
            ? parsePaymentType(element['payment']['type'])
            : false,
      );
    }).toList();

    orders = results;
    yield ReportsLoaded(orders, statistics, chartData, totalAmount);
  }

  Stream<ReportsState> loadOrdersStatisticsToState(
      DateTime from, DateTime to, bool allTimes) async* {
    yield ReportsLoading(orders, statistics, chartData, totalAmount);
    final queryResults =
        await this._request.getOrdersStatistics(from, to, allTimes);
    final totalAmountQueryResult =
        await this._request.getTotalOrdersAmount(from, to, allTimes);

    if (queryResults.hasException) {
      print(queryResults.exception);
      yield StatisticsLoadingFailed(orders, statistics, chartData, totalAmount,
          queryResults.exception.graphqlErrors);
      return;
    }
    final rawData = queryResults.data['getOrdersStatisticsListByShopAdmin'];
    var results = OrdersStatistics(
      numberOfSales: rawData['numberOfSales'],
      receivedOrders: rawData['receivedOrders'],
      returnedOrders: rawData['returnedOrders'],
      successfulOrders: rawData['successfulOrders'],
      unSuccessfulOrders: rawData['unSuccessfulOrders'],
      cashDaySales: double.parse(rawData['cashDaySales'].toString()),
      cardDaySales: double.parse(rawData['cardDaySales'].toString()),
      companyCommission: double.parse(rawData['companyCommission'].toString()),
    );
    var total = double.parse(totalAmountQueryResult
        .data['getShopTotalOrdersCountAndTotalAmountByShopAdmin']
            ['totalOrderAmount']
        .toString());
    totalAmount = total;
    statistics = results;
    yield ReportsLoaded(orders, statistics, chartData, totalAmount);
  }

  Stream<ReportsState> loadOrdersStatisticsForChartsToState(
      DateTime from, DateTime to, bool allTimes) async* {
    yield ReportsLoading(orders, statistics, chartData, totalAmount);
    final queryResults =
        await this._request.getOrdersStatisticsForCharts(from, to, allTimes);
    if (queryResults.hasException) {
      print(queryResults.exception);
      yield StatisticsLoadingFailed(orders, statistics, chartData, totalAmount,
          queryResults.exception.graphqlErrors);
      return;
    }
    final rawData =
        queryResults.data['getOrderStatisticsListCountValuesByShopAdmin']
            as Map<String, dynamic>;

    List<DateTime> dates = List<DateTime>();
    List<int> numberOfSales = List<int>();
    List<int> receivedOrders = List<int>();
    List<int> successfulOrders = List<int>();
    List<int> unSuccessfulOrders = List<int>();
    List<int> returnedOrders = List<int>();
    List<double> cashDaySales = List<double>();
    List<double> cardDaySales = List<double>();
    List<double> companyCommission = List<double>();
    for (int i = 0; i < rawData['numberOfSales'].length; i++) {
      dates.add(DateTime.parse(rawData['numberOfSales'][i]['date']));
      numberOfSales.add(rawData['numberOfSales'][i]['num']);
      receivedOrders.add(rawData['receivedOrders'][i]['num']);
      successfulOrders.add(rawData['successfulOrders'][i]['num']);
      unSuccessfulOrders.add(rawData['unSuccessfulOrders'][i]['num']);
      returnedOrders.add(rawData['returnedOrders'][i]['num']);
      cashDaySales
          .add(double.parse(rawData['cashDaySales'][i]['num'].toString()));
      cardDaySales
          .add(double.parse(rawData['cardDaySales'][i]['num'].toString()));
      companyCommission
          .add(double.parse(rawData['companyCommission'][i]['num'].toString()));
    }
    var result = ChartDataModel(
        dates: dates,
        numberOfSales: numberOfSales,
        receivedOrders: receivedOrders,
        successfulOrders: successfulOrders,
        unSuccessfulOrders: unSuccessfulOrders,
        returnedOrders: returnedOrders,
        cashDaySales: cashDaySales,
        cardDaySales: cardDaySales,
        companyCommission: companyCommission);
    chartData = result;
    yield ReportsLoaded(orders, statistics, chartData, totalAmount);
  }
}
