import 'package:bedo_shop_admin/model/chart_data_model.dart';
import 'package:bedo_shop_admin/model/order.dart';
import 'package:bedo_shop_admin/model/orders_statistics.dart';
import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';

abstract class ReportsState extends Equatable {
  final List<Order> orders;
  final OrdersStatistics statistics;
  final ChartDataModel chartData;
  final double totalOrdersAmount;
  final List<GraphQLError> errors;

  ReportsState(
      [this.orders,
      this.statistics,
      this.chartData,
      this.totalOrdersAmount,
      this.errors])
      : super();

  List<Object> get props => [orders];
}

class ReportsInitial extends ReportsState {
  ReportsInitial() : super([]);
}

class ReportsLoading extends ReportsState {
  ReportsLoading(orders, statistics, chartData, totalOrdersAmount)
      : super(orders, statistics, chartData, totalOrdersAmount);
}

class ReportsLoaded extends ReportsState {
  ReportsLoaded(orders, statistics, chartData, totalOrdersAmount)
      : super(orders, statistics, chartData, totalOrdersAmount);
}

class ReportsLoadingFailed extends ReportsState {
  ReportsLoadingFailed(orders, statistics, chartData, totalOrdersAmount, errors)
      : super(orders, statistics, chartData, totalOrdersAmount, errors);
}

class StatisticsLoadingFailed extends ReportsState {
  StatisticsLoadingFailed(
      orders, statistics, chartData, totalOrdersAmount, errors)
      : super(orders, statistics, chartData, totalOrdersAmount, errors);
}
