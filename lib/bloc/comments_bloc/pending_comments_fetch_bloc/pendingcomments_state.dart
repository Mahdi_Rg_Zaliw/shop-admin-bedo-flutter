part of 'pendingcomments_bloc.dart';

abstract class PendingcommentsState extends Equatable {
  const PendingcommentsState();

  @override
  List<Object> get props => [];
}

class PendingcommentsInitial extends PendingcommentsState {}

class TryingToGetPendingComments extends PendingcommentsState {}

class TryingToReplyPendingComments extends PendingcommentsState {}

class TryingToRejectPendingComments extends PendingcommentsState {}

class FailedGettingPendingComments extends PendingcommentsState {
  //show error
  final String error;

  FailedGettingPendingComments(this.error);
}

class FailedRejectingPendingComments extends PendingcommentsState {
  //show error
  final String error;

  FailedRejectingPendingComments(this.error);
}

class SuccessGettingPendingComments extends PendingcommentsState {
  // show comments
  final List<Comment> result;

  SuccessGettingPendingComments({this.result});
}

class SuccessRejectingPendingComments extends PendingcommentsState {
  // show comments
  final String result;

  SuccessRejectingPendingComments({this.result});
}

class SuccessAcceptingPendingComments extends PendingcommentsState {
  // show comments
  final String result;

  SuccessAcceptingPendingComments({this.result});
}

class SomeThingWentWrongA extends PendingcommentsState {
  // show error
  final String error;

  SomeThingWentWrongA(this.error);
}
