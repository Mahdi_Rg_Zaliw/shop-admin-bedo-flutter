part of 'pendingcomments_bloc.dart';

abstract class PendingcommentsEvent extends Equatable {
  const PendingcommentsEvent();

  @override
  List<Object> get props => [];
}

class PendingCommentsVisisted extends PendingcommentsEvent {}

class PendingCommentsRefresh extends PendingcommentsEvent {}

class PendingCommentAccepting extends PendingcommentsEvent {
  // response text
  final String responseText;
  // id
  final String id;

  PendingCommentAccepting({this.responseText, this.id});
  @override
  List<Object> get props => [id, responseText];
}

class PendingCommentRejecting extends PendingcommentsEvent {
  // id
  final String id;

  PendingCommentRejecting({this.id});
  @override
  List<Object> get props => [id];
}
