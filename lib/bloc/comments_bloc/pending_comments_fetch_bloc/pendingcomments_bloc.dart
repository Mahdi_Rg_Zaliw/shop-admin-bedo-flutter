import 'dart:async';

import 'package:bedo_shop_admin/bloc/comments_bloc/approved_comments_fetch_bloc/approvedcomments_bloc.dart';
import 'package:bedo_shop_admin/data/graphql/comments/comments_request.dart';
import 'package:bedo_shop_admin/model/comments_model.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';

part 'pendingcomments_event.dart';
part 'pendingcomments_state.dart';

class PendingcommentsBloc
    extends Bloc<PendingcommentsEvent, PendingcommentsState> {
  final CommentsRequest commentsRequest;
  ApprovedcommentsBloc approvedcommentsBloc;
  PendingcommentsBloc(this.approvedcommentsBloc)
      : this.commentsRequest = CommentsRequest(),
        super(PendingcommentsInitial());

  List<Comment> comments;

  @override
  void onError(Object error, StackTrace stackTrace) {
    super.onError(error, stackTrace);
    printGreenBg(error);

    this.emit(SomeThingWentWrongA('$error'));
    this.emit(FailedGettingPendingComments('$error'));
  }

  @override
  Stream<PendingcommentsState> mapEventToState(
    PendingcommentsEvent event,
  ) async* {
    if (event is PendingCommentsVisisted) {
      yield* _mapPendingCommentsVisitedToState();
    }
    if (event is PendingCommentsRefresh) {
      yield* _mapPendingCommentsVisitedToState();
    }
    if (event is PendingCommentAccepting) {
      yield* _mapPendingCommentsAcceptingToState(event);
    }
    if (event is PendingCommentRejecting) {
      yield* _mapPendingCommentsRejectingToState(event.id);
    }
  }

  //
  //
  //
  //         _mapPendingCommentsAcceptingToState
  //
  Stream<PendingcommentsState> _mapPendingCommentsAcceptingToState(
      PendingCommentAccepting event) async* {
    yield TryingToReplyPendingComments();
    QueryResult rawData;

    try {
      rawData = await this
          .commentsRequest
          .replyToCommentMutation(event.id, event.responseText);
    } catch (e) {
      yield FailedRejectingPendingComments('$e');
    }
    if (rawData.hasException) {
      yield FailedRejectingPendingComments(
          rawData.exception.graphqlErrors.first.message);
    }

    if (rawData.data != null) {
      yield SuccessAcceptingPendingComments(result: 'success');
      this.add(PendingCommentsRefresh());
      approvedcommentsBloc.add(ApprovedcommentsRefresh());
    }
  }

  //
  //
  //
  //
  //
  //
  //_mapPendingCommentsRejectingToState
  //
  Stream<PendingcommentsState> _mapPendingCommentsRejectingToState(
      commentID) async* {
    yield TryingToRejectPendingComments();
    QueryResult rawData;

    try {
      rawData = await this.commentsRequest.rejectCommentMutation(commentID);
    } catch (e) {
      yield FailedRejectingPendingComments('$e');
    }
    if (rawData.hasException) {
      yield FailedRejectingPendingComments(
          rawData.exception.graphqlErrors.first.message);
    }

    if (rawData.data != null) {
      String data = rawData.data["rejectCommentOnShopByShopAdmin"]["message"];

      yield SuccessRejectingPendingComments(result: data);
      this.add(PendingCommentsRefresh());
    }
  }

  //
  //
  //
  //
  //

  Stream<PendingcommentsState> _mapPendingCommentsVisitedToState() async* {
    yield TryingToGetPendingComments();
    QueryResult rawData;

    try {
      // printYellowBg('getting data');
      rawData = await this.commentsRequest.getPendingCommentsQuery();
    } catch (e) {
      // printYellowBg('failed $e');
      yield FailedGettingPendingComments('$e');
    }
    if (rawData.hasException) {
      yield FailedGettingPendingComments('failed getting waiting comments');
      if (rawData.exception.graphqlErrors.length != 0) {
        // printYellowBg(rawData.exception.graphqlErrors.first.message);

        yield FailedGettingPendingComments(
            rawData?.exception?.graphqlErrors?.first?.message ??
                'failed getting waiting comments');
      }
    }
    if (rawData.data == null) {
      yield FailedGettingPendingComments('failed getting waiting comments');
    }
    if (rawData.data != null) {
      List<dynamic> rawComments =
          rawData.data["getShopAdminCommentsOnShop"] as List<dynamic>;

      List<Comment> results = rawComments
          .map(
            (dynamic e) => Comment.pending(
                pfp: e['profileImageUrl'],
                fullUserName: e['user']['fullName'],
                rank: e['readyComments'],
                commentsText: e['userComment'],
                createdAt: DateTime.parse(e['createdAt']),
                id: e['_id']),
          )
          .toList();
      // printCyanBg(results);
      comments = results;
      //data =
      yield SuccessGettingPendingComments(result: results);
    }
  }
}
