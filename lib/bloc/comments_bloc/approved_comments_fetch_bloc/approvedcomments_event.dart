part of 'approvedcomments_bloc.dart';

abstract class ApprovedcommentsEvent extends Equatable {
  const ApprovedcommentsEvent();

  @override
  List<Object> get props => [];
}

class ApprovedcommentsVisited extends ApprovedcommentsEvent {}

class ApprovedcommentsRefresh extends ApprovedcommentsEvent {}
