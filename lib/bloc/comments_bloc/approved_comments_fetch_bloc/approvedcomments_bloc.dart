import 'dart:async';

import 'package:bedo_shop_admin/data/graphql/comments/comments_request.dart';
import 'package:bedo_shop_admin/model/comments_model.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';

part 'approvedcomments_event.dart';
part 'approvedcomments_state.dart';

class ApprovedcommentsBloc
    extends Bloc<ApprovedcommentsEvent, ApprovedcommentsState> {
  ApprovedcommentsBloc()
      : this.commentsRequest = CommentsRequest(),
        super(ApprovedcommentsInitial());

  final CommentsRequest commentsRequest;

  List<Comment> comments;

  @override
  void onError(Object error, StackTrace stackTrace) {
    super.onError(error, stackTrace);
    printRedBg(error);
    this.emit(SomeThingWentWrongW('$error'));
    printRedBg(error);
  }

  @override
  Stream<ApprovedcommentsState> mapEventToState(
    ApprovedcommentsEvent event,
  ) async* {
    if (event is ApprovedcommentsVisited) {
      yield* _mapApprovedcommentsVisitedToState();
    }
    if (event is ApprovedcommentsRefresh) {
      yield* _mapApprovedcommentsVisitedToState();
    }
  }

  Stream<ApprovedcommentsState> _mapApprovedcommentsVisitedToState() async* {
    yield TryingToGetApprovedcomments();
    QueryResult rawData;

    try {
      // printYellowBg('getting data');
      rawData = await this.commentsRequest.getConfirmedCommentsQuery();
    } catch (e) {
      printYellowBg('failed $e');
      yield FailedGettingApprovedComments('$e');
    }
    if (rawData.data == null) {
      yield FailedGettingApprovedComments('failed getting approved comments');
    }
    if (rawData.hasException) {
      yield FailedGettingApprovedComments('failed');
      if (rawData.exception.graphqlErrors.length != 0) {
        printYellowBg(
            rawData?.exception?.graphqlErrors?.first?.message ?? 'failed');
        yield FailedGettingApprovedComments(
            rawData.exception.graphqlErrors.first.message);
      }
    }

    if (rawData.data != null) {
      List<dynamic> rawComments =
          rawData.data["getShopAdminCommentsOnShop"] as List<dynamic>;
      // printGreenBg(rawComments);
      // printCyanBg('no data ?');
      List<Comment> results = rawComments
          .map(
            (dynamic e) => Comment.accepted(
                adminReply: AdminReply(
                    admin: Admin(
                        id: e['shopAdminReply']['admin']['_id'],
                        name: e['shopAdminReply']['admin']['fullName']),
                    replyText: e['shopAdminReply']['comment']['userComment']),
                pfp: e['profileImageUrl'],
                fullUserName: e['user']['fullName'],
                rank: e['readyComments'],
                commentsText: e['userComment'],
                createdAt: DateTime.parse(e['createdAt']),
                id: e['_id']),
          )
          .toList();
      // printCyanBg(results);
      comments = results;
      //data =
      yield SuccessGettingApprovedComments(result: results);
    }
  }
}
