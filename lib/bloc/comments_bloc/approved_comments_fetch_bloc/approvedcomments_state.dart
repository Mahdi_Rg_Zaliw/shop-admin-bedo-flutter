part of 'approvedcomments_bloc.dart';

abstract class ApprovedcommentsState extends Equatable {
  const ApprovedcommentsState();

  @override
  List<Object> get props => [];
}

class ApprovedcommentsInitial extends ApprovedcommentsState {}

class TryingToGetApprovedcomments extends ApprovedcommentsState {}

class SomeThingWentWrongW extends ApprovedcommentsState {
  //error
  final String error;

  SomeThingWentWrongW(this.error);
}

class FailedGettingApprovedComments extends ApprovedcommentsState {
  //error
  final String error;

  FailedGettingApprovedComments(this.error);
}

class SuccessGettingApprovedComments extends ApprovedcommentsState {
  // show data
  final List<Comment> result;

  SuccessGettingApprovedComments({this.result});
}
