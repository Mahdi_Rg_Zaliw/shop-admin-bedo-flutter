part of 'total_comments_bloc.dart';

abstract class TotalCommentsEvent extends Equatable {
  const TotalCommentsEvent();

  @override
  List<Object> get props => [];
}

class TotalCommentsVisited extends TotalCommentsEvent {}

class TotalCommentsRefresh extends TotalCommentsEvent {}
