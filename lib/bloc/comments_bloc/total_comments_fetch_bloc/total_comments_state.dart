part of 'total_comments_bloc.dart';

abstract class TotalCommentsState extends Equatable {
  const TotalCommentsState();

  @override
  List<Object> get props => [];
}

class TotalCommentsInitialState extends TotalCommentsState {}

class TryingToLoadTotalComments extends TotalCommentsState {}

// class CommentsLoaded extends TotalCommentsState {
//   final List<Comment> approvedComments;
//   final List<Comment> pendingComments;
//   final int total;
//   CommentsLoaded({
//     this.approvedComments,
//     this.pendingComments,
//     this.total,
//   });
//   @override
//   List<Object> get props => [pendingComments, approvedComments, total];
// }

class CommentsCountLoadedSuccess extends TotalCommentsState {
  final int amount;

  CommentsCountLoadedSuccess({this.amount});
  @override
  List<Object> get props => [amount];
}

class CommentsCountLoadingFail extends TotalCommentsState {
  // show error
  final String error;

  CommentsCountLoadingFail(this.error);
}

class SomeThingWentWrong extends TotalCommentsState {
  //show error
  final String error;

  SomeThingWentWrong(this.error);
}
