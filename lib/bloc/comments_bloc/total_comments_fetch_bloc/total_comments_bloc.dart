import 'dart:async';

import 'package:bedo_shop_admin/data/graphql/comments/comments_request.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';

part 'total_comments_event.dart';
part 'total_comments_state.dart';

class TotalCommentsBloc extends Bloc<TotalCommentsEvent, TotalCommentsState> {
  final CommentsRequest commentsRequest;
  TotalCommentsBloc()
      : this.commentsRequest = CommentsRequest(),
        super(TotalCommentsInitialState());

  @override
  void onError(Object error, StackTrace stackTrace) {
    printCyanBg(error);
    super.onError(error, stackTrace);
    // this.add(event)
    this.emit(SomeThingWentWrong(error.toString()));
  }

  @override
  Stream<TotalCommentsState> mapEventToState(
    TotalCommentsEvent event,
  ) async* {
    if (event is TotalCommentsVisited) {
      yield* _mapTotalCommentsInitialToState();
    }
    if (event is TotalCommentsRefresh) {
      yield* _mapTotalCommentsInitialToState();
    }
  }

  //
  //
  //
  //
  //
  //
  //
  //
  Stream<TotalCommentsState> _mapTotalCommentsInitialToState() async* {
    yield TryingToLoadTotalComments();

    QueryResult rawData;

    try {
      rawData = await this.commentsRequest.getTotalCommentsQuery();
    } catch (e) {
      yield CommentsCountLoadingFail(e.toString());
    }

    if (rawData.hasException) {
      // printRedBg(rawData.exception.graphqlErrors.first.message);

      yield CommentsCountLoadingFail(
          rawData?.exception?.graphqlErrors?.first?.message ?? 'failed');
    }

    if (rawData.data != null) {
      // printGreenBg(rawData.data);

      final totalComments =
          rawData.data['getCommentsOnShopByShopAdminCount'] as int;

      // printGreenBg(totalComments);
      yield CommentsCountLoadedSuccess(amount: totalComments);
    }
  }
  //
  //
  //
  //
  //
  //
  //
  //
  //
}
