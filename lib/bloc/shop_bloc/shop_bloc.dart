import 'dart:async';

import 'package:bedo_shop_admin/data/graphql/shop/shop_request.dart';
import 'package:bedo_shop_admin/presentation/screens/vanils_showcases/upload_image_test.dart';
import 'package:bedo_shop_admin/presentation/widgets/form_input_select_shop_func.dart';
import 'package:bedo_shop_admin/presentation/widgets/form_input_select_working_day.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:bedo_shop_admin/utils/time_utils.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:graphql/client.dart';

part 'shop_event.dart';
part 'shop_state.dart';

class ShopBloc extends Bloc<ShopEvent, ShopState> {
  ShopBloc() : super(ShopInitial());
  ShopRequests _shopRequests = ShopRequests();
  QueryResult _queryResult;
  List weekTime;
  var location;
  var phone;
  var preptime;
  @override
  Stream<ShopState> mapEventToState(
    ShopEvent event,
  ) async* {
    if (event is EditShopVisited) {
      yield* _mapShopEditShopPageVisitedToState();
    } else if (event is ShopEditing) {
      yield* _mapShopEditingToState(event.data);
    } else if (event is ShopCreating) {
      yield* _mapShopCreatingToState(event.data);
    } else if (event is NavOut) {
      yield ShopInitial();
    } else if (event is AddShopVisited) {
      yield* _mapAddShopVisitedToState();
    }
  }

  Stream<ShopState> _mapAddShopVisitedToState() async* {
    // yield ;

    try {
      _queryResult = await _shopRequests.getCategoriesAlone();
    } catch (e) {
      printCyanBg('trying to load data of shop for edit blcok');
      printCyanBg('Error : $e');
      yield FailGettingCategories(e);
    }
    if (_queryResult.hasException) {
      printCyanBg('request failiure when getting data for edit');
      printCyanBg('Error : ${_queryResult.exception}');
      yield FailGettingCategories(_queryResult.exception);
    }
    if (!_queryResult.hasException && _queryResult.data != null) {
      printGreenBg(_queryResult.data);
      final d = _queryResult.data['getCategories'] as List;
      printColor(Colors.pink, d);
      if (d.isNotEmpty) {
        var data = d
            .map(
              (e) => CategoryDataModel(
                check: false,
                id: e['_id'].toString(),
                title: e['title'].toString(),
              ),
            )
            .toList();
        printColor(Colors.purple, data);
        printColorBgLight(Colors.purple, data);
        yield ShopCategoriesLoaded(data);
      } else {
        yield FailGettingCategories('no category found');
      }
    }
  }

  Stream<ShopState> _mapShopCreatingToState(data) async* {
    try {
      String url;
      if (data['file'] != null) {
        url = await uploadImage(data['file']);
      }
      printYellowBg(url);
      _queryResult = await _shopRequests.createShopRequest(
        logoUrl: url,
        name: data['name'],
        coordinates: data['coordinates'],
        phoneNumbers: data['phoneNumbers'],
        taxCode: data['taxCode'],
        workingHoursInMinutes: data['workingHoursInMinutes'],
        cardNumber: data['cardNumber'],
        categories: data['categories'],
        preparingTime: data['preparingTime'],
      );
    } catch (e) {
      printCyanBg('try blcok');
      printCyanBg('Error : $e');
      yield FailedShopDataOperation(msg: e);
      yield ShopInitial();
    }
    if (_queryResult.hasException) {
      printCyanBg('request failiure when getting data for edit');
      printCyanBg('Error : ${_queryResult.exception}');
      if (_queryResult.exception.graphqlErrors.isNotEmpty) {
        yield FailedShopDataOperation(
            clientErr:
                _queryResult.exception.graphqlErrors.first.message.toString(),
            msg: _queryResult.exception);
      } else {
        yield FailedShopDataOperation(msg: _queryResult.exception);
      }

      yield ShopInitial();
    }
    if (!_queryResult.hasException && _queryResult.data != null) {
      printRedBg('we won editing data');
      printRedBg(_queryResult.data);
      yield SuccessShopOperation();
    }
  }

  Stream<ShopState> _mapShopEditingToState(data) async* {
    yield WaitingForResults();

    try {
      String url;
      if (data['file'] != null) {
        url = await uploadImage(data['file']);
      }
      printYellowBg(url);
      _queryResult = await _shopRequests.updateShop(
        logoUrl: url ?? data['logoUrl'],
        name: data['name'],
        coordinates: data['coordinates'],
        phoneNumbers: data['phoneNumbers'],
        taxCode: data['taxCode'],
        workingHoursInMinutes: data['workingHoursInMinutes'],
        cardNumber: data['cardNumber'],
        preparingTime: data['preparingTime'],
        categories: data['categories'],
      );
    } catch (e) {
      printCyanBg('trying to load data of shop for edit blcok');
      printCyanBg('Error : $e');
      yield FailedShopDataOperation(msg: e);
    }
    if (_queryResult.hasException) {
      printCyanBg('request failiure when getting data for edit');
      printCyanBg('Error : ${_queryResult.exception}');
      yield FailedShopDataOperation(msg: _queryResult.exception);
    }
    if (!_queryResult.hasException && _queryResult.data != null) {
      printRedBg('we won editing data');
      printRedBg(_queryResult.data);
      yield SuccessShopOperation();
      this.add(EditShopVisited());
    }
  }

  Stream<ShopState> _mapShopEditShopPageVisitedToState() async* {
    try {
      _queryResult = await _shopRequests.getShopDatas();
    } catch (e) {
      printCyanBg('trying to load data of shop for edit blcok');
      printCyanBg('Error : $e');
      yield FailedLoadingEditData(e);
    }
    if (_queryResult.hasException) {
      printCyanBg('request failiure when getting data for edit');
      printCyanBg('Error : ${_queryResult.exception}');
      yield FailedLoadingEditData(_queryResult.exception);
    }
    if (!_queryResult.hasException && _queryResult.data != null) {
      printGreenBg(_queryResult.data);

      final d = _queryResult.data['getShopByShopAdmin'];
      phone = d['phoneNumbers'];
      location = d['location']['coordinates'];
      preptime = d['preparingTime'];
      String address = d['address'] ?? ' no Adress set';
      weekTime = d['workingHoursInMinutes'];
      printCyanBg(d['logoUrl']);
      printGreenBg(d['workingHoursInMinutes']);
      printGreenBg(d['getTotalTransactionsAmountByShopAdmin']);
      printGreenBg(d['workingHoursInMinutes'].runtimeType);
      printGreenBg(d['preparingTime'].runtimeType);
      printGreenBg(d['preparingTime']);
      printGreenBg(d['categories']);
      printGreenBg(d['categories'].runtimeType);
      List selectedCats = d['categories'];
      dynamic wholeCategories = _queryResult.data['getCategories'];
      printGreenBg(wholeCategories);
      List c = wholeCategories
          .map(
            (e) => CategoryDataModel(
              check: false,
              id: e['_id'].toString(),
              title: e['title'].toString(),
            ),
          )
          .toList();
      selectedCats.forEach((element) {
        var ed = element['_id'];
        c.forEach((e) {
          if (e.id == ed) {
            e.check = true;
          }
        });
      });
      printColor(Colors.pink, c);
      Map data = {
        'address': address,
        'name': d['name'],
        'preparingTime': d['preparingTime'],
        'location': location,
        'phoneNumbers': d['phoneNumbers'],
        'workingHoursInMinutes': weekData,
        'taxCode': d['taxCode'],
        'cardNumber': d['cardNumber'],
        'logoUrl': d['logoUrl'],
        'getTotalTransactionsAmountByShopAdmin':
            _queryResult.data['getTotalTransactionsAmountByShopAdmin'] ?? 0,
        'categories': c,
      };
      yield ShopEditableDataLoaded(data);
    }
  }

  List<DayWorkingTime> get weekData {
    List<DayWorkingTime> data = List.generate(
        7,
        (index) => DayWorkingTime(
              dayNumber: index,
            ));
    weekTime?.forEach((e) {
      int day = getWeekDayIndexAr(e['type']);
      printBlueBg(day);
      final start = Duration(minutes: int.parse(e['from'].toString()));
      final end = Duration(minutes: int.parse(e['to'].toString()));
      data[day] =
          DayWorkingTime(dayNumber: day, check: true, start: start, end: end);
    });
    printRedBg(data);
    return data;
  }
}
