part of 'shop_bloc.dart';

abstract class ShopEvent extends Equatable {
  const ShopEvent();

  @override
  List<Object> get props => [];
}

class EditShopVisited extends ShopEvent {}

class NavOut extends ShopEvent {}

class AddShopVisited extends ShopEvent {}

class ShopCreating extends ShopEvent {
  final Map data;

  ShopCreating(this.data);
}

class ShopEditing extends ShopEvent {
  final Map data;

  ShopEditing(this.data);
  @override
  List<Object> get props => [data];
}
