part of 'shop_bloc.dart';

abstract class ShopState extends Equatable {
  const ShopState();

  @override
  List<Object> get props => [];
}

class ShopInitial extends ShopState {}

class ShopEditableDataLoaded extends ShopState {
  final Map data;

  ShopEditableDataLoaded(this.data);
  @override
  List<Object> get props => [data];
}

class FailedLoadingEditData extends ShopState {
  dynamic msg;

  FailedLoadingEditData(this.msg);
}

class WaitingForResults extends ShopState {}

class FailedShopDataOperation extends ShopState {
  dynamic msg;
  dynamic clientErr;
  FailedShopDataOperation({this.msg, this.clientErr = ''});
}

class SuccessShopOperation extends ShopState {}

class FailGettingCategories extends ShopState {
  final data;

  FailGettingCategories(this.data);
}

class ShopCategoriesLoaded extends ShopState {
  final data;

  ShopCategoriesLoaded(this.data);
}
