part of 'transactions_bloc.dart';

abstract class TransactionsState extends Equatable {
  const TransactionsState({this.outputs, this.inputs, this.totals});

  final List outputs;
  final List inputs;
  final List totals;

  @override
  List<Object> get props => [];
}

class TransactionsInitial extends TransactionsState {}

class TransactionsLoadSuccess extends TransactionsState {
  TransactionsLoadSuccess({outputs, inputs, totals})
      : super(outputs: outputs, inputs: inputs, totals: totals);
}
