import 'dart:async';

import 'package:bedo_shop_admin/data/graphql/shop/shop_request.dart';
import 'package:bedo_shop_admin/presentation/screens/restaurant_wallet_transaction_screen.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:graphql/client.dart';

part 'transactions_event.dart';
part 'transactions_state.dart';

// enum TransactionsType {
//   INPUT,
//   OUTPUT,
//   TOTAL,
// }

class TransactionsBloc extends Bloc<TransactionsEvent, TransactionsState> {
  TransactionsBloc() : super(TransactionsInitial());

  ShopRequests _requests = ShopRequests();

  String getTxTypeVariable(TxType type) {
    switch (type) {
      case TxType.Input:
        return 'INPUT';
        break;
      case TxType.Output:
        return 'OUTPUT';
        break;

      default:
        return 'TOTAL';
    }
  }

  @override
  Stream<TransactionsState> mapEventToState(
    TransactionsEvent event,
  ) async* {
    if (event is TransactionsVisited) {
      yield* _transactionsVisitedToState(event);
    }
  }

  Stream<TransactionsState> _transactionsVisitedToState(event) async* {
    //total transactions
    QueryResult _result;
    // getTxTypeVariable(event.type);
    try {
      _result = await _requests.getAllTransActionsTogather();
    } catch (e) {
      printRedBg(e);
      // yield err;
    }
    if (_result.hasException) {
      printRedBg(_result.exception);
      if (_result.exception.graphqlErrors.isNotEmpty) {
        printRedBg(_result.exception.graphqlErrors.first);
        //yield err;
      }
    }

    if (!_result.hasException && _result.data != null) {
      printColorBgLight(Colors.deepPurple, _result.data);
      var d = _result.data;
      var outputs = d['outputs'];
      var inputs = d['inputs'];
      var totals = d['totals'];
      yield TransactionsLoadSuccess(
          outputs: outputs, inputs: inputs, totals: totals);
    }

// _requests.getAllTransActions(type, max, distance)
  }
}
