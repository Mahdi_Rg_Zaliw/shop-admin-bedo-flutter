import 'dart:async';

import 'package:bedo_shop_admin/data/graphql/shop/shop_request.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:graphql/client.dart';

part 'signinrouter_event.dart';
part 'signinrouter_state.dart';

class SigninrouterBloc extends Bloc<SigninrouterEvent, SigninrouterState> {
  ShopRequests _shopRequests = ShopRequests();
  QueryResult _queryResult;
  SigninrouterBloc() : super(SigninrouterInitial());

  @override
  Stream<SigninrouterState> mapEventToState(
    SigninrouterEvent event,
  ) async* {
    if (event is UserLoggedInSuccess) {
      ///*
      // check if has made a shop or not
      printOrange('bloc init');
      try {
        _queryResult = await _shopRequests.getShopDatas();
      } catch (e) {
        printOrange('bloc internal');

        printCyanBg('load data Failure no data receive');
        printCyanBg('Error [internal error]: $e');
        yield FailedLoadingData(e);
      }
      if (_queryResult.hasException) {
        if (_queryResult.exception.graphqlErrors.isNotEmpty) {
          printColorBgLight(Colors.lime, _queryResult.toString());
          String errMsg = _queryResult.exception.graphqlErrors.first.message
              .toString()
              .trim();
          printRedBg(errMsg);
          String errCode = _queryResult
              .exception.graphqlErrors.first.extensions['code']
              .toString()
              .trim();
          printOrange(errCode);
          if (('shop not found'.trim() == errMsg) && (errCode == '404')) {
            printOrange('user has no shop created');
            yield UserHasNoShop();
            yield SigninrouterInitial();
          }
        } else {
          printOrange('bloc exep');
          yield SigninrouterInitial();

          yield SomethingWentWrong();
        }
      } else if (!_queryResult.hasException && _queryResult.data != null) {
        yield UserHasShop();
        yield SigninrouterInitial();
      }
      // yield SomethingWentWrong();
      // printOrange('yay');
    }
  }
}
