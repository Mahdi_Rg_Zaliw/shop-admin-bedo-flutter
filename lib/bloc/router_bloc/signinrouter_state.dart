part of 'signinrouter_bloc.dart';

abstract class SigninrouterState extends Equatable {
  const SigninrouterState();

  @override
  List<Object> get props => [];
}

class SigninrouterInitial extends SigninrouterState {}

class UserHasShop extends SigninrouterState {}

class SomethingWentWrong extends SigninrouterState {}

class UserHasNoShop extends SigninrouterState {}

class FailedLoadingData extends SigninrouterState {
  final String err;

  FailedLoadingData(this.err);
}
