part of 'signinrouter_bloc.dart';

abstract class SigninrouterEvent extends Equatable {
  const SigninrouterEvent();

  @override
  List<Object> get props => [];
}

class UserLoggedInSuccess extends SigninrouterEvent {}
