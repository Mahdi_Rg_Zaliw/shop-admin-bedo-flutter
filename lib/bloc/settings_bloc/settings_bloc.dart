import 'dart:async';

import 'package:bedo_shop_admin/data/graphql/settings/settings_request.dart';
import 'package:bedo_shop_admin/data/secure_storage/secure_storage.dart';
import 'package:bedo_shop_admin/model/howi_t_works.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';

part 'settings_event.dart';
part 'settings_state.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  SettingsBloc() : super(SettingsInitial());
  SettingsRequests _settingsRequests = SettingsRequests();
  QueryResult queryResult;
  // String name;
  List<Object> phone;
  // String card;
  int ptime;
  List<Object> location;
  // some neccessary fields must be extracted;

  @override
  Stream<SettingsState> mapEventToState(
    SettingsEvent event,
  ) async* {
    if (event is SettingsVisited) {
      yield* _mapSettingsVisitedToState();
    } else if (event is PasswordChanging) {
      yield* _mapPasswordChangingToState(event.data);
    } else if (event is CardNumberChanging) {
      yield* _mapCardNumberChangingToState(event.data);
    } else if (event is Logout) {
      yield* _mapLogOutToState();
    } else if (event is HowItWorksVisited) {
      yield* _mapHowItWorksVisitedToState();
    }
  }

  Stream<SettingsState> _mapHowItWorksVisitedToState() async* {
    try {
      queryResult = await _settingsRequests.getHowitWorks();
    } catch (e) {
      printRedBg('try error is for How It works '.padRight(50, ' '));
      printRedBg('error $e');
    }
    if (!queryResult.hasException && queryResult.data != null) {
      printGreenBg(queryResult.data);
      final howItWorksList = queryResult.data['getShopHowItWorks'] as List;
      final data = howItWorksList.map(
        (e) {
          return HowItWorks(
              title: e['title'].toString(),
              describtion: e['description'].toString());
        },
      ).toList();

      yield HowItWorksLoaded(data: data);
    }
  }

  Stream<SettingsState> _mapLogOutToState() async* {
    try {
      queryResult = await _settingsRequests.logout();
    } catch (e) {
      printRedBg('try error is'.padRight(50, ' '));
      printRedBg('error $e');
    }
    if (!queryResult.hasException && queryResult.data != null) {
      printGreenBg(queryResult.data);
      final msg = queryResult.data['signOut']['message'];
      printGreenBg(msg);
      await SecureStorage.deleteAllData();
      printGreenBg('done log out');
      yield LogoutDone(msg: '$msg');
    }
  }

  Stream<SettingsState> _mapCardNumberChangingToState(data) async* {
    yield TryingToChangeCard();
    try {
      queryResult = await _settingsRequests.changeCard(
        cardNumber: data.toString(),
        coordinates: location,
        phoneNumbers: phone,
        preparingTime: ptime,
      );
    } catch (e) {
      printRedBg('try error is'.padRight(50, ' '));
      printRedBg('error $e');
    }
    if (queryResult.hasException || queryResult.data == null) {
      printWhiteBg('some thing went wrong');
      printWhiteBg('result is'.padRight(50, ' '));
      printWhiteBg(queryResult);
      printWhiteBg('error is'.padRight(50, ' '));
      printWhiteBg(queryResult.exception);
      yield ChangeCardResult(msg: 'failed to change password', success: false);
    }
    if (queryResult.data != null) {
      final x = queryResult.data['updateShopByShopAdmin']['cardNumber'];
      printYellowBg(x);
      yield ChangeCardResult(
        msg: 'Successfully changed card NUmber \n\n$x',
        success: true,
      );
    }
    // this.add(SettingsVisited());
  }

  Stream<SettingsState> _mapPasswordChangingToState(data) async* {
    yield TryingToChangePass();
    try {
      queryResult = await _settingsRequests.changePassword(
          currentPassword: data['currentPassword'],
          newPassword: data['newPassword']);
    } catch (e) {
      printRedBg(e.runtimeType);
      printRedBg('try error is'.padRight(50, ' '));
      printRedBg('error $e');
    }
    if (queryResult.hasException || queryResult.data == null) {
      printWhiteBg('some thing went wrong');
      printWhiteBg('result is'.padRight(50, ' '));
      printWhiteBg(queryResult);
      printWhiteBg('error is'.padRight(50, ' '));
      printWhiteBg(queryResult.exception);
      yield ChangePasswordResult(
          msg: 'failed to change password', success: false);
    }
    if (queryResult.data != null) {
      yield ChangePasswordResult(
          msg: 'Successfully changed password', success: true);
    }
    // this.add(SettingsVisited());
  }

  Stream<SettingsState> _mapSettingsVisitedToState() async* {
    yield LoadingSettings();
    try {
      queryResult = await _settingsRequests.getUserSettings();
    } catch (e) {
      printRedBg('try error is'.padRight(50, ' '));
      printRedBg('error $e');
    }
    if (queryResult.hasException || queryResult.data == null) {
      printRedBg('some thing went wrong');
      printRedBg('result is'.padRight(50, ' '));
      printRedBg(queryResult);
      printRedBg('error is'.padRight(50, ' '));
      printRedBg(queryResult.exception);
    }
    if (queryResult.data != null) {
      final d = queryResult.data['getShopByShopAdmin'];
      printGreenBg(d);
      printGreenBg(d['name']);
      printGreenBg(d['name'].runtimeType);
      printGreenBg(d['cardNumber']);
      printGreenBg(d['cardNumber'].runtimeType);
      printGreenBg(d['phoneNumbers']);
      printGreenBg(d['phoneNumbers'].runtimeType);
      printGreenBg(d['preparingTime']);
      printGreenBg(d['preparingTime'].runtimeType);
      printGreenBg(d['location']['coordinates']);
      printGreenBg(d['location']['coordinates'].runtimeType);
      printGreenBg(d['shopAdmin']['fullName']);
      printGreenBg(d['shopAdmin']['fullName'].runtimeType);
      printGreenBg(d['shopAdmin']['email']);
      printGreenBg(d['shopAdmin']['email'].runtimeType);

      ptime = d['preparingTime'];
      // name = d['name'];
      // card = d['cardNumber'];
      phone = d['phoneNumbers'];
      final coordinates = d['location']['coordinates'];
      location = coordinates;

      final Map settingLoadedData = {
        // 'name': d['name'],
        'cardNumber': d['cardNumber']
            .replaceAllMapped(RegExp(r".{4}"), (match) => "${match.group(0)} "),
        'phoneNumbers': d['phoneNumbers'][0],
        // 'preparingTime': d['preparingTime'],
        'fullName': d['shopAdmin']['fullName'],
        'email': d['shopAdmin']['email'],
      };
      yield SettingsLoadSuccess(settingLoadedData);
    }
  }
}
