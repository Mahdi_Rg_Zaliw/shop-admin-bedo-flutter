part of 'settings_bloc.dart';

abstract class SettingsEvent extends Equatable {
  const SettingsEvent();

  @override
  List<Object> get props => [];
}

class SettingsVisited extends SettingsEvent {}

class Logout extends SettingsEvent {}

class HowItWorksVisited extends SettingsEvent {}

class PasswordChanging extends SettingsEvent {
  final Map data;

  PasswordChanging(this.data);
}

class CardNumberChanging extends SettingsEvent {
  final String data;

  CardNumberChanging(this.data);
}
