part of 'settings_bloc.dart';

abstract class SettingsState extends Equatable {
  const SettingsState();

  @override
  List<Object> get props => [];
}

class SettingsInitial extends SettingsState {}

class LoadingSettings extends SettingsState {}

class TryingToChangePass extends SettingsState {}

class LogoutDone extends SettingsState {
  final String msg;

  LogoutDone({this.msg});
}

class HowItWorksLoaded extends SettingsState {
  final List<HowItWorks> data;

  HowItWorksLoaded({this.data});
}

class TryingToChangeCard extends SettingsState {}

class ChangePasswordResult extends SettingsState {
  final String msg;
  final bool success;

  ChangePasswordResult({this.msg, this.success});
}

class ChangeCardResult extends SettingsState {
  final String msg;
  final bool success;

  ChangeCardResult({this.msg, this.success});
}

class SettingsLoadSuccess extends SettingsState {
  final Map data;

  SettingsLoadSuccess(this.data);
}
