import 'package:bedo_shop_admin/bloc/auth_bloc/auth_events.dart';
import 'package:bedo_shop_admin/bloc/auth_bloc/auth_states.dart';
import 'package:bedo_shop_admin/constants/constants.dart';
import 'package:bedo_shop_admin/data/graphql/auth/auth_request.dart';
import 'package:bedo_shop_admin/data/secure_storage/secure_storage.dart';
import 'package:bedo_shop_admin/model/user_model.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql/client.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthRequest _authRequests;
  User user;

  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    print(event);
    if (event is AuthBlocReset) {
      yield* _mapTryAgainToState();
    } else if (event is LoginEvents) {
      yield* _mapLoginToState(event);
    } else if (event is SignUpEvents) {
      yield* _mapSignUpToState(event);
    } else if (event is ResendVerificationCodeRequest) {
      yield* _mapResendVerificationCodeRequestToState(event);
    } else if (event is CheckVerificationCode) {
      yield* _mapCheckVerificationCodeToState(event);
    } else if (event is ComplateInformationSignUp) {
      yield* _mapComplateInformationSignUpToState(event);
    }
    // else if (event is DeleteAccountEvent) {
    //   if (await SecureStorage.getToken(accessTokenKey) != null) {
    //     deleteAllSecureData();
    //     yield AuthInitial();
    //   } else {
    //     print('You Already Log Out');
    //   }
    // }
  }

  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///

  AuthBloc() : super(AuthInitial()) {
    _authRequests = AuthRequest();
    user = User();
  }

  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///

  Future<void> deleteAllSecureData() async {
    var deleteAll = await SecureStorage.deleteAllData();
    return deleteAll;
  }

  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///

  Future<void> setTokenResualt(access, refresh, fcm) async {
    return await SecureStorage.setTokens(
      access,
      refresh,
      fcm,
    );
  }

  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///

  Future<void> getTokenResualt(access) async {
    String accessToken = await SecureStorage.getToken(
      access,
    );
    String refreshToken = await SecureStorage.getToken(
      access,
    );
    print('Access Token: ' + accessToken);
    print('Refresh Token: ' + refreshToken);
    return accessToken;
  }

  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///

  Future<void> setIdResualt(id) async {
    return await SecureStorage.setId(id);
  }

  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///              RESET
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  Stream<AuthState> _mapTryAgainToState() async* {
    yield AuthInitial();
  }

  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///              LOGIN
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  Stream<AuthState> _mapLoginToState(event) async* {
    yield AuthLoading();
    user.emailOrPhoneNumber = event.emailOrPhoneNumber;
    print("Email Or PhoneNumber : " + user.emailOrPhoneNumber);
    QueryResult queryResult;
    try {
      queryResult = await _authRequests
          .adminLoginRequest(
        event.emailOrPhoneNumber,
        event.password,
        event.fcm,
      )
          .timeout(
        Duration(seconds: 8),
        onTimeout: () {
          return QueryResult(source: null)
            ..exception = OperationException(
              graphqlErrors: [
                GraphQLError(message: 'request timeout'),
              ],
            );
        },
      );
    } catch (e) {
      printRedBg('swr');
      yield LoginErrors(clientErrors: 'somthing went wrong!');
    }
    printCyanBg(queryResult);
    print(queryResult.data);
    print(queryResult.exception);

    if (queryResult.hasException) {
      printYellowBg('qr has exception');
      printYellowBg(queryResult);

      if (queryResult.exception.graphqlErrors.isNotEmpty) {
        yield LoginErrors(
          message: queryResult.exception.graphqlErrors,
          clientErrors: queryResult.exception.graphqlErrors?.first?.message,
        );
        print(
            'Error IS : ${queryResult.exception.graphqlErrors?.first?.message}');
      } else {
        printRedBg('some other thing went wrong');
        yield LoginErrors(clientErrors: 'somthing went wrong!');
      }
    }

    if (queryResult.data != null && !queryResult.hasException) {
      print(queryResult.data);

      // printBlueBg(queryResult.)
      //*
      //*
      //*         check stuff for lost user pass loss
      //*
      //*
      final accessToken =
          // "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDRkZDgwZjA4NzdkYTYxMjBjMDM2ODciLCJzdWIiOiI2MDRkZDgwZjA4NzdkYTYxMjBjMDM2ODciLCJpc3MiOiJzcGFyay5jb20iLCJ0b2tlblR5cGUiOiJBQ0NFU1NfVE9LRU4iLCJyb2xlcyI6IlNIT1BfQURNSU4iLCJ0b2tlbktleSI6IjE3ZDI5Mjk0LWI1NDEtNDI3MS05MDQ4LTBkMTE5N2YzNTI3NCIsInNob3AiOiI2MDRkZDg0MjA4NzdkYWI1MjhjMDM2OGMiLCJpYXQiOjE2MTgyMDk4NTYsImV4cCI6MTYyMDgwMTg1Nn0.jIgAsA9apEPUGRNxxnA4mTdDKMygKu72CxfZH9Tg7Gw";
          queryResult.data['adminLogin']['token']['accessToken'];
      //*
      //*
      //*
      //*
      final refreshToken =
          queryResult.data['adminLogin']['token']['refreshToken'];
      await SecureStorage.setTokens(
        accessToken,
        refreshToken,
        event.fcm,
      );
      printMagentBg('done'.padLeft(100, ' '));
      printYellowBg(' fcm was '.padLeft(100, ' '));
      printYellowBg(event.fcm);
      // Graphql.addHeader({"authorization": accessToken});
      await SecureStorage.setId(queryResult.data['adminLogin']['_id']);

      yield UserLoginSuccess();
    }
  }

  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  Stream<AuthState> _mapSignUpToState(event) async* {
    yield AuthLoading();
    user.phoneNumber = event.phoneNumber;
    print("PhoneNumber For Verification Is " + user.phoneNumber);

    QueryResult queryResult = await _authRequests
        .getShopAdminPhoneVerificationCodeRequest(event.phoneNumber)
        .timeout(
      Duration(seconds: 8),
      onTimeout: () {
        return QueryResult(source: null)
          ..exception = OperationException(
            graphqlErrors: [
              GraphQLError(message: 'request timeout'),
            ],
          );
      },
    );

    if (queryResult.hasException) {
      printYellowBg(queryResult);
      if (queryResult.exception.graphqlErrors.isNotEmpty) {
        yield SignUpErrors(
          message: queryResult.exception.graphqlErrors,
          clientErrors: queryResult.exception.graphqlErrors?.first?.message,
        );
        print(
            'Error IS : ${queryResult.exception.graphqlErrors?.first?.message}');
      } else {
        yield SignUpErrors(clientErrors: 'somthing went wrong');
      }

      print(queryResult.exception);
    }

    ///
    ///
    ///
    ///
    ///
    ///
    ///
    ///                   TEST EVENTS
    ///
    ///
    ///
    ///
    queryResult =
        await _authRequests.testingVerification(event.phoneNumber).timeout(
      Duration(seconds: 8),
      onTimeout: () {
        return QueryResult(source: null)
          ..exception = OperationException(
            graphqlErrors: [
              GraphQLError(message: 'request timeout'),
            ],
          );
      },
    );

    if (queryResult.hasException) {
      printYellowBg(queryResult);
      if (queryResult.exception.graphqlErrors.isNotEmpty) {
        yield SignUpErrors(
          message: queryResult.exception.graphqlErrors,
          clientErrors: queryResult.exception.graphqlErrors?.first?.message,
        );
        print(
            'Error IS : ${queryResult.exception.graphqlErrors?.first?.message}');
      } else {
        yield SignUpErrors(clientErrors: 'somthing went wrong');
      }

      print(queryResult.exception);
    }

    ///
    ///
    ///
    ///
    ///
    ///
    ///
    ///
    ///
    ///
    ///

    if (queryResult.data != null && !queryResult.hasException) {
      print(queryResult.data);
      String code = queryResult.data['getShopAdminPhoneVerificationCode']
          ['phoneVerificationCode'];
      yield SendingCodeVerification(code: code);
      print("Sending Code For Number : ${user.phoneNumber}");
    }
  }

  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///```
  ///   if (queryResult.data != null && !queryResult.hasException) {
  ///     print(queryResult.data);
  ///     yield SendingCodeVerification();
  ///     print("Sending Code For Number : ${user.phoneNumber}");
  ///   }
  /// }

  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///

  Stream<AuthState> _mapResendVerificationCodeRequestToState(event) async* {
    yield AuthLoading();
    user.phoneNumber = event.phoneNumber;
    print("PhoneNumber For Verification Is " + user.phoneNumber);

    QueryResult queryResult = await _authRequests
        .getShopAdminPhoneVerificationCodeRequest(event.phoneNumber)
        .timeout(
      Duration(seconds: 8),
      onTimeout: () {
        return QueryResult(source: null)
          ..exception = OperationException(
            graphqlErrors: [
              GraphQLError(message: 'request timeout'),
            ],
          );
      },
    );

    if (queryResult.hasException) {
      printYellowBg(queryResult);
      if (queryResult.exception.graphqlErrors.isNotEmpty) {
        yield FailResenVerficationCode(
          message: queryResult.exception.graphqlErrors,
          clientErrors: queryResult.exception.graphqlErrors?.first?.message,
        );
        print(
            'Error IS : ${queryResult.exception.graphqlErrors?.first?.message}');
      } else {
        yield FailResenVerficationCode(clientErrors: 'somthing went wrong');
      }

      print(queryResult.exception);
    }

    ///
    ///
    ///
    ///
    ///
    ///
    ///
    ///
    ///
    ///
    ///                   TEST EVENTS
    ///
    ///
    ///
    ///
    queryResult =
        await _authRequests.testingVerification(event.phoneNumber).timeout(
      Duration(seconds: 8),
      onTimeout: () {
        return QueryResult(source: null)
          ..exception = OperationException(
            graphqlErrors: [
              GraphQLError(message: 'request timeout'),
            ],
          );
      },
    );

    if (queryResult.hasException) {
      printYellowBg(queryResult);
      if (queryResult.exception.graphqlErrors.isNotEmpty) {
        yield SignUpErrors(
          message: queryResult.exception.graphqlErrors,
          clientErrors: queryResult.exception.graphqlErrors?.first?.message,
        );
        print(
            'Error IS : ${queryResult.exception.graphqlErrors?.first?.message}');
      } else {
        yield SignUpErrors(clientErrors: 'somthing went wrong');
      }

      print(queryResult.exception);
    }

    ///
    ///
    ///
    ///
    ///
    ///
    ///
    ///
    ///
    ///
    ///

    if (queryResult.data != null && !queryResult.hasException) {
      print(queryResult.data);
      String code = queryResult.data['getShopAdminPhoneVerificationCode']
          ['phoneVerificationCode'];
      yield ReSendingCodeVerification(code: code);
      print("Sending Code For Number : ${user.phoneNumber}");
    }
  }

  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
//  if (queryResult.data != null && !queryResult.hasException) {
//       print(queryResult.data);

//       yield ReSendingCodeVerification();
//       print("Sending Code For Number : ${user.phoneNumber}");
//     }

  ///
  ///
  ///
  ///
  ///
  ///
  ///

  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  Stream<AuthState> _mapCheckVerificationCodeToState(event) async* {
    yield AuthLoading();

    print('User Phone Has To Be ${user.phoneNumber}');

    QueryResult queryResult = await _authRequests
        .checkShopAdminPhoneVerificationCodeRequest(
      event.phoneNumber,
      event.phoneVerificationCode,
    )
        .timeout(
      Duration(seconds: 8),
      onTimeout: () {
        return QueryResult(source: null)
          ..exception = OperationException(
            graphqlErrors: [
              GraphQLError(message: 'request timeout'),
            ],
          );
      },
    );

    if (queryResult.hasException) {
      printYellowBg(queryResult);
      if (queryResult.exception.graphqlErrors.isNotEmpty) {
        yield CodeVerifyErrors(
          message: queryResult.exception.graphqlErrors,
          clientErrors: queryResult.exception.graphqlErrors?.first?.message,
        );
        print(
            'Error IS : ${queryResult.exception.graphqlErrors?.first?.message}');
      } else {
        yield CodeVerifyErrors(clientErrors: 'somthing went wrong');
      }

      print(queryResult.exception);
    }

    if (queryResult.data != null && !queryResult.hasException) {
      print(queryResult.data);

      yield ComplateInformation(
        phoneSignUpCode: queryResult.data['checkShopAdminPhoneVerificationCode']
            ['phoneSignUpCode'],
      );
    }
  }

  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  Stream<AuthState> _mapComplateInformationSignUpToState(event) async* {
    yield AuthLoading();

    print(user.phoneNumber);
    print(user.phoneSignUpCode);

    QueryResult queryResult = await _authRequests
        .shopAdminSignUpMutationRequest(
      event.phoneNumber,
      event.signUpVerificationCode,
      event.fullName,
      event.email,
      event.password,
    )
        .timeout(
      Duration(seconds: 8),
      onTimeout: () {
        return QueryResult(source: null)
          ..exception = OperationException(
            graphqlErrors: [
              GraphQLError(message: 'request timeout'),
            ],
          );
      },
    );

    if (queryResult.hasException) {
      printYellowBg(queryResult);
      if (queryResult.exception.graphqlErrors.isNotEmpty) {
        yield SignUpCompeletErrors(
          message: queryResult.exception.graphqlErrors,
          clientErrors: queryResult.exception.graphqlErrors?.first?.message,
        );
        print(
            'Error IS : ${queryResult.exception.graphqlErrors?.first?.message}');
      } else {
        yield SignUpCompeletErrors(clientErrors: 'somthing went wrong');
      }

      print(queryResult.exception);
    }

    if (queryResult.data != null && !queryResult.hasException) {
      print(queryResult.data);

      yield UserSignUpSuccess();
      // await SecureStorage.setId(queryResult.data['shopAdminSignUp']['_id']);
    }
  }
}
