import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';

abstract class AuthState extends Equatable {
  @override
  List<Object> get props => [];
}

class AuthInitial extends AuthState {}

class AuthLoading extends AuthState {}

class SendingCodeVerification extends AuthState {
  ///
  ///
  ///
  ///
  ///       secreect code we use for test
  //
  final String code;

  SendingCodeVerification({this.code});

  ///
  ///
  ///
  ///
}

class ReSendingCodeVerification extends AuthState {
  ///
  ///
  ///
  ///
  ///       secreect code we use for test
  final String code;

  ReSendingCodeVerification({this.code});

  ///
  ///
  ///
}

class RegisterUser extends AuthState {}

class ComplateInformation extends AuthState {
  final String phoneSignUpCode;

  ComplateInformation({this.phoneSignUpCode});
}

class UserSignUpSuccess extends AuthState {}

class UserLoginSuccess extends AuthState {}

class LoginErrors extends AuthState {
  final List<GraphQLError> message;
  final String clientErrors;

  LoginErrors({this.message, this.clientErrors});

  @override
  List<String> get props => message.map((e) => e.message).toList();
}

class SignUpErrors extends AuthState {
  final List<GraphQLError> message;
  final String clientErrors;

  SignUpErrors({this.message, this.clientErrors});

  @override
  List<String> get props => message.map((e) => e.message).toList();
}

class CodeVerifyErrors extends AuthState {
  final List<GraphQLError> message;
  final String clientErrors;

  CodeVerifyErrors({this.message, this.clientErrors});

  @override
  List<String> get props => message.map((e) => e.message).toList();
}

class SignUpCompeletErrors extends AuthState {
  final List<GraphQLError> message;
  final String clientErrors;

  SignUpCompeletErrors({this.message, this.clientErrors});

  @override
  List<String> get props => message.map((e) => e.message).toList();
}

class FailResenVerficationCode extends AuthState {
  final List<GraphQLError> message;
  final String clientErrors;

  FailResenVerficationCode({this.message, this.clientErrors});

  @override
  List<String> get props => message.map((e) => e.message).toList();
}
