import 'package:equatable/equatable.dart';

abstract class AuthEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class AuthBlocReset extends AuthEvent {}

class LoginEvents extends AuthEvent {
  final String emailOrPhoneNumber;
  final String password;
  final String fcm;

  LoginEvents({
    this.emailOrPhoneNumber,
    this.password,
    this.fcm,
  });
}

class SignUpEvents extends AuthEvent {
  final String phoneNumber;
  SignUpEvents({this.phoneNumber});
}

class CheckVerificationCode extends AuthEvent {
  final String phoneVerificationCode;
  final String phoneNumber;
  CheckVerificationCode({this.phoneVerificationCode, this.phoneNumber});
}

// class DeleteAccountEvent extends AuthEvent {}

class ComplateInformationSignUp extends AuthEvent {
  final String email;
  final String phoneNumber;
  final String signUpVerificationCode;
  final String fullName;
  final String password;

  ComplateInformationSignUp({
    this.email,
    this.fullName,
    this.password,
    this.signUpVerificationCode,
    this.phoneNumber,
  });
}

class ResendVerificationCodeRequest extends AuthEvent {
  final String phoneNumber;
  ResendVerificationCodeRequest({this.phoneNumber});
}
