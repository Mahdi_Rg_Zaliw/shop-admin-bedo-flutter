import 'package:equatable/equatable.dart';

abstract class ProductEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetProductsEvent extends ProductEvent {
  final Map pagination;
  GetProductsEvent({this.pagination});
}
