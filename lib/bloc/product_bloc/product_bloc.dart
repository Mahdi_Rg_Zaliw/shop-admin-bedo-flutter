import 'package:bedo_shop_admin/bloc/product_bloc/product_event.dart';
import 'package:bedo_shop_admin/bloc/product_bloc/product_state.dart';
import 'package:bedo_shop_admin/data/graphql/product/product_request.dart';
import 'package:bedo_shop_admin/model/product_model.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql/client.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  List<Product> product = [];
  ProductRequest productRequest;

  ProductBloc(ProductState initialState) : super(initialState) {
    productRequest = ProductRequest();
  }

  @override
  Stream<ProductState> mapEventToState(ProductEvent event) async* {
    print(event);
    if (event is GetProductsEvent) {
      yield ProductLoading();

      QueryResult queryResult = await productRequest
          .getProductsByShopAdmin(event.pagination)
          .timeout(Duration(seconds: 8), onTimeout: () {
        return QueryResult(source: null)
          ..exception = OperationException(
              graphqlErrors: [GraphQLError(message: 'request timeout')]);
      });

      print(queryResult.data);
      print(queryResult.hasException);
      print(queryResult.exception);

      if (queryResult.hasException) {
        yield ProductExceptionState(
          message: queryResult.exception.graphqlErrors,
          clientErrors: queryResult.exception.graphqlErrors[0].message,
        );

        print('Error IS : ${queryResult.exception.graphqlErrors[0].message}');
      }

      if (queryResult.data != null && !queryResult.hasException) {
        print(state);
        queryResult.data['getProductsByShopAdmin']
            .map(
              (element) => product.add(
                Product.fromJson(element),
              ),
            )
            .toList();
        yield ProductArrived(products: product);
      }
    }
  }
}
