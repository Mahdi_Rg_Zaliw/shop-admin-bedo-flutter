import 'package:bedo_shop_admin/model/product_model.dart';
import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';

abstract class ProductState extends Equatable {
  @override
  List<Object> get props => [];
}

class ProductInitial extends ProductState {}

class ProductLoading extends ProductState {}

class ProductArrived extends ProductState {
  final List<Product> products;

  ProductArrived({this.products});
}

class ProductExceptionState extends ProductState {
  final List<GraphQLError> message;
  final String clientErrors;

  ProductExceptionState({this.message, this.clientErrors});

  @override
  List<String> get props => message.map((e) => e.message).toList();
}
