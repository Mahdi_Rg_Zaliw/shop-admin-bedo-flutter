import 'dart:async';

import 'package:bedo_shop_admin/data/graphql/notifications/notifications_request.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';

part 'notifications_event.dart';
part 'notifications_state.dart';

class NotificationsBloc extends Bloc<NotificationsEvent, NotificationsState> {
  NotificationRequest _notificationRequest = NotificationRequest();

  QueryResult _queryResult;

  NotificationsBloc() : super(NotificationsInitial());

  @override
  Stream<NotificationsState> mapEventToState(
    NotificationsEvent event,
  ) async* {
    if (event is NotificationsVisited) {
      yield* _mapNotificationsVisitedToState();
    }
    if (event is NotificationsChanged) {
      yield* _mapNotificationsChangedToState(event.data);
    }
  }

  Stream<NotificationsState> _mapNotificationsChangedToState(dt) async* {
    try {
      _queryResult =
          await _notificationRequest.changeNotifications(variable: dt);
    } catch (e) {
      printRedBg('Try Notif Rqq Err '.padLeft(50, ' '));
      printRedBg('$e');
      yield NotificationResult(
          data: null,
          success: false,
          err: 'failed loading notifications status');
    }
    if (_queryResult.hasException) {
      printRedBg('Query Get Notif Result Error '.padLeft(50, ' '));
      printRedBg(_queryResult);
      yield NotificationResult(
          data: null, success: false, err: _queryResult.exception.toString());
    }
    if (!_queryResult.hasException && _queryResult.data != null) {
      final d = _queryResult.data['updateNotificationsStatus'];
      final data = {
        'isOrderNotificationActive': d['isOrderNotificationActive'],
        'isOrderStatusNotificationActive': d['isOrderStatusNotificationActive'],
        'isPaymentNotificationActive': d['isPaymentNotificationActive'],
        'isDeliveryNotificationActive': d['isDeliveryNotificationActive'],
        'isMessageNotificationActive': d['isMessageNotificationActive'],
      };
      printYellowBg(data);
      yield NotificationResult(data: data, success: true, err: null);
      this.add(NotificationsVisited());
      // return;
    }
  }

  Stream<NotificationsState> _mapNotificationsVisitedToState() async* {
    // yield TryingToLoadNotificationsStatus();
    try {
      _queryResult = await _notificationRequest.getUserSettings();
    } catch (e) {
      printRedBg('Try Notif Rqq Err '.padLeft(50, ' '));
      printRedBg('$e');
      yield NotificationResult(
          data: null,
          success: false,
          err: 'failed loading notifications status');
    }
    if (_queryResult.hasException) {
      printRedBg('Query Get Notif Result Error '.padLeft(50, ' '));
      printRedBg(_queryResult);
      yield NotificationResult(
          data: null, success: false, err: _queryResult.exception.toString());
    }
    if (!_queryResult.hasException && _queryResult.data != null) {
      printBlueBg('notif get res '.padLeft(50, ' '));

      final d = _queryResult.data['getNotificationsStatus'];
      final data = {
        'isOrderNotificationActive': d['isOrderNotificationActive'],
        'isOrderStatusNotificationActive': d['isOrderStatusNotificationActive'],
        'isPaymentNotificationActive': d['isPaymentNotificationActive'],
        'isDeliveryNotificationActive': d['isDeliveryNotificationActive'],
        'isMessageNotificationActive': d['isMessageNotificationActive'],
      };
      printBlueBg(' request sent from bloc on revisit');
      printBlueBg(data);
      yield NotificationResult(data: data, success: true, err: null);
    }
  }
}
