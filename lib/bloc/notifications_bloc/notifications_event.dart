part of 'notifications_bloc.dart';

abstract class NotificationsEvent extends Equatable {
  const NotificationsEvent();

  @override
  List<Object> get props => [];
}

class NotificationsVisited extends NotificationsEvent {}

class NotificationsChanged extends NotificationsEvent {
  final data;

  NotificationsChanged(this.data);
}
