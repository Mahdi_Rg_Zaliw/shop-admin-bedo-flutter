part of 'notifications_bloc.dart';

abstract class NotificationsState extends Equatable {
  const NotificationsState();

  @override
  List<Object> get props => [];
}

class NotificationsInitial extends NotificationsState {}

// class TryingToLoadNotificationsStatus extends NotificationsState {}

class NotificationResult extends NotificationsState {
  final Map data;
  final String err;
  final bool success;
  NotificationResult({
    this.data,
    this.success,
    this.err,
  });
}
