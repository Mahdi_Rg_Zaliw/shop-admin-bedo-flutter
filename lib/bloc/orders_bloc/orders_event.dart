import 'package:bedo_shop_admin/model/order.dart';
import 'package:equatable/equatable.dart';

abstract class OrdersEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class LoadCurrentOrders extends OrdersEvent {}

class LoadPendingOrders extends OrdersEvent {}

class AcceptOrder extends OrdersEvent {
  final AcceptOrderModel order;

  AcceptOrder(this.order);
}

class RejectOrder extends OrdersEvent {
  final Order order;

  RejectOrder(this.order);
}

class StartListenToOrders extends OrdersEvent {}

class StopListenToOrders extends OrdersEvent {}
