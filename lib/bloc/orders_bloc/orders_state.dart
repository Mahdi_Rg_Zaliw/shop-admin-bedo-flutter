import 'package:bedo_shop_admin/model/order.dart';
import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';

abstract class OrdersState extends Equatable{
  final List<Order> pendingOrders;
  final List<Order> currentOrders;
  final int totalOrdersCount;
  final double totalOrdersAmount;
  OrdersState(this.pendingOrders,this.currentOrders ,{this.totalOrdersCount, this.totalOrdersAmount});

  @override
  List<Object> get props => [pendingOrders, currentOrders, totalOrdersAmount, totalOrdersCount];
}

class OrdersInitial extends OrdersState {
  OrdersInitial(List<Order> pendingOrders, List<Order> currentOrders) : super(pendingOrders,currentOrders);


  @override
  String toString() {
    return "Initial";
  }
}

class OrdersLoaded extends OrdersState {

  OrdersLoaded(pendingOrders,currentOrders , {totalOrdersCount, totalOrdersAmount}):
  super(pendingOrders, currentOrders);

}

class OrdersLoading extends OrdersState {
  OrdersLoading(List<Order> pendingOrders, List<Order> currentOrders) : super(pendingOrders, currentOrders);

  @override
  String toString() => 'OrdersLoading';
}

class OrderLoadFailed extends OrdersState{
  final List<GraphQLError> errors;

  OrderLoadFailed([this.errors]) : super([],[]);

  @override
  String toString() => 'OrderLoadFailed';
}

class OrderAcceptSent extends OrdersState{
  OrderAcceptSent(List<Order> pendingOrders, List<Order> currentOrders) : super(pendingOrders, currentOrders);

  @override
  String toString() => 'OrderAcceptSent';
}

class OrderAcceptDone extends OrdersState{
  OrderAcceptDone(List<Order> pendingOrders, List<Order> currentOrders) : super(pendingOrders, currentOrders);

  @override
  String toString() => 'OrderAcceptDone';
}

class OrderAcceptFailed extends OrdersState{
  final List<GraphQLError> errors;

  OrderAcceptFailed([this.errors]) : super([],[]);

  @override
  String toString() => 'OrderAcceptFailed';
}

class OrderRejectSent extends OrdersState{
  OrderRejectSent(List<Order> pendingOrders, List<Order> currentOrders) : super(pendingOrders, currentOrders);

  @override
  String toString() => 'OrderRejectSent';
}

class OrderRejectDone extends OrdersState{
  OrderRejectDone(List<Order> pendingOrders, List<Order> currentOrders) : super(pendingOrders, currentOrders);

  @override
  String toString() => 'OrderRejectDone';
}

class OrderRejectFailed extends OrdersState{
  final List<GraphQLError> errors;

  OrderRejectFailed([this.errors]) : super([], []);

  @override
  String toString() => 'OrderRejectFailed';
}
