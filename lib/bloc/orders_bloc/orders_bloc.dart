import 'dart:async';

import 'package:bedo_shop_admin/bloc/orders_bloc/orders_event.dart';
import 'package:bedo_shop_admin/bloc/orders_bloc/orders_state.dart';
import 'package:bedo_shop_admin/data/graphql/orders/orders_request.dart';
import 'package:bedo_shop_admin/model/cart.dart';
import 'package:bedo_shop_admin/model/cart_product.dart';
import 'package:bedo_shop_admin/model/order.dart';
import 'package:bedo_shop_admin/model/product.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql/client.dart';

class OrdersBloc extends Bloc<OrdersEvent, OrdersState> {
  final request = OrderRequest();

  List<Order> pendingOrders = List<Order>();
  List<Order> currentOrders = List<Order>();
  int totalOrdersCount;
  double totalOrdersAmount;

  OrdersBloc() : super(OrdersInitial([], []));

  @override
  Stream<OrdersState> mapEventToState(OrdersEvent event) async* {
    if (event is LoadPendingOrders) {
      yield* _loadOrdersToState('PENDING');
    } else if (event is AcceptOrder) {
      yield* _acceptOrderToState(event.order);
    } else if (event is LoadCurrentOrders) {
      yield* _loadOrdersToState('ACCEPTED');
    } else if (event is RejectOrder) {
      yield* _rejectOrderToState(event.order);
    } else if (event is StartListenToOrders) {
      yield* _startListenToOrders('PENDING');
    } else if (event is StopListenToOrders) {
      yield* _stopListenToOrders('PENDING');
    }
  }

  Stream<OrdersState> _stopListenToOrders(String status) async* {
    _sub.cancel();
  }

  StreamSubscription<QueryResult> _sub;

  ///
  ///
  ///
  Stream<QueryResult> _subscription = OrderRequest().subscribeToOrders();

  ///
  ///
  ///
  Stream<OrdersState> _startListenToOrders(String status) async* {
    ///
    ///
    var ll = await _subscription.length;

    ///
    ///

    printYellowBg(ll);

    ///
    ///

    printColor(Colors.purple, 'starting to listen');
    _sub = _subscription.listen((result) {
      printGreenBg(result);
      if (result.hasException) {
        printPink(result.exception.toString());

        return;
      }

      if (result.isLoading) {
        printPink('awaiting results');
        return;
      }

      printPink('New data: ${result.data}');

      /// I am using artemis here to parse data. Highly recommended tool
    });
    // _sub.onData((d) {
    //   printPink(d.toString());
    // });
  }

  Stream<OrdersState> _loadOrdersToState(String status) async* {
    yield OrdersLoading(pendingOrders, currentOrders);
    final queryResults = await this.request.getOrders(status);
    await getTotalOrdersAmountToday();

    if (queryResults.hasException) {
      print(queryResults.exception.toString());
      yield OrderLoadFailed(queryResults.exception.graphqlErrors);
      return;
    }

    List<dynamic> rawOrders =
        queryResults.data['getOrdersByShopAdmin'] as List<dynamic>;
    List<Order> results = rawOrders.map((element) {
      final cart = element['cart'];
      final rawProducts = cart['products'] as List<dynamic>;
      final products = rawProducts
          .map((e) => CartProduct(
              Product(
                  id: e['product']['_id'],
                  title: e['product']['title'],
                  price: (e['product']['productDetails'][0]['price'] as int)
                      .toDouble()),
              e['quantity']))
          .toList();
      return Order(
          id: element['_id'],
          cart: Cart(
              id: cart['_id'] as String,
              shipmentCost: (cart['shipmentCost'] as int).toDouble(),
              products: products),
          trackID: (element['tracking']['trackId'] as String).toUpperCase(),
          estimatedDelivery:
              DateTime.parse(element['tracking']['estimatedDelivery']),
          userLocation: {
            'latitude': element['userLocation']['lat'] as double,
            'longitude': element['userLocation']['long'] as double,
          },
          status: Order.parseOrderStatus(element['status']));
    }).toList();
    if (status == 'PENDING') {
      pendingOrders = results;
      yield OrdersLoaded(pendingOrders, currentOrders,
          totalOrdersAmount: totalOrdersAmount,
          totalOrdersCount: totalOrdersCount);
    } else if (status == 'ACCEPTED') {
      currentOrders = results;
      yield OrdersLoaded(pendingOrders, currentOrders,
          totalOrdersAmount: totalOrdersAmount,
          totalOrdersCount: totalOrdersCount);
    }
    yield OrdersLoaded(pendingOrders, currentOrders,
        totalOrdersAmount: totalOrdersAmount,
        totalOrdersCount: totalOrdersCount);
  }

  Future<void> getTotalOrdersAmountToday() async {
    final from =
        DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
    final to = DateTime(
        DateTime.now().year, DateTime.now().month, DateTime.now().day + 1);

    final results = await this
        .request
        .getTotalOrdersAmount(from.toIso8601String(), to.toIso8601String());

    if (results.hasException) {
      totalOrdersAmount = 0;
      totalOrdersCount = 0;
      return;
    }

    totalOrdersAmount = double.parse(results
        .data['getShopTotalOrdersCountAndTotalAmountByShopAdmin']
            ['totalOrderAmount']
        .toString());
    totalOrdersCount =
        results.data['getShopTotalOrdersCountAndTotalAmountByShopAdmin']
            ['totalOrdersCount'] as int;
  }

  Stream<OrdersState> _acceptOrderToState(AcceptOrderModel order) async* {
    yield OrderAcceptSent(pendingOrders, currentOrders);
    final queryResults = await this.request.acceptOrder(order);

    if (queryResults.hasException) {
      print(queryResults.exception.toString());
      yield OrderAcceptFailed(queryResults.exception.graphqlErrors);
      return;
    }
    yield OrderAcceptDone(pendingOrders, currentOrders);
    yield* _loadOrdersToState('PENDING');
  }

  Stream<OrdersState> _rejectOrderToState(Order order) async* {
    yield OrderRejectSent(pendingOrders, currentOrders);
    final queryResults = await this.request.rejectOrder(order.id);
    if (queryResults.hasException) {
      print(queryResults.exception.toString());
      yield OrderRejectFailed(queryResults.exception.graphqlErrors);
      return;
    }

    yield OrderRejectDone(pendingOrders, currentOrders);
    yield* _loadOrdersToState('PENDING');
  }
}

class JobsSubscriptionCubit extends Cubit<String> {
  JobsSubscriptionCubit(this._orderRequests) : super(null);

  final OrderRequest _orderRequests;

  subscribe() {
    Stream<QueryResult> _subscription = _orderRequests.subscribeToOrders();

    _subscription.listen((result) {
      if (result.hasException) {
        print(result.exception.toString());

        /// You can emit error here
        return;
      }

      if (result.isLoading) {
        print('awaiting results');
        return;
      }

      print('New Review: ${result.data}');

      /// I am using artemis here to parse data. Highly recommended tool
      // final data = NewJobs$Subscription.fromJson(result.data).newJobs;

      // emit(data);
    });
  }
}
