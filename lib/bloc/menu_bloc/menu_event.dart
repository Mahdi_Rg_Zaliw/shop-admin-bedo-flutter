import 'package:equatable/equatable.dart';

abstract class MenuEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetMenuEvent extends MenuEvent {}

class CreateMenuEvent extends MenuEvent {
  final Map<String, dynamic> data;
  CreateMenuEvent({this.data});
}

class UpdateMenuEvent extends MenuEvent {
  final Map<String, dynamic> data;
  final String id;
  UpdateMenuEvent({this.data, this.id});
}
