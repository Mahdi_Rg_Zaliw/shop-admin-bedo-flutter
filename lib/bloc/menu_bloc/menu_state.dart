import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';
import 'package:bedo_shop_admin/model/menu_model.dart';

abstract class MenuState extends Equatable {
  @override
  List<Object> get props => [];
}

class MenuInitial extends MenuState {}

class MenuLoading extends MenuState {}

class MenuExceptionState extends MenuState {
  final List<GraphQLError> message;
  final String clientErrors;

  MenuExceptionState({this.message, this.clientErrors});

  @override
  List<String> get props => message.map((e) => e.message).toList();
}

class MenuArrived extends MenuState {
  final List menu;
  final String updateID;

  MenuArrived({this.menu, this.updateID});
}

class ShopMenuCreatedSuccess extends MenuState {}

class ShopMenuUpdatedSuccess extends MenuState {}

class ShopMenuEmpty extends MenuState {}
