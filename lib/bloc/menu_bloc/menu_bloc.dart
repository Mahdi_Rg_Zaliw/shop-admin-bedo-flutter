import 'package:bedo_shop_admin/bloc/menu_bloc/menu_event.dart';
import 'package:bedo_shop_admin/bloc/menu_bloc/menu_state.dart';
import 'package:bedo_shop_admin/data/graphql/menu/menu_request.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql/client.dart';
import 'package:bedo_shop_admin/model/menu_model.dart';
import 'package:bedo_shop_admin/utils/logs.dart';

class MenuBloc extends Bloc<MenuEvent, MenuState> {
  List<Menu> menu = [];
  MenuRequest menuRequest;

  MenuBloc(MenuState initialState) : super(initialState) {
    menuRequest = MenuRequest();
  }

  @override
  Stream<MenuState> mapEventToState(MenuEvent event) async* {
    print(event);
    if (event is GetMenuEvent) {
      yield MenuLoading();

      QueryResult queryResult =
          await menuRequest.getShopByShopAdminRequest().timeout(
        Duration(seconds: 8),
        onTimeout: () {
          return QueryResult(source: null)
            ..exception = OperationException(
              graphqlErrors: [
                GraphQLError(message: 'request timeout'),
              ],
            );
        },
      );

      print(queryResult.data);

      if (queryResult.hasException) {
        if (queryResult.exception.graphqlErrors.isNotEmpty) {
          yield MenuExceptionState(
            message: queryResult.exception.graphqlErrors,
            clientErrors: queryResult.exception.graphqlErrors[0].message,
          );

          print('Error IS : ${queryResult.exception.graphqlErrors[0].message}');
        } else {
          yield MenuExceptionState(clientErrors: 'something went wrong');
        }
      }

      if (queryResult.data != null && !queryResult.hasException) {
        if (queryResult.data['getShopByShopAdmin']['shopMenu'] == null) {
          print(queryResult.data);

          yield ShopMenuEmpty();
          // Create ShopMenu
        } else {
          final menuDataResualt =
              queryResult.data['getShopByShopAdmin']['shopMenu']['subMenus']
                  .map(
                    (element) => Menu.fromJson(element),
                  )
                  .toList();
          yield MenuArrived(
            menu: menuDataResualt,
            updateID: queryResult.data['getShopByShopAdmin']['shopMenu']['_id'],
          );
          print(state);
        }
      }
    } else if (event is CreateMenuEvent) {
      yield MenuLoading();

      QueryResult queryResult = await menuRequest
          .createShopMenuByShopAdminRequest(event.data)
          .timeout(
        Duration(seconds: 8),
        onTimeout: () {
          return QueryResult(source: null)
            ..exception = OperationException(
              graphqlErrors: [
                GraphQLError(message: 'request timeout'),
              ],
            );
        },
      );

      if (queryResult.hasException) {
        yield MenuExceptionState(
          message: queryResult.exception.graphqlErrors,
          clientErrors: queryResult.exception.graphqlErrors[0].message,
        );
        print('Error IS : ${queryResult.exception.graphqlErrors[0].message}');
      }

      if (queryResult.data != null && !queryResult.hasException) {
        yield ShopMenuCreatedSuccess();
      }
    } else if (event is UpdateMenuEvent) {
      yield MenuLoading();

      QueryResult queryResult = await menuRequest
          .updateShopMenuByShopAdminRequest(event.id, event.data)
          .timeout(
        Duration(seconds: 8),
        onTimeout: () {
          return QueryResult(source: null)
            ..exception = OperationException(
              graphqlErrors: [
                GraphQLError(message: 'request timeout'),
              ],
            );
        },
      );

      if (queryResult.hasException) {
        yield MenuExceptionState(
          message: queryResult.exception.graphqlErrors,
          clientErrors: queryResult.exception.graphqlErrors[0].message,
        );
        print('Error IS : ${queryResult.exception.graphqlErrors[0].message}');
      }

      if (queryResult.data != null && !queryResult.hasException) {
        yield ShopMenuUpdatedSuccess();
      }
    }
  }
}
