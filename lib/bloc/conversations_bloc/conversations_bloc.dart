import 'dart:async';
import 'dart:io';
import 'package:bedo_shop_admin/config/config.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import 'package:bedo_shop_admin/bloc/conversations_bloc/conversations_events.dart';
import 'package:bedo_shop_admin/bloc/conversations_bloc/conversations_state.dart';
import 'package:bedo_shop_admin/data/graphql/conversations/conversations_request.dart';
import 'package:bedo_shop_admin/model/chat_user.dart';
import 'package:bedo_shop_admin/model/conversation_model.dart';
import 'package:bedo_shop_admin/model/message_model.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:http_parser/http_parser.dart';
import 'package:open_file/open_file.dart';

class ConversationBloc extends Bloc<ConversationsEvent, ConversationsState> {
  final request = ConversationsRequest();

  List<Conversation> conversations;
  Set<Message> sendingMessages = Set.identity();
  Set<Message> failedMessages = Set.identity();

  ConversationBloc() : super(ConversationsInitial());

  @override
  Stream<ConversationsState> mapEventToState(ConversationsEvent event) async* {
    if (event is LoadConversations) {
      yield* loadConversationsToState();
    } else if (event is LoadConversation) {
      yield* getConversation(event.id);
    } else if (event is SendMessageEvent) {
      yield* sendMessage(event.message, event.conversationID);
    } else if (event is UploadFileEvent) {
      yield* uploadFile(event.file, event.conversationID);
    } else if (event is DownloadFileEvent) {
      yield* downloadFile(event.url, event.conversationID, event.fileName);
    } else if (event is OpenFileEvent) {
      yield* openFile(event.filePath);
    }
  }

  ChatUser getUser(var conversation) {
    if (conversation['driver'] != null && conversation['user'] == null) {
      return ChatUser(
          ID: conversation['driver']['_id'],
          name: conversation['driver']['fullName'],
          userType: UserType.Driver);
    } else if (conversation['driver'] == null && conversation['user'] != null) {
      return ChatUser(
          ID: conversation['user']['_id'],
          name: conversation['user']['fullName'],
          userType: UserType.User);
    }
  }

  String getFileNameFromUrl(String url) {
    return url.substring(
      url.lastIndexOf('/') + 1,
    );
  }

  String getNameFromUrl(String url) {
    String fileName = getFileNameFromUrl(url);
    String fileExtension = fileName.substring(fileName.lastIndexOf('.'));
    String name = fileName.substring(
        0, fileName.substring(0, fileName.lastIndexOf('-')).lastIndexOf('-'));
    return name + fileExtension;
  }

  Stream<ConversationsState> loadConversationsToState() async* {
    try {
      yield ConversationsLoading(
          conversations, sendingMessages, failedMessages);
      var queryResults = await this.request.getConversations();

      if (queryResults.hasException) {
        yield ConversationsLoadFailed(
            conversations, sendingMessages, failedMessages);
        return;
      }

      var rawResults = queryResults.data['getConversations'] as List<dynamic>;
      List<Conversation> results = rawResults.map((element) {
        var messageType = element['lastMessage']['messageType'] == 'Text'
            ? MessageType.Text
            : MessageType.Upload;

        var fileName;
        var fileNameToShow;

        if (messageType == MessageType.Upload) {
          fileName = getFileNameFromUrl(element['lastMessage']['text']);
          fileNameToShow = getNameFromUrl(element['lastMessage']['text']);
        }
        var lastMessage = Message(
          id: element['lastMessage']['_id'],
          text: element['lastMessage']['text'],
          time: DateTime.parse(element['lastMessage']['createdAt']),
          type: messageType,
          senderType: element['lastMessage']['senderType'] == 'Driver'
              ? MessageSenderType.Driver
              : element['lastMessage']['senderType'] == 'User'
                  ? MessageSenderType.User
                  : MessageSenderType.Admin,
          fileName: fileName,
          fileNameToShow: fileNameToShow,
        );

        var user = getUser(element);
        return Conversation(
          lastMessage: lastMessage,
          user: user,
          messages: [lastMessage],
          title: element['title'],
          ID: element['_id'],
          repliedByAdmin: element['repliedByAdmin'],
          updatedAt: DateTime.parse(
            element['updatedAt'],
          ),
        );
      }).toList();

      conversations = results;
      yield ConversationsLoaded(conversations, sendingMessages, failedMessages);
    } catch (e) {
      print(e);
      yield ConversationsLoadFailed(
          conversations, sendingMessages, failedMessages);
    }
  }

  Stream<ConversationsState> getConversation(String id) async* {
    try {
      yield ConversationsLoading(
          conversations, sendingMessages, failedMessages);
      var queryResults = await this.request.getConversation(id);

      if (queryResults.hasException) {
        print(queryResults.exception);
        yield ConversationsLoadFailed(
            conversations, sendingMessages, failedMessages);
        return;
      }

      var rawMessages =
          queryResults.data['getConversation']['messages'] as List<dynamic>;
      List<Message> messages = rawMessages.map((message) {
        var messageType = message['messageType'] == 'Text'
            ? MessageType.Text
            : MessageType.Upload;

        var fileName;
        var fileNameToShow;

        if (messageType == MessageType.Upload) {
          fileName = getFileNameFromUrl(message['text']);
          fileNameToShow = getNameFromUrl(message['text']);
        }
        return Message(
          id: message['_id'],
          text: message['text'],
          time: DateTime.parse(message['createdAt']),
          type: messageType,
          senderType: message['senderType'] == 'Driver'
              ? MessageSenderType.Driver
              : message['senderType'] == 'User'
                  ? MessageSenderType.User
                  : MessageSenderType.Admin,
          fileName: fileName,
          fileNameToShow: fileNameToShow,
        );
      }).toList();

      conversations
          .firstWhere((element) => element.ID == id)
          .setMessages(messages);
      yield ConversationsLoaded(conversations, sendingMessages, failedMessages);
    } catch (e) {
      print(e);
      yield ConversationsLoadFailed(
          conversations, sendingMessages, failedMessages);
    }
  }

  Stream<ConversationsState> sendMessage(
      Message message, String conversationID) async* {
    try {
      print('conv ID : $conversationID ');
      var results = await this
          .request
          .sendMessage(conversationID, message.type, message.text);
      // sendingMessages.add(message);
      yield ConversationsLoading(
          conversations, sendingMessages, failedMessages);

      if (results.hasException) {
        // sendingMessages.remove(message);
        // failedMessages.add(message);
        print(results.exception);
        yield ConversationsLoaded(
            conversations, sendingMessages, failedMessages);
        return;
      }

      var rawSentMessage = results.data['sendMessage'];

      var messageType = rawSentMessage['messageType'] == 'Text'
          ? MessageType.Text
          : MessageType.Upload;
      var fileName;
      var fileNameToShow;

      if (messageType == MessageType.Upload) {
        fileName = getFileNameFromUrl(rawSentMessage['text']);
        fileNameToShow = getNameFromUrl(rawSentMessage['text']);
      }
      var sentMessage = Message(
        id: rawSentMessage['_id'],
        text: rawSentMessage['text'],
        time: DateTime.parse(rawSentMessage['createdAt']),
        type: rawSentMessage['messageType'] == 'Text'
            ? MessageType.Text
            : MessageType.Upload,
        senderType: rawSentMessage['senderType'] == 'Driver'
            ? MessageSenderType.Driver
            : rawSentMessage['senderType'] == 'User'
                ? MessageSenderType.User
                : MessageSenderType.Admin,
        fileName: fileName,
        fileNameToShow: fileNameToShow,
      );
      conversations
          .firstWhere((element) => element.ID == conversationID)
          .messages
          .insert(0, sentMessage);
      yield ConversationsLoaded(conversations, sendingMessages, failedMessages);
    } catch (e) {
      print(e);
      yield ConversationsLoadFailed(
          conversations, sendingMessages, failedMessages);
    }
  }

  Stream<ConversationsState> uploadFile(
      FilePickerResult filePickerResult, String conversationID) async* {
    try {
      File file = File(filePickerResult.files.single.path);
      var bytes = file.readAsBytesSync();
      var fileData = bytes.buffer.asUint8List();
      var fileType = filePickerResult.files.first.extension == 'pdf'
          ? 'application'
          : 'image';
      var multiPartFile = http.MultipartFile.fromBytes(
        "pickedFile",
        fileData,
        filename: filePickerResult.files.first.name,
        contentType:
            MediaType(fileType, filePickerResult.files.first.extension),
      );
      var url = await this.request.uploadFile(multiPartFile, conversationID);

      if (url.hasException) {
        print(url.exception);
        return;
      }

      yield* sendMessage(
          Message(
              id: null,
              text: url.data['uploadFile']['url'],
              time: DateTime.now(),
              type: MessageType.Upload,
              senderType: MessageSenderType.Admin),
          conversationID);
    } catch (e) {
      print(e);
    }
  }

  Stream<ConversationsState> downloadFile(
      String url, String conversationID, String fileName) async* {
    try {
      var httpClient = HttpClient();
      var request = await httpClient
          .getUrl(Uri.parse(uri.replaceAll('graphql', '') + url));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);
      File file = new File(fileName);
      await file.writeAsBytes(bytes);

      yield ConversationsLoaded(conversations, sendingMessages, failedMessages);
    } catch (e) {
      print(e);
    }
  }

  openFile(String filePath) async {
    try {
      await OpenFile.open(filePath);
    } catch (e) {
      print(e);
    }
  }
}
