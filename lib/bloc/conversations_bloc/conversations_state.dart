import 'package:bedo_shop_admin/model/conversation_model.dart';
import 'package:bedo_shop_admin/model/message_model.dart';
import 'package:equatable/equatable.dart';

abstract class ConversationsState extends Equatable {
  final List<Conversation> conversations;
  final Set<Message> sendingMessages;
  final Set<Message> failedMessages;
  final Set<Message> downloadingMessages;

  ConversationsState(
      [this.conversations,
      this.sendingMessages,
      this.failedMessages,
      this.downloadingMessages]);

  List<Conversation> get repliedConversations {
    return conversations
        .where((element) => element.repliedByAdmin == true)
        .toList();
  }

  List<Conversation> get notRepliedConversations {
    return conversations
        .where((element) => element.repliedByAdmin == false)
        .toList();
  }

  List<Object> get props => [conversations, sendingMessages, failedMessages];
}

class ConversationsInitial extends ConversationsState {}

class ConversationsLoading extends ConversationsState {
  ConversationsLoading(
      List<Conversation> conversations, sendingMessages, failedMessages)
      : super(conversations, sendingMessages, failedMessages);
}

class ConversationsLoaded extends ConversationsState {
  ConversationsLoaded(
      List<Conversation> conversations, sendingMessages, failedMessages)
      : super(conversations, sendingMessages, failedMessages);
}

class ConversationsLoadFailed extends ConversationsState {
  ConversationsLoadFailed(
      List<Conversation> conversations, sendingMessages, failedMessages)
      : super(conversations, sendingMessages, failedMessages);
}

class DownloadingFileState extends ConversationsState {
  DownloadingFileState(List<Conversation> conversations, sendingMessages,
      failedMessages, downloadingMessages)
      : super(conversations, sendingMessages, failedMessages,
            downloadingMessages);
}
