import 'package:bedo_shop_admin/model/message_model.dart';
import 'package:file_picker/file_picker.dart';

abstract class ConversationsEvent {}

class LoadConversations extends ConversationsEvent {}

class LoadConversation extends ConversationsEvent {
  final String id;

  LoadConversation(this.id);
}

class SendMessageEvent extends ConversationsEvent {
  final Message message;
  final String conversationID;

  SendMessageEvent(this.message, this.conversationID);
}

class UploadFileEvent extends ConversationsEvent {
  final FilePickerResult file;
  final String conversationID;

  UploadFileEvent(
    this.file,
    this.conversationID,
  );
}

class DownloadFileEvent extends ConversationsEvent {
  final String conversationID;
  final String url;
  final String fileName;

  DownloadFileEvent(this.url, this.conversationID, this.fileName);
}

class OpenFileEvent extends ConversationsEvent {
  final String filePath;

  OpenFileEvent(this.filePath);
}
