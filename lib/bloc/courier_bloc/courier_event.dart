import 'package:equatable/equatable.dart';

abstract class CourierEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetCourierList extends CourierEvent {}

class CreateCourierEvent extends CourierEvent {
  final Map<String, dynamic> data;

  CreateCourierEvent({this.data});
}

class UpdateCourierEvent extends CourierEvent {
  final String id;
  final Map<String, dynamic> updateCourierData;

  UpdateCourierEvent({this.id, this.updateCourierData});
}
