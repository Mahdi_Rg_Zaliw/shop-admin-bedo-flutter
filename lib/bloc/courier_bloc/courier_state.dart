import 'package:bedo_shop_admin/model/courier_list_model.dart';
import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';

abstract class CourierState extends Equatable {
  @override
  List<Object> get props => [];
}

class CourierInitial extends CourierState {}

class CourierLoading extends CourierState {}

class CourierListArrived extends CourierState {
  final List courierListData;

  CourierListArrived({this.courierListData});
}

class CourierCreatedSuccess extends CourierState {}

class UpdateCourierSuccess extends CourierState {}

class CourierExceptionState extends CourierState {
  final List<GraphQLError> message;
  final String clientErrors;

  CourierExceptionState({this.message, this.clientErrors});

  @override
  List<String> get props => message.map((e) => e.message).toList();
}
