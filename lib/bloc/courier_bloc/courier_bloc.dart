import 'package:bedo_shop_admin/bloc/courier_bloc/courier_event.dart';
import 'package:bedo_shop_admin/bloc/courier_bloc/courier_state.dart';
import 'package:bedo_shop_admin/data/graphql/courier/courier_request.dart';
import 'package:bedo_shop_admin/model/courier_list_model.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql/client.dart';

class CourierBloc extends Bloc<CourierEvent, CourierState> {
  CourierRequest courierRequest;
  List<CourierList> courierListModel = [];

  CourierBloc(CourierState initialState) : super(initialState) {
    courierRequest = CourierRequest();
  }

  @override
  Stream<CourierState> mapEventToState(CourierEvent event) async* {
    print(event);
    if (event is GetCourierList) {
      yield CourierLoading();

      QueryResult queryResult =
          await courierRequest.getDriversByShopAdminRequest().timeout(
        Duration(seconds: 8),
        onTimeout: () {
          return QueryResult(source: null)
            ..exception = OperationException(
              graphqlErrors: [
                GraphQLError(message: 'request timeout'),
              ],
            );
        },
      );

      printOrange(queryResult.data);
      print(queryResult.exception);

      if (queryResult.hasException) {
        if (queryResult.exception.graphqlErrors.isNotEmpty) {
          yield CourierExceptionState(
            message: queryResult.exception.graphqlErrors,
            clientErrors: queryResult.exception.graphqlErrors[0].message,
          );
          print('Error IS : ${queryResult.exception.graphqlErrors[0].message}');
          print(queryResult.exception);
        } else {
          CourierExceptionState(clientErrors: 'something went wrong');
        }
      }

      if (queryResult.data != null && !queryResult.hasException) {
        final mdd = queryResult.data['getDriversByShopAdmin']
            .map(
              (element) => CourierList.fromJson(element),
            )
            .toList();
        yield CourierListArrived(
          courierListData: mdd,
        );
      }
    } else if (event is CreateCourierEvent) {
      yield CourierLoading();

      QueryResult queryResult = await courierRequest
          .createDriverByShopAdminRequest(event.data)
          .timeout(
        Duration(seconds: 8),
        onTimeout: () {
          return QueryResult(source: null)
            ..exception = OperationException(
              graphqlErrors: [
                GraphQLError(message: 'request timeout'),
              ],
            );
        },
      );

      printOrange(queryResult.data);
      print(queryResult.exception);

      if (queryResult.hasException) {
        yield CourierExceptionState(
          message: queryResult.exception.graphqlErrors,
          clientErrors: queryResult.exception.graphqlErrors[0].message,
        );
      }

      if (queryResult.data != null && !queryResult.hasException) {
        yield CourierCreatedSuccess();
        this.add(GetCourierList());
      }
    } else if (event is UpdateCourierEvent) {
      yield CourierLoading();

      QueryResult queryResult = await courierRequest
          .updateDriverInfoByShopAdminRequest(event.id, event.updateCourierData)
          .timeout(
        Duration(seconds: 8),
        onTimeout: () {
          return QueryResult(source: null)
            ..exception = OperationException(
              graphqlErrors: [
                GraphQLError(message: 'request timeout'),
              ],
            );
        },
      );

      print(queryResult.data);
      print(queryResult.exception);

      if (queryResult.hasException) {
        yield CourierExceptionState(
          message: queryResult.exception.graphqlErrors,
          clientErrors: queryResult.exception.graphqlErrors[0].message,
        );
      }

      if (queryResult.data != null && !queryResult.hasException) {
        yield UpdateCourierSuccess();
        this.add(GetCourierList());
      }
    }
  }
}
