import 'dart:async';
import 'dart:convert';
import 'package:bedo_shop_admin/data/secure_storage/secure_storage.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/rendering.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:meta/meta.dart';

part 'lang_event.dart';
part 'lang_state.dart';

class LangBloc extends Bloc<LangEvent, LangState> with HydratedMixin {
  LangBloc() : super(Lang('en'));

  @override
  Stream<LangState> mapEventToState(
    LangEvent event,
  ) async* {
    if (event is LnagChange) {
      yield Lang(event.code);
    }
  }

  @override
  LangState fromJson(Map<String, dynamic> json) {
    return Lang.fromMap(json);
  }

  @override
  Map<String, dynamic> toJson(LangState state) {
    return state.toMap();
  }
}

extension FullName on LangState {
  String get langueNameInFull {
    String l;
    switch (languageCode) {
      case 'en':
        l = 'English';
        break;
      case 'ru':
        l = 'русский';
        break;
      case 'az':
        l = 'Azerbayjani';
        break;
      default:
        l = '';
    }
    return l;
  }
}
