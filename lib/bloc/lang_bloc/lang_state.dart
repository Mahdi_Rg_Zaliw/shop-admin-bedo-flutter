part of 'lang_bloc.dart';

@immutable
abstract class LangState {
  String get languageCode;
  Map<String, dynamic> toMap();
  LangState();
  String toJson();
  LangState.fromJson(String source);
  LangState.fromMap(Map<String, dynamic> map);
}

class Lang extends LangState {
  final String languageCode;

  Lang(this.languageCode);

  Map<String, dynamic> toMap() {
    return {
      'languageCode': languageCode,
    };
  }

  factory Lang.fromMap(Map<String, dynamic> map) {
    return Lang(
      map['languageCode'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Lang.fromJson(String source) => Lang.fromMap(json.decode(source));
}
