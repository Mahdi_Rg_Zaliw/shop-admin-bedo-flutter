part of 'lang_bloc.dart';

@immutable
abstract class LangEvent {
  Locale get locale;
}

class LnagChange extends LangEvent {
  final String code;
  Locale get locale => Locale.fromSubtags(languageCode: code);

  LnagChange(this.code);
}
