import 'package:bedo_shop_admin/models/discount.dart';
import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';
import 'package:meta/meta.dart';

@immutable
abstract class DiscountsState extends Equatable {
  DiscountsState([List props = const []]);

  List<Object> get props => [props];
}

class DiscountsInitial extends DiscountsState {
  @override
  String toString() => 'DiscountsInitial';
}

class DiscountsLoading extends DiscountsState {
  @override
  String toString() => 'DiscountsLoading';
}

class DiscountsLoaded extends DiscountsState {
  final List<Discount> results;

  DiscountsLoaded({@required this.results})
      : assert(results != null),
        super([results]);

  List<Object> get props => results;

  @override
  String toString() => 'DiscountsLoaded: { Discounts: $results }';
}

class DiscountsNotLoaded extends DiscountsState {
  final List<GraphQLError> errors;

  DiscountsNotLoaded([this.errors]) : super([errors]);

  @override
  String toString() => 'DiscountsNotLoaded';
}

class CreateDiscountFailed extends DiscountsState {
  final List<GraphQLError> errors;

  CreateDiscountFailed([this.errors]) : super([errors]);

  @override
  String toString() => 'CreateDiscountFailed';
}

class DiscountRequestSent extends DiscountsState {
  DiscountRequestSent();

  @override
  String toString() => 'DiscountRequestSent';
}

class DeleteDiscountFailed extends DiscountsState {
  final List<GraphQLError> errors;

  DeleteDiscountFailed([this.errors]) : super([errors]);

  @override
  String toString() => 'CreateDiscountFailed';
}

class DeleteDiscountDone extends DiscountsState {
  final Discount discount;

  DeleteDiscountDone([this.discount]) : super([discount]);

  @override
  String toString() => 'DeleteDiscountDone';
}
