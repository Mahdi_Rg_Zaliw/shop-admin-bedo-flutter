import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:bedo_shop_admin/models/discount.dart';

@immutable
abstract class DiscountsEvent extends Equatable {
  DiscountsEvent([List props = const []]);

  List<Object> get props => [props];
}

class LoadDiscounts extends DiscountsEvent {
  @override
  String toString() => 'LoadDiscounts';
}

class CreateDiscount extends DiscountsEvent {
  final Discount discount;

  CreateDiscount(this.discount);

  @override
  String toString() => 'CreateDiscounts';
}

class MutateDiscount extends DiscountsEvent {
  final Discount discount;

  MutateDiscount({this.discount}) : super([discount]);

  @override
  String toString() => 'MutateDiscounts';
}

class RefreshDiscounts extends DiscountsEvent {
  @override
  String toString() => 'RefreshDiscounts';
}

class ExpireDiscount extends DiscountsEvent {
  final Discount discount;

  ExpireDiscount(this.discount);

  @override
  String toString() => 'DeleteDiscounts';
}
