import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql/client.dart';

import 'package:bedo_shop_admin/data/graphql/discounts/discount_request.dart';
import 'package:bedo_shop_admin/models/discount.dart';
import 'discount_events.dart';
import 'discount_states.dart';

class DiscountBloc extends Bloc<DiscountsEvent, DiscountsState> {
  final request = DiscountRequest();

  List<Discount> discounts;

  DiscountBloc() : super(DiscountsInitial());

  @override
  Stream<DiscountsState> mapEventToState(DiscountsEvent event) async* {
    try {
      if (event is LoadDiscounts) {
        yield* _mapDiscountsToState();
      } else if (event is CreateDiscount) {
        yield* _createDiscountEventToState(event.discount);
      } else if (event is ExpireDiscount) {
        yield* _expireDiscount(event.discount);
      } else if (event is MutateDiscount) {
        yield* _mutateDiscount(event.discount);
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield state;
    }
  }

  Stream<DiscountsState> _mapDiscountsToState() async* {
    try {
      yield DiscountsLoading();
      var queryResults = await this.request.getDiscountsRequest();

      if (queryResults.hasException) {
        yield DiscountsNotLoaded(queryResults.exception.graphqlErrors);
        return;
      }

      List<dynamic> rawDiscounts =
          queryResults.data["getOrderPromotionsByShopAdmin"] as List<dynamic>;
      DateTime biasDate = DateTime(
          DateTime.now().year,
          DateTime.now().month,
          DateTime.now().day,
          DateTime.now().hour,
          DateTime.now().minute,
          DateTime.now().second + 10);
      List<Discount> results = rawDiscounts
          .map((dynamic e) => Discount(
              id: e['_id'] as String,
              startDate: DateTime.parse(e['from']),
              endDate: DateTime.parse(e['to']),
              percent: double.parse(e['percent'].toString()),
              promotionCode: e['promotionCode'] as String,
              isExpired: DateTime.parse(e['to']).isBefore(biasDate)))
          .toList();
      discounts = results;
      yield DiscountsLoaded(results: discounts);
    } catch (error) {
      print(error);
      yield DiscountsNotLoaded(
          [GraphQLError(message: "An error has occurred")]);
    }
  }

  Stream<DiscountsState> _createDiscountEventToState(Discount discount) async* {
    try {
      yield DiscountRequestSent();
      final queryResults = await this.request.createDiscount(discount);

      if (queryResults.hasException) {
        yield CreateDiscountFailed(queryResults.exception.graphqlErrors);
        return;
      }

      yield* _mapDiscountsToState();
    } catch (error) {
      yield CreateDiscountFailed(error);
    }
  }

  Stream<DiscountsState> _expireDiscount(Discount discount) async* {
    try {
      Discount expiringDiscount = Discount(
          id: discount.id,
          startDate: discount.startDate,
          endDate: DateTime.now(),
          percent: discount.percent,
          promotionCode: discount.promotionCode,
          isExpired: true);
      final queryResults = await this.request.expireDiscount(expiringDiscount);

      if (queryResults.hasException) {
        yield DeleteDiscountFailed(queryResults.exception.graphqlErrors);
        return;
      }

      yield DeleteDiscountDone(expiringDiscount);
    } catch (error) {
      yield DeleteDiscountFailed(error);
    }
  }

  Stream<DiscountsState> _mutateDiscount(Discount discount) async* {
    try {
      yield DiscountRequestSent();
      final queryResult = await this.request.updateDiscount(discount);

      if (queryResult.hasException) {
        yield DeleteDiscountFailed(queryResult.exception.graphqlErrors);
        return;
      }

      yield* _mapDiscountsToState();
    } catch (error) {
      yield DeleteDiscountFailed();
    }
  }
}
