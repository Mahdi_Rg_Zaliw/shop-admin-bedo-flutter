const String accessTokenKey = 'ACCESS_TOKEN_KEY';
const String refreshTokenKey = 'REFRESH_TOKEN_KEY';
const String userIdKey = 'USER_ID_KEY';
const String savedLocale = 'USER_SAVED_LOCALE';
const String fcmTokenKey = 'FCM_TOKEN_KEY';
