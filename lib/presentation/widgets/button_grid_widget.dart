import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:flutter/material.dart';

class ButtonGrid extends StatefulWidget {
  final String title;
  final int crossAxisCount;
  final List<String> options;
  final Function onChanged;

  ButtonGrid(
      {@required this.title, this.crossAxisCount = 3, @required this.options, @required this.onChanged});

  @override
  _ButtonGridState createState() => _ButtonGridState();
}

class _ButtonGridState extends State<ButtonGrid> {
  int _selectedIndex = 0;

  Widget gridButton(text, index) {
    bool isSelected = index == _selectedIndex;
    return GestureDetector(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          border: Border.all(
              color: isSelected ? Theme.of(context).primaryColor : lightGreyBg,
              width: 1),
        ),
        child: Center(
          child: Text(
            text,
            style: isSelected ? black14medium : grey14regular,
          ),
        ),
      ),
      onTap: () {
        setState(() {
          _selectedIndex = index;
        });
        widget.onChanged(_selectedIndex);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    bool _isLarge = widget.crossAxisCount == 2;

    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            widget.title,
            style: TextStyle(fontSize: 18),
          ),
          Container(
            width: 324,
            height: 96,
            margin: EdgeInsets.only(left: 24, right: 24, top: 20),
            child: GridView.count(
              crossAxisSpacing: 21,
              childAspectRatio: _isLarge ? 3.9 : 2.35,
              mainAxisSpacing: 16,
              crossAxisCount: widget.crossAxisCount,
              padding: EdgeInsets.zero,
              physics: NeverScrollableScrollPhysics(),
              children: [
                ...widget.options.map((item) {
                  return gridButton(item, widget.options.indexOf(item));
                }).toList()
              ],
            ),
          )
        ],
      ),
    );
  }
}
