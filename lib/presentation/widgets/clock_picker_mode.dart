import 'package:bedo_shop_admin/presentation/widgets/borderless_cupertino_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:flutter/rendering.dart';

class CustomTimePickerCupertino extends StatefulWidget {
  final Function(Duration) onChange;
  final int initialHour;
  final int initialMinaute;
  CustomTimePickerCupertino(
      {Key key, this.onChange, this.initialHour = 0, this.initialMinaute = 0})
      : super(key: key);

  @override
  _CustomTimePickerCupertinoState createState() =>
      _CustomTimePickerCupertinoState();
}

class _CustomTimePickerCupertinoState extends State<CustomTimePickerCupertino> {
  List<String> minutes = List.generate(60, (index) => '$index'.padLeft(2, '0'));

  List<String> hours = List.generate(24, (index) => '$index'.padLeft(2, '0'));

  int initialHour;
  int initialMinaute;

  @override
  void initState() {
    initialHour = widget.initialHour;
    initialMinaute = widget.initialMinaute;
    // WidgetsBinding.instance.addPostFrameCallback((_) {
    // widget.onChange(Duration(hours: initialHour, minutes: initialMinaute));
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // var phoneSize = MediaQuery.of(context).size;

    return
        //
        //
        //
        //
        //
        Expanded(
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            clockPickField(
              onChange: (v) {
                setState(() {
                  initialHour = v;
                });
                final d = Duration(hours: initialHour, minutes: initialMinaute);
                print(d.inHours);
                print(d.inMinutes % 60);

                widget.onChange(d);
              },
              children: hours,
              initialvalue: initialHour ?? DateTime.now().hour,
            ),
            Text(
              ':',
              style: black24medium,
            ),
            clockPickField(
              onChange: (v) {
                setState(() {
                  initialMinaute = v;
                });
                final d = Duration(hours: initialHour, minutes: initialMinaute);
                print(d.inMinutes % 60);
                print(d.inHours);
                widget.onChange(d);
              },
              children: minutes,
              initialvalue: initialMinaute ?? DateTime.now().minute,
            ),
          ],
        ),
      ),
    )
        //
        //
        //
        ;
  }

  Widget clockPickField(
      {ValueChanged<int> onChange, List children, int initialvalue}) {
    return Expanded(
      child: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                border: Border.all(width: 1, color: lightGreyBg),
              ),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Container(
              alignment: Alignment.center,
              height: 150,
              child: BorderLessCupertinoPicker(
                itemExtent: 40.0,
                scrollController:
                    FixedExtentScrollController(initialItem: initialvalue),
                looping: true,
                squeeze: 1.2,
                diameterRatio: 0.8,
                onSelectedItemChanged: onChange,
                children: children
                    .map(
                      (e) => Center(
                        child: Text(
                          e,
                          style: black24medium,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                    .toList(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
