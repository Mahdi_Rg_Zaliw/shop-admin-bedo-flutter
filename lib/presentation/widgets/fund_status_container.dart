import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FundStatusContainer extends StatelessWidget {
  final bool grey;
  final Map<String, num> statistics;

  FundStatusContainer({this.grey, this.statistics});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      decoration: BoxDecoration(
        color: grey ? lightGreyBg : Colors.white,
        borderRadius: BorderRadius.circular(15),
        border: Border.all(
          width: 1,
          color: grey ? Colors.white : lightGreyBg,
        ),
      ),
      child: Column(
        children: statistics.entries
            .map(
              (data) => Column(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          data.key,
                          style: grey12regular,
                        ),
                        Text(
                          data.value.toString(),
                          style: grey14regular,
                        ),
                      ],
                    ),
                  ),
                  if(data.key != 'Company commission') Divider(
                    thickness: 1.5,
                    color: grey ? Colors.white : lightGreyBg,
                  ),
                ],
              ),
            )
            .toList(),
      ),
    );
  }
}
