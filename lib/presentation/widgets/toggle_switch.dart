import 'package:bedo_shop_admin/design/theme.dart';
import 'package:flutter/material.dart';

class ToggleSwitch extends StatefulWidget {
  final bool initialValue;

  final void Function(bool) onToggle;
  ToggleSwitch({this.initialValue, this.onToggle});

  @override
  _ToggleSwitchState createState() => _ToggleSwitchState();
}

class _ToggleSwitchState extends State<ToggleSwitch>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation _toggleAnimation;
  bool val;
  @override
  void initState() {
    val = widget.initialValue;

    _animationController = AnimationController(
      vsync: this,
      value: val ? 1.0 : 0.0,
      duration: Duration(milliseconds: 60),
    );
    _toggleAnimation = AlignmentTween(
      begin: Alignment.centerLeft,
      end: Alignment.centerRight,
    ).animate(
      CurvedAnimation(parent: _animationController, curve: Curves.linear),
    );
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Color _switchColor;

    if (val) {
      _switchColor = Theme.of(context).primaryColor;
    } else {
      _switchColor = switchBg;
    }
    return AnimatedBuilder(
        animation: _animationController,
        builder: (ctx, child) {
          return GestureDetector(
              onTap: () {
                if (val) {
                  _animationController.reverse();
                  widget.onToggle(!val);
                } else {
                  _animationController.forward();
                  widget.onToggle(!val);
                }
                onToggle(!val);
              },
              child: Container(
                  width: 52,
                  height: 24,
                  padding: EdgeInsets.all(1.5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: _switchColor,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      _toggleAnimation.value == Alignment.centerRight
                          ? Expanded(
                              child: Container(),
                            )
                          : Container(),
                      Align(
                        alignment: _toggleAnimation.value,
                        child: Container(
                          width: 20,
                          height: 20,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                          ),
                          child: val
                              ? Icon(
                                  Icons.done,
                                  size: 15,
                                  color: Theme.of(context).primaryColor,
                                )
                              : Container(),
                        ),
                      ),
                      _toggleAnimation.value == Alignment.centerLeft
                          ? Expanded(
                              child: Container(
                                alignment: Alignment.centerRight,
                              ),
                            )
                          : Container(),
                    ],
                  )));
        });
  }

  void onToggle(bool value) {
    setState(() {
      val = value;
    });
  }
}
