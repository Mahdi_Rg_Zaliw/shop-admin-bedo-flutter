import 'package:flutter/material.dart';

import '../../design/theme.dart';

class RadioButtonBedo extends StatefulWidget {
  final int radioValueIndex;
  final List<String> radioButtonList;
  final Axis direction;
  final Color color;
  final ValueChanged<String> onChanged;

  RadioButtonBedo({
    @required this.radioValueIndex,
    @required this.radioButtonList,
    @required this.direction,
    this.color = primaryMaterialColor,
    @required this.onChanged,
  });

  @override
  _RadioButtonBedoState createState() => _RadioButtonBedoState();
}

class _RadioButtonBedoState extends State<RadioButtonBedo> {
  int radioValueIndex;

  void _handleRadioValueChange1(int value) {
    if (value != radioValueIndex) {
      setState(() {
        radioValueIndex = value;
      });
    }
    widget.onChanged(widget.radioButtonList[value]);
  }

  @override
  void initState() {
    radioValueIndex = widget.radioValueIndex;
    super.initState();
  }

  Widget rowWrapRadio() {
    return Wrap(
      children: widget.radioButtonList
          .map(
            (e) => Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                FlatButton(
                  onPressed: () {
                    _handleRadioValueChange1(widget.radioButtonList.indexOf(e));
                  },
                  child: Row(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: 22,
                        height: 22,
                        margin: EdgeInsets.all(9),
                        decoration: BoxDecoration(
                          color: widget.radioButtonList.indexOf(e) ==
                                  radioValueIndex
                              ? widget.color
                              : Colors.transparent,
                          shape: BoxShape.circle,
                          border: Border.all(
                            color: widget.radioButtonList.indexOf(e) ==
                                    radioValueIndex
                                ? widget.color
                                : Color(0xff999999),
                            width: 1.5,
                          ),
                        ),
                        child: Icon(
                          Icons.check,
                          color: Colors.white,
                          size: 14,
                        ),
                      ),
                      Text(
                        e,
                        style: new TextStyle(
                          fontSize: 14.0,
                          color: widget.radioButtonList.indexOf(e) ==
                                  radioValueIndex
                              ? textColor
                              : Color(0xff888888),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
          .toList(),
    );
  }

  Widget columnWrapRadio() {
    return Wrap(
      children: widget.radioButtonList
          .map(
            (e) => Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                FlatButton(
                  onPressed: () {
                    _handleRadioValueChange1(widget.radioButtonList.indexOf(e));
                  },
                  child: Row(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: 22,
                        height: 22,
                        margin: EdgeInsets.all(9),
                        decoration: BoxDecoration(
                          color: widget.radioButtonList.indexOf(e) ==
                                  radioValueIndex
                              ? widget.color
                              : Colors.transparent,
                          shape: BoxShape.circle,
                          border: Border.all(
                            color: widget.radioButtonList.indexOf(e) ==
                                    radioValueIndex
                                ? widget.color
                                : Color(0xff999999),
                            width: 1.5,
                          ),
                        ),
                        child: Icon(
                          Icons.check,
                          color: Colors.white,
                          size: 14,
                        ),
                      ),
                      Text(
                        e,
                        style: new TextStyle(
                          fontSize: 16.0,
                          color: widget.radioButtonList.indexOf(e) ==
                                  radioValueIndex
                              ? textColor
                              : Color(0xff888888),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
          .toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return widget.direction == Axis.vertical
        ? columnWrapRadio()
        : rowWrapRadio();
  }
}
