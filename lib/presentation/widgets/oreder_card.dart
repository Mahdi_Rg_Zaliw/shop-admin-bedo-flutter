import 'package:bedo_shop_admin/design/icons_f.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/model/cart_product.dart';
import 'package:bedo_shop_admin/model/order.dart';
import 'package:bedo_shop_admin/presentation/screens/order_delivery_map.dart';
import 'package:bedo_shop_admin/presentation/widgets/custom_card.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/custom_modal_bottom_sheet.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/order_details_modal.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/reject_discount_modal.dart';
import 'package:bedo_shop_admin/presentation/widgets/round_botton.dart';
import 'package:flutter/material.dart';

class OrderCard extends StatelessWidget {
  final String orderId;
  final Duration duration;
  final double cash;
  final List<CartProduct> cartProducts;
  final OrderStatus status;
  final Order order;

  //Todo fix cash and other properties of the card
  OrderCard({Key key, Order order, this.status})
      : this.cartProducts = order.cart.products,
        this.orderId = order.id,
        // this.duration = Duration(
        //     minutes: order.estimatedDelivery.minute - DateTime.now().minute,
        //     seconds: order.estimatedDelivery.second - DateTime.now().second),
  this.duration = order.estimatedDelivery.difference(DateTime.now()),
        this.cash = order.cart.shipmentCost,
        this.order = order,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16),
      child: CustomCard(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              RichText(
                text: TextSpan(
                  text: S.of(context).orderNumber + ':',
                  style: grey12regular,
                  children: [
                    TextSpan(
                      text: ' ${order.trackID}',
                      style: grey12regular.copyWith(color: textColor),
                    )
                  ],
                ),
              ),
              status == null
                  ? Container()
                  : Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(S.of(context).status + ' :'),
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 8,
                          ),
                          child: (status == OrderStatus.preparing)
                              ? Icon(
                                  Iconsf.i05_box,
                                  size: 25,
                                )
                              : Icon(
                                  Iconsf.i04_bike,
                                  size: 25,
                                ),
                        )
                      ],
                    ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(
                    Icons.access_time,
                    size: 20,
                    color: titleGrey,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text(duration.isNegative
                      ? '0 : 0'
                      : duration.inMinutes != 0
                          ? '${duration.inMinutes} : ${duration.inSeconds % duration.inMinutes}'
                          : '0 : 0')
                ],
              ),
              RichText(
                text: TextSpan(
                  text: '${S.of(context).cash} :',
                  style: grey12regular,
                  children: [
                    TextSpan(
                      text: ' $cash',
                      style: grey12regular.copyWith(color: textColor),
                    )
                  ],
                ),
              )
            ],
          ),
          Divider(),
          ...(cartProducts).map((e) => InvoiceItem(
                item: e.product.title,
                count: e.quantity,
                amount: e.product.price,
              )),
          Total(total: order.cart.shipmentCost),
          status != null
              ? Container(
                  margin: EdgeInsets.symmetric(vertical: 10),
                  width: MediaQuery.of(context).size.width / 1.2,
                  child: RaisedButton(
                    child: Text(S.of(context).tracking),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => OrderDeliveryMap(order: order,)));
                    },
                  ),
                )
              : Container(
                  margin: EdgeInsets.symmetric(vertical: 10),
                  width: MediaQuery.of(context).size.width / 1.2,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        flex: 5,
                        child: Buttons.grey(
                          child: Text(S.of(context).reject),
                          onPressed: () {
                            customModalSheet(
                                context: context,
                                height: 300,
                                child: RejectOrderModal(order));
                          },
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        flex: 7,
                        child: RaisedButton(
                          child: Text(S.of(context).accept),
                          onPressed: () {
                            orderDetailsModal(
                                context: context, orderID: orderId);
                          },
                        ),
                      ),
                    ],
                  ),
                ),
        ],
      ),
    );
  }
}

class InvoiceItemData {
  final String name;
  final int count;
  final double amount;

  const InvoiceItemData({this.amount, this.count, this.name});
}

class Total extends StatelessWidget {
  final double total;

  const Total({Key key, @required this.total}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Text(
        '${S.of(context).invoiceAmount}: ${total.toStringAsFixed(2)} ₼',
        style: black12regular,
      ),
    );
  }
}

class InvoiceItem extends StatelessWidget {
  final String item;
  final int count;
  final double amount;

  const InvoiceItem({
    @required this.item,
    @required this.count,
    @required this.amount,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Text(
            '$item x $count',
            style: grey12regular,
          ),
          Text(
            '$amount ₼',
            style: grey12regular,
          ),
        ],
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
      ),
    );
  }
}
