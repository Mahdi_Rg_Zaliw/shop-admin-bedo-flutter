import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/screens/delivery_map.dart';
import 'package:bedo_shop_admin/utils/device_dimensions.dart';
import 'package:flutter/material.dart';

class CustomLocationField extends FormField<Location> {
  CustomLocationField(
      {BuildContext context,
      FormFieldSetter<Location> onSaved,
      Location initial,
      FormFieldValidator<Location> validator,
      bool autovalidate = false})
      : super(
            onSaved: onSaved,
            validator: validator,
            initialValue: initial,
            autovalidate: autovalidate,
            builder: (FormFieldState<Location> state) {
              return GestureDetector(
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return DeliveryMap(
                        onSelectedMapChanges: (lat, lng) {
                          state.didChange(Location(x: lng, y: lat));
                        },
                      );
                    },
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Text(
                        S.of(context).restaurantLocation,
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            color: titleGrey,
                            fontSize: 12),
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      width: DeviceDimensions.widthSize(context),
                      height: 55,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: state.hasError
                              ? red.withOpacity(.3)
                              : lightGreyBg,
                        ),
                        borderRadius: BorderRadius.circular(14),
                        color: lightGreyBg,
                      ),
                      margin:
                          EdgeInsets.symmetric(horizontal: 24, vertical: 10),
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: Row(
                        children: [
                          Icon(
                            Icons.location_on,
                            color: colorIcon,
                          ),
                          SizedBox(width: 5),
                          if (state.value?.x == 0 && state.value?.y == 0)
                            Text(
                              'select a location please  ',
                              style: grey14regular,
                            ),
                          if (!(state.value?.x == 0 && state.value?.y == 0))
                            Flexible(
                              child: Text(
                                '  lat : ' +
                                    ((state.value?.y == null)
                                        ? ' not selected'
                                        : '${state.value?.y?.toStringAsFixed(6)}') +
                                    '  lon : ' +
                                    ((state.value?.x == null)
                                        ? ' not selected '
                                        : '${state.value?.x?.toStringAsFixed(6)}'),
                                style: grey14regular,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                        ],
                      ),
                    ),
                    if (state.hasError)
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        child: Text(
                          state.errorText,
                          style: TextStyle(color: Theme.of(context).errorColor),
                        ),
                      )
                  ],
                ),
              );
            });
}

class Location {
  num x;
  num y;
  Location({this.x, this.y});
}
