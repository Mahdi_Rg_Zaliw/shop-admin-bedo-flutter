import 'package:bedo_shop_admin/design/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

/// Shows a grid of checkboxes for the given titles
/// [crossAxisCount] is number of columns in the grid and should only be 2 or 3
/// [options] is a list of strings
class CheckboxGrid extends StatefulWidget {
  final List<String> options;
  final int crossAxisCount;
  final double width;
  final double height;
  final bool scroll;

  CheckboxGrid(
      {this.options,
      this.crossAxisCount,
      this.width,
      this.height,
      this.scroll});

  @override
  _CheckboxGridState createState() => _CheckboxGridState();
}

class _CheckboxGridState extends State<CheckboxGrid> {
  List<bool> _selectionState;
  bool isLarge;
  bool isVeryLarge;

  // this widget is each checkbox in the grid
  Widget checkboxButton(String text, index, isSelected) {
    return GestureDetector(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Flexible(
            // fit: FlexFit.tight,
            // flex: 1, //? can be used to align
            child: Checkbox(
              value: _selectionState[index],
              onChanged: (value) {
                setState(() {
                  _selectionState[index] = value;
                });
              },
            ),
          ),
          Flexible(
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: isLarge || isSelected ? black14regular : grey14regular,
            ),
          )
        ],
      ),
      onTap: () {
        setState(() {
          _selectionState[index] = !_selectionState[index];
        });
      },
    );
  }

  @override
  void initState() {
    _selectionState = List<bool>.filled(widget.options.length, false);
    isLarge = widget.crossAxisCount == 2;
    isVeryLarge = widget.crossAxisCount == 1;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width ?? 320,
      height: widget.height ?? 200,
      child: GridView.count(
        shrinkWrap: true,
        crossAxisCount: widget.crossAxisCount,
        physics: widget.scroll == null ? NeverScrollableScrollPhysics() : null,
        childAspectRatio: isLarge
            ? 3.9
            : isVeryLarge
                ? 6
                : 2.2,
        children: [
          ...widget.options.map((item) {
            return checkboxButton(item, widget.options.indexOf(item),
                _selectionState[widget.options.indexOf(item)]);
          }).toList()
        ],
      ),
    );
  }
}
