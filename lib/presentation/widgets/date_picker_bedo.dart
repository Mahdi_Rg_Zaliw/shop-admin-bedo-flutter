import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:bedo_shop_admin/design/styles.dart';

// ignore: must_be_immutable
class DatePickerBedo extends StatefulWidget {
  // String range;

  // DatePickerBedo({this.range});
  @override
  _DatePickerBedoState createState() => _DatePickerBedoState();
}

class _DatePickerBedoState extends State<DatePickerBedo> {
  String range;
  void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
    setState(() {
      if (args.value is PickerDateRange) {
        range =
            DateFormat('dd/MM/yyyy').format(args.value.startDate).toString() +
                ' - ' +
                DateFormat('dd/MM/yyyy')
                    .format(args.value.endDate ?? args.value.startDate)
                    .toString();
        print(range);
      } else {
        print('Null');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SfDateRangePicker(
      // backgroundColor: Colors.white,
      monthViewSettings: DateRangePickerMonthViewSettings(
        viewHeaderStyle: DateRangePickerViewHeaderStyle(
          textStyle: TextStyle(
            fontSize: 12,
            color: Color(0xffaabfbb),
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      todayHighlightColor: Color(0xff259c84),
      rangeSelectionColor: Color(0xff259c84).withOpacity(0.2),
      endRangeSelectionColor: Color(0xff259c84),
      startRangeSelectionColor: Color(0xff259c84),
      headerStyle: DateRangePickerHeaderStyle(
        backgroundColor: Colors.transparent,
        textAlign: TextAlign.center,
        textStyle: black14medium,
      ),
      view: DateRangePickerView.month,
      onSelectionChanged: _onSelectionChanged,
      selectionMode: DateRangePickerSelectionMode.range,
      initialSelectedRange: PickerDateRange(
        DateTime.now().subtract(
          const Duration(days: 4),
        ),
        DateTime.now().add(
          const Duration(days: 3),
        ),
      ),
    );
  }
}
