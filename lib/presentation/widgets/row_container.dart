import 'package:bedo_shop_admin/design/theme.dart';
import 'package:flutter/cupertino.dart';

///This widget creates a container with border for order and discount widgets
///and only should be used with these widgets
class RowContainer extends StatelessWidget {
  final Row child;
  RowContainer({this.child});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
        border: Border.all(color: lightGreyBg, width: 1),
        borderRadius: BorderRadius.circular(15),
      ),
      child: child,
    );
  }
}
