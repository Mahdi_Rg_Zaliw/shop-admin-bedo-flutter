import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:bedo_shop_admin/design/icons_f.dart';

class CustomBottomNavigationBar extends StatefulWidget {
  final Function(int) onChanege;
  final int changeTo;
  CustomBottomNavigationBar({Key key, this.onChanege, this.changeTo})
      : super(key: key);

  @override
  CustomBottomNavigationBarState createState() =>
      CustomBottomNavigationBarState();
}

class CustomBottomNavigationBarState extends State<CustomBottomNavigationBar> {
  @override
  void initState() {
    super.initState();
    // setState(() {
    //   index = widget.changeTo;
    // });
  }

  void indexChange(int ix) {
    setState(() {
      index = ix;
    });
  }

  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'bttomnavbar',
      child: Container(
        height: 60,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0)),
          color: Colors.white,
          boxShadow: [
            BoxShadow(color: Colors.grey[300], spreadRadius: 0, blurRadius: 2),
          ],
        ),
        child: Container(
            padding: EdgeInsets.only(top: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                showIcons(
                    input: 1 - 1,
                    selectedIcon: Iconsf.i28_home_selected,
                    unselectedIcon: Iconsf.i26_home,
                    selectedSize: 20,
                    unSelectedSize: 19),
                showIcons(
                    input: 2 - 1,
                    selectedIcon: Iconsf.i49_stats_selected,
                    unselectedIcon: Iconsf.i48_stats,
                    selectedSize: 20,
                    unSelectedSize: 19),
                showIcons(
                    input: 3 - 1,
                    selectedIcon: Iconsf.i03_brifcase,
                    unselectedIcon: Iconsf.i06_brifcase,
                    selectedSize: 20,
                    unSelectedSize: 19),
                showIcons(
                    input: 4 - 1,
                    selectedIcon: Iconsf.i11_chat_selected,
                    unselectedIcon: Iconsf.i10_chat,
                    selectedSize: 20,
                    unSelectedSize: 19),
                showIcons(
                    input: 5 - 1,
                    selectedIcon: Iconsf.i41_settings_gear,
                    unselectedIcon: Iconsf.i42_settings_outline,
                    selectedSize: 20,
                    unSelectedSize: 19),
              ],
            )),
      ),
    );
  }

  Widget showIcons(
      {int input = 1,
      IconData selectedIcon,
      IconData unselectedIcon,
      double selectedSize,
      double unSelectedSize}) {
    return Expanded(
      child: FlatButton(
          onPressed: () {
            setState(() {
              index = input;
            });
            printRedBg('$input clicked');
            widget.onChanege(index);
          },
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0).copyWith(top: 16),
                child: index == input
                    ? Icon(
                        selectedIcon,
                        color: Colors.black,
                        size: selectedSize,
                      )
                    : Icon(
                        unselectedIcon,
                        color: Colors.black,
                        size: unSelectedSize,
                      ),
              ),
              Expanded(
                child: SizedBox(),
              ),
              index == input
                  ? Container(
                      width: 30,
                      height: 3,
                      color: Colors.black,
                    )
                  : SizedBox(),
            ],
          )),
    );
  }
}
