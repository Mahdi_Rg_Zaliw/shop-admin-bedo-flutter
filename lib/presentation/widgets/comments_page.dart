import 'package:bedo_shop_admin/bloc/comments_bloc/approved_comments_fetch_bloc/approvedcomments_bloc.dart';
import 'package:bedo_shop_admin/bloc/comments_bloc/pending_comments_fetch_bloc/pendingcomments_bloc.dart';
import 'package:bedo_shop_admin/bloc/comments_bloc/total_comments_fetch_bloc/total_comments_bloc.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/widgets/bottom_navigation.dart';
import 'package:bedo_shop_admin/presentation/widgets/comment_widggets/comment_tile.dart';
import 'package:bedo_shop_admin/presentation/widgets/comment_widggets/response_tile.dart';
import 'package:bedo_shop_admin/presentation/widgets/filtered_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'modal_widgets/custom_modal_bottom_sheet.dart';
import 'modal_widgets/modal_delete_page.dart';

// final r = Random();

class CommentsPage extends StatefulWidget {
  static const routeName = 'comments';

  @override
  _CommentsPageState createState() => _CommentsPageState();
}

class _CommentsPageState extends State<CommentsPage> {
  @override
  void initState() {
    super.initState();

    context.read<TotalCommentsBloc>().add(TotalCommentsVisited());
    context.read<PendingcommentsBloc>().add(PendingCommentsVisisted());
    context.read<ApprovedcommentsBloc>().add(ApprovedcommentsVisited());
  }

  @override
  Widget build(BuildContext context) {
    // context.read<TotalCommentsBloc>().add(TotalCommentsVisited());
    // context.read<PendingcommentsBloc>().add(PendingCommentsVisisted());
    // context.read<ApprovedcommentsBloc>().add(ApprovedcommentsVisited());
    return FilteredList.fix(
      appBarTitlesList: [
        Text(
          S.of(context).comments,
          style: grey12regular,
        )
      ],
      appBarBottomList: [
        BlocBuilder<TotalCommentsBloc, TotalCommentsState>(
          builder: (context, state) {
            if (state is CommentsCountLoadedSuccess) {
              return Text(
                state.amount.toString(),
                style: black24medium,
              );
            }
            if (state is TryingToGetApprovedcomments) {
              return Waiting();
            } else {
              return Center(
                child: Text(
                  '0',
                  style: red12semibold,
                ),
              );
            }
          },
        )
      ],
      optionsList: [S.of(context).confirmed, S.of(context).waiting],
      pagesList: [ConfirmedCommentsPage(), WaitingCommentsPage()],
      // bottomNavigationBar: CustomBottomNavigationBar(),
    );
  }
}

class Waiting extends StatelessWidget {
  const Waiting({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: SizedBox(
            width: 25, height: 25, child: CircularProgressIndicator()));
  }
}

class ConfirmedCommentsPage extends StatelessWidget {
  final itemCount = 12;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ApprovedcommentsBloc, ApprovedcommentsState>(
      builder: (context, state) {
        if (state is SomeThingWentWrongW) {
          return Center(
            child: Text(
              state.error,
              style: red12semibold,
            ),
          );
        }
        if (state is FailedGettingApprovedComments) {
          return Center(
            child: Text(
              state.error,
              style: red12semibold,
            ),
          );
        }
        if (state is SuccessGettingApprovedComments) {
          if (state.result.length == 0) {
            return Center(
              child: Text(' No approved comments '),
            );
          }
          return ListView.builder(
            itemBuilder: (context, index) {
              final data = state.result[index];

              return Column(
                children: [
                  CommentTile.approved(
                      id: data.id,
                      imageUrl: data.pfp ?? '',
                      personName: data.fullUserName,
                      rank: data.rate,
                      date: data.createdAt,
                      message: data.commentsText),
                  if (data.adminReply.replyText != null)
                    ResponceTile(
                      reply: data.adminReply.replyText,
                    )
                ],
              );
            },
            itemCount: state.result.length,
          );
        }
        if (state is TryingToGetApprovedcomments) {
          return Waiting();
        } else {
          return Text(
            'failed getting approved comments',
            style: red12semibold,
          );
        }
      },
    );
  }
}

class WaitingCommentsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PendingcommentsBloc, PendingcommentsState>(
      builder: (context, state) {
        if (state is SomeThingWentWrongA) {
          return Center(
            child: Text(
              state.error,
              style: red12semibold,
            ),
          );
        }
        if (state is FailedGettingPendingComments) {
          return Center(
            child: Text(
              state.error,
              style: red12semibold,
            ),
          );
        }
        if (state is SuccessGettingPendingComments) {
          if (state.result.length == 0) {
            return Center(
              child: Text(' No Waintg comments  '),
            );
          }
          return ListView.builder(
            itemBuilder: (context, index) {
              final data = state.result[index];
              final comment = CommentTile.waiting(
                id: data.id,
                onSubmitReply: (text, id) {
                  context
                      .read<PendingcommentsBloc>()
                      .add(PendingCommentAccepting(id: id, responseText: text));
                  context
                      .read<ApprovedcommentsBloc>()
                      .add(ApprovedcommentsRefresh());
                },
                onAccepted: (id, text) {
                  //

                  //
                },
                onRejected: (id) {
                  //ModalDeletePage
                  customModalSheet(
                    height: MediaQuery.of(context).size.height / 2,
                    context: context,
                    child: ModalDeletePage(
                      onCancel: () {
                        Navigator.of(context).pop();
                      },
                      onDelete: () {
                        context
                            .read<PendingcommentsBloc>()
                            .add(PendingCommentRejecting(id: id));
                        Navigator.of(context).pop();
                      },
                      title: 'are you sure you want to reject this comment',
                    ),
                  );

                  //
                },
                imageUrl: data.pfp ?? '',
                personName: data.fullUserName,
                rank: data.rate,
                date: data.createdAt,
                message: data.commentsText,
              );
              return comment;
            },
            itemCount: state.result.length,
          );
        }
        if (state is TryingToGetPendingComments) {
          return Waiting();
        } else {
          return Center(
            child: Text(
              'failed getting waiting comments',
              style: red12semibold,
            ),
          );
        }
      },
    );
  }
}
