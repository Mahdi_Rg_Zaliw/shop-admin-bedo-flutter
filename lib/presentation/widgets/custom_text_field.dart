import 'dart:ffi';

import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTextField extends StatelessWidget {
  final int maxline;
  final String initialValue;
  final Widget perfixIcon;
  final Widget suffixIcon;
  final TextInputType keyboardType;
  final String hintText;
  final TextStyle style;
  final void Function(String) onChanged;
  final TextStyle hintStyle;
  final bool isMultiLine;
  final String suffixText;
  final bool isPassword;
  final bool isRequired;
  final String Function(String) validator;
  final String title;
  final TextEditingController controller;
  final void Function(String) onSaved;
  final double paddingHorizontal;
  final List<TextInputFormatter> formatters;
  const CustomTextField({
    Key key,
    this.maxline,
    this.onSaved,
    this.perfixIcon,
    this.style,
    this.hintStyle,
    this.validator,
    this.hintText = 'enter me',
    this.title,
    this.initialValue,
    this.isMultiLine = false,
    this.isPassword = false,
    this.isRequired = false,
    this.controller,
    this.paddingHorizontal = 24,
    this.suffixIcon,
    this.keyboardType,
    this.suffixText,
    this.onChanged,
    this.formatters,
  }) : assert(!(isMultiLine && isPassword));

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: paddingHorizontal),
      child: Column(
        children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              (title != null)
                  ? Text(
                      title,
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          color: titleGrey,
                          fontSize: 12),
                    )
                  : SizedBox(),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                initialValue: initialValue,
                onChanged: onChanged,
                onSaved: onSaved,
                controller: controller,
                validator: validator ??
                    (String value) {
                      if (value.trim().isEmpty && isRequired) {
                        return S.of(context).thisFieldIsRequired;
                      }
                      return null;
                    },
                style: style ?? TextStyle(fontSize: 14, color: textColor),
                keyboardType: keyboardType != null
                    ? keyboardType
                    : isMultiLine
                        ? TextInputType.multiline
                        : TextInputType.text,
                obscureText: isPassword,
                inputFormatters: formatters,
                maxLines: isPassword ? 1 : maxline,
                decoration: InputDecoration(
                  prefixIcon: perfixIcon,
                  suffixIcon: suffixIcon,
                  // suffix: Icon(Icons.add),
                  suffixText: suffixText,
                  // labelText: 'hello',
                  hintText: hintText,
                  hintStyle: hintStyle ??
                      TextStyle(
                        color: hintTextColor,
                        fontSize: 14,
                      ),

                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 1,
                      color: Theme.of(context).primaryColor,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(12),
                    ),
                  ),
                  fillColor: lightGreyBg,
                  filled: true,
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 1,
                      color: lightGreyBg,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(12),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 8,
              )
            ],
          ),
        ],
      ),
    );
  }
}

// ^(?=\D*(?:\d\D*){1,12}$)\d+(?:\.\d{1,4})?$

extension EmailValidator on String {
  bool isValidEmail() {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(this.trim());
  }
}

extension ValidPercent on String {
  bool isValidPercent() {
    return RegExp(r'^(\d+(\.\d{0,2})?|\.?\d{1,2})$').hasMatch(this);
  }
}

class CustomMaskedTextInputFormatter extends TextInputFormatter {
  final String mask;
  final String separator;

  CustomMaskedTextInputFormatter({
    @required this.mask,
    @required this.separator,
  }) {
    assert(mask != null);
    assert(separator != null);
  }

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.length > 0) {
      if (newValue.text.length > oldValue.text.length) {
        if (newValue.text.length > mask.length) return oldValue;
        if (newValue.text.length < mask.length &&
            mask[newValue.text.length - 1] == separator) {
          return TextEditingValue(
            text:
                '${oldValue.text}$separator${newValue.text.substring(newValue.text.length - 1)}',
            selection: TextSelection.collapsed(
              offset: newValue.selection.end + 1,
            ),
          );
        }
      }
    }
    return newValue;
  }
}

extension Gmail on String {
  bool isGmail() {
    return this.contains(RegExp(r'gmail', caseSensitive: false));
  }
}

extension ValidPhone on String {
  bool isValidMobile() {
    String pattern = r'(^(?:[+0]9)?[0-9]{10,15}$)';
    RegExp regExp = new RegExp(pattern);
    if (!regExp.hasMatch(this)) {
      return false;
    }
    return true;
  }
}
