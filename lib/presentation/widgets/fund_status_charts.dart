import 'package:bedo_shop_admin/bloc/reports_bloc/reports_bloc.dart';
import 'package:bedo_shop_admin/bloc/reports_bloc/reports_state.dart';
import 'package:bedo_shop_admin/presentation/widgets/report_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FundStatusCharts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReportsBloc, ReportsState>(builder: (context, state) {
      if (state is ReportsLoaded) {
        return state is ReportsLoaded
            ? ListView(
                children: [
                  ReportChart(
                      data: state.chartData.numberOfSales,
                      dates: state.chartData.dates,
                      title: 'Number of sales',
                      colors: [Colors.teal]),
                  ReportChart(
                      data: state.chartData.receivedOrders,
                      dates: state.chartData.dates,
                      title: 'Number of received orders',
                      colors: [
                        Colors.blue,
                      ]),
                  ReportChart(
                      data: state.chartData.successfulOrders,
                      dates: state.chartData.dates,
                      title: 'Number of successful orders',
                      colors: [Colors.green]),
                  ReportChart(
                      data: state.chartData.unSuccessfulOrders,
                      dates: state.chartData.dates,
                      title: 'Number of unsuccessful orders',
                      colors: [Colors.red]),
                  ReportChart(
                      data: state.chartData.returnedOrders,
                      dates: state.chartData.dates,
                      title: 'Number of returned orders',
                      colors: [Colors.grey.shade800]),
                  ReportChart(
                      data: state.chartData.cashDaySales,
                      dates: state.chartData.dates,
                      title: 'Cash sales',
                      colors: [Colors.yellow]),
                  ReportChart(
                      data: state.chartData.cardDaySales,
                      dates: state.chartData.dates,
                      title: 'Card sales',
                      colors: [Colors.deepOrange]),
                  ReportChart(
                      data: state.chartData.companyCommission,
                      dates: state.chartData.dates,
                      title: 'Company commissions',
                      colors: [Colors.lime]),
                ],
              )
            : Center(
                child: Text('Loading status failed'),
              );
      }
      if (state is ReportsLoading) {
        return Container(
          child: Center(child: CupertinoActivityIndicator()),
        );
      }
      if (state is ReportsLoadingFailed) {
        return Container(
          child: Center(child: Text('Loading status failed')),
        );
      }
      return Container();
    });
  }
}
