import 'dart:io';
import 'package:bedo_shop_admin/design/icons_f.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';

enum UploadPickerType { remove, edit }

// ignore: must_be_immutable
class UploadPickPictureButton extends StatefulWidget {
  final void Function(File) onChanged;
  final UploadPickerType pickerType;
  String imagePicked;

  UploadPickPictureButton({
    this.pickerType = UploadPickerType.edit,
    this.imagePicked,
    @required this.onChanged,
  });

  @override
  _UploadPickPictureButtonState createState() =>
      _UploadPickPictureButtonState();
}

class _UploadPickPictureButtonState extends State<UploadPickPictureButton> {
  final picker = ImagePicker();

  Future getImage(BuildContext context) async {
    showTypeImagePickerModal(
      context: context,
      onChanged: (file) {
        widget.onChanged(file);
      },
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80,
      height: 80,
      child: Stack(
        children: [
          Container(
            width: 80,
            height: 80,
            child: widget.imagePicked == ''
                ? FlatButton(
                    color: primaryMaterialColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(60),
                    ),
                    onPressed: () {
                      getImage(context);
                    },
                    child: Icon(
                      Iconsf.i09_camera,
                      color: Colors.white,
                      size: 24,
                    ),
                  )
                : ClipRRect(
                    borderRadius: BorderRadius.circular(40),
                    child: Image.network(
                      widget.imagePicked,
                      width: 80,
                      height: 80,
                      fit: BoxFit.cover,
                    ),
                  ),
          ),
          widget.imagePicked != ''
              ? widget.pickerType == UploadPickerType.edit
                  ? Align(
                      alignment: Alignment.bottomRight,
                      child: GestureDetector(
                        onTap: () {
                          getImage(context);
                        },
                        child: Container(
                          width: 30,
                          height: 30,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                          ),
                          child: Container(
                            width: 25,
                            height: 25,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: primaryMaterialColor,
                            ),
                            child: Icon(
                              Iconsf.i09_camera,
                              color: Colors.white,
                              size: 14,
                            ),
                          ),
                        ),
                      ),
                    )
                  : Align(
                      alignment: Alignment.topRight,
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            widget.imagePicked = '';
                          });
                        },
                        child: Container(
                          width: 30,
                          height: 30,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                          ),
                          child: Container(
                            width: 25,
                            height: 25,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: red,
                            ),
                            child: Icon(
                              Icons.delete_outline,
                              color: Colors.white,
                              size: 14,
                            ),
                          ),
                        ),
                      ),
                    )
              : Container(),
        ],
      ),
    );
  }
}

Future showTypeImagePickerModal({
  BuildContext context,
  final ValueChanged<File> onChanged,
}) {
  return showModalBottomSheet(
    isScrollControlled: false,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(24),
        topRight: Radius.circular(24),
      ),
    ),
    context: context,
    builder: (context) {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        height: 100,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            FlatButton(
              padding: EdgeInsets.all(9),
              onPressed: () async {
                final pickedFile =
                    await ImagePicker().getImage(source: ImageSource.gallery);
                onChanged(File(pickedFile.path));
                Navigator.pop(context);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.image,
                    color: primaryMaterialColor,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'From Gallery',
                    style: black14medium,
                  ),
                ],
              ),
            ),
            FlatButton(
              padding: EdgeInsets.all(9),
              onPressed: () async {
                final pickedFile =
                    await ImagePicker().getImage(source: ImageSource.camera);
                onChanged(File(pickedFile.path));
                Navigator.pop(context);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.camera,
                    color: primaryMaterialColor,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'From Camera',
                    style: black14medium,
                  ),
                ],
              ),
            )
          ],
        ),
      );
    },
  );
}
