import 'package:bedo_shop_admin/design/theme.dart';
import 'package:flutter/material.dart';

///This widget creates an animated container for order widget
///and only should be used with that widget
class AnimatedRowContainer extends StatefulWidget {
  final Row child;
  bool isOpened = false;
  final int productsCount;

  AnimatedRowContainer({this.child, this.isOpened, this.productsCount});

  @override
  _AnimatedRowContainerState createState() => _AnimatedRowContainerState();
}

class _AnimatedRowContainerState extends State<AnimatedRowContainer> {
  @override
  Widget build(BuildContext context) {
    var height = widget.productsCount != null
        ? (widget.productsCount * 39 + 150).toDouble()
    // if your child doesn't fit in this widget change this line
        : 160;
    return LayoutBuilder(
      builder: (context, constraints) => AnimatedContainer(
        duration: Duration(milliseconds: 300),
        height:
            widget.isOpened ? height : 42,
        margin: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
        padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
          border: Border.all(color: lightGreyBg, width: 1),
          borderRadius: BorderRadius.circular(15),
        ),
        child: widget.child,
      ),
    );
  }
}
