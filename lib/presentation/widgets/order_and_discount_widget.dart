import 'package:bedo_shop_admin/design/icons_f.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/model/cart_product.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/presentation/widgets/animated_row_container.dart';
import 'package:bedo_shop_admin/presentation/widgets/row_container.dart';

class OrderAndDiscountWidget extends StatefulWidget {
  final String id;
  final bool isDone;
  final String title;
  final double orderPrice;
  final DateTime date;
  final double percent;
  final String description;
  final bool isExpanded;
  final bool expandable;
  final bool _isOrder;
  final bool inputTransaction;
  final List<CartProduct> products;
  bool isOpened = false;
  double courierFee = 0;
  double tax = 0;
  double totalPrice = 0;

  /// builds different widgets according to its arguments
  /// set [isExpanded] to true to show an expanded order widget
  /// set [expandable] to true to show an expand button on the trailing side of
  /// the row that user can expand the widget by tapping on that.
  /// if [isExpanded] and [expandable] set to true at a same time widget will build
  /// an expandable in expanded state and NOT the normal expanded widget that is
  /// different from expandable.
  /// DON'T SET inputTransaction explicitly in this constructor
  OrderAndDiscountWidget.order({
    @required this.date,
    @required this.title,
    @required this.orderPrice,
    this.isDone = false,
    this.isExpanded = false,
    this.expandable = false,
    this.description,
    this.inputTransaction = true,
    this.id,
    this.products,
    this.courierFee,
    this.tax,
    this.totalPrice,
  })  : percent = null,
        _isOrder = true,
        isOpened = isExpanded && expandable;

  /// builds transaction widget that is a expanded and not expandable order widget.
  OrderAndDiscountWidget.transaction({
    @required DateTime date,
    @required String title,
    @required double transactionPrice,
    @required String description,
    bool inputTransaction = false,
    isDone = false,
  }) : this.order(
            date: date,
            title: title,
            orderPrice: transactionPrice,
            isDone: isDone,
            isExpanded: true,
            expandable: false,
            description: description,
            inputTransaction: inputTransaction);

  /// builds a simple row widget for discounts page
  OrderAndDiscountWidget.discount({
    @required this.date,
    @required this.title,
    @required this.percent,
    @required this.id,
    this.isDone = false,
  })  : orderPrice = null,
        _isOrder = false,
        isExpanded = false,
        expandable = false,
        products = null,
        description = '',
        inputTransaction = null;

  @override
  _OrderAndDiscountWidgetState createState() => _OrderAndDiscountWidgetState();
}

class _OrderAndDiscountWidgetState extends State<OrderAndDiscountWidget> {
  @override
  Widget build(BuildContext context) {
    // this line decides what type of widget to build
    return widget.expandable
        ? AnimatedRowContainer(
            child: buildAnimatedRow(context),
            isOpened: widget.isOpened,
            productsCount: widget.products != null ? widget.products.length : null,
          )
        : RowContainer(child: buildRow(context));
  }

  //this method builds the Main row with ability to expand and show more details about the order
  Row buildAnimatedRow(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        buildIconSizedBox(context),
        SizedBox(
          width: 10,
        ),
        Flexible(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              buildMainRow(),
              if (widget.isOpened)
                Container(
                  padding: EdgeInsets.only(top: 20),
                  child: Column(
                    children: widget.products == null
                        ? [
                            _detailRow(S.of(context).numberOfSales, 4),
                            Divider(
                              height: 23,
                            ),
                            _detailRow(S.of(context).numberOfOrdersReceived, 4),
                            Divider(
                              height: 23,
                            ),
                            _detailRow(
                                S.of(context).numberOfSuccessfulOrders, 4),
                          ]
                        : [
                            ...widget.products
                                .map((e) => Column(
                                      children: [
                                        _detailRow(
                                            e.product.title,
                                            e.product.price * e.quantity,
                                            e.quantity),
                                        Divider(
                                          height: 23,
                                        ),
                                      ],
                                    ))
                                .toList(),
                            _detailRow('courier fee', widget.courierFee),
                            Divider(
                              height: 23,
                            ),
                            _detailRow('tax', widget.tax),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [Text('Total amount: ', style: grey12regular,),
                                Text('${widget.totalPrice.toString()} ₼', style: grey14regular,)],
                              ),
                            )
                          ],
                  ),
                ),
            ],
          ),
        ),
      ],
    );
  }

  // builds the Main order and discount row widget without animation
  Row buildRow(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        buildIconSizedBox(context),
        SizedBox(
          width: widget.isExpanded ? 15 : 10,
        ),
        Flexible(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              buildMainRow(),
              if (widget.isExpanded) ...orderDescription(),
            ],
          ),
        ),
      ],
    );
  }

  //handles all the possible icons in this widget
  SizedBox buildIconSizedBox(BuildContext context) {
    return SizedBox(
      width: 20,
      child: widget.isDone
          ? Icon(
              widget._isOrder
                  ? (widget.inputTransaction ? null : Icons.add) ?? Icons.done
                  : Iconsf.i07_calendar_checked,
              color: Theme.of(context).primaryColor,
              size: 15,
            )
          : Icon(
              widget._isOrder
                  ? (widget.inputTransaction ? null : Icons.minimize) ??
                      Icons.clear
                  : Iconsf.i08_calendar_unchecked,
              color: Theme.of(context).errorColor,
              size: 15),
    );
  }

  // top row that is always visible
  //don`t mess up with above rows this widget is used inside of them
  Row buildMainRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      children: [
        Container(
          width: 80,
          child: Text(
            widget.title,
            style: grey14regular,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        Text(
          DateFormat('d MMM y', 'en_US').format(widget.date),
          style: grey14regular,
        ),
        Row(
          children: [
            Text(
              widget._isOrder
                  ? widget.orderPrice.toString() + ' \$'
                  : widget.percent.toString() + ' %',
              style: widget.isDone ? green14medium : red14medium,
            ),
            if (widget.expandable) ...[
              SizedBox(
                width: 5,
              ),
              GestureDetector(
                  child: SizedBox(
                    width: 15,
                    child: Icon(
                      widget.isOpened ? Icons.expand_less : Icons.expand_more,
                      size: 16,
                    ),
                  ),
                  onTap: () {
                    setState(() {
                      widget.isOpened = !widget.isOpened;
                    });
                  })
            ]
          ],
        ),
      ],
    );
  }

  // details in the expandable state
  Row _detailRow(String text, double total, [int quantity]) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          quantity == null ? text : '$text x${quantity.toString()}',
          style: grey12regular,
        ),
        Text(
          total.toString() + ' ₼',
          style: grey14regular,
        ),
      ],
    );
  }

  //description in the isExpanded state
  orderDescription() {
    return [
      Divider(),
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Flexible(
              child: Text(
            widget.description,
            softWrap: true,
            style: grey12regular,
          )),
        ],
      )
    ];
  }
}
