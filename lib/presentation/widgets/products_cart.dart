import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:flutter/material.dart';

class ProductsCart extends StatelessWidget {
  final String imgUrl;
  final String name;
  final String shortDescribtion;
  final String longDescribrion;
  final bool isMeal;
  final int stock;
  final VoidCallback onPressedActions;
  final double price;
  final double offPrice;
  ProductsCart.menu({
    @required String imgUrl,
    @required String name,
    @required String shortDescribtion,
    @required String longDescribrion,
    @required VoidCallback onPressed,
  })  : isMeal = false,
        name = name,
        shortDescribtion = shortDescribtion,
        longDescribrion = longDescribrion,
        stock = null,
        onPressedActions = onPressed,
        imgUrl = imgUrl,
        price = null,
        offPrice = null;

  ProductsCart.meal({
    @required String imgUrl,
    @required String name,
    @required String shortDescribtion,
    @required int stock,
    @required double price,
    @required VoidCallback onPressedActions,
    double offPrice,
  })  : isMeal = true,
        name = name,
        shortDescribtion = shortDescribtion,
        longDescribrion = null,
        stock = stock,
        onPressedActions = onPressedActions,
        price = price,
        offPrice = offPrice,
        imgUrl = imgUrl;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {},
        child: Container(
            // height: 120,
            // width: 400,
            margin: EdgeInsets.symmetric(
              horizontal: 24,
            ),
            decoration: BoxDecoration(
              // color: Colors.blue,
              border: Border(
                bottom: BorderSide(width: 0.5, color: Colors.grey[300]),
              ),
            ),
            child: Row(children: [
              Expanded(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 18),
                    height: 80,
                    width: 80,
                    // margin: EdgeInsets.only(right: 10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border:
                            Border.all(width: 0.5, color: Colors.grey[300])),
                    child: ProductImage(img: imgUrl),
                  ),
                  Expanded(
                      child: Container(
                          margin: EdgeInsets.all(12.0).copyWith(right: 0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: Text(
                                      name,
                                      style: black14medium,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                  IconButton(
                                    onPressed: onPressedActions,
                                    icon: Icon(
                                      Icons.more_vert,
                                      size: 25,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 8.0),
                              Text(
                                shortDescribtion,
                                maxLines: 2,
                                style: grey14regular,
                                overflow: TextOverflow.ellipsis,
                              ),
                              SizedBox(height: 8.0),
                              Row(
                                children: [
                                  Expanded(
                                    child: isMeal == false
                                        ? Text(
                                            longDescribrion,
                                            style: black14regular,
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          )
                                        : PriceTag(
                                            price: price,
                                            offprice: offPrice,
                                          ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  isMeal == true
                                      ? RichText(
                                          text: TextSpan(
                                            text: '$stock',
                                            style: grey14semiBold,
                                            children: [
                                              TextSpan(
                                                text: S.of(context).inStock,
                                                style: grey12regular,
                                              )
                                            ],
                                          ),
                                        )
                                      : SizedBox()
                                ],
                              ),
                              SizedBox(height: 8.0),
                            ],
                          ))),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      SizedBox(
                        height: 5,
                      ),
                    ],
                  ),
                ],
              ))
            ])));
  }
}

class PriceTag extends StatelessWidget {
  final double price;
  final double offprice;
  const PriceTag({
    Key key,
    this.price,
    this.offprice,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Container(
              child: offprice != null
                  ? CustomPaint(
                      painter: CrossedLine(),
                      // size: Size(constraints.maxWidth, constraints.maxHeight),
                      child: Text(
                        '$price \$',
                        style: red12semibold,
                      ),
                    )
                  : SizedBox()),
          offprice != null ? SizedBox(width: 10) : SizedBox(),
          Text(
            '${offprice ?? price} \$ ',
            style: black14regular,
          )
        ],
      ),
    );
  }
}

class ProductImage extends StatelessWidget {
  const ProductImage({
    Key key,
    @required this.img,
  }) : super(key: key);

  final String img;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: FadeInImage(
        fit: BoxFit.cover,
        placeholder: AssetImage('assets/images/product_image_placeholder.png'),
        image: NetworkImage(img ?? 'https://picsum.photos/250'),
      ),
    );
  }
}

class CrossedLine extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final p1 = Offset(size.width, 0);
    final p2 = Offset(0, size.height);
    final paint = Paint()
      ..color = red
      ..strokeWidth = .9;
    canvas.drawLine(p1, p2, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
    // throw UnimplementedError();
  }
}
