import 'package:bedo_shop_admin/bloc/reports_bloc/reports_bloc.dart';
import 'package:bedo_shop_admin/bloc/reports_bloc/reports_event.dart';
import 'package:bedo_shop_admin/bloc/reports_bloc/reports_state.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/presentation/widgets/order_and_discount_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FullReportPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        BlocProvider.of<ReportsBloc>(context).add(LoadReportsEvent());
        return;
      },
      child: BlocBuilder<ReportsBloc, ReportsState>(builder: (context, state) {
        if (state is ReportsLoaded) {
          return ListView.builder(
            itemCount: state.orders.length,
            itemBuilder: (context, index) => OrderAndDiscountWidget.order(
              date: state.orders[index].createdAt,
              title: state.orders[index].trackID,
              orderPrice: state.orders[index].finalPrice,
              totalPrice: state.orders[index].totalPrice,
              tax: state.orders[index].tax,
              courierFee: state.orders[index].courierFee,
              isDone: state.orders[index].inputOrder,
              products: state.orders[index].cart.products,
              expandable: true,
            ),
          );
        } else if (state is ReportsLoadingFailed) {
          return Center(
            child: Text(
              'Load failed',
              style: red14medium,
            ),
          );
        } else if (state is ReportsLoading) {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        } else
          return Center(
            child: Text('no transactions loaded try again'),
          );
      }),
    );
  }
}
