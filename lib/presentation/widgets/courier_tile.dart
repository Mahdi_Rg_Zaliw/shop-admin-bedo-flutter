// import 'dart:js';

import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/widgets/colored_border.dart';
import 'package:bedo_shop_admin/presentation/widgets/comment_widggets/custom_avatar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

enum CurierStatus { available, inService }

class CourierTile extends StatelessWidget {
  final CurierStatus status;
  final bool selected;
  final String courierName;
  final String imageUrl;
  final String courierDesc;
  final GestureTapCallback onTap;
  final GestureTapCallback moreVertOnTap;

  const CourierTile({
    Key key,
    this.status,
    this.selected = false,
    this.courierName,
    this.imageUrl,
    this.courierDesc,
    this.onTap,
    this.moreVertOnTap,
  })  : assert(!(selected && status == CurierStatus.inService),
            ' in service couriers cant be selected'),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: status != null && status == CurierStatus.available
          ? InkWell(
              // focusColor: primaryMaterialColor,
              // splashColor: !selected ? primaryMaterialColor : null,
              borderRadius: BorderRadius.circular(20),
              // radius: 20,
              onTap: onTap,
              child: buildTile(context),
            )
          : Opacity(
              opacity: status == CurierStatus.inService ? 0.5 : 1,
              child: buildTile(context),
            ),
    );
  }

  ColoredBorder buildTile(BuildContext context) {
    return ColoredBorder(
      borderColor:
          selected && status != null && status == CurierStatus.available
              ? primaryMaterialColor
              : lightGreyBg,
      radius: 20,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: IntrinsicHeight(
          child: Stack(
            alignment: Alignment.topRight,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                // mainAxisAlignment: MainAxisAlignment.z,
                children: [
                  Expanded(
                    flex: 3,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 15),
                      child: Column(
                        children: [
                          CustomAvatar(
                            imageUrl: imageUrl ?? 'https://picsum.photos/250',
                            radius: 31,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(child: SizedBox()),
                  Expanded(
                    flex: 8,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 17),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            courierName ?? 'Liam Parker',
                            // 'courier name so no man if we cank n want so we  describtion man no so when man can you do this thi',
                            style: black14regular,
                          ),
                          SizedBox(
                            height: 9,
                          ),
                          Text(
                            courierDesc ?? 'Ford Fiesta - 2019 dark hair',
                            // 'courier describtion man no so when man can you do this thingccXcXCcds fsdfadf sdff dsfa fadfadf',
                            style: grey14medium,
                          ),
                        ],
                        mainAxisSize: MainAxisSize.min,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.stretch,
                      // mainAxisSize: MainAxisSize.min,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 5),
                          child: status == null
                              ? InkWell(
                                  borderRadius: BorderRadius.circular(30),
                                  child: Icon(
                                    Icons.more_vert,
                                  ),
                                  onTap: moreVertOnTap,
                                )
                              : SizedBox(),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              status != null
                  ? Positioned(
                      right: 10,
                      top: -2,
                      child: status == CurierStatus.available
                          ? Row(
                              children: [
                                Icon(
                                  Icons.circle,
                                  color: primaryMaterialColor,
                                  size: 7,
                                ),
                                SizedBox(
                                  width: 6,
                                ),
                                Text(
                                  S.of(context).available,
                                  style: green12semiBold,
                                )
                              ],
                            )
                          : Row(
                              children: [
                                Icon(
                                  Icons.circle,
                                  color: red,
                                  size: 7,
                                ),
                                SizedBox(
                                  width: 6,
                                ),
                                Text(
                                  S.of(context).inService,
                                  style: red12semibold,
                                )
                              ],
                            ),
                    )
                  : SizedBox(),
            ],
          ),
        ),
      ),
    );
  }
}
