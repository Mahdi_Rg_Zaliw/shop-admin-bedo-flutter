import 'package:bedo_shop_admin/bloc/conversations_bloc/conversations_bloc.dart';
import 'package:bedo_shop_admin/bloc/conversations_bloc/conversations_state.dart';
import 'package:bedo_shop_admin/model/conversation_model.dart';
import 'package:bedo_shop_admin/model/message_model.dart';
import 'package:bedo_shop_admin/presentation/widgets/message_bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Messages extends StatelessWidget {
  final Conversation conversation;

  Messages(this.conversation);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ConversationBloc, ConversationsState>(
      buildWhen: (prev, now) {
        return prev is ConversationsLoading && now is ConversationsLoaded;
      },
      builder: (context, state) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: ListView.builder(
          itemCount: conversation.messages.length,
          reverse: true,
          itemBuilder: (ctx, index) => MessageBubble(
            message: conversation.messages[index].text,
            isMe: conversation.messages[index].senderType ==
                MessageSenderType.Admin,
            userName: conversation.user != null
                ? conversation.user.name != null
                    ? conversation.user.name
                    : " "
                : " ",
            date: conversation.messages[index].time,
            isFile: conversation.messages[index].type == MessageType.Upload,
            fileName: conversation.messages[index].fileName,
            fileNameToShow: conversation.messages[index].fileNameToShow,
            conversationID: conversation.ID,
            showUserName: index == conversation.messages.length - 1
                ? true
                : conversation.messages[index].senderType !=
                    conversation.messages[index + 1].senderType,
          ),
        ),
      ),
    );
  }
}
