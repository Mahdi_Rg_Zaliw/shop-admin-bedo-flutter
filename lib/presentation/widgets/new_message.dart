import 'package:bedo_shop_admin/bloc/conversations_bloc/conversations_bloc.dart';
import 'package:bedo_shop_admin/bloc/conversations_bloc/conversations_events.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/model/message_model.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:bedo_shop_admin/generated/l10n.dart';

class NewMessage extends StatefulWidget {
  final String conversationID;

  NewMessage(this.conversationID);

  @override
  _NewMessageState createState() => _NewMessageState();
}

class _NewMessageState extends State<NewMessage> {
  final _controller = TextEditingController();
  var _enteredMessage = '';

  void _sendMessage() async {
    var message = Message(
        id: null,
        text: _controller.text.trimRight(),
        time: DateTime.now(),
        type: MessageType.Text,
        senderType: MessageSenderType.Admin);
    BlocProvider.of<ConversationBloc>(context)
        .add(SendMessageEvent(message, widget.conversationID));
    _controller.clear();
  }

  void _attachFile() async {
    FilePickerResult result = await FilePicker.platform.pickFiles(
        type: FileType.custom, allowedExtensions: ['jpg', 'png', 'pdf']);
    if (result != null) {
      BlocProvider.of<ConversationBloc>(context)
          .add(UploadFileEvent(result, widget.conversationID));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 8, bottom: 8),
      padding: EdgeInsets.all(8),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              controller: _controller,
              style: black14medium,
              decoration: InputDecoration(
                  hintText: S.of(context).chatHint,
                  focusedBorder: InputBorder.none,
                  prefixIcon: IconButton(
                    icon: Icon(FontAwesomeIcons.paperclip),
                    color: Color(0xFF3A3838),
                    splashRadius: 5,
                    onPressed: _attachFile,
                  ),
                  suffixIcon: IconButton(
                    icon: Icon(FontAwesomeIcons.solidPaperPlane),
                    onPressed:
                        _enteredMessage.trim().isEmpty ? null : _sendMessage,
                    highlightColor: Colors.transparent,
                    color: Color(0xFF3A3838),
                  )),
              onChanged: (value) {
                setState(() {
                  _enteredMessage = value;
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
