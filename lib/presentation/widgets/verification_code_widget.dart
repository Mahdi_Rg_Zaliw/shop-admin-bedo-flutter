import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:flutter/material.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';

enum EditingState { editing, waiting, accepted, denied }

class VerificationCodeWidget extends StatefulWidget {
  final Function onStateChanged;
  final ValueChanged<String> onVerificationStringCode;

  VerificationCodeWidget(this.onVerificationStringCode, this.onStateChanged,
      {Key key})
      : super(key: key);

  @override
  VerificationCodeWidgetState createState() => VerificationCodeWidgetState();
}

class VerificationCodeWidgetState extends State<VerificationCodeWidget> {
  final List<FocusNode> _listFocusNode = <FocusNode>[];
  List<TextEditingController> _textControllerList = <TextEditingController>[];
  final _length = 4;
  int _currentIndex = 0;
  EditingState editingState = EditingState.editing;
  String verificationCode = '';

  @override
  void dispose() {
    for (int i = 0; i < _length; i++) {
      _textControllerList[i].dispose();
      _listFocusNode[i].dispose();
    }
    super.dispose();
  }

  @override
  void initState() {
    for (var i = 0; i < _length; i++) {
      _listFocusNode.add(FocusNode());
      var controller = TextEditingController();
      controller.text = ' ';
      _textControllerList.add(controller);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 12),
          child: Text(
            "enter the code we just send to you",
            style: grey14regular,
            textAlign: TextAlign.start,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: _buildListWidget(),
        ),
      ],
    ));
  }

  // Returns the list of the input boxes
  List<Widget> _buildListWidget() {
    List<Widget> listWidget = List();
    for (int index = 0; index < _length; index++) {
      double left = (index == 0) ? 0.0 : 12;
      listWidget.add(Container(
          height: (MediaQuery.of(context).size.width * 0.75) / 4,
          width: (MediaQuery.of(context).size.width * 0.75) / 4,
          decoration: BoxDecoration(
            color: lightGreyBg,
            border: Border.all(
                color: editingState == EditingState.accepted
                    ? primaryMaterialColor
                    : editingState == EditingState.denied
                        ? red
                        : index == _currentIndex
                            ? primaryMaterialColor
                            : lightGreyBg,
                width: 1),
            borderRadius: BorderRadius.circular(10),
          ),
          margin: EdgeInsets.only(left: left),
          child: Center(child: _buildInputItem(index))));
    }
    return listWidget;
  }

  // This is the transparent textField inside of the boxes
  Widget _buildInputItem(int index) {
    return Theme(
      data: ThemeData(),
      child: TextField(
        keyboardType:
            TextInputType.numberWithOptions(signed: false, decimal: false),
        maxLines: 1,
        maxLength: 2,
        controller: _textControllerList[index],
        focusNode: _listFocusNode[index],
        enableInteractiveSelection: false,
        showCursor: false,
        maxLengthEnforced: true,
        autocorrect: false,
        textAlign: TextAlign.center,
        autofocus: true,
        enabled: editingState == EditingState.editing,
        style: editingState == EditingState.editing ||
                editingState == EditingState.waiting
            ? index == _currentIndex
                ? black35medium
                : grey35medium
            : black35medium,
        decoration: InputDecoration(
          border: InputBorder.none,
          focusedBorder: null,
          contentPadding: EdgeInsets.all(14),
          errorMaxLines: 1,
          counterText: "",
        ),
        onTap: () {
          // This code doesn't allow the user to change the current index
          for (int index = 0; index < _length; index++) {
            if (_textControllerList[index].text == ' ') {
              setState(() {
                _currentIndex = index;
              });
              if (editingState != EditingState.denied) {
                FocusScope.of(context).requestFocus(_listFocusNode[index]);
              }
              break;
            }
          }
        },
        onChanged: (value) {
          if (value.length >= 0) {
            setState(() {
              verificationCode += value;
            });
          }
          if (value == '') {
            setState(() {
              verificationCode.substring(
                  verificationCode.length, verificationCode.length - 1);
            });
          }
          print(verificationCode);
          widget.onVerificationStringCode(verificationCode);
          // if user enter the last character of the code this block will be executed.
          if (value.length >= 2 && index == _length - 1) {
            _textControllerList[index].text =
                _textControllerList[index].text[1];
            onCompleted();
          }
          // if user tap on the backspace this block will be executed.
          else if (value == '' && index != 0) {
            _textControllerList[index].text = ' ';
            _prev(index);
          }
          // if user enter in a box except than last box this bloc will be executed.
          else if (value.length == 2) {
            _textControllerList[index].text =
                _textControllerList[index].text[1];
            _next(index);
          }
          // normally this box should not be executed but it is here to avoid possible bugs.
          if (value.length == 1 && value != ' ') {
            _textControllerList[index].text = value;
            _next(index);
          }
        },
      ),
    );
  }

  // Moves the cursor to the next box.
  void _next(int index) {
    if (index != _length - 1) {
      setState(() {
        _currentIndex = index + 1;
      });
      FocusScope.of(context).requestFocus(_listFocusNode[_currentIndex]);
    }
  }

  // Moves the cursor to the next box and removes its content.
  void _prev(int index) {
    if (index > 0) {
      setState(() {
        _currentIndex = index - 1;
      });
      _textControllerList[_currentIndex].text = ' ';
      FocusScope.of(context).requestFocus(_listFocusNode[_currentIndex]);
    }
  }

  // This is the code that is executed when user completes the editing code and
  // changes the state to the editing state.
  void onCompleted() {
    changeEditingState(EditingState.waiting);
    final code = _getInputVerify();
  }

  void changeEditingState(EditingState value) {
    setState(() {
      editingState = value;
    });
    widget.onStateChanged(value);
  }

  // constructs the input code and returns its value
  String _getInputVerify() {
    String verifyCode = "";
    for (var i = 0; i < _length; i++) {
      verifyCode += _textControllerList[i].text;
    }
    return verifyCode;
  }

  void clear() {
    changeEditingState(EditingState.editing);
    _textControllerList.forEach((element) {
      element.text = ' ';
    });
    setState(() {
      _currentIndex = 0;
    });
    FocusScope.of(context).requestFocus(_listFocusNode[0]);
  }

  void setValue(value) {
    //todo after autoComplete feature completed should be implemented
  }
}
