import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/presentation/screens/custom_expansion_tile.dart';
import 'package:bedo_shop_admin/presentation/widgets/clock_picker_mode.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/set_time_restaurant_modal.dart';
import 'package:bedo_shop_admin/utils/time_utils.dart';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class WorkingWeekDays extends StatefulWidget {
  final List<DayWorkingTime> data;
  final Function(List<DayWorkingTime>) onChange;
  const WorkingWeekDays({
    Key key,
    this.data,
    this.onChange,
  }) : super(key: key);

  @override
  _WorkingWeekDaysState createState() => _WorkingWeekDaysState();
}

class _WorkingWeekDaysState extends State<WorkingWeekDays> {
  @override
  void initState() {
    super.initState();
    if (widget.data == null) {
      data = List.generate(
          7,
          (index) => DayWorkingTime(
                dayNumber: index,
              ));
    } else {
      data = widget.data;
    }
  }

  String get selectedDays {
    int selectedDays = 0;
    data.forEach((e) {
      if (e.check == true) {
        selectedDays += 1;
      }
    });
    if (selectedDays == 0) {
      return '';
    } else {
      return selectedDays.toString() + ' selected';
    }
  }

  List<DayWorkingTime> data;

  @override
  Widget build(BuildContext context) {
    return CustomExpansionTile(
      title: Row(
        children: [
          Text(
            'working days ',
            style: black14medium,
          ),
          Text(
            '    $selectedDays',
            style: green14regular.copyWith(fontWeight: FontWeight.bold),
          ),
        ],
      ),
      children: List.generate(
        7,
        (index) {
          final chunk = data[index];
          return WeekDayTile(
            end: chunk.start,
            start: chunk.end,
            check: chunk.check,
            endChanged: (dur) {
              setState(() {
                chunk.start = dur;
                widget.onChange(data);
              });
            },
            startChanged: (dur) {
              setState(() {
                chunk.end = dur;
                widget.onChange(data);
              });
            },
            idx: chunk.dayNumber,
            onChangeCbx: (v) {
              setState(() {
                chunk.check = v;
                widget.onChange(data);
              });
            },
          );
        },
      ),
    );
  }
}

class TimeTitle extends StatelessWidget {
  final String text;
  const TimeTitle({
    this.text,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(9),
      child: Text(
        text,
        style: grey12regular,
        textAlign: TextAlign.center,
      ),
      decoration: BoxDecoration(
        color: Colors.grey.shade300,
        borderRadius: BorderRadius.circular(8),
      ),
    );
  }
}

class DayWorkingTime {
  // String nameOfDay;

  int dayNumber;

//
  int get startDuartionInMin {
    return start.inMinutes;
  }

//
  int get endDuartionInMin {
    return end.inMinutes;
  }

  DayWorkingTime(
      {this.dayNumber,
      this.start = Duration.zero,
      this.end = Duration.zero,
      this.check = false});
//
  Duration start;
  Duration end;
  @override
  String toString() {
    String en = makeVarEnums(dayNumber);
    int ss = start.inMinutes;
    int ee = end.inMinutes;
    return {
      'type': en,
      'from': ss,
      'to': ee,
    }.toString();
  }

  Map toMap() {
    String en = makeVarEnums(dayNumber);
    int ss = start.inMinutes;
    int ee = end.inMinutes;
    return {
      'type': en,
      'from': ss,
      'to': ee,
    };
  }

  bool check;
}

class WeekDayTile extends StatelessWidget {
  final bool check;
  final void Function(bool) onChangeCbx;
  final idx;
  final Duration start;
  final Duration end;
  final Function(Duration) startChanged;
  final Function(Duration) endChanged;

  String get startTime {
    String h = start.inHours.toString().padLeft(2, '0');
    String m = (start.inMinutes % 60).toString().padLeft(2, '0');
    return '$h:$m';
  }

  String get endTime {
    String h = end.inHours.toString().padLeft(2, '0');
    String m = (end.inMinutes % 60).toString().padLeft(2, '0');
    return '$h:$m';
  }

  const WeekDayTile(
      {Key key,
      this.check,
      this.onChangeCbx,
      this.idx,
      this.start,
      this.end,
      this.startChanged,
      this.endChanged})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Checkbox(
          value: check,
          onChanged: onChangeCbx,
        ),
        Expanded(
          flex: 6,
          child: Text(
            DateFormat.EEEE().format(
              DateTime(2000).add(Duration(days: idx)),
            ),
            style: black12regular,
          ),
        ),
        // start.inHours
        // start.inMinutes%60
        Expanded(
          flex: 9,
          child: Text(
            '$startTime - $endTime',
            style: check ? black12regular : grey12regular,
          ),
        ),
        FlatButton(
            onPressed: () {
              if (check) {
                showModalBottomSheet(
                  enableDrag: false,
                  context: context,
                  builder: (context) {
                    return Container(
                        padding: EdgeInsets.all(24),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.all(24).copyWith(bottom: 0),
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 10,
                                    child: TimeTitle(
                                      text: 'starting time', // todo translate
                                    ),
                                  ),
                                  Expanded(flex: 4, child: SizedBox()),
                                  Expanded(
                                    flex: 10,
                                    child: TimeTitle(
                                      text:
                                          'Ending Time time', // todo Translate
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Row(
                              children: [
                                CustomTimePickerCupertino(
                                  onChange: (d) {
                                    startChanged(d);
                                  },
                                ),
                                CustomTimePickerCupertino(
                                  onChange: (d) {
                                    endChanged(d);
                                  },
                                ),
                              ],
                            ),
                            RaisedButton(
                              child: Text('Done'),
                              onPressed: () => Navigator.of(context).pop(),
                            )
                          ],
                        ));
                  },
                );
              }
            },
            child: Text(
              'change',
              style: grey12regular.copyWith(
                  decoration: check ? TextDecoration.underline : null),
            )),
      ],
    );
  }
}
