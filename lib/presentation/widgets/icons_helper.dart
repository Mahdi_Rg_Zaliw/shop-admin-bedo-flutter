import 'package:bedo_shop_admin/design/icons_f.dart';
import 'package:flutter/material.dart';

class Ico {
  final IconData data;
  final String name;

  Ico(this.data, this.name);
}

List<Ico> x = [
  Ico(Iconsf.i01_add_to_menu, 'i01_add_to_menu'),
  Ico(Iconsf.i03_brifcase, 'i03_brifcase'),
  Ico(Iconsf.i04_bike, 'i04_bike'),
  Ico(Iconsf.i05_box, 'i05_box'),
  Ico(Iconsf.i06_brifcase, 'i06_brifcase'),
  Ico(Iconsf.i07_calendar_checked, 'i07_calendar_checked'),
  Ico(Iconsf.i08_calendar_unchecked, 'i08_calendar_unchecked'),
  Ico(Iconsf.i09_camera, 'i09_camera'),
  Ico(Iconsf.i10_chat, 'i10_chat'),
  Ico(Iconsf.i11_chat_selected, 'i11_chat_selected'),
  Ico(Iconsf.i18_delete, 'i18_delete'),
  Ico(Iconsf.i19_discount, 'i19_discount'),
  Ico(Iconsf.i23_fast_food, 'i23_fast_food'),
  Ico(Iconsf.i24_hamburger_menu, 'i24_hamburger_menu'),
  Ico(Iconsf.i25_hamburger_menu_2, 'i25_hamburger_menu_2'),
  Ico(Iconsf.i26_home, 'i26_home'),
  Ico(Iconsf.i28_home_selected, 'i28_home_selected'),
  Ico(Iconsf.i31_location_marker, 'i31_location_marker'),
  Ico(Iconsf.i32_logo, 'i32_logo'),
  Ico(Iconsf.i36_paper_plane, 'i36_paper_plane'),
  Ico(Iconsf.i37_peincil, 'i37_peincil'),
  Ico(Iconsf.i38_pencil_drawing_line, 'i38_pencil_drawing_line'),
  Ico(Iconsf.i40_resturant_location_marker, 'i40_resturant_location_marker'),
  Ico(Iconsf.i41_settings_gear, 'i41_settings_gear'),
  Ico(Iconsf.i42_settings_outline, 'i42_settings_outline'),
  Ico(Iconsf.i43_settings_outline_2, 'i43_settings_outline_2'),
  Ico(Iconsf.i44_square_location_marker, 'i44_square_location_marker'),
  Ico(Iconsf.i45_star, 'i45_star'),
  Ico(Iconsf.i47_star_location_marker, 'i47_star_location_marker'),
  Ico(Iconsf.i48_stats, 'i48_stats'),
  Ico(Iconsf.i49_stats_selected, 'i49_stats_selected'),
  Ico(Iconsf.i50_timer, 'i50_timer'),
  Ico(Iconsf.i51_view_produxt_menue, 'i51_view_produxt_menue'),
  Ico(Iconsf.i52_target, 'i52_target'),
  Ico(Iconsf.i53_coupon, 'i53_coupon'),
  Ico(Iconsf.i54_message_to, 'i54_message_to'),
];

class IconsTestShowCase extends StatelessWidget {
  const IconsTestShowCase({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.all(7),
        child: Column(
          children: [
            Icon(
              x[index].data,
              size: 100,
            ),
            Text(
              x[index].name,
              style: TextStyle(fontSize: 20),
            )
          ],
        ),
      ),
      itemCount: x.length,
    );
  }
}
