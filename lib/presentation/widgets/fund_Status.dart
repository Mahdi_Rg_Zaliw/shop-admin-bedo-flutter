import 'package:bedo_shop_admin/bloc/reports_bloc/reports_bloc.dart';
import 'package:bedo_shop_admin/bloc/reports_bloc/reports_state.dart';
import 'package:bedo_shop_admin/presentation/widgets/fund_status_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FundStatusScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReportsBloc, ReportsState>(builder: (context, state) {
      if (state is ReportsLoaded) {
        return SingleChildScrollView(
          child: state is ReportsLoaded
              ? FundStatusContainer(
                  grey: false,
                  statistics: state.statistics.toMap(),
                )
              : Center(
                  child: Text('Loading status failed'),
                ),
        );
      }
      if (state is ReportsLoading) {
        return Container(
          child: Center(child: CupertinoActivityIndicator()),
        );
      }
      if (state is ReportsLoadingFailed) {
        return Container(
          child: Center(child: Text('Loading status failed')),
        );
      }
      return Container();
    });
  }
}
