import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/widgets/custom_text_field.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/custom_modal_bottom_sheet.dart';
import 'package:flutter/material.dart';

import 'package:bedo_shop_admin/design/icons_f.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/presentation/widgets/comment_widggets/comment_text.dart';
import 'package:bedo_shop_admin/presentation/widgets/comment_widggets/custom_avatar.dart';
import 'package:bedo_shop_admin/utils/time_utils.dart';

class CommentTile extends StatelessWidget {
  final String id;
  final bool support;
  final bool replying;
  final bool waiting;
  final String imageUrl;
  final String personName;
  final String rank;
  final DateTime date;
  final Function(String text, String id) onSubmitReply;
  final String message;
  final Function(String id, String text) onAccepted;
  final Function(String) onRejected;
  final String acceptLable;
  final String rejectLable;
  const CommentTile({
    Key key,
    this.replying = false,
    this.waiting = false,
    this.support = false,
    this.acceptLable,
    this.rejectLable,
    @required this.imageUrl,
    @required this.personName,
    @required this.rank,
    @required this.date,
    @required this.message,
    this.onAccepted,
    this.onRejected,
    this.onSubmitReply,
    this.id,
  })  : assert(imageUrl != null, '\n\n you must inset image url\n\n'),
        assert(
            (!waiting && onAccepted == null && onRejected == null) ||
                (waiting && onAccepted != null && onRejected != null),
            '\n\n  for a waiting commrnt you must provide accept and reject functions\n\n '),
        assert(!(replying && waiting),
            '\n\n a comment can only be in waiting state or in replying state not both at same time\n\n '),
        assert(
            (!waiting && acceptLable == null && rejectLable == null) ||
                (waiting && acceptLable != null && rejectLable != null),
            '\n\n  waitnig comments must have accept or reject lable\n\n '),
        super(key: key);

  CommentTile.waiting({
    @required this.imageUrl,
    @required this.personName,
    @required this.rank,
    @required this.date,
    @required this.message,
    @required this.onAccepted,
    @required this.onRejected,
    this.acceptLable = 'accep and reply',
    this.rejectLable = 'reject',
    this.onSubmitReply,
    this.id,
  })  : this.waiting = true,
        this.support = false,
        this.replying = false;

  CommentTile.approved({
    this.imageUrl = 'https://picsum.photos/250',
    this.personName,
    this.rank,
    this.date,
    this.message,
    this.id,
  })  : this.replying = false,
        this.support = false,
        this.acceptLable = null,
        this.rejectLable = null,
        this.onAccepted = null,
        this.onSubmitReply = null,
        this.onRejected = null,
        this.waiting = false;

  CommentTile.support({
    this.personName,
    this.date,
    this.message,
    this.id,
  })  : this.replying = false,
        this.rank = '4.5',
        this.imageUrl = 'https://picsum.photos/250',
        this.support = true,
        this.acceptLable = null,
        this.rejectLable = null,
        this.onAccepted = null,
        this.onSubmitReply = null,
        this.onRejected = null,
        this.waiting = false;

  CommentTile reply() {
    return CommentTile(
      imageUrl: this.imageUrl,
      personName: this.personName,
      rank: this.rank,
      date: this.date,
      message: this.message,
      replying: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 24, vertical: 12),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (!support)
            CustomAvatar(
              imageUrl: imageUrl,
              radius: 30,
            ),
          Flexible(
            fit: FlexFit.loose,
            child: Container(
              padding: EdgeInsets.only(left: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        flex: 2,
                        child: Text(
                          personName,
                          style: black14medium,
                          softWrap: true,
                          overflow: TextOverflow.visible,
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                          relativeTime(date, context),
                          style: grey12regular,
                          textAlign: TextAlign.end,
                          softWrap: true,
                          overflow: TextOverflow.visible,
                        ),
                      )
                    ],
                  ),
                  if (!support)
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8).copyWith(left: 0),
                          child: Icon(
                            Iconsf.i45_star,
                            color: gold,
                            size: 16,
                          ),
                        ),
                        Text(
                          rank,
                          // .toStringAsFixed(1),
                          style: grey12regular,
                        ),
                      ],
                    ),
                  Container(
                    height: (replying) ? 50 : null,
                    child: (replying)
                        ? SingleChildScrollView(
                            child: CommentText(text: message),
                          )
                        : CommentText(text: message),
                  ),
                  (waiting)
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            FlatButton(
                              splashColor: primaryMaterialColor,
                              onPressed: () async {
                                final _controller = TextEditingController();
                                await buildReplyModalSheet(
                                    context, _controller);
                                onAccepted(id, _controller.text);
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 8),
                                child: Text(
                                  acceptLable ?? S.of(context).confirmAndReply,
                                  style: green12semiBold,
                                ),
                              ),
                            ),
                            FlatButton(
                              splashColor: red,
                              onPressed: () {
                                onRejected(id);
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 8),
                                child: Text(
                                  rejectLable ?? S.of(context).reject,
                                  style: red12semibold,
                                ),
                              ),
                            )
                          ],
                        )
                      : SizedBox(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  buildReplyModalSheet(
      BuildContext context, TextEditingController _controller) async {
    customModalSheet(
      context: context,
      child: Column(
        children: [
          SizedBox(
            height: 24,
          ),
          Text(
            S.of(context).reply,
            style: black24medium,
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            S.of(context).replyingTo,
            style: grey14regular,
          ),
          SizedBox(
            height: 12,
          ),
          this.reply(),
          CustomTextField(
            hintText: S.of(context).writeYourReplyHere,
            controller: _controller,
            maxline: 8,
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(
                    left: 24,
                    right: 24,
                    top: 8,
                    bottom: 16,
                  ),
                  child: RaisedButton(
                    onPressed: () async {
                      await onSubmitReply(_controller.text, id);
                      Navigator.of(context).pop();
                      // _controller.dispose();
                    },
                    child: Text(S.of(context).submitReply),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
