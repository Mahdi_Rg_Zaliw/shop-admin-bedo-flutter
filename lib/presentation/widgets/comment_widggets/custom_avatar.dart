import 'package:flutter/material.dart';

class CustomAvatar extends StatelessWidget {
  final String imageUrl;
  final double radius;
  const CustomAvatar({
    Key key,
    this.radius = 50,
    this.imageUrl = 'https://picsum.photos/250',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 2 * radius,
      height: 2 * radius,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(radius),
        child: FadeInImage(
          fit: BoxFit.cover,
          placeholder:
              AssetImage('assets/images/avatar_image_place_holder.jpg'),
          image: NetworkImage(imageUrl ?? ''),
        ),
      ),
    );
  }
}
