import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/presentation/widgets/comment_widggets/comment_text.dart';
import 'package:bedo_shop_admin/utils/time_utils.dart';
import 'package:flutter/material.dart';

class InboxMessage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24).copyWith(top: 20),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 2,
                child: Text(
                  'personName',
                  style: black14medium,
                  softWrap: true,
                  overflow: TextOverflow.visible,
                ),
              ),
              Expanded(
                flex: 1,
                child: Text(
                  relativeTime(DateTime.now(),context),
                  style: grey12regular,
                  textAlign: TextAlign.end,
                  softWrap: true,
                  overflow: TextOverflow.visible,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 8,
          ),
          CommentText(
              text:
                  'hello man this is a message from ther support team in this week we launching the new things')
        ],
      ),
    );
  }
}
