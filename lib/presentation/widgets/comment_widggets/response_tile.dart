import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/widgets/comment_widggets/comment_text.dart';
import 'package:bedo_shop_admin/presentation/widgets/colored_border.dart';
import 'package:flutter/material.dart';

import '../../../design/theme.dart';

enum ResponseType { comment, inbox }

class ResponceTile extends StatelessWidget {
  final String reply;
  final String lable;
  final ResponseType type;

  const ResponceTile(
      {Key key, this.reply, this.lable, this.type = ResponseType.comment})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 24, vertical: 12).copyWith(
              top: 0,
              right: type == ResponseType.inbox ? 24 : 12,
              bottom: type == ResponseType.inbox ? 0 : null),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              type == ResponseType.comment
                  ? SizedBox(
                      width: 50,
                      height: 60,
                    )
                  : SizedBox(),
              Flexible(
                fit: FlexFit.loose,
                child: Container(
                  padding: EdgeInsets.only(
                      left: (type == ResponseType.inbox) ? 0 : 12),
                  child: Stack(
                    children: [
                      ColoredBorder(
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 12),
                        margin: EdgeInsets.only(top: 12),
                        child: CommentText(
                          text: reply,
                          color: textColor,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 15),
                        color: Colors.white,
                        padding: EdgeInsets.all(7),
                        child: Text(
                          lable ?? S.of(context).yourResponce,
                          style: green12semiBold,
                          softWrap: true,
                          overflow: TextOverflow.visible,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        type == ResponseType.inbox
            ? Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Divider(
                    // color: lightGreyBg,
                    ),
              )
            : SizedBox(),
      ],
    );
  }
}
