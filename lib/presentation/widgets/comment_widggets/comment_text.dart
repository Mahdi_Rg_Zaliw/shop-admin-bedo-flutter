import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:flutter/material.dart';

class CommentText extends StatelessWidget {
  final String text;
  final TextStyle style;
  final Color color;
  final TextOverflow overflow;
  final int maxLines;
  const CommentText({
    Key key,
    this.style,
    this.color,
    @required this.text,
    this.maxLines,
    this.overflow,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text.padRight(200) ?? 'no text to show',
      maxLines: maxLines,
      overflow: overflow,
      style: style ??
          grey14regular.copyWith(height: 1.5, color: color ?? titleGrey),
      // textAlign: TextAlign.justify,
    );
  }
}
