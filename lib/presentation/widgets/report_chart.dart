import 'package:bedo_shop_admin/design/styles.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ReportChart extends StatefulWidget {
  final List<num> data;
  final List<DateTime> dates;
  final String title;
  final List<Color> colors;

  ReportChart({this.data, this.dates, this.title, this.colors});

  @override
  _ReportChartState createState() => _ReportChartState();
}

class _ReportChartState extends State<ReportChart> {
  var _chartData = List<BarChartGroupData>();

  @override
  Widget build(BuildContext context) {
    for (int i = 0; i < widget.data.length; i++) {
      _chartData.add(BarChartGroupData(
        x: i,
        barRods: [
          BarChartRodData(
              y: widget.data[i].toDouble(),
              width: 18,
              colors: widget.colors,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(2), topRight: Radius.circular(2))),
        ],
      ));
    }

    double getChartWidth(BuildContext context, int dataCount) {
      double width = MediaQuery.of(context).size.width * 0.95;
      if (dataCount * 20 > width) {
        return dataCount * 20.toDouble();
      } else {
        return width;
      }
    }

    double maxY() {
      double max = 0;
      widget.data.forEach((element) {
        if (element > max) {
          max = element.toDouble();
        }
      });
      return max + 5;
    }

    String getTitles(double value) {
      int index = value.toInt();
      if (widget.dates[index].day == 1) {
        String title = widget.dates[index].month.toString() +
            '/' +
            widget.dates[index].day.toString();
        return title;
      } else if(widget.dates[index].day % 2 == 0){

      }
      return widget.dates[index].day.toString();
    }

    getTextStyles(value) {
      if (value % 2 == 0) {
        return const TextStyle(color: Colors.black, fontSize: 12);
      }
      return const TextStyle(color: Colors.black45, fontSize: 12);
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical:10.0, horizontal: 30),
          child: Text(widget.title, style: black18medium,),
        ),
        SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Container(
              width: getChartWidth(context, widget.data.length),
              margin: EdgeInsets.only(top: 5, right: 20),
              child: BarChart(
                BarChartData(
                  barGroups: _chartData,
                  backgroundColor: Colors.grey.shade50,
                  maxY: maxY(),
                  gridData: FlGridData(
                    horizontalInterval: 5
                  ),
                  titlesData: FlTitlesData(
                    show: true,
                    bottomTitles: SideTitles(
                      showTitles: true,
                      getTextStyles: (value) => getTextStyles(value),
                      getTitles: (double value) => getTitles(value),
                    ),
                  ),
                ),
              ),
            )),
      ],
    );
  }
}
