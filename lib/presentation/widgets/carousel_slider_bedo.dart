import 'package:bedo_shop_admin/design/theme.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CarouselSliderBedo extends StatefulWidget {
  int initialIndex;
  final List sliderList;

  CarouselSliderBedo({
    this.initialIndex = 0,
    this.sliderList = const [
      'https://image.freepik.com/free-vector/dark-abstract-tech-background_53876-90630.jpg',
      'https://image.freepik.com/free-vector/gradient-geometric-background_52683-54199.jpg',
      'https://image.freepik.com/free-photo/web-design-concept-3d-rendering_72104-3665.jpg',
      'https://image.freepik.com/free-vector/gradient- geometric-background_52683-54352.jpg',
    ],
  });

  @override
  _CarouselSliderBedoState createState() => _CarouselSliderBedoState();
}

class _CarouselSliderBedoState extends State<CarouselSliderBedo> {
  CarouselController _carouselController = CarouselController();
  int idx;
  @override
  void initState() {
    super.initState();

    idx = widget.initialIndex;
  }

  Widget arrowChangeSlider(IconData icon) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15),
      width: 25,
      height: 25,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.white,
      ),
      child: Padding(
        child: Icon(
          icon,
          color: colorIcon,
          size: 14,
        ),
        padding: EdgeInsets.symmetric(horizontal: 8),
      ),
    );
  }

  Widget dottedCarousel(Color color) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 3),
      width: 7,
      height: 7,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: color,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CarouselSlider(
                  carouselController: _carouselController,
                  options: CarouselOptions(
                    initialPage: idx,
                    onPageChanged: (val, i) {
                      setState(() {
                        idx = val;
                      });
                    },
                    aspectRatio: 2.5,
                    autoPlay: false,
                    enlargeCenterPage: true,
                  ),
                  items: widget.sliderList
                      .map(
                        (e) => Container(
                          width: MediaQuery.of(context).size.width / 1.2,
                          height: MediaQuery.of(context).size.height / 3.6,
                          decoration: BoxDecoration(
                            color: Color(0xffcdbaab),
                            borderRadius: BorderRadius.circular(8),
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(e),
                            ),
                          ),
                        ),
                      )
                      .toList(),
                ),
              ],
            ),
          ),
          Align(
            child: GestureDetector(
              child: arrowChangeSlider(Icons.arrow_back_ios),
              onTap: () => _carouselController.previousPage(
                duration: Duration(milliseconds: 500),
                curve: Curves.linear,
              ),
            ),
            alignment: Alignment.centerLeft,
          ),
          Align(
            child: GestureDetector(
              child: arrowChangeSlider(Icons.arrow_forward_ios),
              onTap: () => _carouselController.nextPage(
                duration: Duration(milliseconds: 500),
                curve: Curves.linear,
              ),
            ),
            alignment: Alignment.centerRight,
          ),
          Container(
            margin: EdgeInsets.only(
              top: (MediaQuery.of(context).size.height / 3).toDouble(),
            ),
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: widget.sliderList
                  .map(
                    (e) => idx == widget.sliderList.indexOf(e)
                        ? dottedCarousel(Colors.orange)
                        : dottedCarousel(Color(0xffbababa)),
                  )
                  .toList(),
            ),
          ),
        ],
      ),
    );
  }
}
