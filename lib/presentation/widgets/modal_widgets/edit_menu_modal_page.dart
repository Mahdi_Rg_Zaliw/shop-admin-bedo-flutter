import 'package:bedo_shop_admin/bloc/menu_bloc/menu_bloc.dart';
import 'package:bedo_shop_admin/bloc/menu_bloc/menu_event.dart';
import 'package:bedo_shop_admin/bloc/menu_bloc/menu_state.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/screens/edit_shop.dart';
import 'package:bedo_shop_admin/presentation/widgets/custom_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// ignore: must_be_immutable
class EditMenuModalPage extends StatelessWidget {
  final String updateNameMenu;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final String updateID;
  int menuIndex;
  String newUpdateNameMenu;

  final List menuData;
  EditMenuModalPage(
      {this.menuData, this.updateID, this.menuIndex, this.updateNameMenu});

  void dataFormClear() {
    formKey.currentState.reset();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(24),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          AppBar(),
          Text(
            'thing ' + S.of(context).menu,
            textAlign: TextAlign.center,
            style: black24medium,
          ),
          Text('\n 4 fruits',
              textAlign: TextAlign.center, style: grey14regular),
          Expanded(child: SizedBox()),
          StatefulBuilder(
            builder: (BuildContext context, setState) {
              return Form(
                key: formKey,
                child: CustomTextField(
                  initialValue: updateNameMenu,
                  validator: (String value) {
                    if (value.trim().isEmpty) {
                      return "Edit Text Field Cannot Be Empty !";
                    }
                    return null;
                  },
                  paddingHorizontal: 0,
                  onSaved: (String value) {
                    setState(() {
                      newUpdateNameMenu = value;
                    });
                  },
                ),
              );
            },
          ),
          Expanded(child: SizedBox()),
          BlocConsumer<MenuBloc, MenuState>(
            listener: (context, state) {
              if (state is MenuLoading) {
                print('Loading ...');
              }
              if (state is MenuExceptionState) {
                dataFormClear();
                showDialog(
                    context: context,
                    builder: (context) => CustomDialogFailed(
                          title: 'Failed: ${state.clientErrors}',
                        ));
                print('Failed : ${state.clientErrors}');
              }
              if (state is ShopMenuUpdatedSuccess) {
                print('Successfuly Updated');
                dataFormClear();
                Navigator.pop(context);
              }
            },
            builder: (context, state) {
              return RaisedButton(
                onPressed: () {
                  formKey.currentState.save();
                  UpdateMenuEvent updateMenuEvent = UpdateMenuEvent(
                    id: updateID,
                    data: {
                      "subMenus": menuData.map(
                        (e) {
                          if (menuData.indexOf(e) != menuIndex) {
                            return {
                              "name": [
                                {"lang": "en", "value": e.titleEN},
                                {"lang": "az", "value": e.titleAZ},
                                {"lang": "ru", "value": e.titleRU},
                              ],
                              "products": [
                                "6066b94ec6db1aea41ec56b7",
                              ],
                            };
                          } else {
                            return {
                              "name": [
                                {"lang": "en", "value": newUpdateNameMenu},
                                {"lang": "az", "value": e.titleAZ},
                                {"lang": "ru", "value": e.titleRU},
                              ],
                              "products": [],
                            };
                          }
                        },
                      ).toList()
                    },
                  );

                  if (formKey.currentState.validate()) {
                    context.read<MenuBloc>().add(updateMenuEvent);
                  } else {
                    print('Form Not Vallidate');
                  }
                },
                child: Text(S.of(context).edit),
              );
            },
          )
        ],
      ),
    );
  }
}

class CustomDialogFailed extends StatelessWidget {
  final String title;
  const CustomDialogFailed({
    Key key,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            '$title',
            style: red14regular,
          ),
          SizedBox(
            height: 8,
          ),
          // Icon(
          //   Icons.check,
          //   color: primaryMaterialColor,
          //   size: 25,
          // ),
        ],
      ),
      actions: [
        FlatButton(
          color: Colors.red,
          child: Text('ok'),
          onPressed: () => Navigator.of(context).pop(),
        )
      ],
    );
  }
}
