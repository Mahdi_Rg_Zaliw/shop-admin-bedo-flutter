import 'package:bedo_shop_admin/bloc/menu_bloc/menu_bloc.dart';
import 'package:bedo_shop_admin/bloc/menu_bloc/menu_event.dart';
import 'package:bedo_shop_admin/bloc/menu_bloc/menu_state.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/model/menu_model.dart';
import 'package:bedo_shop_admin/presentation/widgets/custom_text_field.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/edit_menu_modal_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// ignore: must_be_immutable
class AddMenuModalPage extends StatelessWidget {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final String updateID;
  final VoidCallback onCancel;
  Map<String, dynamic> formData = {
    "addNameMenuEN": '',
    "addNameMenuAZ": '',
    "addNameMenuRU": '',
  };

  final List menuData;
  AddMenuModalPage({this.menuData, this.updateID, this.onCancel});

  void dataFormClear() {
    formKey.currentState.reset();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(24),
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            AppBar(
              automaticallyImplyLeading: false,
              leading: IconButton(
                splashRadius: 22,
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
            Text(
              'thing add',
              textAlign: TextAlign.center,
              style: black24medium,
            ),
            Text('\n 4 fruits',
                textAlign: TextAlign.center, style: grey14regular),
            Expanded(child: SizedBox()),
            StatefulBuilder(
              builder: (BuildContext context, setState) {
                return Form(
                  key: formKey,
                  child: Expanded(
                    flex: 6,
                    child: ListView(
                      children: [
                        CustomTextField(
                          hintText: 'Add Text English',
                          isRequired: true,
                          validator: (String value) {
                            if (value.trim().isEmpty) {
                              return "Add Text Field English Cannot Be Empty !";
                            }
                            return null;
                          },
                          paddingHorizontal: 0,
                          onSaved: (String value) {
                            setState(() {
                              formData['addNameMenuEN'] = value;
                            });
                          },
                        ),
                        CustomTextField(
                          hintText: 'Add Text Azari',
                          isRequired: true,
                          validator: (String value) {
                            if (value.trim().isEmpty) {
                              return "Add Text Field Azari Cannot Be Empty !";
                            }
                            return null;
                          },
                          paddingHorizontal: 0,
                          onSaved: (String value) {
                            setState(() {
                              formData['addNameMenuAZ'] = value;
                            });
                          },
                        ),
                        CustomTextField(
                          hintText: 'Add Text Russian',
                          isRequired: true,
                          validator: (String value) {
                            if (value.trim().isEmpty) {
                              return "Add Text Field Russian Cannot Be Empty !";
                            }
                            return null;
                          },
                          paddingHorizontal: 0,
                          onSaved: (String value) {
                            setState(() {
                              formData['addNameMenuRU'] = value;
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
            Expanded(child: SizedBox()),
            BlocConsumer<MenuBloc, MenuState>(
              listener: (context, state) {
                if (state is MenuLoading) {
                  print('Loading ...');
                }
                if (state is MenuExceptionState) {
                  showDialog(
                      context: context,
                      builder: (context) => CustomDialogFailed(
                            title: 'Failed: ${state.clientErrors}',
                          ));
                  print('Failed : ${state.clientErrors}');
                  menuData.removeLast();

                  // dataFormClear();
                }
                if (state is ShopMenuUpdatedSuccess) {
                  print('Successfuly Added To Menu');
                  // dataFormClear();
                  Navigator.pop(context);
                }
              },
              builder: (context, state) {
                return RaisedButton(
                  onPressed: () {
                    formKey.currentState.save();

                    print(formData['addNameMenuEN']);
                    print(formData['addNameMenuAZ']);
                    print(formData['addNameMenuRU']);
                    if (formKey.currentState.validate()) {
                      menuData.add(
                        Menu.fromJson(
                          {
                            "name": [
                              {
                                "lang": "en",
                                "value": formData['addNameMenuEN']
                              },
                              {
                                "lang": "az",
                                "value": formData['addNameMenuAZ']
                              },
                              {
                                "lang": "ru",
                                "value": formData['addNameMenuRU']
                              },
                            ],
                            "products": [],
                          },
                        ),
                      );
                    }

                    UpdateMenuEvent updateMenuEvent = UpdateMenuEvent(
                      id: updateID,
                      data: {
                        "subMenus": menuData.map(
                          (e) {
                            return {
                              "name": [
                                {"lang": "en", "value": e.titleEN},
                                {"lang": "az", "value": e.titleAZ},
                                {"lang": "ru", "value": e.titleRU},
                              ],
                              "products": [],
                            };
                          },
                        ).toList()
                      },
                    );

                    if (formKey.currentState.validate()) {
                      context.read<MenuBloc>().add(updateMenuEvent);
                    } else {
                      print('Form Not Vallidate');
                    }
                  },
                  child: Text('Add'),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
