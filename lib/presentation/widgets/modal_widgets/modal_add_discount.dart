import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../../design/styles.dart';
import '../custom_text_field.dart';
import 'custom_modal_bottom_sheet.dart';

class ModalAddDiscount extends StatelessWidget {
  final modalNav = GlobalKey<NavigatorState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Map<String, String> _formData = {
    'discountName': '',
    'Amount': '',
  };
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: RaisedButton(
          onPressed: () {
            customModalSheet(
              keyboardVisibleCoveringHeight:
                  MediaQuery.of(context).size.height * 0.13,
              context: context,
              height: MediaQuery.of(context).size.height * 0.75,
              child: Padding(
                padding: const EdgeInsets.all(24),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "Add Discount",
                          style: black24semibold,
                        ),
                        Text(
                          "Add new discount code for",
                          style: grey14regular,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Form(
                          key: _formKey,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 15),
                                child: CustomTextField(
                                  title: "Discount Name",
                                  hintText: "write a name for the promotion",
                                  style: black14regular,
                                  isRequired: true,
                                  validator: (String value) {
                                    if (value.isNotEmpty) {
                                      return null;
                                    }
                                    return "please enter name for the discount";
                                  },
                                  onSaved: (String value) {
                                    _formData['discountName'] = value;
                                  },
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 15),
                                child: CustomTextField(
                                  formatters: [
                                    // FilteringTextInputFormatter.allow(
                                    //     RegExp(r'^d$')),
                                  ],
                                  keyboardType: TextInputType.number,
                                  suffixIcon: Icon(
                                    FontAwesomeIcons.percent,
                                    size: 20,
                                  ),
                                  title: "Discount Amount",
                                  hintText: "Enter the Discount amount",
                                  controller: _passwordController,
                                  style: black14regular,
                                  isRequired: true,
                                  validator: (String value) {
                                    if (value.isEmpty) {
                                      return "please enter a discount amount ";
                                    }

                                    if (!value.isValidPercent()) {
                                      return " please enter a number with two decimal points";
                                    }

                                    if (value.isValidPercent() &&
                                        (double.parse(value) > 100 ||
                                            double.parse(value) == 0)) {
                                      return " please enter a number in between 100 and 0 ";
                                    }
                                    return null;
                                  },
                                  onSaved: (String value) {
                                    _formData['Amount'] =
                                        double.parse(value).toStringAsFixed(2);
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: RaisedButton(
                              child: Text(
                                'Add Discount',
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: _submit,
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: RaisedButton(
                              color: Colors.grey[100],
                              child: Text(
                                'close',
                                style: TextStyle(color: Colors.black),
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          )
                        ]),
                  ],
                ),
              ),
            );
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'show discount \n modal',
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _submit() {
    if (!_formKey.currentState.validate()) {
      // Invalid!
      return null;
    }

    if (_formKey.currentState.validate()) {
      // Invalid!
      _formKey.currentState.save();

      print(_formData);
    }
  }
}
