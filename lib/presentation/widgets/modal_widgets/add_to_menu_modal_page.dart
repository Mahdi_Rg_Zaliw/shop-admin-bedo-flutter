import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/widgets/checkbox_grid.dart';
import 'package:flutter/material.dart';

class AddToAMenuModalPage extends StatelessWidget {
  const AddToAMenuModalPage({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(24),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          AppBar(),
          Text(
            S.of(context).addToAMenu,
            textAlign: TextAlign.center,
            style: black24medium,
          ),
          Text('\n'+S.of(context).youHave+ '10'+ S.of(context).menus,
              textAlign: TextAlign.center, style: grey14regular),
          Expanded(
            child: CheckboxGrid(
              // title: 'null',
              options: [
                'menu one',
                'm two',
                'me four',
                'm three',
                ' we are',
                'here',
                'we can',
                'so we can',
                'so go',
                ' mano',
                'sss',
                'vvv',
                'ioprion'
              ],
              scroll: true,
              crossAxisCount: 2,
            ),
          ),
          RaisedButton(
            onPressed: () {},
            child: Text(S.of(context).edit),
          )
        ],
      ),
    );
  }
}
