import 'package:flutter/material.dart';

class ModalHomePage extends StatelessWidget {
  final GlobalKey<NavigatorState> navKey;
  final Widget Function(GlobalKey<NavigatorState>) childBuilder;
  const ModalHomePage({
    Key key,
    this.navKey,
    this.childBuilder,
  })  : assert(navKey != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Navigator(
        key: navKey,
        // initialRoute: TabNavigatorRoutes.root,
        onGenerateRoute: (_) {
          return MaterialPageRoute(
            builder: (context) => Center(
              child: childBuilder(navKey),
            ),
          );
        }
        // ModalPage(),
        );
  }
}
