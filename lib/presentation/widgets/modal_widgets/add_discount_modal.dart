import 'package:bedo_shop_admin/bloc/discount_bloc/discount_bloc.dart';
import 'package:bedo_shop_admin/bloc/discount_bloc/discount_events.dart';
import 'package:bedo_shop_admin/bloc/discount_bloc/discount_states.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/models/discount.dart';
import 'package:bedo_shop_admin/presentation/widgets/custom_text_field.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/custom_modal_bottom_sheet.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';

Future addDiscountModal(
    {BuildContext context, bool update = false, Discount updatingDiscount}) {
  String promotionCode = updatingDiscount?.promotionCode;
  double percent = updatingDiscount?.percent;
  DateTime start;
  DateTime end;

  TextEditingController _discountTitleController =
      TextEditingController(text: updatingDiscount?.promotionCode);
  TextEditingController _discountAmountController = TextEditingController(
      text: updatingDiscount != null
          ? updatingDiscount?.percent.toString()
          : null);
  GlobalKey<FormState> _formKey = GlobalKey();
  Discount discount;

  void submit(BuildContext context) {
    if (_formKey.currentState.validate()) {
      if (start != null) {
        if (end != null) {
          if (start.isBefore(end)) {
            if (update) {
              discount = Discount(
                  startDate: start,
                  endDate: end,
                  id: updatingDiscount.id,
                  percent: percent,
                  isExpired: updatingDiscount.isExpired,
                  promotionCode: promotionCode);
              BlocProvider.of<DiscountBloc>(context, listen: false)
                  .add(MutateDiscount(discount: discount));
            } else {
              discount = Discount(
                  promotionCode: promotionCode,
                  startDate: start,
                  endDate: end,
                  percent: percent,
                  isExpired: false);
              BlocProvider.of<DiscountBloc>(context, listen: false)
                  .add(CreateDiscount(discount));
            }
          }
        }
      }
    }

    return;
  }

  return customModalSheet(
    keyboardVisibleCoveringHeight: null,
    context: context,
    height: MediaQuery.of(context).size.height < 550
        ? MediaQuery.of(context).size.height
        : MediaQuery.of(context).size.height < 700
            ? MediaQuery.of(context).size.height * 0.8
            : MediaQuery.of(context).size.height * 0.7,
    child:

        // LayoutBuilder(
        //   builder: (context, constraints) =>
        Padding(
      padding: const EdgeInsets.all(8.0).copyWith(
          top: 24,
          bottom: (MediaQuery.of(context).viewInsets.bottom > 0)
              ? MediaQuery.of(context).size.height * 0.4
              : 24),
      child: Container(
        height: MediaQuery.of(context).size.height < 550
            ? MediaQuery.of(context).size.height
            : MediaQuery.of(context).size.height < 700
                ? MediaQuery.of(context).size.height * 0.8
                : MediaQuery.of(context).size.height * 0.7,
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.all(25),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Icon(
                        Icons.close,
                        color: Colors.transparent,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            update
                                ? S.of(context).editDiscount
                                : S.of(context).addDiscount,
                            style: black24medium,
                          ),
                          Text(
                            update
                                ? S.of(context).editThisDiscount
                                : S.of(context).addNewDiscountCode,
                            style: grey14regular,
                          )
                        ],
                      ),
                      GestureDetector(
                        child: Icon(
                          Icons.close,
                          color: titleGrey,
                        ),
                        onTap: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      bottom: 30, top: 10, left: 10, right: 10),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        CustomTextField(
                          title: S.of(context).discountTitle,
                          hintText: S.of(context).discountTitleHint,
                          isRequired: true,
                          // maxline: 1,
                          isMultiLine: false,
                          controller: _discountTitleController,
                          onChanged: (value) {
                            promotionCode = value.trim();
                          },
                          validator: (value) {
                            if (value.trim().isEmpty) {
                              return S
                                  .of(context)
                                  .pleaseEnterNameForTheDiscount;
                            } else if (value.trim().length < 3) {
                              return S.of(context).tooShortDiscountTitle;
                            } else if (value.trim().length > 16) {
                              return S.of(context).tooLongDiscountTitle;
                            }
                            return null;
                          },
                        ),
                        CustomTextField(
                          title: S.of(context).discountAmount,
                          hintText: S.of(context).enterTheDiscountAmount,
                          suffixIcon: Icon(
                            FontAwesomeIcons.percent,
                            color: iconColor,
                            size: 20,
                          ),
                          style: black14regular,
                          controller: _discountAmountController,
                          onChanged: (value) {
                            percent = double.parse(value);
                          },
                          isRequired: true,
                          validator: (String value) {
                            if (value.isEmpty) {
                              return S.of(context).pleaseEnterADiscountAmount;
                            }

                            if (!value.isValidPercent()) {
                              return S
                                  .of(context)
                                  .pleaseEnterANumberWithTwoDecimalPoints;
                            }

                            if (value.isValidPercent() &&
                                (double.parse(value) > 100 ||
                                    double.parse(value) == 0)) {
                              return S
                                  .of(context)
                                  .pleaseEnterANumberInBetween100And0;
                            }
                            return null;
                          },
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            DateField(
                              context: context,
                              start: true,
                              onSaved: (DateTime value) {
                                start = value;
                              },
                              dateTime: updatingDiscount != null
                                  ? updatingDiscount.startDate
                                  : null,
                            ),
                            DateField(
                              context: context,
                              start: false,
                              onSaved: (DateTime value) {
                                end = value;
                              },
                              dateTime: updatingDiscount != null
                                  ? updatingDiscount.endDate
                                  : null,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Flexible(child: Container()),
                Center(
                  child: Column(
                    children: [
                      Container(
                        width: double.infinity,
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.06,
                            vertical: 10),
                        child: BlocListener<DiscountBloc, DiscountsState>(
                          listenWhen: (prev, currentState) {
                            if (prev is DiscountRequestSent) {
                              return currentState is DiscountsLoading;
                            }
                            return false;
                          },
                          listener: (ctx, _) {
                            Navigator.of(ctx).pop();
                          },
                          child: RaisedButton(
                            child: Text(
                              update
                                  ? S.of(context).editDiscount
                                  : S.of(context).addDiscountCode,
                            ),
                            elevation: 0,
                            highlightElevation: 1,
                            onPressed: () => submit(context),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    ),
    // ),
  );
}

class DateField extends StatefulWidget {
  final BuildContext context;
  final bool start;
  final Function onSaved;
  final DateTime dateTime;

  DateField({
    this.context,
    this.start,
    this.onSaved,
    this.dateTime,
  });

  @override
  _DateFieldState createState() => _DateFieldState();
}

class _DateFieldState extends State<DateField> {
  DateTime date;
  TextEditingController _dateController = TextEditingController();

  initState() {
    super.initState();
    date = widget.dateTime;
    if (date != null) _dateController.text = dateToString(timeSetter(date));
    if (date != null) widget.onSaved(date);
  }

  String dateToString(DateTime date) {
    String result = "";
    result += date.day.toString();
    result += "/";
    result += date.month.toString();
    result += "/";
    result += date.year.toString();
    return result;
  }

  DateTime timeSetter(DateTime date) {
    return DateTime(
        date.year, date.month, widget.start ? date.day : date.day + 1, 0, 0, 0);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          left: widget.start ? 25 : 5, right: widget.start ? 5 : 25),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Text(
              widget.start ? S.of(context).startDate : S.of(context).endDate,
              style: grey12regular,
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.37,
            child: TextFormField(
              controller: _dateController,
              decoration: InputDecoration(
                hintText: widget.start
                    ? S.of(context).startDate
                    : S.of(context).endDate,
                suffixIcon: (MediaQuery.of(context).size.width > 350)
                    ? Icon(
                        FontAwesomeIcons.solidCalendarAlt,
                        color: hintTextColor,
                        size: 20,
                      )
                    : null,
              ),
              readOnly: true,
              onTap: () => showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime.now(),
                  lastDate: DateTime(
                    DateTime.now().year + 100,
                  )).then((pickedDate) {
                if (pickedDate == null) {
                  return;
                } else {
                  final value = dateToString(timeSetter(pickedDate));
                  _dateController.text = value;
                  date = timeSetter(pickedDate);
                }
                widget.onSaved(date);
              }),
              validator: (value) {
                if (value.isEmpty) {
                  return widget.start
                      ? S.of(context).selectStartDate
                      : S.of(context).selectEndDate;
                }
                return null;
              },
            ),
          )
        ],
      ),
    );
  }
}
