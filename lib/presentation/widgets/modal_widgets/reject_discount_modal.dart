import 'package:bedo_shop_admin/bloc/orders_bloc/orders_bloc.dart';
import 'package:bedo_shop_admin/bloc/orders_bloc/orders_event.dart';
import 'package:bedo_shop_admin/bloc/orders_bloc/orders_state.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/model/order.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RejectOrderModal extends StatefulWidget {
  final Order order;

  RejectOrderModal(this.order);

  @override
  _RejectOrderModalState createState() => _RejectOrderModalState();
}

class _RejectOrderModalState extends State<RejectOrderModal> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                icon: Icon(Icons.arrow_back_rounded),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              Text(
                S.of(context).reject,
                style: black24medium,
              ),
              IconButton(
                icon: Icon(
                  Icons.arrow_back_rounded,
                  color: Colors.transparent,
                ),
                onPressed: (){},),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Text(
            S.of(context).rejectOrderConfirmation,
            style: grey14regular,
          ),
        ),
        Expanded(child: Container()),
        BlocConsumer(
          cubit: BlocProvider.of<OrdersBloc>(context),
          listenWhen: (prev, now) {
            return prev is OrderRejectSent && now is OrderRejectDone;
          },
          listener: (ctx, state) {
            Navigator.of(context).pop();
          },
          builder: (context, state) => Container(
            margin: EdgeInsets.only(top: 20),
            width: MediaQuery.of(context).size.width * 0.9,
            child: RaisedButton(
                child: state is OrderRejectSent
                    ? CupertinoActivityIndicator()
                    : Text(
                  S.of(context).yesRejectIt,
                ),
                color: red,
                elevation: 0,
                highlightElevation: 0,
                onPressed: () {
                  BlocProvider.of<OrdersBloc>(context)
                      .add(RejectOrder(widget.order));
                }),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 20, bottom: 20),
          width: MediaQuery.of(context).size.width * 0.9,
          child: RaisedButton(
              child: Text(
                S.of(context).cancel,
                style: grey14medium,
              ),
              color: lightGreyBg,
              elevation: 0,
              highlightElevation: 0,
              onPressed: () {
                BlocProvider.of<OrdersBloc>(context).add(LoadPendingOrders());
                Navigator.of(context).pop();
              }),
        )
      ],
    );
  }
}
