import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/presentation/widgets/radio_button_bedo.dart';
import 'package:flutter/material.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';

Future timeRangeModal({
  BuildContext context,
  Function onChanged,
  int index,
}) {
  var choices = [
    S.of(context).today,
    S.of(context).last3Days,
    S.of(context).last7Days,
    'Last 30 days',
    'All the time'
  ];
  return showModalBottomSheet(
    isScrollControlled: false,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(24),
        topRight: Radius.circular(24),
      ),
    ),
    context: context,
    builder: (context) {
      return Column(
        children: [
          Padding(
            padding: EdgeInsets.all(25),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  S.of(context).selectTimeRange,
                  style: black24medium,
                ),
                GestureDetector(
                  child: Icon(Icons.close),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          ),
          RadioButtonBedo(
            radioValueIndex: index,
            radioButtonList: choices,
            direction: Axis.vertical,
            onChanged: (String value) {
              DateTime choice;
              if (value == S.of(context).today) choice = DateTime.now();
              if (value == S.of(context).last3Days)
                choice = DateTime(DateTime.now().year, DateTime.now().month,
                    DateTime.now().day - 2);
              if (value == S.of(context).last7Days)
                choice = DateTime(DateTime.now().year, DateTime.now().month,
                    DateTime.now().day - 6);
              if (value == 'Last 30 days')
                choice = DateTime(DateTime.now().year, DateTime.now().month - 1,
                    DateTime.now().day);
              if (value == 'All the time') choice = null;
              int index = choices.indexOf(value);
              onChanged(choice, value, index);
              Navigator.of(context).pop();
            },
          )
        ],
      );
    },
  );
}
