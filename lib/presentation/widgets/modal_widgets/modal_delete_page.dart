import 'package:bedo_shop_admin/bloc/menu_bloc/menu_bloc.dart';
import 'package:bedo_shop_admin/bloc/menu_bloc/menu_event.dart';
import 'package:bedo_shop_admin/bloc/menu_bloc/menu_state.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/screens/edit_shop.dart';
import 'package:bedo_shop_admin/presentation/widgets/round_botton.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ModalDeletePage extends StatelessWidget {
  final String title;
  final Function onCancel;
  final Function onDelete;

  final List menuData;
  final int menuIndex;
  String updateID;

  ModalDeletePage({
    Key key,
    this.title,
    this.onCancel,
    this.onDelete,
    this.menuData,
    this.menuIndex,
    this.updateID,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(24),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          AppBar(),
          Text(
            S.of(context).deleteMenu,
            textAlign: TextAlign.center,
            style: black24medium,
          ),
          Expanded(child: SizedBox()),
          Text(title ?? S.of(context).areYouSureYouWantToDeleteMenu,
              textAlign: TextAlign.center, style: grey14regular),
          Expanded(child: SizedBox()),
          BlocConsumer<MenuBloc, MenuState>(
            listener: (context, state) {
              if (state is MenuLoading) {
                print('Loading ...');
              }
              if (state is MenuExceptionState) {
                print('Failed ! ${state.clientErrors}');
                showDialog(
                    context: context,
                    builder: (context) => FailureText(
                          title: 'Failed: ${state.clientErrors}',
                        ));
              }
              if (state is ShopMenuUpdatedSuccess) {
                print('Deleted Successfuly');
                Navigator.pop(context);
              }
            },
            builder: (context, state) {
              return StatefulBuilder(
                builder: (BuildContext context, setState) {
                  return Buttons.red(
                    onPressed: () async {
                      setState(() {
                        menuData.removeAt(menuIndex);
                      });

                      UpdateMenuEvent updateMenuEvent = UpdateMenuEvent(
                        id: updateID,
                        data: {
                          "subMenus": menuData
                              .map(
                                (e) => {
                                  "name": [
                                    {"lang": "en", "value": e.titleEN},
                                    {"lang": "az", "value": e.titleAZ},
                                    {"lang": "ru", "value": e.titleRU},
                                  ],
                                  "products": [],
                                },
                              )
                              .toList()
                        },
                      );
                      context.read<MenuBloc>().add(updateMenuEvent);
                    },
                    child: Text(S.of(context).yesDeleteIt),
                  );
                },
              );
            },
          ),
          SizedBox(
            height: 10,
          ),
          Buttons.grey(
            onPressed: onCancel,
            child: Text(S.of(context).cancel),
          )
        ],
      ),
    );
  }
}
