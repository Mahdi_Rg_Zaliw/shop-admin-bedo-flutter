import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:flutter/material.dart';

class ModaOptionlItem extends StatelessWidget {
  final String title;
  final IconData icon;
  final VoidCallback onPress;
  const ModaOptionlItem({
    Key key,
    this.title,
    this.icon,
    this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      splashColor: lightGreyBg,
      onPressed: onPress,
      child: Row(
        children: [
          Icon(
            icon,
            size: 20,
            color: hintTextColor,
          ),
          SizedBox(
            width: 10,
          ),
          Text(
            title,
            style: black14medium,
          )
        ],
      ),
    );
  }
}
