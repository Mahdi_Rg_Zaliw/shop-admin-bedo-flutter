import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/modal_home_page.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/modal_slide_transition.dart';
import 'package:flutter/material.dart';
import 'package:bedo_shop_admin/presentation/widgets/date_picker_bedo.dart';
import 'package:bedo_shop_admin/presentation/widgets/checkbox_grid.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/utils/device_dimensions.dart';

Future datePromotionPeriodModal({
  BuildContext context,
}) {
  return showModalBottomSheet(
    isScrollControlled: true,
    context: context,
    builder: (context) {
      return ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(24),
          topRight: Radius.circular(24),
        ),
        child: Container(
          height: MediaQuery.of(context).size.height / 1.4,
          child: ModalHomePage(
            navKey: GlobalKey<NavigatorState>(),
            childBuilder: (nav) {
              return ListView(
                shrinkWrap: true,
                children: [
                  headerPromotion(
                    context,
                    S.of(context).datePromotionPeriod,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(24).copyWith(bottom: 0),
                    child: DatePickerBedo(),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          child: defaultButton(S.of(context).done, () {}),
                          flex: 3,
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Expanded(
                          child: transparentActionButton(
                            S.of(context).setTime,
                            Icons.arrow_forward_ios,
                            () {
                              nav.currentState.push(Transitions(
                                widget: Column(
                                  children: [
                                    AppBar(),
                                    Expanded(
                                      child: Container(
                                        color: Colors.white,
                                        child: Center(child: Text('set time')),
                                      ),
                                    ),
                                  ],
                                ),
                              ));
                            },
                          ),
                          flex: 2,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              );
            },
          ),
        ),
      );
    },
  );
}

Widget defaultButton(String text, VoidCallback onPressed) {
  return RaisedButton(
    highlightElevation: 4,
    elevation: 0,
    onPressed: onPressed,
    child: Text(text),
  );
}

Widget transparentActionButton(
    String text, IconData icon, VoidCallback onPressed) {
  return RaisedButton(
    highlightColor: colorIcon.withOpacity(0.2),
    elevation: 0,
    highlightElevation: 0,
    color: Colors.transparent,
    onPressed: onPressed,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          text,
          style: grey14regular,
        ),
        SizedBox(width: 5),
        Icon(
          icon,
          color: titleGrey.withOpacity(0.5),
          size: 14,
        )
      ],
    ),
  );
}

Widget headerPromotion(BuildContext context, String title) {
  return Padding(
    padding: EdgeInsets.only(top: 10),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          flex: 2,
          child: Container(),
        ),
        Expanded(
          flex: 4,
          child: Text(
            title,
            style: black18medium,
          ),
        ),
        Expanded(
          flex: 1,
          child: Padding(
            padding: EdgeInsets.only(right: 10),
            child: IconButton(
              splashRadius: 24,
              icon: Icon(
                Icons.close,
                size: 19,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        ),
      ],
    ),
  );
}
