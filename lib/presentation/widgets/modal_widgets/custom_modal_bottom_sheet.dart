import 'package:flutter/material.dart';

import 'modal_home_page.dart';

Future customModalSheetWithNavigation(
    {BuildContext context, double height, ModalHomePage child}) {
  assert(height != null, '\n \n modal must have a width \n \n ');
  return buildAdaptiveModalBottomSheet<ModalHomePage>(
      context: context, height: height, child: child);
}

Future customModalSheet(
    {BuildContext context,
    double height,
    Widget child,
    double keyboardVisibleCoveringHeight}) {
  return buildAdaptiveModalBottomSheet<Widget>(
      context: context,
      height: height,
      child: child,
      kbd: keyboardVisibleCoveringHeight);
}

Future buildAdaptiveModalBottomSheet<T extends Widget>(
    {BuildContext context, double height, T child, double kbd}) {
  return showModalBottomSheet(
    isScrollControlled: true,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(24),
        topRight: Radius.circular(24),
      ),
    ),
    context: context,
    builder: (context) {
      return SingleChildScrollView(
        child: ClipRRect(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(24),
            topRight: Radius.circular(24),
          ),
          child: Container(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom > 0
                    ? kbd ?? MediaQuery.of(context).viewInsets.bottom
                    : MediaQuery.of(context).viewInsets.bottom),
            child: Container(
              height: height,
              child: child,
            ),
          ),
        ),
      );
    },
  );
}
