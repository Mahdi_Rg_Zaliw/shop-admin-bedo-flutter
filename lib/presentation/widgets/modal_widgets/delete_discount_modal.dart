import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/bloc/discount_bloc/discount_bloc.dart';
import 'package:bedo_shop_admin/bloc/discount_bloc/discount_events.dart';
import 'package:bedo_shop_admin/bloc/discount_bloc/discount_states.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/models/discount.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/add_discount_modal.dart';
import 'package:bedo_shop_admin/presentation/widgets/round_botton.dart';

Future deleteDiscountModal({
  BuildContext context,
  Discount discount,
}) {
  String dateToString(DateTime date) {
    String result = "";
    result += date.day.toString();
    result += "/";
    result += date.month.toString();
    result += "/";
    result += date.year.toString();
    return result;
  }

  return showModalBottomSheet(
    isScrollControlled: true,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(24),
        topRight: Radius.circular(24),
      ),
    ),
    context: context,
    backgroundColor: Colors.white,
    builder: (context) {
      return LayoutBuilder(
        builder: (context, constraints) => FractionallySizedBox(
          heightFactor: constraints.maxHeight < 550
              ? 0.65
              : constraints.maxHeight < 700
                  ? 0.65
                  : 0.5,
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: EdgeInsets.all(25),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Icon(
                        Icons.close,
                        color: Colors.transparent,
                      ),
                      Text(
                        S.of(context).discount,
                        style: black24medium,
                      ),
                      GestureDetector(
                        child: Icon(
                          Icons.close,
                          color: titleGrey,
                        ),
                        onTap: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      bottom: 0, top: 10, left: 10, right: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        children: [
                          Text(
                            S.of(context).discountTitle,
                            style: grey14regular,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            discount.promotionCode,
                            style: constraints.maxHeight < 550
                                ? black18regular
                                : constraints.maxHeight < 700
                                    ? black20regular
                                    : black24regular,
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            S.of(context).startDate,
                            style: grey14regular,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            dateToString(discount.startDate),
                            style: constraints.maxHeight < 550
                                ? black18regular
                                : constraints.maxHeight < 700
                                    ? black20regular
                                    : black24regular,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Text(
                            S.of(context).discountAmount,
                            style: grey14regular,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            discount.percent.toString() + " %",
                            style: constraints.maxHeight < 550
                                ? black18regular
                                : constraints.maxHeight < 700
                                    ? black20regular
                                    : black24regular,
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            S.of(context).endDate,
                            style: grey14regular,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            dateToString(discount.endDate),
                            style: constraints.maxHeight < 550
                                ? black18regular
                                : constraints.maxHeight < 700
                                    ? black20regular
                                    : black24regular,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                Column(
                  children: [
                    Container(
                      width: double.infinity,
                      padding: EdgeInsets.symmetric(
                          horizontal: MediaQuery.of(context).size.width * 0.06,
                          vertical: 10),
                      child: RaisedButton(
                        child: Text(
                          S.of(context).editDiscount,
                          style: black14medium,
                        ),
                        color: lightGreyBg,
                        elevation: 0,
                        highlightElevation: 1,
                        onPressed: () {
                          addDiscountModal(
                              context: context,
                              update: true,
                              updatingDiscount: discount);
                        },
                      ),
                    ),
                    BlocListener<DiscountBloc, DiscountsState>(
                      listenWhen: (prev, now) {
                        if (prev is DeleteDiscountDone &&
                            now is DiscountsLoaded) {
                          return false;
                        } else
                          return true;
                      },
                      listener: (context, state) {
                        if (state is DeleteDiscountDone) {
                          BlocProvider.of<DiscountBloc>(context, listen: false)
                              .add(LoadDiscounts());
                        } else if (state is DiscountsLoaded) {
                          Navigator.of(context).pop();
                        }
                      },
                      child: Container(
                        width: double.infinity,
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.06,
                            vertical: 10),
                        child: Buttons.redOutLine(
                          child: Text(
                            S.of(context).expireDiscount,
                            style: TextStyle(color: red),
                          ),
                          onPressed: () async {
                            bool userConfirmed = await showDialog(
                                context: context,
                                builder: (ctx) {
                                  return AlertDialog(
                                    title: Text(S.of(context).ExpireDiscount),
                                    content: Text(
                                      S.of(context).expireDiscountConfirm,
                                      style: black14medium,
                                    ),
                                    actions: [
                                      FlatButton(
                                        child: Text(
                                          S.of(context).yesExpireIt,
                                          style: red14medium,
                                        ),
                                        onPressed: () =>
                                            Navigator.of(context).pop(true),
                                      ),
                                      FlatButton(
                                        child: Text(
                                          S.of(context).cancel,
                                          style: black14medium,
                                        ),
                                        onPressed: () =>
                                            Navigator.of(context).pop(false),
                                      ),
                                    ],
                                  );
                                });
                            if (userConfirmed) {
                              BlocProvider.of<DiscountBloc>(context)
                                  .add(ExpireDiscount(discount));
                            }
                          },
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      );
    },
  );
}
