import 'dart:async';

import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/presentation/widgets/circular_number_pick.dart';
import 'package:bedo_shop_admin/utils/device_dimensions.dart';
import 'package:flutter/material.dart';

Future setTimeRestaurantModal({
  BuildContext context,
  final void Function(TimeOfDay) onSelectedTimeChanged,
}) {
  TimeOfDay t = TimeOfDay.now();
  return showModalBottomSheet(
    isScrollControlled: false,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(24),
        topRight: Radius.circular(24),
      ),
    ),
    context: context,
    builder: (context) {
      return Column(
        children: [
          Expanded(
            flex: 1,
            child: Padding(
              padding: EdgeInsets.all(25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Set time restaurant',
                    style: black24medium,
                  ),
                  GestureDetector(
                    child: Icon(Icons.close),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: CircularNumberPicker(
              onSelectedTimeChange: (time) {
                t = time;
              },
            ),
          ),
          Container(
            width: DeviceDimensions.widthSize(context),
            padding: EdgeInsets.only(left: 24, right: 24, bottom: 18, top: 10),
            child: RaisedButton(
              elevation: 0,
              onPressed: () {
                onSelectedTimeChanged(t);
                Navigator.pop(context);
              },
              child: Text('Confirm'),
            ),
          ),
        ],
      );
    },
  );
}
