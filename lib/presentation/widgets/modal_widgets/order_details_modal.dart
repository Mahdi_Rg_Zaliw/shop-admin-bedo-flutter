import 'package:bedo_shop_admin/bloc/orders_bloc/orders_bloc.dart';
import 'package:bedo_shop_admin/bloc/orders_bloc/orders_event.dart';
import 'package:bedo_shop_admin/bloc/orders_bloc/orders_state.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/model/order.dart';
import 'package:bedo_shop_admin/presentation/widgets/button_grid_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

Future orderDetailsModal({BuildContext context, String orderID}) {
  int preparationTime = 10;
  ShipmentModel shipmentModel = ShipmentModel.BEDO;
  return showModalBottomSheet(
    isDismissible: true,
    backgroundColor: Colors.transparent,
    isScrollControlled: true,
    context: context,
    builder: (context) {
      return LayoutBuilder(
        builder: (context, constraints) => FractionallySizedBox(
          heightFactor: constraints.maxHeight < 550
              ? 0.8
              : constraints.maxHeight < 700
                  ? 0.72
                  : 0.55,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                alignment: Alignment.bottomCenter,
                margin: EdgeInsets.symmetric(vertical: 10),
                width: 35,
                height: 5,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  color: Colors.white.withOpacity(0.7),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(24),
                  topRight: Radius.circular(24),
                ),
                child: Container(
                  height: MediaQuery.of(context).size.height *
                      (constraints.maxHeight < 550
                          ? 0.75
                          : constraints.maxHeight < 700
                              ? 0.67
                              : 0.51),
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: ListView(
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 30),
                        child: ButtonGrid(
                          title: S.of(context).preparationTime,
                          options: [
                            '10 min',
                            '15 min',
                            '20 min',
                            '30 min',
                            '45 min',
                            '60 min'
                          ],
                          onChanged: (int value) {
                            switch (value) {
                              case (0):
                                preparationTime = 10;
                                break;
                              case (1):
                                preparationTime = 15;
                                break;
                              case (2):
                                preparationTime = 20;
                                break;
                              case (3):
                                preparationTime = 30;
                                break;
                              case (4):
                                preparationTime = 45;
                                break;
                              case (5):
                                preparationTime = 60;
                                break;
                              default:
                                preparationTime = null;
                                break;
                            }
                          },
                        ),
                      ),
                      ButtonGrid(
                        title: S.of(context).typeOfCourier,
                        options: [
                          'Bedo',
                          'Courier Shop',
                        ],
                        crossAxisCount: 2,
                        onChanged: (value) {
                          switch (value) {
                            case (0):
                              shipmentModel = ShipmentModel.BEDO;
                              break;
                            case (1):
                              shipmentModel = ShipmentModel.SHOP;
                              break;
                            default:
                              shipmentModel = null;
                              break;
                          }
                        },
                      ),
                      BlocConsumer(
                        cubit: BlocProvider.of<OrdersBloc>(context),
                        listener: (context, state) {
                          if(state is OrderAcceptDone){
                            Navigator.of(context).pop();
                          }
                        },
                        buildWhen: (prev, now) {
                          return (now is OrderAcceptSent ||
                              now is OrderAcceptDone);
                        },
                        builder: (context, state) => Padding(
                          padding: EdgeInsets.symmetric(horizontal: 25),
                          child: RaisedButton(
                            padding: EdgeInsets.symmetric(vertical: 10),
                            elevation: 0,
                            onPressed: () {
                              if(shipmentModel == ShipmentModel.BEDO) {
                                BlocProvider.of<OrdersBloc>(
                                    context, listen: false)
                                    .add(AcceptOrder(AcceptOrderModel(
                                    orderID, shipmentModel, preparationTime)));
                              }
                              if(shipmentModel == ShipmentModel.SHOP){
                                //todo add navigation to select courier screen
                                throw UnimplementedError();
                              }
                            },
                            child: state is OrderAcceptSent
                                ? CupertinoActivityIndicator()
                                : state is OrderAcceptFailed ? Text(S.of(context).tryAcceptOrderAgain) :Text(S.of(context).accept),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}
