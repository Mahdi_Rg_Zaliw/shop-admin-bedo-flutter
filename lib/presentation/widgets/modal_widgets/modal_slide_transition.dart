import 'package:flutter/material.dart';

class Transitions<T> extends PageRouteBuilder<T> {
  final Curve curve;
  final Curve reverseCurve;
  final Duration duration;
  final Widget widget;

  Transitions(
      {this.curve = Curves.elasticInOut,
      this.reverseCurve = Curves.easeOut,
      this.duration = const Duration(milliseconds: 500),
      this.widget})
      : super(
          transitionDuration: duration,
          pageBuilder: (BuildContext context, Animation<double> animation,
              Animation<double> secondaryAnimation) {
            return widget;
          },
          transitionsBuilder: (BuildContext context,
              Animation<double> animation,
              Animation<double> secondaryAnimation,
              Widget child) {
            //animation = CurvedAnimation(parent: animation, curve: curve, reverseCurve: reverseCurve);
            return SlideTransition(
              position: Tween<Offset>(
                begin: const Offset(1.0, 0.0),
                end: const Offset(0.0, 0.0),
              ).animate(animation),
              //).animate(CurvedAnimation(parent: animation,curve: curve, reverseCurve: reverseCurve)),
              child: child,
            );
          },
        );
}
