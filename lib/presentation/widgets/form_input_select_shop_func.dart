import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/presentation/screens/custom_expansion_tile.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:flutter/material.dart';

class ShopFunction extends StatefulWidget {
  final List data;
  final Function(List) onCahange;
  const ShopFunction({
    Key key,
    this.data,
    this.onCahange,
  }) : super(key: key);

  @override
  _ShopFunctionState createState() => _ShopFunctionState();
}

class _ShopFunctionState extends State<ShopFunction> {
  //
  List cat;
  //
  @override
  void initState() {
    if (widget.data == null) {
      printColor(Colors.lightGreen, 'null received ========================');
      cat = [
        // CategoryDataModel(title: 'resturant', check: true, id: 'l54fj'),
        // CategoryDataModel(title: 'pharmacy', check: false, id: '15df'),
        // CategoryDataModel(title: 'housekeeping', check: true, id: 'fdsjkln'),
        // CategoryDataModel(title: 'ohter', check: false, id: '257'),
      ];
    } else {
      setState(() {
        cat = widget.data;
      });
      printOrange(cat.toString());
    }
    super.initState();
  }

  int get selected {
    int count = 0;
    if (cat.isNotEmpty) {
      cat?.forEach((element) {
        if (element.check) {
          count++;
        }
      });
    }
    return count;
  }

  List get catVariables {
    List<String> ans = [];
    if (cat.isNotEmpty) {
      cat?.forEach((element) {
        if (element.check == true) {
          ans.add(element.id);
        }
      });
    }
    return ans;
  }

  @override
  Widget build(BuildContext context) {
    return CustomExpansionTile(
      // onExpansionChanged: (value) => ,
      title: Row(
        children: [
          Text(
            'select shop function    ',
            style: black14medium,
          ),
          Text(
            (selected == 0) ? '' : '$selected selected',
            style: green14medium,
          ),
        ],
      ),
      children: (cat.isNotEmpty)
          ? List.generate(
              cat.length,
              (index) {
                final d = cat[index];
                return CategoryTile(
                  check: d.check,
                  title: d.title,
                  onChangeCbx: (v) {
                    setState(
                      () {
                        d.check = v;
                      },
                    );
                    widget.onCahange(cat);
                    // widget.onCahange(catVariables);
                  },
                );
              },
            )
          : [
              SizedBox(
                height: 70,
                child: Center(
                  child: Text('could not fetch categories'),
                ),
              )
            ],
    );
  }
}

class CategoryTile extends StatefulWidget {
  final bool check;
  final String title;
  final Function(bool) onChangeCbx;
  const CategoryTile(
      {Key key, this.check = false, this.title, this.onChangeCbx})
      : super(key: key);

  @override
  _CategoryTileState createState() => _CategoryTileState();
}

class _CategoryTileState extends State<CategoryTile> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Checkbox(
          value: widget.check,
          onChanged: widget.onChangeCbx,
        ),
        Text('${widget.title}'),
      ],
    );
  }
}

class CategoryDataModel {
  String title;
  String id;
  bool check;

  CategoryDataModel({this.title, this.check, this.id});

  Map toMap() {
    return {"_id": id};
  }

  @override
  String toString() {
    return ' title $title check $check id $id';
  }
}
