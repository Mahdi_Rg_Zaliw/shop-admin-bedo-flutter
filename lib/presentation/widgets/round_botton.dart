import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:bedo_shop_admin/design/theme.dart';

class Buttons extends RaisedButton {
  // final Color color;
  // final String text;
  // final Color textColor;
  // final double width;
  // final double height;
  final Widget child;
  final double radius;
  final VoidCallback onPressed;
  // final RoundedRectangleBorder shape;
  Buttons({this.child, this.onPressed, this.radius})
      : super(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(radius)),
          onPressed: onPressed,
          elevation: 0,
          color: lightGreen,
          textColor: textColor,
          child: child,
        );

  /// red out line on red

  /// red on whie

  Buttons.red({this.onPressed, this.child, this.radius = 100})
      : super(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(radius)),
          onPressed: onPressed,
          elevation: 0,
          color: red,
          textColor: Colors.white,
          child: child,
        );

  /// light grey on black
  Buttons.grey({this.onPressed, this.child, this.radius = 100})
      : super(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(radius),
          ),
          onPressed: onPressed,
          elevation: 0,
          color: lightGreyBg,
          textColor: textColor,
          child: child,
        );

  /// orange on white
  // Buttons.orange({this.onPressed, this.child, this.radius = 100})
  //     : super(
  //         shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.circular(radius)),
  //         onPressed: onPressed,
  //         elevation: 0,
  //         color: orange,
  //         textColor: textColor,
  //         child: child,
  //       );

  /// lime on black
  // Buttons.lime({this.onPressed, this.child, this.radius = 100})
  //     : super(
  //         shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.circular(radius)),
  //         onPressed: onPressed,
  //         elevation: 0,
  //         color: lightGreen,
  //         textColor: textColor,
  //         child: child,
  //       );

  /// teal on white

  /// yello on black
  // Buttons.yellow({this.onPressed, this.child, this.radius = 100})
  //     : super(
  //         shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.circular(radius)),
  //         onPressed: onPressed,
  //         elevation: 0,
  //         color: yellow,
  //         textColor: textColor,
  //         child: child,
  //       );

  /// light blue on black
  // Buttons.lightblue({this.onPressed, this.child, this.radius = 100})
  //     : super(
  //         shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.circular(radius)),
  //         onPressed: onPressed,
  //         elevation: 0,
  //         color: lightBlue,
  //         textColor: Colors.white,
  //         child: child,
  //       );

  /// black on white
  // Buttons.black({this.onPressed, this.child, this.radius = 100})
  //     : super(
  //         shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.circular(radius)),
  //         onPressed: onPressed,
  //         elevation: 0,
  //         color: Colors.black,
  //         textColor: Colors.white,
  //         child: child,
  //       );

  Buttons.redOutLine({this.onPressed, this.child, this.radius = 100})
      : super(
          shape: RoundedRectangleBorder(
              side: BorderSide(color: red, width: 1),
              borderRadius: BorderRadius.circular(radius)),
          onPressed: onPressed,
          elevation: 0,
          color: Colors.white,
          textColor: red,
          child: child,
        );

  Buttons.transparentWhiteOutline(
      {this.onPressed, this.child, this.radius = 100})
      : super(
          shape: RoundedRectangleBorder(
              side: BorderSide(color: Colors.white, width: 2),
              borderRadius: BorderRadius.circular(radius)),
          onPressed: onPressed,
          elevation: 0,
          color: Colors.transparent,
          textColor: Colors.white,
          child: child,
        );

  Buttons.primaryOutline({this.onPressed, this.child, this.radius = 100})
      : super(
          shape: RoundedRectangleBorder(
              side: BorderSide(color: primaryMaterialColor, width: 1),
              borderRadius: BorderRadius.circular(radius)),
          onPressed: onPressed,
          elevation: 0,
          color: Colors.white,
          textColor: primaryMaterialColor,
          child: child,
        );

  Buttons.whiteOutline({this.onPressed, this.child, this.radius = 100})
      : super(
          shape: RoundedRectangleBorder(
              side: BorderSide(color: hintTextColor, width: 1),
              borderRadius: BorderRadius.circular(radius)),
          onPressed: onPressed,
          elevation: 0,
          splashColor: primaryMaterialColor,
          color: Colors.white,
          textColor: textColor,
          child: child,
        );
}

// class RedOutlineBtn extends OutlineButton {
//   final VoidCallback onPressed;
//   final Widget child;
//   RedOutlineBtn({this.onPressed, this.child, this.radius=100})
//       : super(
//           shape: RoundedRectangleBorder(
//               borderRadius: BorderRadius.circular(radius)),
//           onPressed: onPressed,
//           child: child,
//           textColor: red,
//           borderSide: BorderSide(color: red, width: 1),
//         );
// }

// extension w on RaisedButton {
//   Widget light() {
//     return this(
//       textColor: Colors.white,
//     );
//   }
// }

class FlatBtn extends FlatButton {
  final Widget child;
  final double radius;
  final VoidCallback onPressed;
  FlatBtn.transparentWhiteOutline(
      {this.onPressed, this.child, this.radius = 100})
      : super(
          shape: RoundedRectangleBorder(
              side: BorderSide(color: Colors.white, width: 2),
              borderRadius: BorderRadius.circular(radius)),
          onPressed: onPressed,
          splashColor: Colors.white54,
          // elevation: 0,
          color: Colors.transparent,
          textColor: Colors.white,
          child: child,
        );
}
