import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';

class CircularNumberPicker extends StatefulWidget {
  final void Function(TimeOfDay) onSelectedTimeChange;
  CircularNumberPicker({Key key, this.onSelectedTimeChange}) : super(key: key);

  @override
  _CircularNumberPickerState createState() => _CircularNumberPickerState();
}

class _CircularNumberPickerState extends State<CircularNumberPicker> {
  int _currentIntValueHour = TimeOfDay.now().hour;
  int _currentIntValueMinute = TimeOfDay.now().minute;

  @override
  Widget build(BuildContext context) {
    var phoneSize = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.center,
          child: Container(
            width: phoneSize.width / 1.2,
            height: 56,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              border: Border.all(
                width: 1,
                color: lightGreyBg,
              ),
            ),
          ),
        ),
        Align(
          alignment: Alignment.center,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 35),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                NumberPicker.integer(
                    textStyle: grey22medium,
                    selectedTextStyle: black24medium,
                    initialValue: _currentIntValueHour,
                    minValue: 0,
                    maxValue: 23,
                    zeroPad: true,

                    // decimalPlaces: 2,
                    onChanged: (newValue) {
                      setState(() => _currentIntValueHour = newValue);
                      final selectedTime = TimeOfDay(
                          hour: _currentIntValueHour,
                          minute: _currentIntValueMinute);
                      widget.onSelectedTimeChange(selectedTime);
                    }),
                NumberPicker.integer(
                    textStyle: grey22medium,
                    selectedTextStyle: black24medium,
                    initialValue: _currentIntValueMinute,
                    minValue: 0,
                    zeroPad: true,
                    maxValue: 59,
                    // decimalPlaces: 2,
                    onChanged: (newValue) {
                      setState(() => _currentIntValueMinute = newValue);
                      final selectedTime = TimeOfDay(
                          hour: _currentIntValueHour,
                          minute: _currentIntValueMinute);
                      widget.onSelectedTimeChange(selectedTime);
                    }),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
