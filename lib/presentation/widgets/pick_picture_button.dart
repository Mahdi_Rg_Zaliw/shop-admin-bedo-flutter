import 'dart:io';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:bedo_shop_admin/design/icons_f.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';

enum PickerType { remove, edit }

// ignore: must_be_immutable
class PickPictureButton extends StatefulWidget {
  final void Function(File) onChanged;
  final PickerType pickerType;
  final String previewImg;

  File image;

  PickPictureButton({
    this.pickerType = PickerType.edit,
    @required this.onChanged,
    this.previewImg,
    this.image,
  });

  @override
  _PickPictureButtonState createState() => _PickPictureButtonState();
}

class _PickPictureButtonState extends State<PickPictureButton> {
  // File image;
  String uri;

  @override
  void initState() {
    super.initState();
    if (widget.image == null && widget.previewImg != null) {
      // fileFromImageUrl(widget.previewImg).then((value) {
      //   setState(() {
      //     image = value;
      //   });
      // image = null;
    }
    // });
    uri = widget.previewImg;
    printWhiteBg('uri init ');
    printWhiteBg(uri);
  }

  void fileChanged(file) {
    setState(() {
      if (file != null) {
        widget.image = file;
      } else {
        print('Try Again!');
      }
      uri = null;
      printWhiteBg('uri change ');
      printWhiteBg(uri);
    });
    widget.onChanged(file);
  }

  Future getImage(BuildContext context) async {
    showTypeImagePickerModal(
      context: context,
      onChanged: fileChanged,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80,
      height: 80,
      child: Stack(
        children: [
          Container(
            width: 80,
            height: 80,
            child: (widget.image == null && uri == null)
                // ? (uri != null)
                // ? Image.network(uri)
                //  FadeInImage(
                //       fit: BoxFit.cover,
                //       image: NetworkImage(uri),
                //       placeholder: AssetImage(
                //           'assets/images/product_image_placeholder.png'),
                //     )
                ? FlatButton(
                    color: primaryMaterialColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(60),
                    ),
                    onPressed: () {
                      getImage(context);
                    },
                    child: Icon(
                      Iconsf.i09_camera,
                      color: Colors.white,
                      size: 24,
                    ),
                  )
                : ClipRRect(
                    borderRadius: BorderRadius.circular(40),
                    child: (uri == null && widget.image != null)
                        ? Image.file(
                            widget.image,
                            width: 80,
                            height: 80,
                            fit: BoxFit.cover,
                          )
                        : FadeInImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(uri),
                            placeholder: AssetImage(
                                'assets/images/product_image_placeholder.png'),
                          ),
                  ),
          ),
          (widget.image != null || uri != null)
              ? widget.pickerType == PickerType.edit
                  ? Align(
                      alignment: Alignment.bottomRight,
                      child: GestureDetector(
                        onTap: () {
                          getImage(context);
                        },
                        child: Container(
                          width: 30,
                          height: 30,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                          ),
                          child: Container(
                            width: 25,
                            height: 25,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: primaryMaterialColor,
                            ),
                            child: Icon(
                              Iconsf.i09_camera,
                              color: Colors.white,
                              size: 14,
                            ),
                          ),
                        ),
                      ),
                    )
                  : Align(
                      alignment: Alignment.topRight,
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            widget.image = null;
                          });
                        },
                        child: Container(
                          width: 30,
                          height: 30,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                          ),
                          child: Container(
                            width: 25,
                            height: 25,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: red,
                            ),
                            child: Icon(
                              Icons.delete_outline,
                              color: Colors.white,
                              size: 14,
                            ),
                          ),
                        ),
                      ),
                    )
              : Container(),
        ],
      ),
    );
  }
}

Future showTypeImagePickerModal({
  BuildContext context,
  final ValueChanged<File> onChanged,
}) {
  return showModalBottomSheet(
    isScrollControlled: false,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(24),
        topRight: Radius.circular(24),
      ),
    ),
    context: context,
    builder: (context) {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        height: 100,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            FlatButton(
              padding: EdgeInsets.all(9),
              onPressed: () async {
                final pickedFile =
                    await FilePicker.platform.pickFiles(type: FileType.image);
                if (pickedFile != null) {
                  onChanged(File(pickedFile.files.first.path));
                }
                Navigator.pop(context);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.image,
                    color: primaryMaterialColor,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'From Gallery',
                    style: black14medium,
                  ),
                ],
              ),
            ),
            FlatButton(
              padding: EdgeInsets.all(9),
              onPressed: () async {
                final pickedFile =
                    await ImagePicker().getImage(source: ImageSource.camera);
                if (pickedFile != null) {
                  onChanged(File(pickedFile.path));
                }
                Navigator.pop(context);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.camera,
                    color: primaryMaterialColor,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'From Camera',
                    style: black14medium,
                  ),
                ],
              ),
            )
          ],
        ),
      );
    },
  );
}
