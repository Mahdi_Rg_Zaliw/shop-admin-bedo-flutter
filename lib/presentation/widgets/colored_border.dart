import 'package:bedo_shop_admin/design/theme.dart';
import 'package:flutter/cupertino.dart';

class ColoredBorder extends StatelessWidget {
  final Widget child;
  final EdgeInsets padding;
  final EdgeInsets margin;
  final double width;
  final double radius;
  final Color borderColor;
  ColoredBorder(
      {this.child,
      this.padding,
      this.margin,
      this.width,
      this.radius = 15,
      this.borderColor = lightGreyBg});
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 42,
      margin: margin,
      padding: padding,
      decoration: BoxDecoration(
        border: Border.all(color: borderColor, width: 1),
        borderRadius: BorderRadius.circular(radius),
      ),
      child: child,
    );
  }
}
