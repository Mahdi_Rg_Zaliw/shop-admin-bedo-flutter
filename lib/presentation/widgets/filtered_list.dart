import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/presentation/widgets/comment_widggets/custom_avatar.dart';
import 'package:flutter/material.dart';

class FilteredList extends StatefulWidget {
  final String title;
  final bool showBackButton;
  final bool fixed;
  final double totalAmount;
  final List<Widget> appBarTitlesList;
  final List<Widget> appBarBottomList;
  final List<Widget> bottomTitleList;
  final List<Widget> topTitlesList;
  final String imgurl;
  final String courierName;
  final String courierId;
  final bool isCourierProfile;
  final Function(int) onSwipeTo;

  // final bool appBarTitleFromOptions;
  final List<String> optionsList;
  final List<bool> optionShouldNotifyList;
  final List<Widget> pagesList;
  final Widget bottomNavigationBar;

  const FilteredList.common({
    this.title = 'total amount today',
    this.totalAmount = 138.00,
    this.optionsList = const ['fund status', 'full report', 'nigg'],
    this.pagesList = const [
      PagePlaceHolder(
        'fund status',
      ),
      PagePlaceHolder(
        'full report',
      ),
      PagePlaceHolder(
        'full report',
      ),
    ],
    this.optionShouldNotifyList = const [true, true, true],
    this.showBackButton = false,
    this.appBarTitlesList,
    this.appBarBottomList,
    this.bottomNavigationBar,
    // this.appBarTitleFromOptions = false,
    this.bottomTitleList = const [
      Text(
        '987',
        style: black24medium,
      ),
      Text(
        '234',
        style: black24medium,
      ),
      Text(
        '345',
        style: black24medium,
      )
    ],
    this.topTitlesList = const [Text('one'), Text('two'), Text('three')],
    this.onSwipeTo,
  })  : this.isCourierProfile = false,
        this.courierId = null,
        this.fixed = null,
        this.courierName = '',
        this.imgurl = 'https://picsum.photos/250',
        assert(bottomTitleList.length == pagesList.length),
        assert(topTitlesList.length == pagesList.length),
        assert(optionsList.length == pagesList.length);

  FilteredList.curierProfile({
    this.title = 'total amount today',
    this.totalAmount = 138.00,
    this.optionsList = const ['fund status', 'full report', 'nigg'],
    this.pagesList = const [
      PagePlaceHolder(
        'fund status',
      ),
      PagePlaceHolder(
        'full report',
      ),
      PagePlaceHolder(
        'full report',
      ),
    ],
    this.courierName = 'courier name',
    this.optionShouldNotifyList = const [true, true, true],
    this.showBackButton = false,
    this.appBarTitlesList, //

    this.imgurl = 'https://picsum.photos/250',
    this.courierId = '#dlj978324',
    this.onSwipeTo,
    // this.appBarTitleFromOptions = false,

    //
  })  : this.bottomTitleList = null,
        this.appBarBottomList = null,
        this.topTitlesList = null,
        this.isCourierProfile = true,
        this.fixed = null,
        this.bottomNavigationBar = null,
        assert(optionsList.length == pagesList.length);

  FilteredList.fix({
    this.title = 'total amount today',
    this.totalAmount = 138.00,
    this.optionsList = const ['fund status', 'full report', 'nigg'],
    this.pagesList = const [
      PagePlaceHolder(
        'fund status',
      ),
      PagePlaceHolder(
        'full report',
      ),
      PagePlaceHolder(
        'full report',
      ),
    ],
    this.optionShouldNotifyList = const [true, true, true],
    this.showBackButton = false,
    this.appBarTitlesList, //
    this.appBarBottomList,
    this.imgurl = '',
    this.courierName = 'h',
    this.bottomNavigationBar,
    this.onSwipeTo,
  })  : this.bottomTitleList = null,
        this.courierId = null,
        this.fixed = true,
        this.topTitlesList = null,
        this.isCourierProfile = false,
        assert(optionsList.length == pagesList.length);

  @override
  _FilteredListState createState() => _FilteredListState();
}

class FilteredListPage {
  final Widget page;
  final String pageName;

  const FilteredListPage({this.page, this.pageName});
}

class _FilteredListState extends State<FilteredList>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  int _selectedIndex = 0;
  List<bool> n = [];

  @override
  void initState() {
    _tabController =
        TabController(length: widget.optionsList.length, vsync: this);
    _tabController.addListener(() {
      setState(() {
        _selectedIndex = _tabController.index;
        n[_selectedIndex] = false;
      });
      print('$_selectedIndex selected');

      if (widget.onSwipeTo != null) {
        widget.onSwipeTo(_selectedIndex);
      }
    });
    super.initState();
    n = [...widget.optionShouldNotifyList];
    n[_selectedIndex] = false;
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: widget.optionsList.length,
      child: Scaffold(
        appBar: buildHeader(),
        body: buildPages(),
        bottomNavigationBar: widget.bottomNavigationBar,
      ),
    );
  }

  TabBarView buildPages() {
    return TabBarView(controller: _tabController, children: widget.pagesList
        //  [widget.pages.first.page, widget.pages.first.page]
        );
  }

  AppBar buildHeader() {
    return AppBar(
      automaticallyImplyLeading: widget.showBackButton,
      toolbarHeight: widget.isCourierProfile
          ? 300
          : (widget.appBarBottomList == null)
              ? 120
              : 170,
      title: widget?.topTitlesList == null
          ? widget.appBarTitlesList.length == 1
              ? widget.appBarTitlesList[0]
              : widget.appBarTitlesList[_tabController.index]
          : widget.topTitlesList[_tabController.index],
      centerTitle: true,
      elevation: 0.0,
      backgroundColor: Colors.white,
      bottom: buildBottom(),
    );
  }

  PreferredSize buildBottom() {
    return PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: Column(
          children: [
            Container(
                height: widget.isCourierProfile
                    ? 3 * kToolbarHeight
                    : (widget.appBarBottomList == null)
                        ? 0
                        : kToolbarHeight,
                child: widget.isCourierProfile
                    ? Column(
                        children: [
                          CustomAvatar(
                            imageUrl: widget.imgurl,
                            radius: 55,
                          ),
                          SizedBox(
                            height: 12,
                          ),
                          Text(
                            widget.courierName,
                            style: black24medium,
                          )
                        ],
                      )
                    : widget.bottomTitleList == null
                        ? widget.appBarBottomList == null
                            ? null
                            : widget.appBarBottomList.length == 0
                                ? null
                                : widget.appBarBottomList.length == 1
                                    ? widget.appBarBottomList[0]
                                    : widget
                                        .appBarBottomList[_tabController.index]
                        : widget.bottomTitleList[_tabController.index]),
            Align(
              alignment: Alignment.center,
              child: Container(
                decoration: BoxDecoration(
                  // color: red,
                  color: lightGreyBg,
                  borderRadius: BorderRadius.circular(50),
                ),
                margin: EdgeInsets.symmetric(horizontal: 10),
                width: double.infinity,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 5,
                  ),
                  child: TabBar(
                    controller: _tabController,
                    labelColor: textColor,
                    unselectedLabelColor: titleGrey,
                    indicatorSize: TabBarIndicatorSize.label,
                    indicator: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: Colors.white),
                    tabs: widget.optionsList
                        .map(
                          (e) => buildFilteredListOption(
                            option: e,
                            index: widget.optionsList.indexOf(e),
                            notify: n[widget.optionsList.indexOf(e)],
                          ),
                        )
                        .toList(),
                  ),
                ),
              ),
            ),
          ],
        ));
  }

  CustomTab buildFilteredListOption({String option, int index, bool notify}) {
    return CustomTab(
      tabController: _tabController,
      index: index,
      option: option,
      shouldNotify: notify,
    );
  }
}

class CustomTab extends StatefulWidget {
  final int index;
  final String option;
  final bool shouldNotify;

  CustomTab(
      {Key key,
      @required TabController tabController,
      this.index,
      this.option,
      this.shouldNotify})
      : _tabController = tabController,
        super(key: key);

  final TabController _tabController;

  @override
  _CustomTabState createState() => _CustomTabState();
}

class _CustomTabState extends State<CustomTab> {
  @override
  Widget build(BuildContext context) {
    return Tab(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(14),
        ),
        child: Center(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Stack(
                // mainAxisSize: MainAxisSize.min,
                // crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Positioned(
                    right: 0,
                    top: 5,
                    child: (widget._tabController.index != widget.index &&
                            widget.shouldNotify)
                        ? Icon(
                            Icons.circle,
                            size: 6,
                            color: red,
                          )
                        : SizedBox(),
                  ),
                  Center(
                    child: Container(
                      // width: 60,
                      child: Text(
                        widget.option,
                        overflow: TextOverflow.visible,
                        softWrap: true,
                        maxLines: 3,
                        style: TextStyle(fontSize: 12),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class PagePlaceHolder extends StatelessWidget {
  final String pagePlaceHolder;

  const PagePlaceHolder(this.pagePlaceHolder);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        color: Colors.white,
        child: Text(
          pagePlaceHolder,
          style: black24medium,
        ),
      ),
    );
  }
}
