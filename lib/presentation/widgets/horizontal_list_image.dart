import 'package:flutter/material.dart';
import 'package:bedo_shop_admin/presentation/widgets/pick_picture_button.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/design/icons_f.dart';

class HorizontalListImage extends StatefulWidget {
  HorizontalListImage({Key key}) : super(key: key);

  @override
  _HorizontalListImageState createState() => _HorizontalListImageState();
}

class _HorizontalListImageState extends State<HorizontalListImage> {
  final List imageList = [];

  Future getImage(BuildContext context) async {
    showTypeImagePickerModal(
      context: context,
      onChanged: (file) {
        setState(() {
          if (file != null) {
            imageList.add(file);
          } else {
            print('Try Again!');
          }
        });
        print(imageList);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Container(
        // color: Colors.blue,
        height: 90,
        child: ListView(
          // reverse: true,
          scrollDirection: Axis.horizontal,
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 5),
              width: 80,
              height: 80,
              child: Stack(
                children: [
                  Container(
                    width: 80,
                    height: 80,
                    child: FlatButton(
                      color: primaryMaterialColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(60),
                      ),
                      onPressed: () {
                        getImage(context);
                      },
                      child: Icon(
                        Iconsf.i09_camera,
                        color: Colors.white,
                        size: 24,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Row(
              children: imageList
                  .map(
                    (e) => Padding(
                      padding: EdgeInsets.all(5),
                      child: PickPictureButton(
                        pickerType: PickerType.edit,
                        onChanged: (file) {
                          print(file.path);
                        },
                        image: e,
                      ),
                    ),
                  )
                  .toList()
                  .reversed
                  .toList(),
            )
          ],
        ),
      ),
    );
  }
}
