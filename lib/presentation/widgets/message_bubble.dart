import 'dart:io';
import 'dart:math';

import 'package:bedo_shop_admin/bloc/conversations_bloc/conversations_bloc.dart';
import 'package:bedo_shop_admin/bloc/conversations_bloc/conversations_events.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';

class MessageBubble extends StatefulWidget {
  final String message;
  final bool isMe;
  final String userName;
  final Key key;
  final DateTime date;
  final bool isFile;
  final File file;
  final String fileName;
  final String fileNameToShow;
  final bool showUserName;
  final String conversationID;

  MessageBubble(
      {this.message,
      this.isMe,
      this.userName,
      this.date,
      this.key,
      this.isFile,
      this.file,
      this.fileName,
      this.fileNameToShow,
      this.showUserName,
      this.conversationID});

  @override
  _MessageBubbleState createState() => _MessageBubbleState();
}

class _MessageBubbleState extends State<MessageBubble> {
  bool isDownloaded = false;
  String filePath;

  double width(BuildContext context) {
    double maxWidth = MediaQuery.of(context).size.width * 0.6;
    if (!widget.isFile) {
      double minWidth = 60;
      double width = (widget.message.length * 18).toDouble();
      return min(max(width, minWidth), maxWidth);
    } else {
      double minWidth = 80;
      double width = 80 + (widget.fileNameToShow.length * 15).toDouble();
      return min(max(width, minWidth), maxWidth);
    }
  }

  Future<void> isFileDownloaded() async {
    var _localPath = await _findLocalPath();
    Directory(_localPath + '/Downloads').create(recursive: true);
    filePath = '$_localPath/Downloads/${widget.fileName}';
    isDownloaded = await File(filePath).exists();
  }

  // ignore: missing_return
  Future<String> _findLocalPath() async {
    try {
      var platform = Theme.of(context).platform;
      final directory = platform == TargetPlatform.android
          ? await (getExternalStorageDirectory())
          : await getApplicationDocumentsDirectory();
      return directory.path;
    } catch (e) {
      print(e);
    }
  }

  //
  // @override
  // void initState() async{
  //   super.initState();
  //   initialize();
  // }
  //
  // void initialize() async{
  //   isDownloaded = await isFileDownloaded();
  // }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment:
          widget.isMe ? MainAxisAlignment.end: MainAxisAlignment.start,
      children: [
        Column(
          crossAxisAlignment:
              widget.isMe ? CrossAxisAlignment.end:CrossAxisAlignment.start ,
          children: [
            if (widget.showUserName)
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                child: Text(
                  widget.userName,
                  style: grey14regular,
                ),
              ),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: widget.isMe
                      ? lightGreyBg
              :Theme.of(context).primaryColor),
              width: width(context),
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
              margin: EdgeInsets.symmetric(horizontal: 4, vertical: 6),
              child: widget.isFile
                  ? Row(
                      children: [
                        GestureDetector(
                          onTap: () async {
                            await isFileDownloaded();
                            BlocProvider.of<ConversationBloc>(context,
                                    listen: false)
                                .add(isDownloaded
                                    ? OpenFileEvent(filePath)
                                    : DownloadFileEvent(
                                        widget.message,
                                        widget.conversationID,
                                        filePath,
                                      ));
                          },
                          child: CircleAvatar(
                            child: Icon(Icons.insert_drive_file_outlined,
                                color: widget.isMe
                                    ? Colors.white
                            :Theme.of(context).primaryColor),
                            backgroundColor: widget.isMe
                                ? Theme.of(context).primaryColor
                                : Colors.white,
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(8.0),
                          width: width(context) - 80,
                          child: Text(
                            widget.fileNameToShow,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: widget.isMe ? grey14semiBold: white14medium ,
                          ),
                        )
                      ],
                    )
                  : Text(
                      widget.message,
                      style: widget.isMe ? grey14semiBold: white14medium ,
                      textAlign: widget.isMe ? TextAlign.end : TextAlign.start ,
                    ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
              child: Text(
                DateFormat('hh:mm - dd/M/y', 'en_US').format(widget.date),
                style: grey12regular,
              ),
            )
          ],
        ),
      ],
    );
  }
}
