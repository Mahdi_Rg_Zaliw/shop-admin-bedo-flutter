import 'package:bedo_shop_admin/bloc/settings_bloc/settings_bloc.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/widgets/bottom_navigation.dart';
import 'package:bedo_shop_admin/presentation/widgets/custom_text_field.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SettingsPassword extends StatefulWidget {
  @override
  _SettingsPasswordState createState() => _SettingsPasswordState();
}

class _SettingsPasswordState extends State<SettingsPassword> {
  final _k = GlobalKey<FormState>();

  final _p = TextEditingController();

  final _cp = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // bottomNavigationBar: CustomBottomNavigationBar(),
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          S.of(context).changeYourPassword,
          style: grey14medium,
        ),
      ),
      body: SingleChildScrollView(
        child: SizedBox(
          height: MediaQuery.of(context).size.height - kToolbarHeight - 100,
          child: Form(
              key: _k,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  SizedBox(
                    height: 24,
                  ),
                  CustomTextField(
                    title: S.of(context).currentPassword,
                    hintText: S.of(context).enterCurrentPassword,
                    isPassword: true,
                    validator: (value) {
                      if (value.length < 1) {
                        return S.of(context).thisFieldIsRequired;
                      }
                      if (value.length < 8) {
                        return S.of(context).mustBeMoreThan8Characters;
                      }
                      return null;
                    },
                  ),
                  CustomTextField(
                    title: S.of(context).newPassword,
                    hintText: S.of(context).enterNewPassword,
                    controller: _p,
                    isPassword: true,
                    validator: (value) {
                      if (value.length < 1) {
                        return S.of(context).thisFieldIsRequired;
                      }
                      if (value.length < 8) {
                        return S.of(context).mustBeMoreThan8Characters;
                      }

                      return null;
                    },
                  ),
                  CustomTextField(
                    title: S.of(context).confirmPassword,
                    controller: _cp,
                    hintText: S.of(context).enterConfirmPassword,
                    isPassword: true,
                    validator: (value) {
                      if (value.length < 1) {
                        return S.of(context).thisFieldIsRequired;
                      }
                      if (value.length < 8) {
                        return S.of(context).mustBeMoreThan8Characters;
                      }
                      if (_p.text != _cp.text) {
                        return S
                            .of(context)
                            .passwordConfirmMustMatchNewPassword;
                      }
                      return null;
                    },
                  ),
                  Expanded(child: SizedBox()),
                  Padding(
                    padding: const EdgeInsets.all(24),
                    child: BlocConsumer<SettingsBloc, SettingsState>(
                      listener: (context, state) {
                        printMagentBg(state.runtimeType);
                        if (state is ChangePasswordResult) {
                          showModalBottomSheet(
                            context: context,
                            builder: (context) => Padding(
                              padding: const EdgeInsets.all(24),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Expanded(
                                    child: Center(
                                      child: Text(
                                        '${state.msg}',
                                        style: (state.success)
                                            ? black12regular.copyWith(
                                                fontSize: 20)
                                            : red12semibold.copyWith(
                                                fontSize: 20),
                                      ),
                                    ),
                                  ),
                                  RaisedButton(
                                    child: Text('ok'),
                                    onPressed: () {
                                      if (state.success) {
                                        _k.currentState.reset();
                                        _p.clear();
                                        _cp.clear();
                                      }
                                      Navigator.of(context).pop();
                                      Navigator.of(context).pop();
                                    },
                                  )
                                ],
                              ),
                            ),
                          );
                        }
                      },
                      builder: (context, state) {
                        return RaisedButton(
                          onPressed: () {
                            if (state is! TryingToChangePass) {
                              FocusScope.of(context).unfocus();
                              if (_k.currentState.validate()) {
                                _k.currentState.save();
                                // bloc activate
                                final Map data = {
                                  "currentPassword": _cp.text,
                                  "newPassword": _p.text
                                };
                                printRedBg(data);
                                context
                                    .read<SettingsBloc>()
                                    .add(PasswordChanging(data));
                              }
                            }
                          },
                          child: (state is TryingToChangePass)
                              ? SizedBox(
                                  width: 25,
                                  height: 25,
                                  child: CupertinoActivityIndicator(),
                                )
                              : Text(S.of(context).changePassword),
                        );
                      },
                    ),
                  )
                ],
              )),
        ),
      ),
    );
  }
}
