import 'package:bedo_shop_admin/bloc/lang_bloc/lang_bloc.dart';
import 'package:bedo_shop_admin/bloc/notifications_bloc/notifications_bloc.dart';
import 'package:bedo_shop_admin/bloc/settings_bloc/settings_bloc.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/screens/settings_pages/settings_debit_card.dart';
import 'package:bedo_shop_admin/presentation/screens/settings_pages/settings_how_it_works.dart';
import 'package:bedo_shop_admin/presentation/screens/settings_pages/settings_lang.dart';
import 'package:bedo_shop_admin/presentation/screens/settings_pages/settings_notification.dart';
import 'package:bedo_shop_admin/presentation/screens/settings_pages/settings_password.dart';
import 'package:bedo_shop_admin/presentation/screens/splash.dart';
import 'package:bedo_shop_admin/presentation/widgets/custom_text_field.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';

import '../login.dart';

final String assetName = 'assets/svg/Google__G__Logo.svg';
var f = NumberFormat("####-####-####-####");

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
  static const routeName = 'settings';
}

class _SettingsState extends State<Settings> {
  @override
  void initState() {
    context.read<SettingsBloc>().add(SettingsVisited());
    super.initState();
  }
  //! STATES GET CHANGED OUT OF THIS WIDGET
  // @override
  // void didUpdateWidget(Settings oldWidget) {
  //   super.didUpdateWidget(oldWidget);
  //   context.read<SettingsBloc>().add(SettingsVisited());
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // bottomNavigationBar: CustomBottomNavigationBar(),
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          S.of(context).settings,
          style: grey14medium,
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          buildUserNameTile(
              logoutText: S.of(context).logout,
              onPressedLogout: () {
                showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text(
                        'are you sure you want to logout',
                        style: black14medium,
                        textAlign: TextAlign.center,
                      ),
                      actions: [
                        FlatButton(
                          onPressed: () async {
                            context.read<SettingsBloc>().add(Logout());
                            Navigator.of(context).pop();
                            Navigator.of(context).pushNamed(Login.routeName);
                          },
                          child: Text(
                            'yes',
                            style: red12semibold,
                          ),
                        ),
                        FlatButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            'cancel',
                            style: black12regular,
                          ),
                        )
                      ],
                    );
                  },
                );
                printYellowBg('logout');
              }),
          CustomDivider(),
          buildCreditCardTile(
            optionLable: S.of(context).change,
            lable: S.of(context).enterCreditCardNumber, //
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return SettingsDebitCard();
              })).then((value) => BlocProvider.of<SettingsBloc>(context)
                  .add(SettingsVisited()));
            },
          ),
          CustomDivider(),
          buildLanguageTile(
            optionLable: S.of(context).change,
            lable: S.of(context).language,
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) {
                    return BlocBuilder<LangBloc, LangState>(
                      builder: (context, state) {
                        return SettingsLanguage(
                          currentLangue: state.languageCode,
                        );
                      },
                    );
                  },
                ),
              ).then((value) => BlocProvider.of<SettingsBloc>(context)
                  .add(SettingsVisited()));
            },
          ),
          CustomDivider(),
          buildPasswordChangeTile(
            optionLable: S.of(context).change,
            lable: S.of(context).password,
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) {
                    return SettingsPassword();
                  },
                ),
              ).then((value) => BlocProvider.of<SettingsBloc>(context)
                  .add(SettingsVisited()));
            },
          ),
          CustomDivider(),
          buildNotificationTile(
              optionLable: S.of(context).change,
              lable: S.of(context).notification,
              onPressed: () {
                // BlocProvider.of<SettingsBloc>(context).add(SettingsVisited());
                BlocProvider.of<NotificationsBloc>(context)
                    .add(NotificationsVisited());

                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return SettingsNotification();
                }));
              }),
          CustomDivider(),
          buildFAQ(
              lable: 'how it works',
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return SettingsHowItWorks();
                })).then((value) => BlocProvider.of<SettingsBloc>(context)
                        .add(SettingsVisited()));
              }),
          CustomDivider(),
        ],
      ),
    );
  }

  // 'name': d['name'],
  // 'cardNumber': d['cardNumber'],
  // 'phoneNumbers': d['phoneNumbers'][0],
  // // 'preparingTime': d['preparingTime'],
  // 'fullName': d['shopAdmin']['fullName'],
  // 'email': d['shopAdmin']['email'],

  Padding buildUserNameTile(
      {final VoidCallback onPressedLogout, final String logoutText}) {
    return Padding(
      padding: const EdgeInsets.only(left: 16),
      child: Row(
        children: [
          BlocConsumer<SettingsBloc, SettingsState>(
            listener: (context, state) {},
            builder: (context, state) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    (state is SettingsLoadSuccess)
                        ? state.data['fullName'].toString()
                        : 'USER NAME',
                    style: black18medium,
                  ),
                  SizedBox(
                    height: 9,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        height: 15,
                        width: 15,
                        child: (state is SettingsLoadSuccess &&
                                state.data['email'].toString().isGmail())
                            ? SvgPicture.asset(
                                assetName,
                                fit: BoxFit.contain,
                                semanticsLabel: 'google Logo',
                              )
                            : null,
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      if (state is SettingsLoadSuccess)
                        Text(
                          (state.data['email'] ?? state.data['phoneNumbers'])
                              .toString()
                              .toString(),
                          style: grey12regular,
                        ),
                    ],
                  ),
                ],
              );
            },
          ),
          FlatButton(
            splashColor: lightGreyBg,
            onPressed: onPressedLogout,
            child: UnderlineText(
              title: logoutText,
            ),
          )
        ],
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
      ),
    );
  }

  FlatButton buildCreditCardTile(
      {final VoidCallback onPressed,
      final String lable,
      final String optionLable}) {
    return FlatButton(
      splashColor: lightGreyBg,
      onPressed: onPressed,
      child: CustomRow(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                lable,
                style: black12regular,
              ),
              SizedBox(
                height: 7,
              ),
              BlocBuilder<SettingsBloc, SettingsState>(
                builder: (context, state) {
                  return Text(
                    ((state is SettingsLoadSuccess)
                            ? state.data['cardNumber']
                            : 'xxxx xxxx xxxx xxxx')
                        .toString(),
                    style: green12regular,
                  );
                },
              )
            ],
          ),
          UnderlineText(
            title: optionLable,
          )
        ],
      ),
    );
  }

  FlatButton buildLanguageTile(
      {final VoidCallback onPressed,
      final String lable,
      final String optionLable}) {
    return FlatButton(
      splashColor: lightGreyBg,
      onPressed: onPressed,
      child: CustomRow(
        children: [
          Text(
            lable,
            style: black12regular,
          ),
          Row(
            children: [
              BlocBuilder<LangBloc, LangState>(
                builder: (context, state) {
                  print(state);
                  return Text(
                    state.langueNameInFull,
                    style: green12regular,
                  );
                },
              ),
              SizedBox(
                width: 15,
              ),
              UnderlineText(
                title: optionLable,
              )
            ],
          )
        ],
      ),
    );
  }

  FlatButton buildPasswordChangeTile(
      {final VoidCallback onPressed,
      final String lable,
      final String optionLable}) {
    return FlatButton(
      splashColor: lightGreyBg,
      onPressed: onPressed,
      child: CustomRow(
        children: [
          Text(
            lable,
            style: black12regular,
          ),
          UnderlineText(
            title: optionLable,
          )
        ],
      ),
    );
  }

  FlatButton buildNotificationTile(
      {final VoidCallback onPressed,
      final String lable,
      final String optionLable}) {
    return FlatButton(
      splashColor: lightGreyBg,
      onPressed: onPressed,
      child: CustomRow(
        children: [
          Text(
            lable,
            style: black12regular,
          ),
          UnderlineText(
            title: optionLable,
          )
        ],
      ),
    );
  }

  FlatButton buildFAQ({final String lable, final VoidCallback onPressed}) {
    return FlatButton(
      splashColor: lightGreyBg,
      onPressed: onPressed,
      child: Row(
        children: [
          Text(
            lable,
            textAlign: TextAlign.start,
            style: black12regular,
          ),
        ],
      ),
    );
  }
}

class CustomDivider extends StatelessWidget {
  const CustomDivider({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3),
      child: Divider(
        // thickness: .7,
        endIndent: 21.5,
        indent: 16,
      ),
    );
  }
}

class UnderlineText extends StatelessWidget {
  final String title;
  const UnderlineText({
    Key key,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: grey12regular.copyWith(decoration: TextDecoration.underline),
    );
  }
}

class CustomRow extends StatelessWidget {
  final List<Widget> children;
  const CustomRow({
    Key key,
    this.children,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: children,
    );
  }
}
