import 'package:bedo_shop_admin/bloc/settings_bloc/settings_bloc.dart';
import 'package:bedo_shop_admin/data/graphql/settings/settings_gql.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/widgets/bottom_navigation.dart';
import 'package:bedo_shop_admin/presentation/widgets/custom_text_field.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql/client.dart';

class SettingsDebitCard extends StatelessWidget {
  final _controller = TextEditingController(text: '');
  final _key = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: CustomBottomNavigationBar(),
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          S.of(context).creditCardNumber,
          style: grey14medium,
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: const EdgeInsets.all(24),
            child: Text(
              S.of(context).editCreditCardNumber,
              textAlign: TextAlign.start,
              style: black18medium,
            ),
          ),
          Form(
            key: _key,
            child: CustomTextField(
              title: S.of(context).creditCardNumber,
              hintText: S.of(context).enterYourCreditCardNumberHere,
              controller: _controller,
              // onChanged: (t) {
              //   printBlueBg(t.replaceAll('-', ''));
              // },
              keyboardType: TextInputType.number,
              style: black14medium.copyWith(letterSpacing: 3, fontSize: 14),
              hintStyle: grey14regular.copyWith(letterSpacing: 1),
              validator: (v) {
                String g = v.replaceAll('-', '');
                if (g.length < 16) {
                  return 'must be more than ${g.length} characters\nat least 16 characters';
                }
                return null;
              },
              formatters: [
                /// ```
                CustomMaskedTextInputFormatter(
                  mask: 'xxxx-xxxx-xxxx-xxxx-XXXX-XXXX',
                  separator: '-',
                ),

                ///
                /// ```
                ///
                ///
                ///
                /// works for some cards
              ],
            ),
          ),
          Expanded(child: SizedBox()),
          Padding(
            padding: const EdgeInsets.all(24),
            child: BlocListener<SettingsBloc, SettingsState>(
              listener: (context, state) {
                if (state is ChangeCardResult) {
                  showModalBottomSheet(
                      context: context,
                      builder: (context) => Padding(
                            padding: const EdgeInsets.all(24),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Expanded(
                                  child: Center(
                                    child: Text(
                                      '${state.msg}',
                                      textAlign: TextAlign.center,
                                      style: ((state.success)
                                              ? black12regular
                                              : red12semibold)
                                          .copyWith(fontSize: 16),
                                    ),
                                  ),
                                ),
                                RaisedButton(
                                    child: Text('ok'),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                      Navigator.of(context).pop();
                                    })
                              ],
                            ),
                          ));
                }
              },
              child: RaisedButton(
                onPressed: () {
                  FocusScope.of(context).unfocus();
                  _key.currentState.validate();
                  if (_key.currentState.validate()) {
                    final x = _controller.text.replaceAll('-', '');
                    print(x);
                    BlocProvider.of<SettingsBloc>(context)
                        .add(CardNumberChanging(x));
                  }
                },
                child: Text(S.of(context).submit),
              ),
            ),
          )
        ],
      ),
    );
  }
}
