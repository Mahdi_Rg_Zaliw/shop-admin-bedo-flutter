import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:flutter/material.dart';

import '../custom_expansion_tile.dart';

class SettingsFAQ extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // bottomNavigationBar: CustomBottomNavigationBar(),
      appBar: AppBar(
        bottom: PreferredSize(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Row(
                children: [
                  Expanded(
                    child: FlatButton(
                      onPressed: () {},
                      // splashColor: red,
                      // trailing: SizedBox(),
                      child: Row(
                        children: [
                          Text(
                            S.of(context).faq,
                            style: black18medium,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            preferredSize: Size.fromHeight(kToolbarHeight)),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: ListView(
          shrinkWrap: true,
          children: [
            SizedBox(
              height: 24,
            ),
            CustomExpansionTile(
              childrenPadding: EdgeInsets.all(12),
              title: Text(
                  'Duis ipsum proident fugiat ipsum velit veniam do ea velit consequat enim labore Lorem nisi.',
                  style: black14medium),
              children: [
                Text(
                    'Officia nostrud consequat magna commodo excepteur quis exercitation id mollit consectetur. Adipisicing quis nostrud velit consectetur et eiusmod est laborum veniam pariatur. Consectetur ex quis excepteur et sit irure commodo enim dolore. Fugiat nostrud duis voluptate nulla est magna non. Laborum minim sint culpa nulla minim ut ipsum veniam occaecat anim occaecat tempor quis. Nulla excepteur adipisicing aliqua labore minim eu irure eiusmod. Exercitation ad duis sit excepteur elit ad commodo incididunt.')
              ],
              trailing: Icon(
                Icons.arrow_forward_ios,
                color: textColor,
                size: 15,
              ),
            ),
            CustomExpansionTile(
              childrenPadding: EdgeInsets.all(12),
              title: Text(
                  'Duis ipsum proident fugiat ipsum velit veniam do ea velit consequat enim labore Lorem nisi.',
                  style: black14medium),
              children: [
                Text(
                    'Officia nostrud consequat magna commodo excepteur quis exercitation id mollit consectetur. Adipisicing quis nostrud velit consectetur et eiusmod est laborum veniam pariatur. Consectetur ex quis excepteur et sit irure commodo enim dolore. Fugiat nostrud duis voluptate nulla est magna non. Laborum minim sint culpa nulla minim ut ipsum veniam occaecat anim occaecat tempor quis. Nulla excepteur adipisicing aliqua labore minim eu irure eiusmod. Exercitation ad duis sit excepteur elit ad commodo incididunt.')
              ],
              trailing: Icon(
                Icons.arrow_forward_ios,
                color: textColor,
                size: 15,
              ),
            ),
            CustomExpansionTile(
              childrenPadding: EdgeInsets.all(12),
              title: Text(
                  'Duis ipsum proident fugiat ipsum velit veniam do ea velit consequat enim labore Lorem nisi.',
                  style: black14medium),
              children: [
                Text(
                    'Officia nostrud consequat magna commodo excepteur quis exercitation id mollit consectetur. Adipisicing quis nostrud velit consectetur et eiusmod est laborum veniam pariatur. Consectetur ex quis excepteur et sit irure commodo enim dolore. Fugiat nostrud duis voluptate nulla est magna non. Laborum minim sint culpa nulla minim ut ipsum veniam occaecat anim occaecat tempor quis. Nulla excepteur adipisicing aliqua labore minim eu irure eiusmod. Exercitation ad duis sit excepteur elit ad commodo incididunt.')
              ],
              trailing: Icon(
                Icons.arrow_forward_ios,
                color: textColor,
                size: 15,
              ),
            ),
            CustomExpansionTile(
              childrenPadding: EdgeInsets.all(12),
              title: Text(
                  'Duis ipsum proident fugiat ipsum velit veniam do ea velit consequat enim labore Lorem nisi.',
                  style: black14medium),
              children: [
                Text(
                    'Officia nostrud consequat magna commodo excepteur quis exercitation id mollit consectetur. Adipisicing quis nostrud velit consectetur et eiusmod est laborum veniam pariatur. Consectetur ex quis excepteur et sit irure commodo enim dolore. Fugiat nostrud duis voluptate nulla est magna non. Laborum minim sint culpa nulla minim ut ipsum veniam occaecat anim occaecat tempor quis. Nulla excepteur adipisicing aliqua labore minim eu irure eiusmod. Exercitation ad duis sit excepteur elit ad commodo incididunt.')
              ],
              trailing: Icon(
                Icons.arrow_forward_ios,
                color: textColor,
                size: 15,
              ),
            )
          ],
        ),
      ),
    );
  }
}
