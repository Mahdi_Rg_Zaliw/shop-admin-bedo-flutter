import 'package:bedo_shop_admin/bloc/notifications_bloc/notifications_bloc.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/screens/mohammads_show_case/custom_switch.dart';
import 'package:bedo_shop_admin/presentation/screens/settings_pages/settings.dart';
import 'package:bedo_shop_admin/presentation/widgets/bottom_navigation.dart';
import 'package:bedo_shop_admin/presentation/widgets/toggle_switch.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SettingsNotification extends StatefulWidget {
  @override
  _SettingsNotificationState createState() => _SettingsNotificationState();
}

class _SettingsNotificationState extends State<SettingsNotification> {
  // @override
  // void initState() {
  //   super.initState();
  //   BlocProvider.of<NotificationsBloc>(context).add(NotificationsVisited());
  // }

  @override
  Widget build(BuildContext context) {
    // BlocProvider.of<NotificationsBloc>(context).add(NotificationsVisited());

    return Scaffold(
      // bottomNavigationBar: CustomBottomNavigationBar(),
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          S.of(context).notificationSettings,
          style: grey14medium,
        ),
      ),
      body: BlocConsumer<NotificationsBloc, NotificationsState>(
        listener: (context, state) {
          printColor(Colors.pink, 'state is $state');
          setState(() {});
        },
        builder: (context, state) {
          printColor(Colors.pink, 'state is $state');
          if (state is NotificationsInitial) {
            BlocProvider.of<NotificationsBloc>(context)
                .add(NotificationsVisited());
          }
          if (state is NotificationResult) {
            printColor(Colors.lime, 'state is $state the right choice execec');

            final d = state.data;
            final sortedData = [
              d['isOrderNotificationActive'],
              d['isOrderStatusNotificationActive'],
              d['isPaymentNotificationActive'],
              d['isDeliveryNotificationActive'],
              d['isMessageNotificationActive'],
            ];
            return NotificationToggleList(
              data: sortedData,
            );
          } else {
            printColor(
                Colors.purple, 'state is $state the wrong choice execec');
            return CupertinoActivityIndicator();
            // return NotificationToggleList();
          }
        },
      ),
    );
  }
}

class NotificationToggleList extends StatelessWidget {
  final List data;
  const NotificationToggleList({
    Key key,
    this.data = const [
      false,
      false,
      false,
      false,
      false,
    ],
  }) : super(key: key);

// isOrderNotificationActive
// isOrderStatusNotificationActive
// isPaymentNotificationActive
// isDeliveryNotificationActive
// isMessageNotificationActive
//
//
//
//
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 12,
        ),
        SettingsToggle(
          initialValue: data[0],
          onToggle: (v) {
            BlocProvider.of<NotificationsBloc>(context)
                .add(NotificationsChanged({'isOrderNotificationActive': v}));
            print(v);
          },
          title: S.of(context).newOrderNotification,
        ),
        Divider(),
        SettingsToggle(
          initialValue: data[1],
          onToggle: (v) {
            BlocProvider.of<NotificationsBloc>(context).add(
                NotificationsChanged({'isOrderStatusNotificationActive': v}));
            print(v);
          },
          title: S.of(context).orderStatusNotification,
        ),
        Divider(),
        SettingsToggle(
          initialValue: data[2],
          onToggle: (v) {
            BlocProvider.of<NotificationsBloc>(context)
                .add(NotificationsChanged({'isPaymentNotificationActive': v}));
            print(v);
          },
          title: S.of(context).paymentNotification,
        ),
        Divider(),
        SettingsToggle(
          initialValue: data[3],
          onToggle: (v) {
            BlocProvider.of<NotificationsBloc>(context)
                .add(NotificationsChanged({'isDeliveryNotificationActive': v}));
            print(v);
          },
          title: S.of(context).deliveryNotification,
        ),
        Divider(),
        SettingsToggle(
          initialValue: data[4],
          onToggle: (v) {
            BlocProvider.of<NotificationsBloc>(context)
                .add(NotificationsChanged({'isMessageNotificationActive': v}));
            print(v);
          },
          title: S.of(context).messageNotification,
        ),
        Divider(),
      ],
    );
  }
}

class SettingsToggle extends StatelessWidget {
  final String title;
  final bool initialValue;
  final void Function(bool) onToggle;
  const SettingsToggle({
    Key key,
    this.title,
    this.initialValue,
    this.onToggle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Text(
          title,
          style: grey12regular,
        ),
        ToggleSwitch(
          initialValue: initialValue,
          onToggle: onToggle,
        )
      ]),
    );
  }
}
