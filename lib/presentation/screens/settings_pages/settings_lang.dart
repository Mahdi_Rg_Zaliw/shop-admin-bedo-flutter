import 'package:bedo_shop_admin/bloc/lang_bloc/lang_bloc.dart';
import 'package:bedo_shop_admin/data/secure_storage/secure_storage.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/widgets/bottom_navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class SettingsLanguage extends StatefulWidget {
  final String currentLangue;

  const SettingsLanguage({Key key, this.currentLangue = 'en'})
      : super(key: key);
  @override
  _SettingsLanguageState createState() => _SettingsLanguageState();
}

class _SettingsLanguageState extends State<SettingsLanguage> {
  String selectedLang = '';
  @override
  void initState() {
    super.initState();
    selectedLang = widget.currentLangue;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // bottomNavigationBar: CustomBottomNavigationBar(),
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          S.of(context).selectLanguage,
          style: grey14medium,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            FlatButton(
              onPressed: () {
                if (selectedLang != 'en') {
                  setState(() {
                    selectedLang = 'en';
                  });
                }
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'English',
                    style:
                        selectedLang == 'en' ? green12semiBold : grey12regular,
                  ),
                  if (selectedLang == 'en')
                    Icon(
                      Icons.check,
                      color: primaryMaterialColor,
                      size: 15,
                    )
                ],
              ),
            ),
            Divider(),
            FlatButton(
              onPressed: () {
                if (selectedLang != 'ru') {
                  setState(() {
                    selectedLang = 'ru';
                  });
                }
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'русский',
                    style:
                        selectedLang == 'ru' ? green12semiBold : grey12regular,
                  ),
                  if (selectedLang == 'ru')
                    Icon(
                      Icons.check,
                      color: primaryMaterialColor,
                      size: 15,
                    )
                ],
              ),
            ),
            Divider(),
            FlatButton(
              onPressed: () {
                if (selectedLang != 'az') {
                  setState(() {
                    selectedLang = 'az';
                  });
                }
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Azerbayjani',
                    style:
                        selectedLang == 'az' ? green12semiBold : grey12regular,
                  ),
                  if (selectedLang == 'az')
                    Icon(
                      Icons.check,
                      color: primaryMaterialColor,
                      size: 15,
                    )
                ],
              ),
            ),
            Expanded(child: SizedBox()),
            Padding(
              padding: const EdgeInsets.all(24),
              child: RaisedButton(
                onPressed: () {
                  context.read<LangBloc>().add(LnagChange('$selectedLang'));
                  SecureStorage.setLocale('$selectedLang');
                  SecureStorage.getLocale()
                      .then((value) => print(value + ' is saved'));
                },
                child: Expanded(
                  child: Text(S.of(context).submit),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
