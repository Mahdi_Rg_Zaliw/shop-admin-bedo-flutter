import 'package:bedo_shop_admin/bloc/settings_bloc/settings_bloc.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/presentation/widgets/comment_widggets/comment_text.dart';
import 'package:bedo_shop_admin/presentation/widgets/round_botton.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SettingsHowItWorks extends StatefulWidget {
  @override
  _SettingsHowItWorksState createState() => _SettingsHowItWorksState();
}

class _SettingsHowItWorksState extends State<SettingsHowItWorks> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<SettingsBloc>(context).add(HowItWorksVisited());
  }

  // @override
  // void dispose() {
  //   super.dispose();
  //   BlocProvider.of<SettingsBloc>(context).add(SettingsVisited());
  // }

  @override
  Widget build(BuildContext context) {
    // BlocProvider.of<SettingsBloc>(context).add(HowItWorksVisited());
    // printCyanBg(BlocProvider.of<SettingsBloc>(context).state.toString());
    return Scaffold(
      // body: ,
      appBar: buildAppBar('how it works'),
      body: Padding(
        padding: const EdgeInsets.only(top: 24),
        child: BlocBuilder<SettingsBloc, SettingsState>(
          builder: (context, state) {
            if (state is HowItWorksLoaded) {
              if (state.data.length == 0) {
                return Center(child: Text('no how it works data'));
              }
              return ListView.builder(
                itemBuilder: (context, index) {
                  return HTile(
                      title: state.data[index].title,
                      desc: state.data[index].describtion);
                },
                itemCount: state.data.length,
              );
            }
            return SizedBox();
          },
        ),
      ),
    );
  }
}

AppBar buildAppBar(String title) {
  return AppBar(
    automaticallyImplyLeading: true,

    // toolbarHeight: 3*kToolbarHeight,
    bottom: PreferredSize(
      preferredSize: Size.fromHeight(kToolbarHeight),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 24,
              vertical: 5,
            ),
            child: Text(
              title.toString(),
              textAlign: TextAlign.left,
              style: black24medium,
            ),
          ),
        ],
      ),
    ),
  );
}

class HTile extends StatelessWidget {
  final String title;
  final String desc;

  const HTile({Key key, this.title, this.desc}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 8),
      child: Buttons.grey(
        radius: 10,
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => Scaffold(
              appBar: buildAppBar('$title'),
              body: Padding(
                padding: const EdgeInsets.all(24),
                child: SingleChildScrollView(
                  child: CommentText(
                    text: '$desc',
                    color: textColor,
                  ),
                ),
              ),
            ),
          ));
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('$title', style: black14medium),
              Icon(
                Icons.arrow_forward_ios,
                color: textColor,
                size: 16,
              )
            ],
          ),
        ),
      ),
    );
  }
}
