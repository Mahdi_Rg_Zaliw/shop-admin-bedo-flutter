import 'package:bedo_shop_admin/bloc/courier_bloc/courier_bloc.dart';
import 'package:bedo_shop_admin/bloc/courier_bloc/courier_event.dart';
import 'package:bedo_shop_admin/bloc/courier_bloc/courier_state.dart';
import 'package:bedo_shop_admin/config/config.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/presentation/widgets/custom_text_field.dart';
import 'package:bedo_shop_admin/presentation/widgets/radio_button_bedo.dart';
import 'package:bedo_shop_admin/presentation/screens/courier_list_screen.dart';
import 'package:bedo_shop_admin/presentation/screens/vanils_showcases/upload_image_test.dart';
import 'package:bedo_shop_admin/presentation/widgets/pick_picture_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:connectivity/connectivity.dart';
import 'dart:io';
import 'package:graphql/client.dart';
import 'package:flutter/cupertino.dart';

class CourierCreateScreen extends StatefulWidget {
  static const routeName = 'courrier_create';
  @override
  _CourierCreateScreenState createState() => _CourierCreateScreenState();
}

class _CourierCreateScreenState extends State<CourierCreateScreen> {
  GlobalKey<FormState> _formKey = GlobalKey();

  final Map<String, String> _formData = {
    'fullName': '',
    'phoneNumber': '',
    'email': '',
    'password': '',
  };
  File uploadImageFile;
  String imageUrl = '';
  String gender = 'Male';
  String errorText = '';
  bool loadingImage = false;

  void dataFormClear() {
    _formKey.currentState.reset();
    setState(() {
      gender = '';
      imageUrl = '';
    });
  }

  Future courierCreatedSuccessModal({
    BuildContext context,
    String fullName,
    String email,
    String phoneNumber,
    String gender,
    String profileUrl,
  }) {
    return showModalBottomSheet(
      isScrollControlled: false,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(24),
          topRight: Radius.circular(24),
        ),
      ),
      builder: (BuildContext context) {
        return Container(
          padding: EdgeInsets.all(33),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'User \"$fullName\" \n With Phone number $phoneNumber \n and Email $email \n and Photo Url $profileUrl \n Successfully Created',
                textAlign: TextAlign.center,
                style: TextStyle(
                  height: 1.5,
                  color: textColor,
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: RaisedButton(
                  onPressed: () async {
                    Navigator.pop(context);
                    Navigator.pop(context);
                  },
                  child: Text('OK'),
                ),
              ),
            ],
          ),
        );
      },
      context: context,
    );
  }

  @override
  void initState() {
    super.initState();
    gender = "MALE";
  }

  Future _uploadImg(img) async {
    setState(() {
      loadingImage = true;
    });
    QueryResult rawData;
    rawData = await upLoadFileMutation(
      file: img,
    );
    print(rawData);

    if (rawData.hasException) {
      print(rawData.exception);

      print('has error');
      return;
    } else if (rawData.data == null) {
      print('null data');
      return;
    } else {
      String url;

      url = rawData.data['uploadFile']['url'];
      print(url);
      setState(() {
        imageUrl = uri.replaceAll('graphql', '') + url;
        loadingImage = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text(
          S.of(context).addCourier,
          style: grey14medium,
        ),
        centerTitle: true,
      ),
      body: Container(
        width: width,
        height: height,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8),
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: PickPictureButton(
                        // previewImg: imageUrl == null ? '' : imageUrl,
                        // pickerType: PickerType.edit,
                        onChanged: (File file) {
                          setState(() {
                            uploadImageFile = file;
                          });
                        },
                      ),
                    ),
                    loadingImage == true
                        ? Align(
                            alignment: Alignment.center,
                            child: Container(
                              alignment: Alignment.center,
                              width: 80,
                              height: 80,
                              child: Container(
                                width: 30,
                                height: 30,
                                child: CircularProgressIndicator(
                                  strokeWidth: 3.0,
                                ),
                              ),
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
              SizedBox(
                height: height * 0.05,
              ),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    CustomTextField(
                      validator: (String value) {
                        if (value.trim().isEmpty) {
                          return 'Full Name Cannot Be Empty !';
                        }
                        return null;
                      },
                      isRequired: true,
                      title: S.of(context).courierFullName,
                      hintText: S.of(context).enterTheCourierName,
                      onSaved: (String value) {
                        setState(() {
                          _formData['fullName'] = value;
                        });
                      },
                    ),
                    const SizedBox(height: 10),
                    CustomTextField(
                      validator: (String value) {
                        if (value.trim().isEmpty) {
                          return 'Phone Number Cannot Be Empty !';
                        }
                        return null;
                      },
                      isRequired: true,
                      title: S.of(context).courierPhoneNumber,
                      hintText: S.of(context).enterTheCourierPhoneNumber,
                      keyboardType: TextInputType.phone,
                      onSaved: (String value) {
                        setState(() {
                          _formData['phoneNumber'] = value;
                        });
                      },
                    ),
                    const SizedBox(height: 10),
                    CustomTextField(
                      validator: (String value) {
                        if (value.trim().isEmpty) {
                          return 'Email Cannot Be Empty !';
                        } else if (!value.isValidEmail()) {
                          return 'Must Be An Email Format !';
                        }
                        return null;
                      },
                      isRequired: true,
                      title: S.of(context).courierEmailAddress,
                      hintText: S.of(context).enterTheCourierEmailAddress,
                      onSaved: (String value) {
                        print(value);
                        setState(() {
                          _formData['email'] = value;
                        });
                      },
                    ),
                    const SizedBox(height: 10),
                    CustomTextField(
                      validator: (String value) {
                        if (value.trim().isEmpty) {
                          return 'Password Cannot Be Empty !';
                        }
                        return null;
                      },
                      isRequired: true,
                      title: S.of(context).courierPassword,
                      hintText: S.of(context).enterTheCourierPassword,
                      isPassword: true,
                      onSaved: (String value) {
                        setState(() {
                          _formData['password'] = value;
                        });
                      },
                    ),
                    const SizedBox(height: 10),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: width * 0.06),
                          child: Text(
                            "Courier Gender",
                            style: grey12regular,
                          ),
                        ),
                        RadioButtonBedo(
                          onChanged: (String value) {
                            print(value);
                            setState(() {
                              gender = value;
                            });
                          },
                          direction: Axis.horizontal,
                          radioButtonList: [
                            S.of(context).male,
                            S.of(context).female
                          ],
                          radioValueIndex: gender == 'Female' ? 1 : 0,
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                  ],
                ),
              ),
              const SizedBox(height: 10),
              Container(
                width: width,
                padding: EdgeInsets.symmetric(horizontal: width * 0.06),
                height: 55,
                child: BlocConsumer<CourierBloc, CourierState>(
                  listener: (context, state) {
                    if (state is CourierCreatedSuccess) {
                      courierCreatedSuccessModal(
                        context: context,
                        fullName: _formData['fullName'],
                        phoneNumber: _formData['phoneNumber'],
                        email: _formData['email'],
                        gender: gender.toUpperCase(),
                        profileUrl: imageUrl,
                      );
                      dataFormClear();
                    }

                    if (state is CourierExceptionState) {
                      setState(() {
                        errorText = 'Failed To Create Courier!';
                      });
                      dataFormClear();
                    }
                  },
                  builder: (context, state) {
                    return RaisedButton(
                      child: Text(S.of(context).addCourier),
                      onPressed: loadingImage == true
                          ? null
                          : () async {
                              if (uploadImageFile != null) {
                                await _uploadImg(uploadImageFile);
                              }
                              _formKey.currentState.save();
                              CreateCourierEvent createCourierEvent =
                                  CreateCourierEvent(
                                data: {
                                  "fullName": _formData['fullName'],
                                  "phoneNumber": _formData['phoneNumber'],
                                  "email": _formData['email'],
                                  "password": _formData['password'],
                                  "gender": gender.toUpperCase(),
                                  "profileImageUrl": imageUrl,
                                },
                              );
                              var connectivityResult =
                                  await (Connectivity().checkConnectivity());
                              if (connectivityResult ==
                                      ConnectivityResult.mobile ||
                                  connectivityResult ==
                                      ConnectivityResult.wifi) {
                                if (_formKey.currentState.validate()) {
                                  setState(() {
                                    errorText = '';
                                  });
                                  context
                                      .read<CourierBloc>()
                                      .add(createCourierEvent);
                                } else {
                                  setState(() {
                                    errorText = 'Input Invalid !';
                                  });
                                }
                              } else {
                                setState(() {
                                  errorText = 'Network Error';
                                });
                              }
                              // dataFormClear();
                            },
                      elevation: 0,
                      highlightElevation: 1,
                    );
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8),
                child: Text(
                  errorText,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: red),
                ),
              ),
              const SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }
}
