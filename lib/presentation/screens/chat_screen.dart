import 'package:bedo_shop_admin/bloc/conversations_bloc/conversations_bloc.dart';
import 'package:bedo_shop_admin/bloc/conversations_bloc/conversations_events.dart';
import 'package:bedo_shop_admin/model/conversation_model.dart';
import 'package:bedo_shop_admin/presentation/widgets/messages.dart';
import 'package:bedo_shop_admin/presentation/widgets/new_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ChatScreen extends StatefulWidget {
  final Conversation conversation;

  ChatScreen(this.conversation);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  @override
  void initState() {
    BlocProvider.of<ConversationBloc>(context)
        .add(LoadConversation(widget.conversation.ID));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Chat'),
        centerTitle: true,
      ),
      body: Container(
        child: Column(
          children: [
            Expanded(child: Messages(widget.conversation)),
            NewMessage(widget.conversation.ID),
          ],
        ),
      ),
    );
  }
}
