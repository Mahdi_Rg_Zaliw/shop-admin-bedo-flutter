import 'package:bedo_shop_admin/bloc/router_bloc/signinrouter_bloc.dart';
import 'package:bedo_shop_admin/constants/constants.dart';
import 'package:bedo_shop_admin/data/secure_storage/secure_storage.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/presentation/screens/add_shop.dart';
import 'package:bedo_shop_admin/presentation/screens/edit_shop.dart';
import 'package:bedo_shop_admin/presentation/screens/home.dart';
import 'package:bedo_shop_admin/presentation/screens/homePage.dart';
import 'package:bedo_shop_admin/presentation/screens/login.dart';
import 'package:bedo_shop_admin/presentation/widgets/round_botton.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Splash extends StatefulWidget {
  static const routeName = 'splash';

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  initMethod(context) async {
    String _token = await SecureStorage.getToken(accessTokenKey);
    if (_token == null || _token == "") {
      printOrange('user has not logged in');
      Navigator.of(context).pushReplacementNamed(Login.routeName);
    } else {
      //decide page
      printOrange('go to router bloc');
      BlocProvider.of<SigninrouterBloc>(context).add(UserLoggedInSuccess());
    }
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      printOrange('init');
      initMethod(context);
    });

    return Scaffold(
      backgroundColor: primaryMaterialColor,
      body: BlocConsumer<SigninrouterBloc, SigninrouterState>(
        // listener: (context, state) {
        listener: (context, state) {
          printGreenBg(state);
          if (state is UserHasNoShop) {
            Navigator.of(context).pushReplacementNamed(AddShopPage.routeName);
            printRedBg('redirect');
          }
          if (state is UserHasShop) {
            Navigator.of(context).pushReplacementNamed(HomePage.routeName);
            printRedBg('redirect');
          }
        },

        builder: (context, state) {
          printGreenBg('state is $state');
          if (state is SomethingWentWrong) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'something went wrong',
                    style: white14medium,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  FlatBtn.transparentWhiteOutline(
                    child: Text(
                      'try again',
                      style: white14medium,
                    ),
                    onPressed: () {
                      initMethod(context);
                    },
                  )
                ],
              ),
            );
          }
          return Center(
            child: new CircularProgressIndicator(
              backgroundColor: Colors.white,
            ),
          );
        },
      ),
    );
  }
}
