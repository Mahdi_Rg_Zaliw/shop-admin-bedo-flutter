import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/screens/prouct_pages/product_management_list.dart';
import 'package:bedo_shop_admin/presentation/widgets/bottom_navigation.dart';
import 'package:bedo_shop_admin/presentation/widgets/filtered_list.dart';
import 'package:flutter/material.dart';

import 'menu_management_list.dart';

class ProductsPage extends StatelessWidget {
  static const String routeName = '/ProductsPage';
  @override
  Widget build(BuildContext context) {
    return FilteredList.common(
      optionsList: [
        S.of(context).menuManagement,
        S.of(context).productManagement
      ],
      topTitlesList: [
        Text(
          S.of(context).numberOfMenus,
        ),
        Text(S.of(context).numberOfProducts)
      ],
      pagesList: [MenuManagementList(), ProductManagementList()],
      bottomTitleList: [
        Text(
          '561',
          style: black24medium,
        ),
        Text(
          '352',
          style: black24medium,
        ),
      ],
      optionShouldNotifyList: [false, false],
      // bottomNavigationBar: CustomBottomNavigationBar(),
    );
  }
}
