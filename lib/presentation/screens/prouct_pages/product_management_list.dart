import 'package:bedo_shop_admin/bloc/product_bloc/product_bloc.dart';
import 'package:bedo_shop_admin/bloc/product_bloc/product_state.dart';
import 'package:bedo_shop_admin/design/icons_f.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/screens/prouct_pages/add_new_product_page.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/add_to_menu_modal_page.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/custom_modal_bottom_sheet.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/modal_delete_page.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/modal_home_page.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/modal_options.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/modal_slide_transition.dart';
import 'package:bedo_shop_admin/presentation/widgets/products_cart.dart';
import 'package:bedo_shop_admin/presentation/widgets/round_botton.dart';
import 'package:bedo_shop_admin/bloc/product_bloc/product_event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProductManagementList extends StatelessWidget {
  const ProductManagementList({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => ProductBloc(ProductInitial()),
      child: ListView(
        children: [
          BlocBuilder<ProductBloc, ProductState>(
            builder: (context, state) {
              if (state is ProductInitial) {
                context.read<ProductBloc>().add(
                      GetProductsEvent(
                        pagination: {'skip': 0, 'limit': 0},
                      ),
                    );
              }
              if (state is ProductArrived) {
                return Column(
                  children: [
                    ...state.products
                        .map((e) => ProductsCart.meal(
                              imgUrl: 'https://picsum.photos/250',
                              name: e.titleEN,
                              shortDescribtion: e.id,
                              stock: 345,
                              price: 1000,
                              onPressedActions: () {},
                            ))
                        .toList(),
                  ],
                );
              }
              return SizedBox(
                  height: MediaQuery.of(context).size.height / 2,
                  child: Center(child: Text('Nothing Happend')));
            },
          ),
        ],
      ),
    );
  }
}
