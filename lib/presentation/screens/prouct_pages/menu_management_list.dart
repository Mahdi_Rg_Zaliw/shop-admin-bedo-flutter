import 'package:bedo_shop_admin/bloc/menu_bloc/menu_bloc.dart';
import 'package:bedo_shop_admin/bloc/menu_bloc/menu_event.dart';
import 'package:bedo_shop_admin/bloc/menu_bloc/menu_state.dart';
import 'package:bedo_shop_admin/design/icons_f.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/screens/prouct_pages/add_new_menu_item_page.dart';
import 'package:bedo_shop_admin/presentation/screens/prouct_pages/product_management_list.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/custom_modal_bottom_sheet.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/edit_menu_modal_page.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/modal_delete_page.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/modal_home_page.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/modal_options.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/modal_slide_transition.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/add_menu_modal_page.dart';
import 'package:bedo_shop_admin/presentation/widgets/products_cart.dart';
import 'package:bedo_shop_admin/presentation/widgets/round_botton.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';

class MenuManagementList extends StatefulWidget {
  MenuManagementList({Key key}) : super(key: key);

  @override
  _MenuManagementListState createState() => _MenuManagementListState();
}

class _MenuManagementListState extends State<MenuManagementList> {
  String buttonLabel;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => MenuBloc(MenuInitial()),
      child: RefreshIndicator(
        onRefresh: () async {
          BlocProvider.of<MenuBloc>(context).add(GetMenuEvent());
          setState(() {});
        },
        child: BlocBuilder<MenuBloc, MenuState>(
          builder: (context, state) {
            return Stack(
              children: [
                ListView(
                  children: [
                    BlocConsumer<MenuBloc, MenuState>(
                      listener: (context, state) {},
                      builder: (context, state) {
                        if (state is MenuInitial) {
                          context.read<MenuBloc>().add(GetMenuEvent());
                        }
                        if (state is MenuExceptionState) {
                          return SizedBox(
                            height: MediaQuery.of(context).size.height / 2,
                            child: Center(
                              child: Text(
                                  'Failed To get Data From Server! \n ${state.clientErrors}'),
                            ),
                          );
                        }
                        if (state is ShopMenuEmpty) {
                          return Container(
                            alignment: Alignment.center,
                            height: MediaQuery.of(context).size.height / 2,
                            child: Center(
                              child: Text(
                                'There are no submenu yet',
                                style: black14regular,
                              ),
                            ),
                          );
                        }
                        if (state is MenuArrived) {
                          return Column(
                            children: [
                              ...state.menu
                                  .map(
                                    (e) => ProductsCart.menu(
                                      imgUrl: 'https://picsum.photos/250',
                                      name: e.titleEN,
                                      shortDescribtion:
                                          '${state.menu.indexOf(e)} Fruit',
                                      longDescribrion: e.id,
                                      onPressed: () {
                                        final screenHeight =
                                            MediaQuery.of(context).size.height;
                                        customModalSheetWithNavigation(
                                          context: context,
                                          height: screenHeight / 1.7,
                                          child: ModalHomePage(
                                            navKey: GlobalKey<NavigatorState>(),
                                            childBuilder: (modalNav) {
                                              return Padding(
                                                padding:
                                                    const EdgeInsets.all(24),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment
                                                          .stretch,
                                                  children: [
                                                    Center(
                                                      child: Column(
                                                        children: [
                                                          Text(
                                                            '${e.titleEN} Menu',
                                                            style:
                                                                black24medium,
                                                          ),
                                                          Text(
                                                              '\n 4 ${e.titleEN}',
                                                              style:
                                                                  grey14regular),
                                                        ],
                                                      ),
                                                    ),
                                                    Expanded(child: SizedBox()),
                                                    ModaOptionlItem(
                                                      onPress: () {
                                                        Navigator.of(context)
                                                            .pop();
                                                        Navigator.of(context)
                                                            .push(
                                                          MaterialPageRoute(
                                                            builder:
                                                                (context) =>
                                                                    Scaffold(
                                                              appBar: AppBar(
                                                                title: Text(
                                                                  '${e.titleEN}',
                                                                  style:
                                                                      grey14medium,
                                                                ),
                                                                centerTitle:
                                                                    true,
                                                              ),
                                                              body:
                                                                  ProductManagementList(),
                                                            ),
                                                          ),
                                                        ); // modalNav.currentState.push(
                                                        //   Transitions(widget: Placeholder()),
                                                        // );
                                                      },
                                                      title: S
                                                          .of(context)
                                                          .viewProductsMenu,
                                                      icon: Iconsf
                                                          .i51_view_produxt_menue,
                                                    ),
                                                    ModaOptionlItem(
                                                      onPress: () {
                                                        modalNav.currentState
                                                            .push(
                                                          Transitions(
                                                            widget:
                                                                EditMenuModalPage(
                                                              updateNameMenu:
                                                                  e.titleEN,
                                                              menuData:
                                                                  state.menu,
                                                              updateID: state
                                                                  .updateID,
                                                              menuIndex: state
                                                                  .menu
                                                                  .indexOf(e),
                                                            ),
                                                          ),
                                                        )
                                                            .then((value) {
                                                          context
                                                              .read<MenuBloc>()
                                                              .add(
                                                                  GetMenuEvent());

                                                          Navigator.pop(
                                                              context);
                                                        });
                                                      },
                                                      title: S
                                                          .of(context)
                                                          .editMenu,
                                                      icon: Iconsf.i37_peincil,
                                                    ),
                                                    ModaOptionlItem(
                                                      onPress: () {
                                                        modalNav.currentState
                                                            .push(
                                                          Transitions(
                                                            widget:
                                                                ModalDeletePage(
                                                              onCancel: () {
                                                                Navigator.pop(
                                                                    context);
                                                              },
                                                              menuData:
                                                                  state.menu,
                                                              menuIndex: state
                                                                  .menu
                                                                  .indexOf(e),
                                                              updateID: state
                                                                  .updateID,
                                                            ),
                                                          ),
                                                        )
                                                            .then((value) {
                                                          context
                                                              .read<MenuBloc>()
                                                              .add(
                                                                  GetMenuEvent());

                                                          Navigator.pop(
                                                              context);
                                                        });
                                                      },
                                                      title: S
                                                          .of(context)
                                                          .deleteMenu,
                                                      icon: Iconsf.i18_delete,
                                                    ),
                                                    Expanded(child: SizedBox()),
                                                    Buttons.grey(
                                                      onPressed: () async {
                                                        Navigator.of(context)
                                                            .pop();
                                                      },
                                                      child: Text('close'),
                                                    ),
                                                  ],
                                                ),
                                              );
                                            },
                                          ),
                                        );
                                      },
                                    ),
                                  )
                                  .toList()
                            ],
                          );
                        }
                        return Container(
                          alignment: Alignment.center,
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height / 2,
                          child: CupertinoActivityIndicator(),
                        );
                      },
                    ),
                  ],
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 12, horizontal: 24),
                          child: RaisedButton(
                            onPressed: () {
                              if (state is MenuArrived) {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => AddMenuModalPage(
                                      menuData: state.menu,
                                      updateID: state.updateID,
                                    ),
                                  ),
                                ).then((value) => context
                                    .read<MenuBloc>()
                                    .add(GetMenuEvent()));
                              } else {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => AddNewMenuItem(),
                                  ),
                                ).then((value) => context
                                    .read<MenuBloc>()
                                    .add(GetMenuEvent()));
                              }
                            },
                            child: Text('Add New Menu'),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}

// class MenuManagementList extends StatelessWidget {}
