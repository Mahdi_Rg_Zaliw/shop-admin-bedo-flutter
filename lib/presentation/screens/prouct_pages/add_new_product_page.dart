import 'dart:io';

import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/widgets/checkbox_grid.dart';
import 'package:bedo_shop_admin/presentation/widgets/custom_text_field.dart';
import 'package:bedo_shop_admin/presentation/widgets/pick_picture_button.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';

class AddNewProduct extends StatefulWidget {
  @override
  _AddNewProductState createState() => _AddNewProductState();
}

class _AddNewProductState extends State<AddNewProduct> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController c1 = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 24),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              RichText(
                text: TextSpan(
                    text: S.of(context).addNewProduct+' \n',
                    style: grey12regular,
                    children: [
                      TextSpan(
                          text: '\n'+S.of(context).fillProductInformation,
                          style: black24medium)
                    ]),
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                child: PickPictureButton(
                  onChanged: (File value) {
                    print(value);
                  },
                ),
              ),
              SizedBox(
                height: 20,
              ),
              CustomTextField(
                paddingHorizontal: 0,
                hintText: S.of(context).enterProductName,
                title: S.of(context).productName,
              ),
              Text(
                S.of(context).selectAMenu,
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    color: titleGrey,
                    fontSize: 12),
              ),
              SizedBox(
                height: 10,
              ),
              DropdownSearch<String>(
                validator: (v) => v == null ? S.of(context).requiredField : null,
                hint: S.of(context).selectAMenu,
                mode: Mode.DIALOG,
                showSearchBox: true,

                showSelectedItem: true,
                items: [
                  S.of(context).deserts,
                  S.of(context).italianFood,
                  S.of(context).turkishFood,
                  S.of(context).indianFood
                ],
                onChanged: print,
                // showClearButton: true,
                // showAsSuffixIcons: false,
                // label: "Menu of foods",
                // showSearchBox: true,
                // popupItemDisabled: (String s) => s.startsWith('A'),
                // selectedItem: "turkish",
              ),
              CustomTextField(
                keyboardType: TextInputType.numberWithOptions(),
                paddingHorizontal: 0,
                hintText: S.of(context).enterPriceHere,
                suffixText: '\$',
                controller: c1,
                onChanged: (s) {
                  print(s);
                },
                title: 'Price',
              ),
              CustomTextField(
                paddingHorizontal: 0,
                hintText: S.of(context).otherWidget,
                title: S.of(context).otherWidget,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Text(
                  S.of(context).productSize,
                  style: black24medium,
                ),
              ),
              CheckboxGrid(
                crossAxisCount: 3,
                options: [S.of(context).small, S.of(context).medium, S.of(context).large],
                height: 65,
              ),
              CustomTextField(
                paddingHorizontal: 0,
                hintText: S.of(context).enterStock,
                title: S.of(context).productStock,
                keyboardType: TextInputType.number,
                suffixText: '#',
              ),
              CustomTextField(
                paddingHorizontal: 0,
                hintText: S.of(context).writeProductInformation,
                title: S.of(context).descriptionAboutYouProduct,
                maxline: 6,
              ),
              SizedBox(
                height: 24,
              ),
              RaisedButton(
                child: Text(S.of(context).add),
                onPressed: () {},
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// class CounterFormField extends FormField<int> {

//   CounterFormField({
//     FormFieldSetter<int> onSaved,
//     FormFieldValidator<int> validator,
//     int initialValue = 0,
//     bool autovalidate = false
//   }) : super(
//     onSaved: onSaved,
//     validator: validator,
//     initialValue: initialValue,
//     autovalidate: autovalidate,
//     builder: (FormFieldState<int> state) {
//       return Row(
//         mainAxisSize: MainAxisSize.min,
//         children: <Widget>[
//           IconButton(
//             icon: Icon(Icons.remove),
//             onPressed: () {
//               state.didChange(state.value - 1);
//             },
//           ),
//           Text(
//             state.value.toString()
//           ),
//           IconButton(
//             icon: Icon(Icons.add),
//             onPressed: () {
//               state.didChange(state.value + 1);
//             },
//           ),
//         ],
//       );
//     }
//   );
// }
