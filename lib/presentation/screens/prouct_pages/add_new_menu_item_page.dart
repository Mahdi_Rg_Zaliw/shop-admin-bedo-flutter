import 'dart:io';

import 'package:bedo_shop_admin/bloc/menu_bloc/menu_bloc.dart';
import 'package:bedo_shop_admin/bloc/menu_bloc/menu_event.dart';
import 'package:bedo_shop_admin/bloc/menu_bloc/menu_state.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/presentation/screens/edit_shop.dart';
import 'package:bedo_shop_admin/presentation/widgets/custom_text_field.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/edit_menu_modal_page.dart';
import 'package:bedo_shop_admin/presentation/widgets/round_botton.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:connectivity/connectivity.dart';

class AddNewMenuItem extends StatefulWidget {
  static const routeName = 'add_menu_item';
  @override
  _AddNewMenuItemState createState() => _AddNewMenuItemState();
}

class _AddNewMenuItemState extends State<AddNewMenuItem> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  // String nameMenuEN = '';
  Map formData = {
    "nameMenuEN": '',
    "nameMenuAZ": '',
    "nameMenuRU": '',
  };
  String errorText = '';
  Color colorText = titleGrey;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height - kToolbarHeight - 30,
          padding: EdgeInsets.symmetric(horizontal: 24, vertical: 12)
              .copyWith(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: ListView(
            // crossAxisAlignment: CrossAxisAlignment.stretch,
            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              RichText(
                text: TextSpan(
                    text: 'Add New Menue Item\n',
                    style: grey12regular,
                    children: [
                      TextSpan(
                          text: '\nFill Menu Information', style: black24medium)
                    ]),
              ),
              SizedBox(height: 10),
              // Center(
              //   child: PickPictureButton(
              //     onChanged: (File value) {
              //       print(value);
              //     },
              //   ),
              // ),
              SizedBox(height: 10),
              Form(
                key: formKey,
                child: Column(
                  children: [
                    CustomTextField(
                      isRequired: true,
                      validator: (String value) {
                        if (value.trim().isEmpty) {
                          return "Field English Name Cannot Be Empty !";
                        }
                        return null;
                      },
                      paddingHorizontal: 0,
                      hintText: 'Enter Menu English Name',
                      title: 'Menu English Name',
                      onSaved: (String value) {
                        setState(() {
                          formData['nameMenuEN'] = value;
                        });
                      },
                    ),
                    CustomTextField(
                      isRequired: true,
                      validator: (String value) {
                        if (value.trim().isEmpty) {
                          return "Field Azeri Name Cannot Be Empty !";
                        }
                        return null;
                      },
                      paddingHorizontal: 0,
                      hintText: 'Enter Menu Azeri Name',
                      title: 'Menu Azeri Name',
                      onSaved: (String value) {
                        setState(() {
                          formData['nameMenuAZ'] = value;
                        });
                      },
                    ),
                    CustomTextField(
                      isRequired: true,
                      validator: (String value) {
                        if (value.trim().isEmpty) {
                          return "Field Russian Name Cannot Be Empty !";
                        }
                        return null;
                      },
                      paddingHorizontal: 0,
                      hintText: 'Enter Menu Russian Name',
                      title: 'Menu Russian Name',
                      onSaved: (String value) {
                        setState(() {
                          formData['nameMenuRU'] = value;
                        });
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20),
              BlocConsumer<MenuBloc, MenuState>(
                listener: (context, state) {
                  if (state is MenuLoading) {
                    setState(() {
                      errorText = 'Loading ...';
                      colorText = titleGrey;
                    });
                  }
                  if (state is MenuExceptionState) {
                    setState(() {
                      errorText = 'Failed ! ${state.clientErrors}';
                      colorText = red;
                    });

                    showDialog(
                        context: context,
                        builder: (context) => CustomDialogFailed(
                              title: 'Failed: ${state.clientErrors}',
                            ));
                  }
                  if (state is ShopMenuCreatedSuccess) {
                    setState(() {
                      errorText = 'Successfully Created';
                      colorText = primaryMaterialColor;
                    });

                    Navigator.pop(context);
                  }
                },
                builder: (context, state) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      RaisedButton(
                        child: Text('Done'),
                        onPressed: () async {
                          formKey.currentState.save();
                          CreateMenuEvent createMenuEvent = CreateMenuEvent(
                            data: {
                              "subMenus": [
                                {
                                  "name": [
                                    {
                                      "value": formData['nameMenuEN'],
                                      "lang": "en"
                                    },
                                    {
                                      "value": formData['nameMenuAZ'],
                                      "lang": "az"
                                    },
                                    {
                                      "value": formData['nameMenuRU'],
                                      "lang": "ru"
                                    }
                                  ],
                                  "products": []
                                }
                              ]
                            },
                          );

                          var connectivityResult =
                              await (Connectivity().checkConnectivity());
                          if (connectivityResult == ConnectivityResult.mobile ||
                              connectivityResult == ConnectivityResult.wifi) {
                            if (formKey.currentState.validate()) {
                              print('Validate');
                              setState(() {
                                errorText = '';
                              });
                              context.read<MenuBloc>().add(createMenuEvent);
                            }
                          } else {
                            print('Network Connection Error');
                            setState(() {
                              errorText = 'Network Connection Error';
                              colorText = red;
                            });
                          }
                        },
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                        child: Text(
                          errorText,
                          textAlign: TextAlign.center,
                          style: TextStyle(color: colorText),
                        ),
                      ),
                      Buttons.whiteOutline(
                        onPressed: () {},
                        child: Text(
                          'add items',
                          style: black14regular,
                        ),
                      )
                    ],
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
