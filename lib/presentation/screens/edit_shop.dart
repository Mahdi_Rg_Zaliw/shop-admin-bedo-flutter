import 'dart:io';

import 'package:bedo_shop_admin/bloc/shop_bloc/shop_bloc.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/presentation/screens/custom_expansion_tile.dart';
import 'package:bedo_shop_admin/presentation/widgets/form_input_custom_location_field.dart';
import 'package:bedo_shop_admin/presentation/widgets/form_input_select_shop_func.dart';
import 'package:bedo_shop_admin/presentation/widgets/form_input_select_working_day.dart';
import 'package:bedo_shop_admin/presentation/widgets/round_botton.dart';
import 'package:bedo_shop_admin/utils/device_dimensions.dart';
import 'package:bedo_shop_admin/presentation/widgets/pick_picture_button.dart';
import 'package:bedo_shop_admin/presentation/widgets/custom_text_field.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EditShopInformation extends StatefulWidget {
  static const routeName = 'editshopinfo';
  EditShopInformation({Key key}) : super(key: key);

  @override
  _EditShopInformationState createState() => _EditShopInformationState();
}

class _EditShopInformationState extends State<EditShopInformation> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<ShopBloc>(context).add(EditShopVisited());
  }

  @override
  void dispose() {
    super.dispose();
    _name.dispose();
    _phone.dispose();
    _tax.dispose();
    _prep.dispose();
    _cnum.dispose();
  }

  final _prep = TextEditingController();

  final _name = TextEditingController();
  final _phone = TextEditingController();
  final _tax = TextEditingController();
  final _cnum = TextEditingController();

  Location _location;

  // String imagePath;
  File uploadIMG;

  List<DayWorkingTime> week;

  List get weekFixed {
    List ans = [];
    week?.forEach((element) {
      if (element.check == true) {
        ans.add(element.toMap());
      }
    });
    return ans;
  }

  List categories;

  List get categoryDataFixed {
    List ans = [];
    categories?.forEach((element) {
      if (element.check == true) {
        ans.add(element.id);
      }
    });
    return ans;
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 70,
        centerTitle: true,
        title: Text(
          S.of(context).editInformation,
          style: grey14regular,
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: BlocConsumer<ShopBloc, ShopState>(
            listener: (context, state) {
              if (state is SuccessShopOperation) {
                showDialog(
                  context: context,
                  builder: (context) => CustomDialogueSuccess(
                    title: 'successfully changed data',
                  ),
                );
              }
            },
            builder: (context, state) {
              if (state is ShopEditableDataLoaded) {
                final d = state.data;
                _name.text = d['name'] ?? '';
                _phone.text = d['phoneNumbers'][0] ?? '';
                _tax.text = d['taxCode'] ?? '';
                _prep.text = d['preparingTime'].toString() ?? '';
                _cnum.text = d['cardNumber'] ?? '';
                var x = d['location'][0] ?? 0.0;
                var y = d['location'][1] ?? 0.0;
                printWhiteBg('$x  $y');
                week = d['workingHoursInMinutes'];
                printYellowBg(d['logoUrl']);
                _location = Location(x: x, y: y);
                return Column(
                  children: [
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(vertical: 30),
                      child: PickPictureButton(
                        // pickerType: ,
                        previewImg: d['logoUrl'],
                        onChanged: (file) {
                          uploadIMG = file;
                        },
                      ),
                    ),
                    //
                    //
                    //
                    //
                    // name
                    //
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: CustomTextField(
                        controller: _name,
                        title: S.of(context).restaurantName,
                        hintText: S.of(context).enterTheRestaurantName,
                        // controller: textFieldController[0],
                        validator: (value) {
                          if (value.length == 0) {
                            return 'this field is required'; //todo translate
                          }
                          return null;
                        },
                      ),
                    ),
                    //
                    //
                    //
                    //
                    // location
                    //
                    CustomLocationField(
                      context: context,
                      initial: Location(x: x, y: y),
                      validator: (selectedLocation) {
                        if (selectedLocation?.x == null ||
                            selectedLocation?.y == null) {
                          return ' please select a location on map'; //todo translate
                        }
                        return null;
                      },
                      onSaved: (val) {
                        _location = val;
                      },
                    ),
                    //
                    //
                    //
                    //
                    // phone number
                    //
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: CustomTextField(
                        controller: _phone,
                        validator: (value) {
                          if (value.length == 0) {
                            return 'thins field is required'; //todo translate
                          }
                          if (!value.isValidMobile()) {
                            return 'plese enter a valid phone number'; // todo translate
                          }
                          return null;
                        },
                        keyboardType: TextInputType.phone,
                        title: S.of(context).restaurantPhoneNumber,
                        hintText: S.of(context).enterTheRestaurantPhoneNumber,
                        // controller: textFieldController[1],
                      ),
                    ),
                    //
                    //
                    //
                    //  week
                    //
                    //
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 24),
                      child: WorkingWeekDays(
                          data: d['workingHoursInMinutes'],
                          onChange: (d) {
                            week = d;
                            printBlueBg(weekFixed);
                            printYellowBg(week);
                          }),
                    ),
                    //
                    //
                    //
                    // cat
                    //
                    //
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 24),
                      child: ShopFunction(
                          onCahange: (v) {
                            printYellowBg(v);
                            categories = v;
                            printGreenBg(categoryDataFixed);
                          },
                          data: state.data['categories']),
                    ),

                    ///
                    ///
                    ///
                    ///
                    ///

                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: CustomTextField(
                        controller: _prep,
                        formatters: [FilteringTextInputFormatter.digitsOnly],

                        validator: (value) {
                          if (value.length == 0) {
                            return 'thins field is required'; //todo translate
                          }

                          return null;
                        },
                        keyboardType: TextInputType.number,
                        title: 'avrage prepairing time ', //todo translate
                        hintText: 'enter time in minutes', //todo translate
                        // controller: textFieldController[1],
                      ),
                    ),
                    //
                    //
                    //
                    // tax
                    //
                    //
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: CustomTextField(
                        controller: _tax,
                        validator: (value) {
                          if (value.length == 0) {
                            return 'thins field is required'; //todo translate
                          }

                          return null;
                        },
                        keyboardType: TextInputType.text,
                        title: 'tax code ', //todo translate
                        hintText: 'enter tax code', //todo translate
                        // controller: textFieldController[1],
                      ),
                    ),
                    //
                    //
                    //
                    // card number
                    //
                    //
                    //
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: CustomTextField(
                        controller: _cnum,
                        validator: (value) {
                          if (value.length == 0) {
                            return 'thins field is required'; //todo translate
                          }

                          return null;
                        },
                        keyboardType: TextInputType.number,
                        title: ' card number', //todo translate
                        hintText: 'enter card number', //todo translate
                        // controller: textFieldController[1],
                      ),
                    ),
                    //
                    //
                    //
                    // submit button
                    //
                    //
                    Container(
                      width: DeviceDimensions.widthSize(context),
                      padding: EdgeInsets.only(
                          left: 24, right: 24, bottom: 24, top: 10),
                      child: RaisedButton(
                        elevation: 0,
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            final data = {
                              'file': uploadIMG,
                              'logoUrl': null,
                              'name': _name.text,
                              'coordinates': [_location.x, _location.y],
                              'phoneNumbers': [_phone.text],
                              'taxCode': _tax.text,
                              'workingHoursInMinutes': weekFixed,
                              'cardNumber': _cnum.text,
                              'preparingTime': int.parse(_prep.text),
                              'categories': categoryDataFixed,
                            };
                            printYellowBg(data);
                            BlocProvider.of<ShopBloc>(context)
                                .add(ShopEditing(data));
                          }
                        },
                        child: Text(S.of(context).confirm),
                      ),
                    ),
                  ],
                );
              }
              if (state is FailedLoadingEditData ||
                  state is FailedShopDataOperation) {
                return SizedBox(
                  height: MediaQuery.of(context).size.height - 100,
                  child: Center(
                    child: FailureText(
                      title:
                          'Something went wrong! \ncould not handle your request at this moment',
                    ),
                  ),
                );
              }
              return SizedBox(
                height: MediaQuery.of(context).size.height - 100,
                child: Center(
                  child: CupertinoActivityIndicator(
                    radius: 20,
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

class FailureText extends StatelessWidget {
  final String title;
  const FailureText({
    Key key,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(
          Icons.warning_amber_rounded,
          size: 25,
          color: red,
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          '$title',
          style: red12semibold,
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}

class CustomDialogueSuccess extends StatelessWidget {
  final String title;
  const CustomDialogueSuccess({
    Key key,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            '$title',
            style: green12regular,
          ),
          SizedBox(
            height: 8,
          ),
          // Icon(
          //   Icons.check,
          //   color: primaryMaterialColor,
          //   size: 25,
          // ),
        ],
      ),
      actions: [
        Buttons.primaryOutline(
          child: Text('ok'),
          onPressed: () => Navigator.of(context).pop(),
        )
      ],
    );
  }
}
