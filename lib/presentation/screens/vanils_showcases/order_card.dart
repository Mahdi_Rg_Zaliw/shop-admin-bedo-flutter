import 'package:bedo_shop_admin/presentation/widgets/oreder_card.dart';
import 'package:flutter/material.dart';

class OrdersCardShowCase extends StatelessWidget {
  // static const routeName = '/';
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                OrderCard(),
                SizedBox(
                  height: 16,
                ),
                OrderCard(),
                SizedBox(
                  height: 16,
                ),
                OrderCard(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
