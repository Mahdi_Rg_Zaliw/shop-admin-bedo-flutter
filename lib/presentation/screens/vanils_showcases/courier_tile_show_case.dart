import 'package:bedo_shop_admin/presentation/widgets/courier_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class CourierTileShowCase extends StatelessWidget {
  static const routeName = '/';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 200),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              CourierTile(
                imageUrl: 'https://picsum.photos/251',
              ),
              SizedBox(
                height: 19,
              ),
              SizedBox(
                height: 19,
              ),
              CourierTile(
                courierName:
                    'hello this is a long describtion man so long text here to check this hello this is a long describtion man so long text here to check this hello this is a long describtion man so long text here to check this ',
                courierDesc:
                    'hello this is a long describtion man so long text here to check this man so long text here to check this hello this is a long describtion man so long text here to check this man so ',
                imageUrl: 'https://picsum.photos/253',
                // status: CurierStatus.inService,
                // selected: false,
              ),
              SizedBox(
                height: 19,
              ),
              CourierTile(
                imageUrl: 'https://picsum.photos/252',
                status: CurierStatus.available,
                selected: true,
              ),
              SizedBox(
                height: 19,
              ),
              CourierTile(
                courierName:
                    'hello this is a long describtion man so long text here to check this hello this is a long describtion man so long text here to check this hello this is a long describtion man so long text here to check this ',
                courierDesc:
                    'hello this is a long describtion man so long text here to check this man so long text here to check this hello this is a long describtion man so long text here to check this man so ',
                imageUrl: 'https://picsum.photos/253',
                status: CurierStatus.available,
                // selected: false,
              ),
              SizedBox(
                height: 19,
              ),
              CourierTile(
                imageUrl: 'https://picsum.photos/253',
                status: CurierStatus.available,
                // selected: true,
              ),
              SizedBox(
                height: 19,
              ),
              CourierTile(
                imageUrl: 'https://picsum.photos/254',
                status: CurierStatus.inService,
                // selected: false,
              ),
              SizedBox(
                height: 19,
              ),
              CourierTile(
                imageUrl: 'https://picsum.photos/231',
                status: CurierStatus.inService,
                // selected: false,
              ),
              SizedBox(
                height: 19,
              ),
              CourierTile(
                courierName:
                    'hello this is a long describtion man so long text here to check this hello this is a long describtion man so long text here to check this hello this is a long describtion man so long text here to check this ',
                courierDesc:
                    'hello this is a long describtion man so long text here to check this man so long text here to check this hello this is a long describtion man so long text here to check this man so ',
                imageUrl: 'https://picsum.photos/253',
                status: CurierStatus.inService,
                // selected: false,
              ),
            ],
          ),
        ),
      )
          // IntrinsicWidth(
          //   child: Column(
          //     crossAxisAlignment: CrossAxisAlignment.stretch,
          //     mainAxisSize: MainAxisSize.min,
          //     children: [
          //       RaisedButton(
          //         onPressed: () {
          //           Navigator.of(context).push(
          //             MaterialPageRoute(
          //               builder: (context) => VanilsMenu(),
          //             ),
          //           );
          //         },
          //         child: Text('vanils widgetss'),
          //       ),
          //       //
          //       //
          //       //
          //       //
          //       RaisedButton(
          //         onPressed: () {
          //           Navigator.of(context).push(
          //             MaterialPageRoute(
          //               builder: (context) => MohammadsMenu(),
          //             ),
          //           );
          //         },
          //         child: Text('mohammads widgets'),
          //       ),
          //       Builder(
          //         builder: (BuildContext ctx) => RaisedButton(
          //           onPressed: () {
          //             Scaffold.of(ctx).showSnackBar(
          //               SnackBar(
          //                 content: Text('work in progress'),
          //                 duration: Duration(seconds: 1),
          //               ),
          //             );
          //           },
          //           child: Text('sinas widgets'),
          //         ),
          //       ),
          //     ],
          //   ),
          // ),
          ),
    );
  }
}
