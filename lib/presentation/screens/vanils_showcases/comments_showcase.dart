import 'package:bedo_shop_admin/presentation/widgets/comment_widggets/comment_tile.dart';
import 'package:bedo_shop_admin/presentation/widgets/comment_widggets/response_tile.dart';
import 'package:bedo_shop_admin/presentation/widgets/verification_code_widget.dart';
import 'package:flutter/material.dart';

class CommentsShowCase extends StatelessWidget {
  // static const routeName = '/';
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                CommentTile(
                  imageUrl: 'https://picsum.photos/250',
                  personName: 'asghr akbari',
                  rank: '2.9',
                  date: DateTime(2010),
                  message:
                      "Lorem Ipsum is simply dummy text of the printing  in the 1960s with",
                ),
                ResponceTile(reply: 'i would be very happy to help sir'),
                CommentTile(
                    imageUrl: 'https://picsum.photos/240',
                    personName: 'asghr akbari',
                    rank: '2.9',
                    date: DateTime(2010),
                    message:
                        'hello man I just wanted to say thanks for every thing'),
                ResponceTile(
                  reply:
                      "Lorem Ipsum is simply dummy text of the printing and typeset software like Aldus PageMaker including versi",
                ),
                CommentTile(
                    imageUrl: 'https://picsum.photos/241',
                    personName: 'asghr akbari',
                    rank: '2.9',
                    date: DateTime(2010),
                    message:
                        'hello man I just wanted to say thanks for every thing'),
                ResponceTile(
                  reply:
                      "Lorem Ipsum is simply dummy text of the printing and typeset software like Aldus PageMaker including versi",
                ),
                CommentTile(
                    imageUrl: 'https://picsum.photos/242',
                    personName: 'asghr akbari',
                    rank: '2.9',
                    date: DateTime(2010),
                    message:
                        'hello man I just wanted to say thanks for every thing'),
                ResponceTile(
                  reply:
                      "Lorem Ipsum is simply dummy text of the printing and typeset software like Aldus PageMaker including versi",
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
