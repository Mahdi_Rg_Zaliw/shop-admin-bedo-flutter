import 'package:bedo_shop_admin/presentation/widgets/filtered_list.dart';
import 'package:flutter/material.dart';

class CommntsPage extends StatefulWidget {
  @override
  _CommntsPageState createState() => _CommntsPageState();
}

class _CommntsPageState extends State<CommntsPage> {
  @override
  Widget build(BuildContext context) {
    return FilteredList.common();
  }
}
