import 'package:dio/dio.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';

class DropDownSerchBoxSC extends StatefulWidget {
  @override
  _DropDownSerchBoxSCState createState() => _DropDownSerchBoxSCState();
}

class _DropDownSerchBoxSCState extends State<DropDownSerchBoxSC> {
  final _formKey = GlobalKey<FormState>();
  final _openDropDownProgKey = GlobalKey<DropdownSearchState<String>>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("DropdownSearch Demo"),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(25),
        child: Form(
          key: _formKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: ListView(
            padding: EdgeInsets.all(4),
            children: <Widget>[
              // /Menu Mode with no searchBox
              DropdownSearch<String>(
                validator: (v) => v == null ? "required field" : null,
                hint: "Select a Menu",
                mode: Mode.MENU,
                // showSearchBox: true,

                showSelectedItem: true,
                items: [
                  "Deserts",
                  "Italian food ",
                  "turkish food",
                  'indian food'
                ],
                onChanged: print,
                // showClearButton: true,
                // showAsSuffixIcons: false,
                // label: "Menu of foods",
                // showSearchBox: true,
                // popupItemDisabled: (String s) => s.startsWith('A'),
                // selectedItem: "turkish",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
