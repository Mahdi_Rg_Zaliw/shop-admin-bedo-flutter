import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/presentation/screens/vanils_showcases/alphabet.dart';
import 'package:flutter/material.dart';

class TextHelper extends StatelessWidget {
  const TextHelper({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: [
          //
          //
          //
          //
          //az1
          //
          //
          //
          Container(
            width: MediaQuery.of(context).size.width / 5,
            height: MediaQuery.of(context).size.height,
            child: ListView.builder(
              itemBuilder: (context, index) {
                return Container(
                  color: (index == 7)
                      ? Colors.red[800]
                      : index == 0
                          ? Colors.green
                          : null,
                  child: Column(
                    children: [
                      Text(
                        index == 0 ? 'AZ1' : azer1[index - 1].toString(),
                        style: TextStyle(
                          fontSize: 30,
                          color: index == 0 ? Colors.white : null,
                        ),
                      ),
                      if (index == 7)
                        Text(
                          index == 0 ? 'AZ1' : azer1[index - 1].toString(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                            fontFamily: 'roboto',
                          ),
                        ),
                      Text(
                        (index == 0 ? '' : index).toString(),
                        style: TextStyle(fontSize: 30),
                      )
                    ],
                  ),
                );
              },
              itemCount: azer1.length,
            ),
          ),
          //
          //
          //
          //az2
          //
          //
          //
          Container(
            width: MediaQuery.of(context).size.width / 5,
            height: MediaQuery.of(context).size.height,
            child: ListView.builder(
              itemBuilder: (context, index) {
                return Container(
                  color: index == 0 ? Colors.green : null,
                  child: Column(
                    children: [
                      Text(
                        index == 0 ? 'AZ2' : az2[index - 1].toString(),
                        style: TextStyle(
                          fontSize: 30,
                          color: index == 0 ? Colors.white : null,
                        ),
                      ),
                      Text(
                        (index == 0 ? '' : index).toString(),
                        style: TextStyle(fontSize: 30),
                      )
                    ],
                  ),
                );
              },
              itemCount: az2.length,
            ),
          ),
          //
          //
          //
          //
          //
          //
          //az3
          //
          //
          //
          //
          //
          //
          //
          Container(
            width: MediaQuery.of(context).size.width / 5,
            height: MediaQuery.of(context).size.height,
            child: ListView.builder(
              itemBuilder: (context, index) {
                return Container(
                  color: (index == 1 + 4 ||
                          index == 1 + 7 ||
                          index == 1 + 14 ||
                          index == 1 + 19 ||
                          index == 1 + 25 ||
                          index == 1 + 28 ||
                          index == 1 + 30)
                      ? Colors.red[800]
                      : index == 0
                          ? Colors.green
                          : null,
                  child: Column(
                    children: [
                      Text(
                        index == 0 ? 'AZ3' : az3[index - 1].toString(),
                        style: TextStyle(
                          fontSize: 30,
                          color: index == 0 ? Colors.white : null,
                        ),
                      ),
                      if ((index == 1 + 4 ||
                          index == 1 + 7 ||
                          index == 1 + 14 ||
                          index == 1 + 19 ||
                          index == 1 + 25 ||
                          index == 1 + 28 ||
                          index == 1 + 30))
                        Text(
                          index == 0 ? 'AZ3' : az3[index - 1].toString(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                            fontFamily: 'roboto',
                          ),
                        ),
                      Text(
                        (index == 0 ? '' : index).toString(),
                        style: TextStyle(fontSize: 30),
                      )
                    ],
                  ),
                );
              },
              itemCount: az3.length + 1,
            ),
          ),
          //
          //
          //
          //
          //
          //az5
          //
          //
          //
          //
          //
          //
          Container(
            width: MediaQuery.of(context).size.width / 5,
            height: MediaQuery.of(context).size.height,
            child: ListView.builder(
              itemBuilder: (context, index) {
                return Container(
                  color: (index == 1 + 6 ||
                          index == 1 + 9 ||
                          index == 1 + 20 ||
                          index == 1 + 31 ||
                          index == 1 + 1)
                      ? Colors.red[800]
                      : index == 0
                          ? Colors.green
                          : null,
                  child: Column(
                    children: [
                      Text(
                        index == 0 ? 'AZ5' : az5[index - 1].toString(),
                        style: TextStyle(
                          fontSize: 30,
                          color: index == 0 ? Colors.white : null,
                        ),
                      ),
                      if (index == 1 + 6 ||
                          index == 1 + 9 ||
                          index == 1 + 20 ||
                          index == 1 + 31 ||
                          index == 1 + 1)
                        Text(
                          index == 0 ? 'AZ5' : az5[index - 1].toString(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                            fontFamily: 'roboto',
                          ),
                        ),
                      Text(
                        (index == 0 ? '' : index).toString(),
                        style: TextStyle(fontSize: 30),
                      )
                    ],
                  ),
                );
              },
              itemCount: az5.length + 1,
            ),
          ),
          //
          //
          //
          //
          //
          //ru
          //
          //
          //
          //
          //
          Container(
            width: MediaQuery.of(context).size.width / 5,
            height: MediaQuery.of(context).size.height,
            child: ListView.builder(
              itemBuilder: (context, index) {
                return Container(
                  color: index == 0 ? Colors.green : null,
                  child: Column(
                    children: [
                      Text(
                        index == 0
                            ? 'RUS'
                            : russianAlphabet[index - 1].toString(),
                        style: TextStyle(
                          fontSize: 30,
                          color: index == 0 ? Colors.white : null,
                        ),
                      ),
                      Text(
                        (index == 0 ? '' : index).toString(),
                        style: TextStyle(fontSize: 30),
                      )
                    ],
                  ),
                );
              },
              itemCount: russianAlphabet.length,
            ),
          ),
        ],
      ),
    );
  }
}
