import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/presentation/widgets/filtered_list.dart';
import 'package:bedo_shop_admin/presentation/widgets/full_report_page.dart';
import 'package:flutter/material.dart';

class ReportScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FilteredList.common(
      appBarTitlesList: null,
      appBarBottomList: [
        Column(
          children: [
            Text(
              'Total amount today:',
              style: grey14regular,
            ),
            Text(
              '138.00',
              style: black24semibold,
            ),
          ],
        )
      ],
      topTitlesList: [Text('1'), Text('2')],
      bottomTitleList: [Text('1'), Text('2')],
      optionsList: ['fund status', 'full report'],
      pagesList: [
        Container(),
        FullReportPage()
      ],
    );
  }
}
