import 'package:flutter/material.dart';

import '../../widgets/custom_text_field.dart';

class ModalPage extends StatefulWidget {
  const ModalPage({
    Key key,
  }) : super(key: key);

  @override
  _ModalPageState createState() => _ModalPageState();
}

class _ModalPageState extends State<ModalPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(26).copyWith(top: 30, bottom: 30),
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(child: Text('title of the modal')),
                    IconButton(
                      icon: Icon(Icons.close),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
                // Text('hello'),
                CustomTextField(),
                // Text('hello'),
                CustomTextField(),
                // Text('hello'),
                CustomTextField(),
                // Text('hello'),
                CustomTextField(),
                // Text('hello'),
                CustomTextField(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
