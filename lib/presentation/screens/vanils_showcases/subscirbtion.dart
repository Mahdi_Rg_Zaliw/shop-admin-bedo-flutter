import 'package:flutter/material.dart';
import 'package:graphql/client.dart';

class SubTest extends StatefulWidget {
  @override
  _SubTestState createState() => _SubTestState();
}

class _SubTestState extends State<SubTest> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('sub'),
      ),
    );
  }
}

//  class Config {
//    static final HttpLink httpLink = HttpLink(
//       'https://hasura.io/learn/graphql',
//    );
//    static String _token;
//    static final AuthLink authLink = AuthLink(getToken: () => _token);
//    static final WebSocketLink websocketLink =
//     WebSocketLink(
//      'wss://hasura.io/learn/graphql',
//        config: SocketClientConfig(
//        autoReconnect: true,
//        inactivityTimeout: Duration(seconds: 30),
//        initPayload: () async {
//           return {
//             'headers': {'Authorization': _token},
//           };
//         },
//      ),
//    );
//    static final Link link = authLink.concat(httpLink).concat(websocketLink);
//    static String token;
//    static ValueNotifier<GraphQLClient> initailizeClient(String token) {
//      _token = token;
//      ValueNotifier<GraphQLClient> client =
//      ValueNotifier(
//        GraphQLClient(
//          cache: OptimisticCache(dataIdFromObject:typenameDataIdFromObject),
//          link: link,
//        ),
//      );
//      return client;
//    }
//  }

//   return GraphQLProvider(
//       client: Config.initailizeClient(token),
//       child: CacheProvider(
//         child: DefaultTabController(
