import 'package:bedo_shop_admin/presentation/widgets/comment_widggets/inbox_message_tile.dart';
import 'package:flutter/material.dart';
import '../../widgets/comment_widggets/response_tile.dart';

class InboxShowCase extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              InboxMessage(),
              ResponceTile(
                type: ResponseType.inbox,
                // lable: 'no',
                reply:
                    'so i wanted to tell you that  this things are true man thank you good work man',
              ),
              InboxMessage(),
              ResponceTile(
                type: ResponseType.inbox,
                // lable: 'no',
                reply:
                    'so i wanted to tell you that  this things are true man thank you good work man',
              ),
              InboxMessage(),
              ResponceTile(
                type: ResponseType.inbox,
                // lable: 'no',
                reply:
                    'so i wanted to tell you that  this things are true man thank you good work man',
              ),
              InboxMessage(),
              ResponceTile(
                type: ResponseType.inbox,
                // lable: 'no',
                reply:
                    'so i wanted to tell you that  this things are true man thank you good work man',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
