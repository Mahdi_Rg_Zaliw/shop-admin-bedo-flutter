import 'package:bedo_shop_admin/design/styles.dart';
import 'package:flutter/material.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';

class InputShowCase extends StatefulWidget {
  @override
  _InputShowCaseState createState() => _InputShowCaseState();
}

class _InputShowCaseState extends State<InputShowCase> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final TextEditingController controller = TextEditingController();
  String initialCountry = 'NG';
  PhoneNumber number = PhoneNumber(isoCode: 'NG');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formKey,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              InternationalPhoneNumberInput(
                onInputChanged: (PhoneNumber number) {
                  print(number.phoneNumber);
                },
                onInputValidated: (bool value) {
                  print(value);
                },
                selectorConfig: SelectorConfig(
                  showFlags: false,
                  setSelectorButtonAsPrefixIcon: true,
                  selectorType: PhoneInputSelectorType.DIALOG,
                ),
                textStyle: black12regular,
                ignoreBlank: false,
                autoValidateMode: AutovalidateMode.disabled,
                selectorTextStyle: black12regular,
                initialValue: number,
                textFieldController: controller,
                formatInput: false,
                keyboardType: TextInputType.numberWithOptions(
                    signed: true, decimal: true),
                inputBorder: OutlineInputBorder(),
                onSaved: (PhoneNumber number) {
                  print('On Saved: $number');
                },
              ),
              SizedBox(height: 12.0),
              RaisedButton(
                onPressed: () {
                  formKey.currentState.validate();
                },
                child: Text('Validate'),
              ),
              SizedBox(height: 12.0),
              RaisedButton(
                onPressed: () {
                  getPhoneNumber('+15417543010');
                },
                child: Text('Update'),
              ),
              SizedBox(height: 12.0),
              RaisedButton(
                onPressed: () {
                  formKey.currentState.save();
                },
                child: Text('Save'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void getPhoneNumber(String phoneNumber) async {
    PhoneNumber number =
        await PhoneNumber.getRegionInfoFromPhoneNumber(phoneNumber, 'US');

    setState(() {
      this.number = number;
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}

//  final x = MaskedInputFormater('(###) ###-####');
