import 'package:flutter/material.dart';

import '../../widgets/custom_text_field.dart';
import '../../widgets/modal_widgets/custom_modal_bottom_sheet.dart';
import '../../widgets/modal_widgets/modal_home_page.dart';
import '../../widgets/modal_widgets/modal_slide_transition.dart';
import 'modal_page.dart';

class ModalNavigationShowCase extends StatelessWidget {
  final modalNav = GlobalKey<NavigatorState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: RaisedButton(
          onPressed: () {
            customModalSheetWithNavigation(
              context: context,
              height: 360,
              child: ModalHomePage(
                navKey: modalNav,
                childBuilder: (modalNav) {
                  return Padding(
                    padding: const EdgeInsets.all(24),
                    child: Column(
                      children: [
                        CustomTextField(),
                        RaisedButton(
                          child: Text('navigate to page'),
                          onPressed: () {
                            modalNav.currentState.push(Transitions(
                              widget: ModalPage(),
                            ));
                          },
                        ),
                        SizedBox(
                          height: 3,
                        ),
                        RaisedButton(
                          child: Text('navigate to page'),
                          onPressed: () {
                            modalNav.currentState.push(Transitions(
                              widget: ModalPage(),
                            ));
                          },
                        ),
                        SizedBox(
                          height: 3,
                        ),
                        RaisedButton(
                          child: Text('navigate to page'),
                          onPressed: () {
                            modalNav.currentState.push(Transitions(
                              widget: ModalPage(),
                            ));
                          },
                        ),
                      ],
                    ),
                  );
                },
              ),
            );
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'show modal with \n navigations transition',
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
