import 'package:bedo_shop_admin/bloc/shop_bloc/shop_bloc.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/presentation/screens/edit_shop.dart';
import 'package:bedo_shop_admin/presentation/screens/settings_pages/settings.dart';
import 'package:bedo_shop_admin/presentation/screens/vanils_showcases/comments_showcase.dart';
import 'package:bedo_shop_admin/presentation/screens/vanils_showcases/courier_tile_show_case.dart';
import 'package:bedo_shop_admin/presentation/screens/vanils_showcases/filtered_list_with_order_card.dart';
import 'package:bedo_shop_admin/presentation/screens/vanils_showcases/inbox_show_case.dart';
import 'package:bedo_shop_admin/presentation/screens/vanils_showcases/order_card.dart';
import 'package:bedo_shop_admin/presentation/screens/vanils_showcases/phone_number_input.dart';
import 'package:bedo_shop_admin/presentation/screens/vanils_showcases/subscirbtion.dart';
import 'package:bedo_shop_admin/presentation/screens/vanils_showcases/text_helper.dart';
import 'package:bedo_shop_admin/presentation/screens/vanils_showcases/upload_image_test.dart';
import 'package:bedo_shop_admin/presentation/widgets/comments_page.dart';
import 'package:bedo_shop_admin/presentation/widgets/filtered_list.dart';
import 'package:bedo_shop_admin/presentation/widgets/form_input_select_shop_func.dart';
import 'package:bedo_shop_admin/presentation/widgets/form_input_select_working_day.dart';
import 'package:bedo_shop_admin/presentation/widgets/icons_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../add_shop.dart';
import '../menu_bedo.dart';
import '../prouct_pages/products_page.dart';
import '../support_pages.dart';
import 'drop_down_with_search.dart';
import 'modal_navigation_showcase.dart';

class VanilsMenu extends StatelessWidget {
  // static const routeName = '/';
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: whiteAllButtonsColor,
      child: Scaffold(
        body: Center(
          child: SingleChildScrollView(
            child: IntrinsicWidth(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 100,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => ModalNavigationShowCase(),
                          ),
                        );
                      },
                      child: Text('modal bottom sheet show case -'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => InboxShowCase(),
                          ),
                        );
                      },
                      child: Text('inbox message tile -'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => FilteredList.fix(
                              showBackButton: true,
                              appBarTitlesList: [
                                Text(
                                  'Total amount today ',
                                  style: grey14medium,
                                )
                              ],
                              appBarBottomList: [
                                Text(
                                  '3526 ',
                                  style: black24medium,
                                )
                              ],
                            ),
                          ),
                        );
                      },
                      child: Text('show filtered list -'),
                    ),
                    //
                    //
                    //
                    //
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => OrdersCardShowCase(),
                          ),
                        );
                      },
                      child: Text('orders card show case -'),
                    ),
                    // FilteredListWithOrderCard
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => FilteredListWithOrderCard(),
                          ),
                        );
                      },
                      child: Text('filtered list with order card -'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => CommentsShowCase(),
                          ),
                        );
                      },
                      child: Text('comments show case -'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => CourierTileShowCase(),
                          ),
                        );
                      },
                      child: Text('courier tiles show case -'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => Scaffold(
                              body: IconsTestShowCase(),
                            ),
                          ),
                        );
                      },
                      child: Text('icons helper -'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => Scaffold(
                              body: InputShowCase(),
                            ),
                          ),
                        );
                      },
                      child: Text(' input phone number -'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => ProductsPage(),
                          ),
                        );
                      },
                      child: Text(' products page -'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => DropDownSerchBoxSC(),
                          ),
                        );
                      },
                      child: Text(' drop down with search -'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => CommentsPage(),
                          ),
                        );
                      },
                      child: Text(' comments page w api +'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => SupportPages(),
                          ),
                        );
                      },
                      child: Text(' support pages -'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => Settings(),
                          ),
                        );
                      },
                      child: Text(' Settings pages -'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => TextHelper(),
                          ),
                        );
                      },
                      child: Text('Language helper'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => UploadImageTest(),
                          ),
                        );
                      },
                      child: Text('upload Image Test'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => AddShopPage(),
                          ),
                        );
                      },
                      child: Text('add shop'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => Shw(),
                          ),
                        );
                      },
                      child: Text('working week days '),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) =>
                                Scaffold(body: Center(child: ShopFunction())),
                          ),
                        );
                      },
                      child: Text('getting the shop function'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context)
                            .push(
                              MaterialPageRoute(
                                builder: (context) => EditShopInformation(),
                              ),
                            )
                            .then((value) => BlocProvider.of<ShopBloc>(context)
                                .add(NavOut()));
                      },
                      child: Text('Edit shop '),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => SettingsMain(),
                          ),
                        );
                      },
                      child: Text('menu bedo '),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => SubTest(),
                          ),
                        );
                      },
                      child: Text('subscribtion test '),
                    ),
                  ),
                  SizedBox(
                    height: 100,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class Shw extends StatefulWidget {
  const Shw({
    Key key,
  }) : super(key: key);

  @override
  _ShwState createState() => _ShwState();
}

class _ShwState extends State<Shw> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: WorkingWeekDays(
          onChange: print,
          // data: ,
        ),
      ),
    );
  }
}
