import 'dart:io';
import 'package:bedo_shop_admin/config/config.dart';
import 'package:bedo_shop_admin/constants/constants.dart';
import 'package:bedo_shop_admin/data/graphql/auth/auth_gql.dart';
import 'package:bedo_shop_admin/data/graphql/comments/comments_gql.dart';
import 'package:bedo_shop_admin/data/graphql/graphql.dart';
import 'package:bedo_shop_admin/data/secure_storage/secure_storage.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/model/comments_model.dart';
import 'package:bedo_shop_admin/presentation/widgets/comment_widggets/custom_avatar.dart';
import 'package:bedo_shop_admin/presentation/widgets/pick_picture_button.dart';
import 'package:graphql/client.dart';
import 'dart:async';

import 'package:bedo_shop_admin/data/graphql/comments/comments_request.dart';
import 'package:bedo_shop_admin/model/comments_model.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';

import 'package:bedo_shop_admin/data/graphql/graphql.dart';
import 'package:flutter/material.dart';
import 'package:graphql/client.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
//
//
//
//
//
//
//
import 'package:http_parser/http_parser.dart';
//
//
//
//
//
//
//
//
//

// class UploadImageTest extends StatefulWidget {
//   UploadImageTest();

//   @override
//   _UploadImageTestState createState() => _UploadImageTestState();
// }

// class _UploadImageTestState extends State<UploadImageTest> {
//   File _image;
//   bool _uploadInProgress = false;

//   _uploadImage(BuildContext context) async {
//     setState(() {
//       _uploadInProgress = true;
//     });

//     var byteData = _image.readAsBytesSync();

//     var multipartFile = http.MultipartFile.fromBytes(
//       'photo',
//       byteData,
//       filename: '${DateTime.now().second}.jpg',
//       contentType: MediaType("image", "jpg")
//       // ("image", "jpg")
//       ,
//     );

//     var opts = MutationOptions(
//       document: '',
//       variables: {
//         "file": multipartFile,
//       },
//     );

//     var client = Graphql.client;

//     QueryResult results = await client.mutate(opts);

//     setState(() {
//       _uploadInProgress = false;
//     });

//     var message = results.hasException
//         ? '${results.exception.graphqlErrors}'
//         : "Image was uploaded successfully!";

//     final snackBar = SnackBar(content: Text(message));
//     Scaffold.of(context).showSnackBar(snackBar);
//   }

//   Future selectImage() async {
//     var image = await ImagePicker.pickImage(source: ImageSource.gallery);

//     setState(() {
//       _image = image;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Column(
//         // mainAxisAlignment: MainAxisAlignment.center,
//         mainAxisSize: MainAxisSize.max,
//         children: <Widget>[
//           if (_image != null)
//             Flexible(
//               flex: 9,
//               child: Image.file(_image),
//             )
//           else
//             Flexible(
//               flex: 9,
//               child: Center(
//                 child: Text("No Image Selected"),
//               ),
//             ),
//           Flexible(
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               mainAxisSize: MainAxisSize.max,
//               children: <Widget>[
//                 FlatButton(
//                   child: Row(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     mainAxisSize: MainAxisSize.min,
//                     children: <Widget>[
//                       Icon(Icons.photo_library),
//                       SizedBox(
//                         width: 5,
//                       ),
//                       Text("Select File"),
//                     ],
//                   ),
//                   onPressed: () => selectImage(),
//                 ),
//                 if (_image != null)
//                   Mutation(
//                     options: MutationOptions(
//                       document: uploadImage,
//                     ),
//                     builder: (RunMutation runMutation, QueryResult result) {
//                       return FlatButton(
//                         child: _isLoadingInProgress(),
//                         onPressed: () {
//                           setState(() {
//                             _uploadInProgress = true;
//                           });

//                           var byteData = _image.readAsBytesSync();

//                           var multipartFile = MultipartFile.fromBytes(
//                             'photo',
//                             byteData,
//                             filename: '${DateTime.now().second}.jpg',
//                             contentType: MediaType("image", "jpg"),
//                           );

//                           runMutation(<String, dynamic>{
//                             "file": multipartFile,
//                           });
//                         },
//                       );
//                     },
//                     onCompleted: (d) {
//                       print(d);
//                       setState(() {
//                         _uploadInProgress = false;
//                       });
//                     },
//                     update: (cache, results) {
//                       var message = results.hasErrors
//                           ? '${results.errors.join(", ")}'
//                           : "Image was uploaded successfully!";

//                       final snackBar = SnackBar(content: Text(message));
//                       Scaffold.of(context).showSnackBar(snackBar);
//                     },
//                   ),
//                 // if (_image != null)
//                 //   FlatButton(
//                 //     child: _isLoadingInProgress(),
//                 //     onPressed: () => _uploadImage(context),
//                 //   ),
//               ],
//             ),
//           )
//         ],
//       ),
//     );
//   }

//   Widget _isLoadingInProgress() {
//     return _uploadInProgress
//         ? CircularProgressIndicator()
//         : Row(
//             mainAxisSize: MainAxisSize.min,
//             children: <Widget>[
//               Icon(Icons.file_upload),
//               SizedBox(
//                 width: 5,
//               ),
//               Text("Upload File"),
//             ],
//           );
//   }
// }

class UploadImageTest extends StatefulWidget {
  @override
  _UploadImageTestState createState() => _UploadImageTestState();
}

class _UploadImageTestState extends State<UploadImageTest> {
  File selectedImage;
  String imageUrl = 'not a url';
  String error = 'errors show here';
  String result = 'results show here ';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(24),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: Center(
                child: PickPictureButton(onChanged: (f) {
                  setState(() {
                    selectedImage = f;
                    printGreenBg('slected ------ ${f.path}');
                  });
                }),
              ),
            ),
            Expanded(
              child: Center(
                child: RaisedButton(
                  onPressed: () {
                    printGreenBg(selectedImage.path);
                    _uploadImg(selectedImage);
                  },
                  child: Text('upload image'),
                ),
              ),
            ),
            Expanded(
              child: Center(
                child: CustomAvatar(
                  imageUrl: '$imageUrl',
                ),
              ),
            ),
            Expanded(
                child: SingleChildScrollView(
              child: SelectableText(
                '$result',
                style: red14medium.copyWith(color: Colors.green[900]),
              ),
            )),
            Expanded(
              child: SingleChildScrollView(
                child: SelectableText(
                  '$error',
                  style: red14medium.copyWith(color: Colors.red[900]),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _uploadImg(img) async {
    QueryResult rawData;
    rawData = await upLoadFileMutation(
      file: img,
    );
    printGreenBg(rawData);
    setState(() {
      result = rawData.toString();
    });
    if (rawData.hasException) {
      printRedBg(rawData.exception);
      setState(() {
        error = rawData.exception.toString();
      });
      printRedBg('has error');
      return;
    } else if (rawData.data == null) {
      printRedBg('null data');
      return;
    } else {
      String url;

      url = rawData.data['uploadFile']['url'];
      printGreenBg(url);
      setState(() {
        imageUrl = uri.replaceAll('graphql', '') + url;
      });
    }
  }
}

Future<QueryResult> upLoadFileMutation(
    {File file, String folderName = 'default'}) async {
  var byteData = file.readAsBytesSync();
  var imageData = byteData.buffer.asUint8List();
  var multipartFile = http.MultipartFile.fromBytes(
    'photo',
    imageData,
    filename: '${DateTime.now().second}.png',
    contentType: MediaType("image", "png"),
  );

  MutationOptions m = MutationOptions(
      fetchPolicy: FetchPolicy.cacheAndNetwork,
      document: gql(upload),
      variables: {"file": multipartFile, "folderName": folderName});
  // Graphql.addHeader({
  //   "authorization":
  //       "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDRkZDgwZjA4NzdkYTYxMjBjMDM2ODciLCJzdWIiOiI2MDRkZDgwZjA4NzdkYTYxMjBjMDM2ODciLCJpc3MiOiJzcGFyay5jb20iLCJ0b2tlblR5cGUiOiJBQ0NFU1NfVE9LRU4iLCJyb2xlcyI6IlNIT1BfQURNSU4iLCJ0b2tlbktleSI6IjgxYWQxNGQ3LTAyZTctNGIxMi04ZjcxLTBhNTkzZmIwNWNlYiIsInNob3AiOiI2MDRkZDg0MjA4NzdkYWI1MjhjMDM2OGMiLCJpYXQiOjE2MTcwNDQxNzEsImV4cCI6MTYxOTYzNjE3MX0.hi9hp1lqBj762Knt_xSsxij1UFAraWV2yFH1a89pFyE"
  // await SecureStorage.getToken(accessTokenKey)
  // });
  return await Graphql.client.mutate(m).timeout(Duration(seconds: 15));
}

String upload = """
mutation uploadShopImage(\$file: Upload!, \$folderName: String!) {
  uploadFile(data: { file: \$file, folderName: \$folderName }) {
    url
  }
}
  """;

Future<String> uploadImage(File image) async {
  QueryResult rawData;
  rawData = await upLoadFileMutation(
    file: image,
  );
  printGreenBg(rawData);

  if (rawData.hasException) {
    printRedBg(rawData.exception);

    printRedBg('has error');
    return null;
  } else if (rawData.data == null) {
    printRedBg('null data');
    return null;
  } else {
    String url;

    url = rawData.data['uploadFile']['url'];
    printGreenBg(url);
    return uri.replaceAll('graphql', '') + url;
  }
}
