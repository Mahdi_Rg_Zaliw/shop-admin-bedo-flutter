import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/screens/vanils_showcases/order_card.dart';
import 'package:bedo_shop_admin/presentation/widgets/bottom_navigation.dart';
import 'package:bedo_shop_admin/presentation/widgets/filtered_list.dart';
import 'package:flutter/cupertino.dart';

class FilteredListWithOrderCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FilteredList.common(
      showBackButton: true,
      appBarTitlesList: [
        Text(
          'Total amount today ',
          style: grey14medium,
        )
      ],
      appBarBottomList: [
        Text(
          '${369} \$',
          style: black24medium,
        )
      ],
      // imgurl: 'https://picsum.photos/253',
      optionShouldNotifyList: [false, true],
      optionsList: [S.of(context).onlineOrder, S.of(context).currentOrder],
      topTitlesList: [Text(' one'), Text('two')],
      bottomTitleList: [Text(' 23551'), Text('156155')],
      pagesList: [OrdersCardShowCase(), OrdersCardShowCase()],
      title: 'total amount today',
      totalAmount: 399,
      bottomNavigationBar: CustomBottomNavigationBar(),
    );
  }
}
