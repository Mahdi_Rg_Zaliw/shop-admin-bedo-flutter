import 'package:bedo_shop_admin/bloc/discount_bloc/discount_bloc.dart';
import 'package:bedo_shop_admin/bloc/discount_bloc/discount_events.dart';
import 'package:bedo_shop_admin/bloc/discount_bloc/discount_states.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/add_discount_modal.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/delete_discount_modal.dart';
import 'package:bedo_shop_admin/presentation/widgets/order_and_discount_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';

class AddDiscountScreen extends StatefulWidget {
  static const routeName = 'add_discount';
  @override
  _AddDiscountScreenState createState() => _AddDiscountScreenState();
}

class _AddDiscountScreenState extends State<AddDiscountScreen> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<DiscountBloc>(context).add(LoadDiscounts());
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text(
          S.of(context).addDiscount,
          style: grey14medium,
        ),
        centerTitle: true,
      ),
      body: Container(
        height: height,
        width: width,
        child: BlocBuilder<DiscountBloc, DiscountsState>(
          builder: (context, state) => RefreshIndicator(
            onRefresh: () async {
              BlocProvider.of<DiscountBloc>(context).add(LoadDiscounts());
              if (state is! DiscountsLoading) {
                return null;
              }
            },
            child: Column(
              children: [
                if (state is DiscountsLoaded)
                  Expanded(
                    child: state.results.length > 0
                        ? ListView(
                            physics: const AlwaysScrollableScrollPhysics(),
                            children: [
                              ...state.results
                                  .map((element) => GestureDetector(
                                        onTap: () {
                                          deleteDiscountModal(
                                              context: context,
                                              discount: element);
                                        },
                                        child: OrderAndDiscountWidget.discount(
                                          id: element.id,
                                          date: element.endDate,
                                          title: element.promotionCode,
                                          percent: element.percent,
                                          isDone: !element.isExpired,
                                        ),
                                      ))
                                  .toList()
                            ],
                          )
                        : Center(
                            child: Padding(
                              padding: const EdgeInsets.all(40),
                              child: Text(
                                S.of(context).noDiscountsMessage,
                                style: grey14medium,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                  ),
                if (state is DiscountsNotLoaded ||
                    state is DeleteDiscountFailed ||
                    state is CreateDiscountFailed)
                  Expanded(
                      child: Center(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        S.of(context).errorWhileFetchingDiscounts,
                        style: red14medium,
                      ),
                    ),
                  )),
                if (state is DiscountsLoading)
                  Expanded(child: CupertinoActivityIndicator()),
                Container(
                    width: width,
                    height: 65,
                    padding: EdgeInsets.symmetric(
                        horizontal: width * 0.06, vertical: 5),
                    child: RaisedButton(
                      onPressed: () {
                        addDiscountModal(context: context);
                      },
                      child: Text(S.of(context).addDiscountCode),
                      elevation: 0,
                      highlightElevation: 1,
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
