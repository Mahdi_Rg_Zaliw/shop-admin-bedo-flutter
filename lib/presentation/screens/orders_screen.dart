import 'package:bedo_shop_admin/bloc/orders_bloc/orders_bloc.dart';
import 'package:bedo_shop_admin/bloc/orders_bloc/orders_event.dart';
import 'package:bedo_shop_admin/bloc/orders_bloc/orders_state.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/presentation/widgets/filtered_list.dart';
import 'package:bedo_shop_admin/presentation/widgets/oreder_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class OrdersScreen extends StatefulWidget {
  static const routeName = 'orders';
  const OrdersScreen({Key key}) : super(key: key);

  @override
  _OrdersScreenState createState() => _OrdersScreenState();
}

class _OrdersScreenState extends State<OrdersScreen> {
  @override
  void initState() {
    BlocProvider.of<OrdersBloc>(context).add(LoadCurrentOrders());
    BlocProvider.of<OrdersBloc>(context).add(StartListenToOrders());

    super.initState();
  }

  @override
  void dispose() {
    BlocProvider.of<OrdersBloc>(context).add(StopListenToOrders());

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<OrdersBloc>(context).add(LoadCurrentOrders());

    return BlocBuilder<OrdersBloc, OrdersState>(
      builder: (context, state) => FilteredList.fix(
        optionShouldNotifyList: [false, true],
        optionsList: [S.of(context).currentOrder, S.of(context).onlineOrder],
        pagesList: [CurrentOrders(), OnlineOrders()],
        appBarTitlesList: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(
                S.of(context).totalAmountToday,
                style: grey14medium,
              ),
              Text(
                S.of(context).totalOrdersToday,
                style: grey14medium,
              ),
            ],
          )
        ],
        appBarBottomList: [
          Padding(
            padding: const EdgeInsets.only(bottom: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  state.totalOrdersAmount == null
                      ? "0.0"
                      : state.totalOrdersAmount.toString(),
                  style: black24medium,
                ),
                Text(
                  state.totalOrdersCount == null
                      ? "0"
                      : state.totalOrdersCount.toString(),
                  style: black24medium,
                ),
              ],
            ),
          )
        ],
        // bottomNavigationBar: CustomBottomNavigationBar(),
      ),
    );
  }
}

// ignore: must_be_immutable
class OnlineOrders extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        BlocProvider.of<OrdersBloc>(context).add(LoadPendingOrders());
        return;
      },
      child: BlocBuilder<OrdersBloc, OrdersState>(
        cubit: BlocProvider.of<OrdersBloc>(context),
        builder: (context, state) {
          if (state is! OrdersLoading) {
            if (state is OrderLoadFailed) {
              return Center(
                child: Padding(
                  padding: const EdgeInsets.all(30),
                  child: Text(
                    S.of(context).errorWhileFetchingOnlineOrders,
                    style: red14medium,
                    textAlign: TextAlign.center,
                  ),
                ),
              );
            }
            return state.pendingOrders.length > 0
                ? ListView(
                    children: state.pendingOrders
                        .map(
                          (e) => OrderCard(
                            order: e,
                          ),
                        )
                        .toList(),
                  )
                : Center(
                    child: Text(
                      S.of(context).noOnlineOrders,
                      style: grey14medium,
                    ),
                  );
          } else if (state is OrdersLoading) {
            return Center(child: CupertinoActivityIndicator());
          }
          return Container();
        },
      ),
    );
  }
}

// ignore: must_be_immutable
class CurrentOrders extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        BlocProvider.of<OrdersBloc>(context).add(LoadCurrentOrders());
        return;
      },
      child: BlocBuilder<OrdersBloc, OrdersState>(
        cubit: BlocProvider.of<OrdersBloc>(context),
        builder: (context, state) {
          if (state is! OrdersLoading) {
            if (state is OrderLoadFailed) {
              return Center(
                child: Padding(
                  padding: const EdgeInsets.all(30),
                  child: Text(
                    S.of(context).errorWhileFetchingOnlineOrders,
                    style: red14medium,
                    textAlign: TextAlign.center,
                  ),
                ),
              );
            }
            return state.currentOrders.length > 0
                ? ListView(
                    children: state.currentOrders
                        .map(
                          (e) => OrderCard(
                            order: e,
                            //todo get status from api
                            status: e.status,
                          ),
                        )
                        .toList(),
                  )
                : Center(
                    child: Text(
                      S.of(context).noOnlineOrders,
                      style: grey14medium,
                    ),
                  );
          } else if (state is OrdersLoading) {
            return Center(child: CupertinoActivityIndicator());
          }
          return Container();
        },
      ),
    );
  }
}
