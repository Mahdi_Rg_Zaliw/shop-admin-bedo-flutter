import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/presentation/screens/edit_shop.dart';
import 'package:bedo_shop_admin/presentation/screens/login.dart';
import 'package:bedo_shop_admin/presentation/widgets/custom_text_field.dart';
import 'package:bedo_shop_admin/bloc/auth_bloc/auth_bloc.dart';
import 'package:bedo_shop_admin/bloc/auth_bloc/auth_events.dart';
import 'package:bedo_shop_admin/bloc/auth_bloc/auth_states.dart';
import 'package:bedo_shop_admin/presentation/widgets/products_cart.dart';
import 'package:bedo_shop_admin/presentation/widgets/round_botton.dart';
import 'package:bedo_shop_admin/utils/device_dimensions.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';

class CompleteProfileScreen extends StatefulWidget {
  final String phoneNumberSignUpCode;
  final String phoneNumber;

  CompleteProfileScreen({this.phoneNumberSignUpCode, this.phoneNumber});
  @override
  _CompleteProfileScreenState createState() => _CompleteProfileScreenState();
}

class _CompleteProfileScreenState extends State<CompleteProfileScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  final Map<String, String> _formData = {
    'name': '',
    'email': '',
    'password': '',
  };
  final _passwordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(1.5 * kToolbarHeight),
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 24,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    S.of(context).createAccount,
                    style: grey14regular,
                    textAlign: TextAlign.start,
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Text(
                    S
                        .of(context)
                        .fillOutTheFollowingFiledsToCompleteYourProfile,
                    style: black24semibold,
                  ),
                  SizedBox(
                    height: 12,
                  )
                ],
              ),
            ),
          ),
        ),
        body: Container(
          alignment: Alignment.bottomCenter,
          child: SingleChildScrollView(
            // reverse: true,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              // mainAxisSize: MainAxisSize.min,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.end,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Form(
                      key: _formKey,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          CustomTextField(
                            title: S.of(context).fullName,
                            hintText: S.of(context).enterTheName,
                            style: black14regular,
                            isRequired: true,
                            validator: (String value) {
                              if (value.length >= 4) {
                                return null;
                              }
                              return S.of(context).enteredNameIsTooShort;
                            },
                            onSaved: (String value) {
                              _formData['name'] = value;
                            },
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 15),
                            child: CustomTextField(
                              title: S.of(context).emailAddress,
                              hintText: S.of(context).enterTheEmail,
                              style: black14regular,
                              isRequired: true,
                              validator: (String value) {
                                if (value.isValidEmail()) {
                                  return null;
                                }
                                return S.of(context).enteredEmailIsInvalid;
                              },
                              onSaved: (String value) {
                                _formData['email'] = value.trim();
                              },
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 15),
                            child: CustomTextField(
                              title: "Password",
                              hintText: S.of(context).enterThePassword,
                              controller: _passwordController,
                              style: black14regular,
                              isRequired: true,
                              isPassword: true,
                              validator: (String value) {
                                if (value.length >= 8) {
                                  return null;
                                }
                                return S.of(context).enteredPasswordIsTooShort;
                              },
                              onSaved: (String value) {
                                _formData['password'] = value;
                              },
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 15),
                            child: CustomTextField(
                              title: "Password",
                              hintText: S.of(context).enterThePassword,
                              controller: _confirmPasswordController,
                              style: black14regular,
                              isRequired: true,
                              isPassword: true,
                              validator: (String value) {
                                if (value.length < 8) {
                                  return S
                                      .of(context)
                                      .enteredPasswordIsTooShort;
                                }
                                if (_passwordController.text !=
                                    _confirmPasswordController.text) {
                                  return 'password confirm is not correct';
                                }
                                return null;
                              },
                              onSaved: (String value) {
                                _formData['password'] = value;
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 12)
                          .copyWith(bottom: 24),
                      child: BlocConsumer<AuthBloc, AuthState>(
                        listener: (context, state) {
                          if (state is UserSignUpSuccess) {
                            showDialog(
                              context: context,
                              builder: (context) => CustomDialogueSuccess(
                                title:
                                    'successfully registed user you can now login',
                              ),
                            ).then((value) => Navigator.of(context)
                                .pushReplacementNamed(Login.routeName));
                          }
                          if (state is SignUpCompeletErrors) {
                            showDialog(
                              context: context,
                              builder: (context) => AlertDialog(
                                title: FailureText(
                                  title: state.clientErrors.toString(),
                                ),
                                actions: [
                                  Buttons.redOutLine(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Text('ok'))
                                ],
                              ),
                            );
                          }
                        },
                        builder: (context, state) {
                          if (state is AuthLoading) {
                            return RaisedButton(
                              child: Text(
                                'Waiting',
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: null,
                            );
                          } else {
                            return RaisedButton(
                              child: Text(
                                'Create account',
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: () {
                                if (!_formKey.currentState.validate()) {
                                  // Invalid!
                                  return null;
                                }

                                if (_formKey.currentState.validate()) {
                                  // Invalid!
                                  _formKey.currentState.save();
                                  print(_formData);

                                  ComplateInformationSignUp
                                      complateInformationSignUp =
                                      ComplateInformationSignUp(
                                    phoneNumber: widget.phoneNumber,
                                    signUpVerificationCode:
                                        widget.phoneNumberSignUpCode,
                                    fullName: _formData['name'],
                                    email: _formData['email'],
                                    password: _formData['password'],
                                  );

                                  context
                                      .read<AuthBloc>()
                                      .add(complateInformationSignUp);
                                }
                              },
                            );
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Future succesfullRegisterModal({
  BuildContext context,
}) {
  return showModalBottomSheet(
    isScrollControlled: false,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(24),
        topRight: Radius.circular(24),
      ),
    ),
    context: context,
    builder: (context) {
      return Column(
        children: [
          Expanded(
            flex: 1,
            child: Padding(
              padding: EdgeInsets.all(25),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'thank you ! \nYour Registeration Is Successfully Complated you can now login',
                    style: black18medium,
                  ),
                ],
              ),
            ),
          ),
          Container(
            width: DeviceDimensions.widthSize(context),
            padding: EdgeInsets.only(left: 24, right: 24, bottom: 18, top: 10),
            child: RaisedButton(
              elevation: 0,
              onPressed: () {
                Navigator.of(context).pushReplacementNamed(Login.routeName);
              },
              child: Text('Confirm'),
            ),
          ),
        ],
      );
    },
  );
}
