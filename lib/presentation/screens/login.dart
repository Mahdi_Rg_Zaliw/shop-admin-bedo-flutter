import 'package:bedo_shop_admin/bloc/auth_bloc/auth_bloc.dart';
import 'package:bedo_shop_admin/bloc/auth_bloc/auth_events.dart';
import 'package:bedo_shop_admin/bloc/auth_bloc/auth_states.dart';
import 'package:bedo_shop_admin/bloc/router_bloc/signinrouter_bloc.dart';
import 'package:bedo_shop_admin/data/graphql/graphql.dart';
import 'package:bedo_shop_admin/presentation/screens/edit_shop.dart';
import 'package:bedo_shop_admin/presentation/screens/splash.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bedo_shop_admin/design/icons_f.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/presentation/screens/sign_up.dart';
import 'package:bedo_shop_admin/presentation/widgets/custom_text_field.dart';
import 'package:bedo_shop_admin/data/graphql/auth/auth_request.dart';
import 'package:bedo_shop_admin/constants/constants.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import '../../data/secure_storage/secure_storage.dart';

class Login extends StatefulWidget {
  static const routeName = 'login';

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  String fcmTokenSave;
  String errorMessage = '';
  TextEditingController emailOrUsername = TextEditingController();
  TextEditingController password = TextEditingController();
  AuthRequest authRequest;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      fcmTokenSave = await fcmTokenget;
    });
    // _firebaseMessaging.getToken().then((token) {
    //   setState(() {
    //     fcmTokenSave = token;
    //   });
    // });
    printOrange('fcm on start was $fcmTokenSave');
  }

  Future<String> get fcmTokenget async {
    String fcm = await _firebaseMessaging.getToken();
    return fcm;
  }

  // @protected
  // @mustCallSuper
  // void deactivate() {
  //   super.deactivate();
  //   BlocProvider.of<AuthBloc>(context).add(AuthBlocReset());
  //   printWhiteBg('out and reset');
  // }

  bool _err = false;
  String message = '';

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          height: height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: height * 0.05),
                    child: Icon(
                      Iconsf.i32_logo,
                      color: Theme.of(context).primaryColor,
                      size: 76,
                    ),
                  ),
                  SizedBox(height: height * 0.05),
                  Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 4.0),
                        child: Text(
                          S.of(context).signIn,
                          style: grey14regular,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0),
                        child: Text(
                          S.of(context).manageStoreOrders,
                          style: black24semibold,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Flexible(child: Container()),
              Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.end,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    BlocConsumer<AuthBloc, AuthState>(
                      listener: (context, state) {
                        printCyanBg('err is $_err ');
                        printGreenBg('we do val');
                        setState(() {
                          _formKey.currentState.validate();
                        });

                        if (state is LoginErrors) {
                          setState(() {
                            _err = true;
                            message = state.clientErrors.toString();
                            _formKey.currentState.validate();
                          });
                        }
                        if (state is AuthInitial) {
                          setState(() {
                            _err = false;
                          });
                        }
                      },
                      builder: (context, state) {
                        if (state is LoginErrors) {
                          _err = true;
                          message = state.clientErrors.toString();
                        }
                        // _formKey.currentState.validate();
                        return Column(
                          children: [
                            CustomTextField(
                              maxline: 1,
                              onChanged: printSomeColor,
                              validator: (v) {
                                if (_err) {
                                  return '⚠ ⛔ ';
                                  // '⚠ ╳ ⯅ 🛆 ⛌ 🟕 🟖 ⨻';
                                }
                                if (!v.isValidEmail()) {
                                  if (v.isValidMobile()) {
                                    return null;
                                  }
                                }
                                if (!v.isValidMobile()) {
                                  if (v.isValidEmail()) {
                                    return null;
                                  }
                                }
                                return 'enter valid email or phone number';
                              },
                              controller: emailOrUsername,
                              isRequired: true,
                              title:
                                  'Email or Phone number', //S.of(context).emailAddress,
                              hintText:
                                  'Enter Email or Phone number here', //S.of(context).enterTheEmail,
                              hintStyle: grey14regular,
                            ),
                            SizedBox(
                              height: height * 0.01,
                            ),
                            CustomTextField(
                              validator: (v) {
                                if (_err) {
                                  return '$message';
                                }
                                if (v.length < 8) {
                                  return 'must be 8 characters at least';
                                }

                                return null;
                              },
                              controller: password,
                              isRequired: true,
                              title: S.of(context).password,
                              hintText: S.of(context).enterThePassword,
                              hintStyle: grey14regular,
                              isPassword: true,
                            ),
                            // if (state is LoginErrors)
                            // Padding(
                            //   padding: EdgeInsets.all(8).copyWith(
                            //     top: 0,
                            //   ),
                            //   child: Text(
                            //     state.clientErrors.toString(),
                            //     // textAlign: TextAlign.center,
                            //     style: TextStyle(color: red),
                            //   ),
                            // ),
                          ],
                        );
                      },
                    ),
                    SizedBox(height: height * 0.02),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 25, vertical: height * 0.01),
                      child: BlocConsumer<AuthBloc, AuthState>(
                          listener: (context, state) {
                        if (state is LoginErrors) {
                          printRedBg(state.clientErrors);
                        }

                        if (state is UserLoginSuccess) {
                          showDialog(
                              context: context,
                              builder: (context) => CustomDialogueSuccess(
                                    title: 'login successfull',
                                  )).then((value) {
                            BlocProvider.of<SigninrouterBloc>(context)
                                .add(UserLoggedInSuccess());
                            Navigator.of(context)
                                .pushReplacementNamed(Splash.routeName);
                          });
                        }
                      }, builder: (context, state) {
                        print(state);
                        return RaisedButton(
                          onPressed: () async {
                            if (fcmTokenSave == null) {
                              fcmTokenSave = await fcmTokenget;
                            }
                            // await fireba
                            // printYellowBg(
                            //     ' fcm was     ===============================   $ftkn');
                            _err = false;
                            LoginEvents loginEvents = LoginEvents(
                              emailOrPhoneNumber: emailOrUsername.text,
                              password: password.text,
                              fcm: fcmTokenSave,
                            );
                            printYellowBg('before $state');
                            // _formKey.currentState.reset();
                            context.read<AuthBloc>().add(AuthBlocReset());
                            printYellowBg('after $state');
                            setState(() {
                              _formKey.currentState.validate();
                              printBlueBg(
                                  'form re validate ${_formKey.currentState.validate()}');
                            });
                            if (_formKey.currentState.validate()) {
                              printGreenBg('form validate is true ');
                              // var connectivityResult =
                              //     await (Connectivity().checkConnectivity());

                              // if (connectivityResult ==
                              //         ConnectivityResult.mobile ||
                              //     connectivityResult ==
                              //         ConnectivityResult.wifi) {
                              context.read<AuthBloc>().add(loginEvents);
                              String accessToken =
                                  await SecureStorage.getToken(accessTokenKey);
                              printGreenBg(
                                  'token in sotrage IS ==> $accessToken');
                              // Graphql.addHeader({"authorization": accessToken});
                              Graphql.sayHeadr();
                              String id = await SecureStorage.getId(userIdKey);
                              print('ID User IS ==> $id');
                              // }
                              // } else {
                              //   setState(() {
                              //     printGreenBg('we seeeeee');
                              //     _err = true;
                              //     _formKey.currentState.validate();
                              //     errorMessage = 'Network Error';
                              //   });
                              // }
                            }
                          },
                          child: Text(
                            S.of(context).signIn,
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w400),
                          ),
                          elevation: 0,
                          highlightElevation: 1,
                        );
                      }),
                    ),
                    FlatButton(
                      onPressed: () {
                        Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                            builder: (context) => SignUp(),
                          ),
                        );
                      },
                      child: Text(
                        S.of(context).createNewAccount,
                        style: black14medium,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
