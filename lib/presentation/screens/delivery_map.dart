import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/widgets/comment_widggets/custom_avatar.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class DeliveryMap extends StatefulWidget {
  final void Function(double lat, double lng) onSelectedMapChanges;
  DeliveryMap({this.onSelectedMapChanges});

  @override
  State<DeliveryMap> createState() => DeliveryMapState();
}

class DeliveryMapState extends State<DeliveryMap> {
  Completer<GoogleMapController> _controller = Completer();
  String option = 'option';

  @override
  void initState() {
    super.initState();
  }

  final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(40.4093, 49.8671),
    zoom: 14.4746,
  );

  Widget optionWidget() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
      height: 90,
      color: Colors.white,
      child: Row(
        children: [
          Expanded(
            child: RaisedButton(
              padding: EdgeInsets.symmetric(vertical: 17),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(40.0),
              ),
              elevation: 0,
              color: Color(0xff00dbba),
              onPressed: () {},
              child: Text(
                S.of(context).callPassenger,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 13,
                ),
              ),
            ),
            flex: 1,
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: RaisedButton(
              padding: EdgeInsets.symmetric(vertical: 17),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(40.0)),
              elevation: 0,
              color: Color(0xfff1f1f1),
              onPressed: () {
                setState(() {
                  option = 'driver_details';
                });
              },
              child: Text(
                S.of(context).driverDetails,
                style: TextStyle(
                  color: Color(0xff565656),
                  fontSize: 13,
                ),
              ),
            ),
            flex: 1,
          ),
        ],
      ),
    );
  }

  Widget driverDetailsWidget() {
    return Container(
      padding: EdgeInsets.only(top: 15, bottom: 15, left: 10),
      // height: 90,
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          CustomAvatar(
            radius: 35,
          ),
          Container(
            width: 150,
            // height: double.maxFinite,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  'Liam Parker',
                  style: black14medium,
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      'Ford Fiesta 2019 - D Fiesta 2019 - Fiesta 2019 - Dark HairFord Fiesta 2019 - D Fiesta 2019 - Dark Hair Ford Fiesta',
                      style: grey14regular,
                    ),
                  ],
                ),
              ],
            ),
          ),
          FlatButton(
            onPressed: () {
              setState(() {
                option = 'option';
              });
            },
            child: Text(
              S.of(context).close,
              style: green14medium,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Stack(
        children: [
          GoogleMap(
            onCameraMove: (CameraPosition pos) {
              setState(() {
                widget.onSelectedMapChanges(
                  pos.target.latitude,
                  pos.target.longitude,
                );
              });
            },
            mapType: MapType.normal,
            initialCameraPosition: _kGooglePlex,
            onMapCreated: (GoogleMapController controller) {
              _controller.complete(controller);
            },
          ),
          Align(
            child: Icon(
              Icons.location_on,
              size: 46,
            ),
            alignment: Alignment.center,
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              alignment: Alignment.center,
              height: 90,
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    child: Icon(Icons.arrow_back),
                    padding: EdgeInsets.only(left: 10, top: 25),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 25),
                    child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: '${S.of(context).orderId}: ',
                            style: TextStyle(
                              color: Color(0xff9a9a9a),
                            ),
                          ),
                          TextSpan(
                            text: 'GE-2346-M2',
                            style: TextStyle(
                              color: Color(0xff232323),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    child: Text(
                      '50 ${S.of(context).min}',
                      style: TextStyle(
                        color: Color(0xff232323),
                        fontSize: 16,
                      ),
                    ),
                    padding: EdgeInsets.only(right: 10, top: 25),
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: option == 'option' ? optionWidget() : driverDetailsWidget(),
          ),
        ],
      ),
    );
  }
}
