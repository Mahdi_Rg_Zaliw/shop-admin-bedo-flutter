import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/widgets/courier_tile.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class SelectCourierScreen extends StatefulWidget {
  final List<Map> courierData;

  SelectCourierScreen(
      {this.courierData = const [
        {
          'courierName': 'Liam Parker',
          'courierDesc': 'Test Description Courier Selection Liam Parker',
          'imageUrl': 'https://picsum.photos/253',
          'status': CurierStatus.available,
        },
        {
          'courierName': 'Zac Parker',
          'courierDesc': 'Test Description Courier Selection Zac Parker',
          'imageUrl': 'https://picsum.photos/253',
          'status': CurierStatus.inService,
        },
        {
          'courierName': 'David Parker',
          'courierDesc':
              'Test Description Courier SelectionTest  Selection David Parker',
          'imageUrl': 'https://picsum.photos/253',
          'status': CurierStatus.available,
        },
        {
          'courierName': 'Jeff ParkerJeff ',
          'courierDesc': 'Test Description Courier Selection Jeff Parker',
          'imageUrl': 'https://picsum.photos/253',
          'status': CurierStatus.inService,
        },
      ]});

  @override
  _SelectCourierScreenState createState() => _SelectCourierScreenState();
}

class _SelectCourierScreenState extends State<SelectCourierScreen> {
  int selectedIndex;
  List<Map> data;
  @override
  void initState() {
    super.initState();
    data = widget.courierData;
  }

  Widget buttonSelect(
    String text,
    GestureTapCallback onTap,
  ) {
    return Opacity(
      opacity: selectedIndex == null ? 0.5 : 1,
      child: RaisedButton(
        highlightElevation: selectedIndex == null ? 0 : 5,
        focusColor:
            selectedIndex == null ? Colors.transparent : primaryMaterialColor,
        splashColor:
            selectedIndex == null ? Colors.transparent : primaryMaterialColor,
        highlightColor: selectedIndex == null
            ? Colors.transparent
            : Colors.white.withOpacity(0.2),
        elevation: 0,
        onPressed: selectedIndex == null ? () {} : onTap,
        child: Text(text),
      ),
    );
  }

  Widget couriers(
    CurierStatus status,
    String imageUrl,
    bool selected,
    String courierName,
    String courierDesc,
    GestureTapCallback onTap,
  ) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: CourierTile(
        courierName: courierName,
        courierDesc: courierDesc,
        imageUrl: imageUrl,
        status: status,
        selected: selected,
        onTap: onTap,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // toolbarHeight: 90,
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text(
          S.of(context).selectCourier,
          style: black14medium,
        ),
      ),
      body: ListView(
        children: widget.courierData
            .map(
              (e) => couriers(
                e['status'],
                e['imageUrl'],
                widget.courierData.indexOf(e) == selectedIndex ? true : false,
                e['courierName'],
                e['courierDesc'],
                () {
                  setState(() {
                    selectedIndex = widget.courierData.indexOf(e);
                  });
                },
              ),
            )
            .toList(),
      ),
      bottomNavigationBar: Container(
        // height: 100,
        padding: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
        child: buttonSelect(
          S.of(context).select,
          () {
            print('${S.of(context).courier} $selectedIndex ${S.of(context).selected}!');
          },
        ),
      ),
    );
  }
}
