import 'package:bedo_shop_admin/bloc/courier_bloc/courier_bloc.dart';
import 'package:bedo_shop_admin/bloc/courier_bloc/courier_event.dart';
import 'package:bedo_shop_admin/bloc/courier_bloc/courier_state.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/presentation/widgets/courier_tile.dart';
import 'package:bedo_shop_admin/presentation/screens/update_courier_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bedo_shop_admin/presentation/screens/add_courier_screen.dart';
import 'package:flutter/cupertino.dart';

// ignore: must_be_immutable
class CourierListScreen extends StatefulWidget {
  static const routeName = 'courier_list';

  @override
  _CourierListScreenState createState() => _CourierListScreenState();
}

class _CourierListScreenState extends State<CourierListScreen> {
  int selectedIndex;
  List<Map> data;
  @override
  void initState() {
    super.initState();
    BlocProvider.of<CourierBloc>(context).add(GetCourierList());
  }

  Widget buttonSelect(
    String text,
    GestureTapCallback onTap,
  ) {
    return Opacity(
      opacity: 1,
      child: RaisedButton(
        highlightElevation: 5,
        focusColor: primaryMaterialColor,
        splashColor: primaryMaterialColor,
        highlightColor: Colors.white.withOpacity(0.2),
        elevation: 0,
        onPressed: onTap,
        child: Text(text),
      ),
    );
  }

  Widget couriers(
    String imageUrl,
    String courierName,
    String courierDesc,
    GestureTapCallback moreVertOnTap,
  ) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: CourierTile(
        courierName: courierName,
        courierDesc: courierDesc,
        imageUrl: imageUrl,
        moreVertOnTap: moreVertOnTap,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<CourierBloc>(context).add(GetCourierList());

    return Scaffold(
      appBar: AppBar(
        // toolbarHeight: 90,
        automaticallyImplyLeading: false,
        // leading: IconButton(
        //   icon: Icon(Icons.arrow_back),
        //   onPressed: () {
        //     Navigator.pushNamed(context, 'home');
        //   },
        // ),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text('Add Courier'),
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          context.read<CourierBloc>().add(GetCourierList());
        },
        child: BlocBuilder<CourierBloc, CourierState>(
          builder: (context, state) {
            if (state is CourierListArrived) {
              return ListView.builder(
                itemCount: state.courierListData.length,
                itemBuilder: (context, index) {
                  final e = state.courierListData[index];
                  return couriers(
                    e.profileImageUrl,
                    e.fullName,
                    e.email,
                    () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return UpdateCourierScreen(
                              id: e.id,
                              fullName: e.fullName,
                              phoneNumber: e.phoneNumber,
                              email: e.email,
                              gender: e.gender,
                              imageUrl: e.profileImageUrl,
                            );
                          },
                        ),
                      ).then((value) => BlocProvider.of<CourierBloc>(context)
                          .add(GetCourierList()));
                    },
                  );
                },
              );
            }
            if (state is CourierExceptionState) {
              return Text(state.clientErrors);
            }
            return Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width * 1.2,
              alignment: Alignment.center,
              child: CupertinoActivityIndicator(),
            );
          },
        ),
      ),
      bottomNavigationBar: Container(
        // height: 100,
        padding: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
        child: buttonSelect(
          'Add Courier',
          () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CourierCreateScreen(),
              ),
            ).then((value) =>
                BlocProvider.of<CourierBloc>(context).add(GetCourierList()));
          },
        ),
      ),
    );
  }
}
