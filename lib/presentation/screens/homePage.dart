import 'package:bedo_shop_admin/presentation/screens/menu_bedo.dart';
import 'package:bedo_shop_admin/presentation/screens/orders_screen.dart';
import 'package:bedo_shop_admin/presentation/screens/prouct_pages/products_page.dart';
import 'package:bedo_shop_admin/presentation/screens/status_report_screen.dart';
import 'package:bedo_shop_admin/presentation/widgets/bottom_navigation.dart';
import 'package:bedo_shop_admin/presentation/widgets/comments_page.dart';
import 'package:bedo_shop_admin/presentation/widgets/full_report_page.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  static const routeName = 'homePage';
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final PageController controller = PageController();
  int currentPage = 0;
  final _nvkey = GlobalKey<CustomBottomNavigationBarState>();
  final _pagesList = [
    OrdersScreen(),
    StatusReportScreen(),
    ProductsPage(),
    CommentsPage(),
    SettingsMain(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: controller,
        // onPageChanged: ,
        onPageChanged: (pageNum) {
          // print(pageNum);
          _nvkey.currentState.indexChange(pageNum);
          // setState(() {
          // currentPage = pageNum;
          // });
        },
        children: _pagesList,
        allowImplicitScrolling: false,
        pageSnapping: true,
      ),
      // child: Text('nig'),
      bottomNavigationBar: CustomBottomNavigationBar(
        // changeTo: 3,
        key: _nvkey,
        onChanege: (idx) {
          if (idx < _pagesList.length) {
            controller.animateToPage(idx,
                duration: Duration(milliseconds: 300), curve: Curves.easeIn);
          }
        },
      ),
    );
    // OrdersScreen();
  }
}
