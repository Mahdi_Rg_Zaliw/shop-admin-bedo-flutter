import 'package:bedo_shop_admin/presentation/widgets/round_botton.dart';
import 'package:flutter/material.dart';

class RbShowCase extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Buttons.red(
              child: Text('Buttons.red'),
              onPressed: () {},
            ),
            SizedBox(
              height: 7,
            ),
            Buttons.grey(
              child: Text('Buttons.grey'),
              onPressed: () {},
            ),
            SizedBox(
              height: 7,
            ),
            Buttons.redOutLine(
              child: Text('Buttons.redOutLine'),
              onPressed: () {
                print('');
              },
            ),
            SizedBox(
              height: 7,
            ),
            RaisedButton(
              onPressed: () {},
              child: Text('RaisedButton'),
            )
          ],
        ),
      ),
    );
  }
}
