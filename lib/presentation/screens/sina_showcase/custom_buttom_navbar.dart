import 'package:bedo_shop_admin/presentation/widgets/bottom_navigation.dart';
import 'package:flutter/material.dart';

class CustomBottomNavBarShowCase extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: CustomBottomNavigationBar(),
    );
  }
}
