import 'package:bedo_shop_admin/presentation/widgets/products_cart.dart';
import 'package:flutter/material.dart';

class ProductsCartShowCase extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 90,
              ),
              ProductsCart.menu(
                imgUrl: 'https://picsum.photos/251',
                // isMeal: false,
                onPressed: () {},
                name:
                    'we are who we are we are who we are we are who we are we are who we are we are who we are we are who we are we are who we are we are who we are we are who we are we are who we are we are who we are we are who we are ',
                shortDescribtion: '8 meals in menu',
                longDescribrion: 'man man man himan man man hi man man man hi',
              ),
              ProductsCart.menu(
                imgUrl: 'https://picsum.photos/252',
                onPressed: () {},
                name: 'we are who we are we are who we are we are who we are',
                shortDescribtion:
                    '8 meals in menu 8 meals in menu 8 meals in menu 8 meals in menu',
                longDescribrion:
                    'pizza salam , pizza hot , pizza super man , and more',
              ),
              ProductsCart.meal(
                imgUrl: 'https://picsum.photos/253',
                onPressedActions: () {},
                name: 'steam punk coffe',
                shortDescribtion: 'medium , small',
                price: 200000000,
                stock: 100,
              ),
              ProductsCart.meal(
                imgUrl: 'https://picsum.photos/254',
                onPressedActions: () {},
                name: 'pizza hot',
                shortDescribtion: 'medium , small',
                price: 1000000,
                offPrice: 5000000,
                stock: 1000,
              ),
              ProductsCart.meal(
                imgUrl: 'https://picsum.photos/255',
                onPressedActions: () {},
                name: 'pizza hot',
                shortDescribtion: 'medium , small',
                price: 100,
                offPrice: 5000000,
                stock: 1000,
              ),
              SizedBox(
                height: 90,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
