import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/modal_add_discount.dart';
import 'package:bedo_shop_admin/presentation/screens/sina_showcase/products_list_showcase.dart';
import 'package:bedo_shop_admin/presentation/screens/sina_showcase/rb.dart';
import 'package:flutter/material.dart';

import 'custom_buttom_navbar.dart';

class SinasMenu extends StatelessWidget {
  // static const routeName = '/';
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: whiteAllButtonsColor,
      child: Scaffold(
        body: Center(
          child: IntrinsicWidth(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => CustomBottomNavBarShowCase(),
                        ),
                      );
                    },
                    child: Text('show bottom navigation bar -'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => ProductsCartShowCase(),
                        ),
                      );
                    },
                    child: Text('show products list -'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => RbShowCase(),
                        ),
                      );
                    },
                    child: Text('show rounded buttons -'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => ModalAddDiscount(),
                        ),
                      );
                    },
                    child: Text('show discount modal -'),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
