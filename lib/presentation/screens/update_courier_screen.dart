import 'package:bedo_shop_admin/bloc/courier_bloc/courier_bloc.dart';
import 'package:bedo_shop_admin/bloc/courier_bloc/courier_event.dart';
import 'package:bedo_shop_admin/bloc/courier_bloc/courier_state.dart';
import 'package:bedo_shop_admin/config/config.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/widgets/custom_text_field.dart';
import 'package:bedo_shop_admin/presentation/widgets/radio_button_bedo.dart';
import 'package:bedo_shop_admin/presentation/widgets/pick_picture_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:connectivity/connectivity.dart';
import 'package:bedo_shop_admin/presentation/screens/courier_list_screen.dart';
import 'package:bedo_shop_admin/presentation/screens/vanils_showcases/upload_image_test.dart';
import 'dart:io';
import 'package:graphql/client.dart';

class UpdateCourierScreen extends StatefulWidget {
  final String id;
  final String fullName;
  final String email;
  final String phoneNumber;
  final String gender;
  final String imageUrl;

  UpdateCourierScreen({
    this.email,
    this.fullName,
    this.id,
    this.phoneNumber,
    this.gender,
    this.imageUrl,
  });

  @override
  _UpdateCourierScreenState createState() => _UpdateCourierScreenState();
}

class _UpdateCourierScreenState extends State<UpdateCourierScreen> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  Map<String, dynamic> formData = {
    "editFullName": "",
    "editPhoneNumber": "",
    "editEmail": "",
  };
  File uploadImageFile;
  String gender = '';
  String imageUrl = '';
  bool loadingImage = false;
  String errorMessage = '';

  Future courierUpdatedSuccessModal({
    BuildContext context,
    String fullName,
    String email,
    String phoneNumber,
    String gender,
    String image,
  }) {
    return showModalBottomSheet(
      isScrollControlled: false,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(24),
          topRight: Radius.circular(24),
        ),
      ),
      builder: (BuildContext context) {
        return Container(
          padding: EdgeInsets.all(33),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 2,
                child: SingleChildScrollView(
                  child: Text(
                    'Your Full Name From \"${widget.fullName}\" To \"$fullName\" \n And Your Email From \"${widget.email}\" To \"$email\" \n And Your Phone Number From \"${widget.phoneNumber}\" To \"$phoneNumber\" And Your Gender From \"${widget.gender}\" To \"$gender\" \n And Image Profile \"$image\" \n Successfully Changed',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      height: 1.5,
                      color: textColor,
                      fontSize: 17,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: RaisedButton(
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.pop(context);

                    BlocProvider.of<CourierBloc>(context).add(GetCourierList());
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(
                    //     builder: (context) => CourierListScreen(),
                    //   ),
                    // );
                  },
                  child: Text('OK'),
                ),
              ),
            ],
          ),
        );
      },
      context: context,
    );
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      gender = widget.gender;
      formData['editFullName'] = widget.fullName;
      formData['editPhoneNumber'] = widget.phoneNumber;
      formData['editEmail'] = widget.email;
      imageUrl = widget.imageUrl;
      errorMessage = '';
    });
  }

  Future _uploadImg(img) async {
    setState(() {
      loadingImage = true;
    });
    QueryResult rawData;
    rawData = await upLoadFileMutation(
      file: img,
    );
    print(rawData);

    if (rawData.hasException) {
      print(rawData.exception);

      print('has error');
      return;
    } else if (rawData.data == null) {
      print('null data');
      return;
    } else {
      String url;

      url = rawData.data['uploadFile']['url'];
      print(url);
      setState(() {
        imageUrl = uri.replaceAll('graphql', '') + url;
        loadingImage = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text(
          'Edit Courier',
          style: grey14medium,
        ),
        centerTitle: true,
      ),
      body: Container(
        width: width,
        height: height,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8),
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: PickPictureButton(
                        previewImg: imageUrl == null ? '' : imageUrl,
                        pickerType: PickerType.edit,
                        onChanged: (File file) {
                          setState(() {
                            uploadImageFile = file;
                          });
                        },
                      ),
                    ),
                    loadingImage == true
                        ? Align(
                            alignment: Alignment.center,
                            child: Container(
                              alignment: Alignment.center,
                              width: 80,
                              height: 80,
                              child: Container(
                                width: 30,
                                height: 30,
                                child: CircularProgressIndicator(
                                  strokeWidth: 3.0,
                                ),
                              ),
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
              SizedBox(
                height: height * 0.05,
              ),
              Form(
                key: formKey,
                child: Column(
                  children: [
                    CustomTextField(
                      initialValue: formData['editFullName'],
                      validator: (String value) {
                        if (value.trim().isEmpty) {
                          return 'Full Name Cannot Be Empty !';
                        }
                        return null;
                      },
                      title: S.of(context).courierFullName,
                      hintText: S.of(context).enterTheCourierName,
                      // controller: formData['editFullName'],
                      onSaved: (String value) {
                        print(value);
                        setState(() {
                          formData['editFullName'] = value;
                        });
                      },
                    ),
                    const SizedBox(height: 10),
                    CustomTextField(
                      initialValue: formData['editPhoneNumber'],

                      validator: (String value) {
                        if (value.trim().isEmpty) {
                          return 'Phone Number Cannot Be Empty !';
                        }
                        return null;
                      },
                      title: S.of(context).courierPhoneNumber,
                      hintText: S.of(context).enterTheCourierPhoneNumber,
                      keyboardType: TextInputType.phone,
                      // controller: formData['editPhoneNumber'],
                      onSaved: (String value) {
                        print(value);
                        setState(() {
                          formData['editPhoneNumber'] = value;
                        });
                      },
                    ),
                    const SizedBox(height: 10),
                    CustomTextField(
                      initialValue: formData['editEmail'],

                      validator: (String value) {
                        if (value.trim().isEmpty) {
                          return 'Email Cannot Be Empty !';
                        } else if (!value.isValidEmail()) {
                          return 'Must Be An Email Format !';
                        }
                        return null;
                      },
                      title: S.of(context).courierEmailAddress,
                      hintText: S.of(context).enterTheCourierEmailAddress,
                      // controller: formData['editEmail'],
                      onSaved: (String value) {
                        print(value);
                        setState(() {
                          formData['editEmail'] = value;
                        });
                      },
                    ),
                    const SizedBox(height: 10),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: width * 0.06),
                          child: Text(
                            "Courier Gender",
                            style: grey12regular,
                          ),
                        ),
                        RadioButtonBedo(
                          onChanged: (String value) {
                            setState(() {
                              gender = value;
                            });
                          },
                          direction: Axis.horizontal,
                          radioButtonList: [
                            S.of(context).male,
                            S.of(context).female
                          ],
                          radioValueIndex: widget.gender == "MALE" ? 0 : 1,
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                  ],
                ),
              ),
              const SizedBox(height: 10),
              Container(
                width: width,
                padding: EdgeInsets.symmetric(horizontal: width * 0.06),
                height: 55,
                child: BlocConsumer<CourierBloc, CourierState>(
                  listener: (context, state) {
                    if (state is CourierLoading) {
                      print('Loading ....');
                    }
                    if (state is CourierExceptionState) {
                      print('Error');
                      setState(() {
                        errorMessage = state.clientErrors;
                      });
                    }
                    if (state is UpdateCourierSuccess) {
                      print('Success');
                      setState(() {
                        errorMessage = '';
                      });
                      courierUpdatedSuccessModal(
                        context: context,
                        fullName: formData['editFullName'],
                        phoneNumber: formData['editPhoneNumber'],
                        email: formData['editEmail'],
                        gender: gender,
                        image: imageUrl,
                      );
                    }
                  },
                  builder: (context, state) {
                    return RaisedButton(
                      child: Text('Edit Courier'),
                      onPressed: loadingImage == true
                          ? null
                          : () async {
                              if (uploadImageFile != null) {
                                await _uploadImg(uploadImageFile);
                              }
                              formKey.currentState.save();
                              UpdateCourierEvent updateCourierEvent =
                                  UpdateCourierEvent(
                                id: widget.id,
                                updateCourierData: {
                                  "fullName": formData['editFullName'],
                                  "phoneNumber": formData['editPhoneNumber'],
                                  "email": formData['editEmail'],
                                  "gender": (gender != null)
                                      ? gender.toUpperCase()
                                      : 'MALE',
                                  "profileImageUrl": imageUrl,
                                },
                              );
                              var connectivityResult =
                                  await (Connectivity().checkConnectivity());
                              if (connectivityResult ==
                                      ConnectivityResult.mobile ||
                                  connectivityResult ==
                                      ConnectivityResult.wifi) {
                                if (formKey.currentState.validate()) {
                                  setState(() {
                                    errorMessage = '';
                                  });

                                  context
                                      .read<CourierBloc>()
                                      .add(updateCourierEvent);
                                } else {
                                  setState(() {
                                    errorMessage = 'Input Invalid !';
                                  });
                                }
                              } else {
                                setState(() {
                                  errorMessage = 'Network Error';
                                });
                              }
                            },
                      elevation: 0,
                      highlightElevation: 1,
                    );
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8),
                child: Text(
                  errorMessage,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: red),
                ),
              ),
              const SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }
}
