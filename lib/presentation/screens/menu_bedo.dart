import 'package:bedo_shop_admin/bloc/shop_bloc/shop_bloc.dart';
import 'package:bedo_shop_admin/design/icons_f.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/screens/add_discount_screen.dart';
import 'package:bedo_shop_admin/presentation/screens/edit_shop.dart';
import 'package:bedo_shop_admin/presentation/screens/prouct_pages/add_new_menu_item_page.dart';
import 'package:bedo_shop_admin/presentation/screens/restaurant_wallet_transaction_screen.dart';
import 'package:bedo_shop_admin/presentation/screens/settings_pages/settings.dart';
import 'package:bedo_shop_admin/presentation/screens/support_pages.dart';
import 'package:bedo_shop_admin/presentation/widgets/comment_widggets/custom_avatar.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql/client.dart';

import 'add_courier_screen.dart';

class SettingsMain extends StatefulWidget {
  static const routeName = 'SettingsMain';

  final String restaurantName;
  final String cityPlace;

  const SettingsMain({
    this.restaurantName = 'Divella Bistro Restaurant',
    this.cityPlace = 'Fatih,istanbul',
  });

  @override
  _SettingsMainState createState() => _SettingsMainState();
}

class _SettingsMainState extends State<SettingsMain> {
  @override
  void initState() {
    super.initState();
    // Bloc visited
    BlocProvider.of<ShopBloc>(context).add(EditShopVisited());
  }

  // @override
  // void dispose() {
  //   super.dispose();
  //   BlocProvider.of<ShopBloc>(context).add(NavOut());
  //   printWhiteBg('outttttttt');
  // }

  @protected
  @mustCallSuper
  void deactivate() {
    super.deactivate();
    BlocProvider.of<ShopBloc>(context).add(NavOut());
    printWhiteBg('out and reset');
  }

  Widget optionMenuRow(
    IconData icon,
    String title,
    double iconSize,
    VoidCallback onPressed,
  ) {
    return FlatButton(
      splashColor: lightGreyBg,
      highlightColor: highlightWhite,
      onPressed: onPressed,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 24),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: highlightWhite, width: 1),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Icon(
                  icon,
                  size: iconSize,
                  color: hintTextColor,
                ),
                SizedBox(width: icon == Iconsf.i24_hamburger_menu ? 20 : 10),
                Text(
                  title,
                  style: grey14medium,
                )
              ],
            ),
            Icon(
              Icons.arrow_forward_ios,
              // Iconsf.i29_icon_forward,
              color: colorIcon,
              size: 16,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // var phoneSize = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            BlocBuilder<ShopBloc, ShopState>(
              builder: (context, state) {
                if (state is ShopEditableDataLoaded) {
                  printGreenBg(state.data);
                  final d = state.data;
                  return Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 70, bottom: 20),
                        child: CustomAvatar(
                          imageUrl: d['logoUrl'] ?? '',
                          radius: 38,
                        ),
                      ),
                      Text(
                        d['name'] ?? 'no name provided for shop',
                        style: black16semibold,
                      ),
                      SizedBox(height: 10),
                      Text(
                        d['address'] ?? 'no address provided shop address',
                        style: grey14medium,
                      ),
                    ],
                  );
                }
                return Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 70, bottom: 20),
                      child: CustomAvatar(
                        imageUrl: '',
                        radius: 38,
                      ),
                    ),
                    Text(
                      'shop name',
                      style: black16semibold,
                    ),
                    SizedBox(height: 10),
                    Text(
                      'shop address',
                      style: grey14medium,
                    ),
                  ],
                );
              },
            ),
            //* //////////////////////
            //
            Padding(
              padding: EdgeInsets.symmetric(vertical: 30, horizontal: 24),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    S.of(context).restaurantWallet,
                    style: black14medium,
                  ),
                  InkWell(
                    borderRadius: BorderRadius.circular(7),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) =>
                            RestaurantWalletTransactionScreen(),
                      ));
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(12.0).copyWith(right: 6),
                      child: Row(
                        children: [
                          Text(
                            S.of(context).detail,
                            style: grey14medium,
                          ),
                          SizedBox(width: 8),
                          Icon(
                            // Icons.keyboard_arrow_down_rounded,
                            Icons.arrow_forward_ios,
                            // Iconsf.i29_icon_forward,
                            color: colorIcon,
                            size: 14,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  BlocBuilder<ShopBloc, ShopState>(
                    builder: (context, state) {
                      num walletPrice = 0;
                      if (state is ShopEditableDataLoaded) {
                        walletPrice = state.data[
                                'getTotalTransactionsAmountByShopAdmin'] ??
                            0;
                        printWhiteBg(walletPrice);
                      }
                      return Text(
                        ' $walletPrice',
                        style: TextStyle(
                          fontSize: 34,
                          fontWeight: FontWeight.w500,
                          color: walletPrice > 0 ? primaryMaterialColor : red,
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
            SizedBox(height: 30),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: Column(
                children: [
                  optionMenuRow(
                    Iconsf.i37_peincil,
                    S.of(context).editInformation,
                    18,
                    () {
                      Navigator.of(context)
                          .pushNamed(EditShopInformation.routeName);
                    },
                  ),
                  optionMenuRow(
                    Iconsf.i23_fast_food,
                    S.of(context).addProducts,
                    18,
                    () {},
                  ),
                  optionMenuRow(
                    Iconsf.i19_discount,
                    S.of(context).addDiscount,
                    18,
                    () {
                      Navigator.of(context)
                          .pushNamed(AddDiscountScreen.routeName);
                    },
                  ),
                  optionMenuRow(
                    Iconsf.i24_hamburger_menu,
                    S.of(context).addMenus,
                    18,
                    () {
                      Navigator.of(context).pushNamed(AddNewMenuItem.routeName);
                    },
                  ),
                  optionMenuRow(
                    Iconsf.i36_paper_plane,
                    S.of(context).addCourier,
                    18,
                    () {
                      Navigator.of(context)
                          .pushNamed(CourierCreateScreen.routeName);
                    },
                  ),
                  optionMenuRow(
                    Iconsf.i32_logo,
                    S.of(context).supportTeam,
                    18,
                    () {
                      Navigator.of(context).pushNamed(SupportPages.routeName);
                    },
                  ),
                  optionMenuRow(
                    Iconsf.i43_settings_outline_2,
                    S.of(context).settings,
                    18,
                    () {
                      Navigator.of(context).pushNamed(Settings.routeName);
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
