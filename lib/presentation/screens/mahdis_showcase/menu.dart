import 'package:bedo_shop_admin/presentation/screens/courier_list_screen.dart';
import 'package:bedo_shop_admin/presentation/screens/delivery_map.dart';
import 'package:bedo_shop_admin/presentation/screens/add_shop.dart';
import 'package:bedo_shop_admin/presentation/screens/menu_bedo.dart';
import 'package:bedo_shop_admin/presentation/widgets/circular_number_pick.dart';
import 'package:bedo_shop_admin/presentation/widgets/clock_picker_mode.dart';
import 'package:bedo_shop_admin/presentation/widgets/horizontal_list_image.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/custom_modal_bottom_sheet.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/modal_home_page.dart';
import 'package:bedo_shop_admin/presentation/screens/select_courier_screen.dart';
import 'package:bedo_shop_admin/presentation/screens/orders_screen.dart';
import 'package:bedo_shop_admin/presentation/screens/status_report_screen.dart';
import 'package:bedo_shop_admin/presentation/widgets/carousel_slider_bedo.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/date_promotion_period.dart';
import 'package:bedo_shop_admin/presentation/widgets/pick_picture_button.dart';
import 'package:bedo_shop_admin/presentation/screens/update_courier_screen.dart';

import 'package:flutter/material.dart';

import '../../../design/theme.dart';
import '../../widgets/radio_button_bedo.dart';

class MehdisMenu extends StatelessWidget {
  // static const routeName = '/';
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: whiteAllButtonsColor,
      child: Scaffold(
        body: ListView(
          // crossAxisAlignment: CrossAxisAlignment.stretch,
          // mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: RaisedButton(
                child: Text('show number time picker -'),
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => Scaffold(
                        body: Center(
                          child: RaisedButton(
                            child: Text('show number picker'),
                            onPressed: () {
                              customModalSheetWithNavigation(
                                  child: ModalHomePage(
                                    childBuilder: (n) {
                                      return CircularNumberPicker(
                                        onSelectedTimeChange: (dur) {
                                          print(
                                              '${dur.hour.toString().padLeft(2, '0')}  HOUR(S) : ${dur.minute.toString().padLeft(2, '0')}  MINUTE(S)  was selected');
                                        },
                                      );
                                    },
                                    navKey: GlobalKey<NavigatorState>(),
                                  ),
                                  context: context,
                                  height: 350 /** 500 big 400 small */);
                            },
                          ),
                        ),
                      ),
                    ),
                  );
                },
                // child: Text('number picker'),
                // builder: (context) => StatusReportScreen(),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => DeliveryMap(),
                    ),
                  );
                },
                child: Text('delivery map -'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => SettingsMain(),
                    ),
                  );
                },
                child: Text('settings menu of bedo -'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => SelectCourierScreen(),
                    ),
                  );
                },
                child: Text('select courier screen -'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => Scaffold(
                        body: Center(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              RadioButtonBedo(
                                direction: Axis.horizontal,
                                radioButtonList: ['male', 'female'],
                                radioValueIndex: 0,
                                onChanged: (String value) {
                                  print(value);
                                },
                              ),
                              RadioButtonBedo(
                                direction: Axis.vertical,
                                radioButtonList: ['10', '20', '30'],
                                radioValueIndex: 0,
                                onChanged: (String value) {
                                  print(value);
                                },
                                // iconColor: textColor,
                                color: gold,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
                child: Text('Radio button -'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(3),
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => Scaffold(body: CarouselSliderBedo()),
                  ));
                },
                child: Text('Carousel slider -'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => StatusReportScreen(),
                    ),
                  );
                },
                child: Text('Status report screen -'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => Scaffold(
                        body: Center(
                          child: Builder(
                            builder: (ctx) => RaisedButton(
                              onPressed: () {
                                datePromotionPeriodModal(context: ctx);
                              },
                              child: Text('show modal date picker'),
                            ),
                          ),
                        ),
                      ),
                    ),
                  );
                },
                child: Text('date picker -'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => OrdersScreen(),
                    ),
                  );
                },
                child: Text('orders screen -'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => CustomTimePickerCupertino(),
                    ),
                  );
                },
                child: Text('Clock Picker -'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => Scaffold(
                        body: Center(
                          child: PickPictureButton(
                              onChanged: print, pickerType: PickerType.remove),
                        ),
                      ),
                    ),
                  );
                },
                child: Text('Image picker -'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => AddShopPage(),
                    ),
                  );
                },
                child: Text('edit Information -'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => CourierListScreen(),
                    ),
                  );
                },
                child: Text('courier list screen -'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => UpdateCourierScreen(),
                    ),
                  );
                },
                child: Text('courier edit screen -'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) =>
                          Scaffold(body: HorizontalListImage()),
                    ),
                  );
                },
                child: Text('horizontal list screen -'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
