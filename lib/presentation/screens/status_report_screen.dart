import 'package:bedo_shop_admin/bloc/reports_bloc/reports_bloc.dart';
import 'package:bedo_shop_admin/bloc/reports_bloc/reports_event.dart';
import 'package:bedo_shop_admin/bloc/reports_bloc/reports_state.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/presentation/widgets/bottom_navigation.dart';
import 'package:bedo_shop_admin/presentation/widgets/filtered_list.dart';
import 'package:bedo_shop_admin/presentation/widgets/full_report_page.dart';
import 'package:bedo_shop_admin/presentation/widgets/fund_Status.dart';
import 'package:bedo_shop_admin/presentation/widgets/fund_status_charts.dart';
import 'package:bedo_shop_admin/presentation/widgets/modal_widgets/time_range_modal.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class StatusReportScreen extends StatefulWidget {
  const StatusReportScreen({Key key}) : super(key: key);

  @override
  _StatusReportScreenState createState() => _StatusReportScreenState();
}

class _StatusReportScreenState extends State<StatusReportScreen> {
  bool showCharts = false;
  bool allTimes = false;
  String selectedTimeRange;
  int selectedTimeRangeIndex = 0;
  DateTime startDate = DateTime.now();
  DateTime endDate = DateTime.now();

  @override
  void initState() {
    loadData();
    super.initState();
  }

  void loadData() {
    BlocProvider.of<ReportsBloc>(context)
        .add(LoadOrdersStatisticsForChartsEvent(startDate, endDate, allTimes));
    BlocProvider.of<ReportsBloc>(context)
        .add(LoadOrdersStatisticsEvent(startDate, endDate, allTimes));
    BlocProvider.of<ReportsBloc>(context).add(LoadReportsEvent());
  }

  Widget fundStatusAppBar(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        GestureDetector(
          onTap: () {},
          child: Text(
            S.of(context).totalAmount,
            style: grey14semiBold,
          ),
        ),
        GestureDetector(
          onTap: () {
            timeRangeModal(
                context: context,
                onChanged: (DateTime date, String value, int index) {
                  if (date == null) {
                    setState(() {
                      allTimes = true;
                    });
                  } else {
                    setState(() {
                      startDate = date;
                      allTimes = false;
                    });
                  }
                  setState(() {
                    selectedTimeRange = value;
                    selectedTimeRangeIndex = index;
                  });
                  loadData();
                },
                index: selectedTimeRangeIndex);
          },
          child: Row(
            children: [
              Text(
                selectedTimeRange,
                style: grey14semiBold,
              ),
              Icon(Icons.expand_more),
            ],
          ),
        ),
      ],
    );
  }

  Widget fundStatusAppBarBottom(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            fullReportAppBarBottom(context),
            Row(
              children: [
                IconButton(
                  icon: Icon(
                    FontAwesomeIcons.solidChartBar,
                    color: showCharts ? Colors.black : titleGrey,
                  ),
                  onPressed: () {
                    setState(() {
                      showCharts = true;
                    });
                  },
                  splashRadius: 25,
                ),
                IconButton(
                  icon: Icon(
                    FontAwesomeIcons.listUl,
                    color: showCharts ? titleGrey : Colors.black,
                  ),
                  onPressed: () {
                    setState(() {
                      showCharts = false;
                    });
                  },
                  splashRadius: 25,
                  padding: EdgeInsets.only(right: 10),
                ),
              ],
            )
          ],
        ),
      ],
    );
  }

  Widget fullReportAppBarBottom(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
          child: Container(
            width: 200,
            height: 30,
            child: BlocBuilder<ReportsBloc, ReportsState>(
              builder: (context, state) => state is ReportsLoaded
                  ? Text(
                      '${state.totalOrdersAmount?.toStringAsPrecision(2)} \$',
                      style: black24medium,
                    )
                  : Text('_ _ _'),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    if (selectedTimeRange == null) selectedTimeRange = S.of(context).today;
    return FilteredList.fix(
      optionShouldNotifyList: [false, true],
      optionsList: [S.of(context).fundStatus, S.of(context).fullReport],
      pagesList: [
        showCharts ? FundStatusCharts() : FundStatusScreen(),
        FullReportPage(),
      ],
      totalAmount: 138.00,
      appBarTitlesList: [
        fundStatusAppBar(context),
        Row(
          children: [
            Text(
              S.of(context).totalAmount + ':',
              style: grey14semiBold,
            ),
          ],
        ),
      ],
      appBarBottomList: [
        fundStatusAppBarBottom(context),
        fullReportAppBarBottom(context),
      ],
      // bottomNavigationBar: CustomBottomNavigationBar(),
      // topTitlesList: [],
      // bottomTitleList: [],
    );
  }
}
