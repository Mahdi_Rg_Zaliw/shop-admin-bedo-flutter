import 'package:bedo_shop_admin/bloc/auth_bloc/auth_bloc.dart';
import 'package:bedo_shop_admin/bloc/auth_bloc/auth_states.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/presentation/screens/verification_code_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';

class VerificationBuilder extends StatelessWidget {
  final String phonNumber;
  final String testcode;

  const VerificationBuilder({Key key, this.phonNumber, this.testcode})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthBloc, AuthState>(
      listener: (context, state) {
        if (state is ReSendingCodeVerification) {
          String c = state.code;

          showToastWidget(
            Container(
              padding: EdgeInsets.symmetric(horizontal: 18.0),
              margin: EdgeInsets.symmetric(horizontal: 50.0),
              decoration: ShapeDecoration(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
                color: primaryMaterialColor,
              ),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: Text(
                      'sms code sent was $c ',
                      style: white14medium,
                    ),
                  ),
                  // IconButton(
                  //   onPressed: () {

                  //   },
                  //   icon: Icon(
                  //     Icons.add_circle_outline_outlined,
                  //     color: Colors.white,
                  //   ),
                  // ),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
              ),
            ),
            context: context,
            isIgnoring: false,
            alignment: Alignment.topCenter,
            position: StyledToastPosition.top,
            duration: Duration(seconds: 4),
          );
        }
      },
      builder: (context, state) {
        String c = '';
        if (state is SendingCodeVerification) {
          c = state.code;
          return VerificationCodeScreen(
            phoneNumber: phonNumber,
            signUp: true,
          );
        }
        if (state is ReSendingCodeVerification) {
          return VerificationCodeScreen(
            phoneNumber: phonNumber,
            signUp: true,
          );
        }
        return Stack(
          children: [
            Column(
              children: [
                CupertinoActivityIndicator(
                  radius: 12,
                ),
                SizedBox(
                  height: 12,
                ),
                Text(
                  'Loading ...',
                  style: black14medium,
                )
              ],
            ),
            AbsorbPointer(
              absorbing: true,
              child: VerificationCodeScreen(
                phoneNumber: phonNumber,
                signUp: true,
              ),
            )
          ],
        );
      },
    );
  }
}
