import 'package:bedo_shop_admin/presentation/widgets/toggle_switch.dart';
import 'package:flutter/material.dart';

class SwitchSC extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ToggleSwitch(
          initialValue: true,
          onToggle: (v) {
            print('${(!v) ? 'switch on' : 'switch off'}');
          },
        ),
      ),
    );
  }
}
