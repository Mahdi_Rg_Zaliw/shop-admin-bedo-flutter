import 'package:bedo_shop_admin/presentation/widgets/verification_code_widget.dart';
import 'package:flutter/material.dart';

class VerficationCodeShowcase extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: VerificationCodeWidget((s) {}, () {}),
        ),
      ),
    );
  }
}
