import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/presentation/screens/add_courier_screen.dart';
import 'package:bedo_shop_admin/presentation/screens/add_discount_screen.dart';
import 'package:bedo_shop_admin/presentation/screens/login.dart';
import 'package:bedo_shop_admin/presentation/screens/mohammads_show_case/button_grid_show_case.dart';
import 'package:bedo_shop_admin/presentation/screens/mohammads_show_case/show_case_order_widgets.dart';
import 'package:bedo_shop_admin/presentation/screens/mohammads_show_case/verfication_code_show_case.dart';
import 'package:bedo_shop_admin/presentation/screens/orders_screen.dart';
import 'package:bedo_shop_admin/presentation/screens/status_report_screen.dart';
import 'package:bedo_shop_admin/presentation/screens/vanils_showcases/report_screen.dart';
import 'package:flutter/material.dart';

import '../complete_profile_screen.dart';
import '../restaurant_wallet_transaction_screen.dart';
import 'checkboxgird_showcase.dart';
import 'custom_switch.dart';

class MohammadsMenu extends StatelessWidget {
  // static const routeName = '/';
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: whiteAllButtonsColor,
      child: Scaffold(
        body: Center(
          child: IntrinsicWidth(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => ShowCaseOrderWidgets(),
                          ),
                        );
                      },
                      child: Text('show orders and discount widgets'),
                    ),
                    //
                    //
                    //
                    //
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => BtnGridShowCase(),
                          ),
                        );
                      },
                      child: Text('button grid show case'),
                    ),
                    // FilteredListWithOrderCard
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) =>
                                VerficationCodeShowcase(), //CehckBoxGridSC
                          ),
                        );
                      },
                      child: Text('verfication code'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => CehckBoxGridSC(),
                          ),
                        );
                      },
                      child: Text('checkbox grid'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => SwitchSC(),
                          ),
                        );
                      },
                      child: Text('custom switch'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => Login(),
                          ),
                        );
                      },
                      child: Text('Login Screen'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => CompleteProfileScreen(),
                          ),
                        );
                      },
                      child: Text('Create Profile Screen'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => CourierCreateScreen(),
                          ),
                        );
                      },
                      child: Text('Add Courier Screen'),
                      //       builder: (context) => RestaurantWalletTransactionScreen(),
                      //     ),
                      //   );
                      // },
                      // child: Text('restaurant wallet transaction Screen'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) =>
                                RestaurantWalletTransactionScreen(),
                          ),
                        );
                      },
                      child: Text('restaurant wallet transaction Screen'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => AddDiscountScreen(),
                          ),
                        );
                      },
                      child: Text('Add Discount Screen'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => OrdersScreen(),
                          ),
                        );
                      },
                      child: Text('Pending Orders'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => ReportScreen(),
                          ),
                        );
                      },
                      child: Text('Report screen'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => StatusReportScreen(),
                          ),
                        );
                      },
                      child: Text('Status report screen -'),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
