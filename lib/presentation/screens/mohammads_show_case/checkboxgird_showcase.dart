import 'package:bedo_shop_admin/presentation/widgets/checkbox_grid.dart';
import 'package:flutter/material.dart';

class CehckBoxGridSC extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              CheckboxGrid(
                options: [
                  'man',
                  'woman',
                  'children',
                  'older',
                  'super man long text so long man yeah',
                  'batman'
                ],
                crossAxisCount: 3,
                width: double.infinity,
              ),
              CheckboxGrid(
                options: ['man', 'woman', 'children', 'older'],
                crossAxisCount: 4,
                width: double.infinity,
              ),
              CheckboxGrid(
                options: ['man', 'woman', 'children', 'older'],
                crossAxisCount: 2,
              ),
              CheckboxGrid(
                options: ['man', 'woman', 'children', 'older'],
                crossAxisCount: 1,
                height: 400,
              ),
              // CehckBoxGridSC(o)
              // CheckboxGrid(options:['man', 'woman', 'children', 'older'], 3),
              // CheckboxGrid(options:['man', 'woman', 'children', 'older'], 2),
              // IntrinsicHeight(
              //     child: CheckboxGrid(['man', 'woman', 'children', 'older'], 4)),
            ],
          ),
        ),
      ),
    );
  }
}
