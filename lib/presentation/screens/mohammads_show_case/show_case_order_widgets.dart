import 'package:bedo_shop_admin/presentation/widgets/order_and_discount_widget.dart';
import 'package:flutter/material.dart';

class ShowCaseOrderWidgets extends StatelessWidget {
  const ShowCaseOrderWidgets({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
            child: SingleChildScrollView(
          child: Column(
            children: [
              //
              //
              //
              //
              //
              SizedBox(
                height: 100,
              ),
              OrderAndDiscountWidget.discount(
                date: DateTime(2000),
                title: '#D-3568',
                percent: 50,
                isDone: true,
                id: "134",
              ),
              OrderAndDiscountWidget.discount(
                date: DateTime(2000),
                title: '#D-3568',
                percent: 50,
                isDone: false,
                id: "134",
              ),
              OrderAndDiscountWidget.discount(
                date: DateTime(2000),
                title: '#D-3568',
                percent: 50,
                isDone: true,
                id: "134",
              ),
              OrderAndDiscountWidget.discount(
                date: DateTime(2000),
                title: '#D-3568',
                percent: 50,
                isDone: false,
                id: "134",
              ),
              SizedBox(
                height: 100,
              ),
              //
              //
              //
              //
              //
              OrderAndDiscountWidget.order(
                  isExpanded: true,
                  expandable: true,
                  date: DateTime(2000),
                  title: '#D-61634',
                  isDone: false,
                  orderPrice: 232),
              OrderAndDiscountWidget.order(
                  isExpanded: true,
                  expandable: true,
                  date: DateTime(2000),
                  title: '#D-61634',
                  isDone: true,
                  orderPrice: 232),
              OrderAndDiscountWidget.order(
                  isExpanded: false,
                  expandable: true,
                  date: DateTime(2000),
                  title: '#D-61634',
                  isDone: false,
                  orderPrice: 232),
              OrderAndDiscountWidget.order(
                  isExpanded: false,
                  expandable: true,
                  date: DateTime(2000),
                  title: '#D-61634',
                  isDone: true,
                  orderPrice: 232),
              //
              //
              //
              //
              //
              //
              SizedBox(
                height: 100,
              ),
              OrderAndDiscountWidget.order(
                  isDone: true,
                  date: DateTime(2000),
                  title: '#D-95456',
                  orderPrice: 232),
              OrderAndDiscountWidget.order(
                  isDone: false,
                  date: DateTime(2000),
                  title: '#D-95456',
                  orderPrice: 232),
              OrderAndDiscountWidget.order(
                  isDone: true,
                  date: DateTime(2000),
                  title: '#D-95456',
                  orderPrice: 232),
              OrderAndDiscountWidget.order(
                  isDone: false,
                  date: DateTime(2000),
                  title: '#D-95456',
                  orderPrice: 232),
              SizedBox(
                height: 100,
              ),
              //
              //
              //
              //
              //
              OrderAndDiscountWidget.transaction(
                date: DateTime(2019),
                title: '#D-51574',
                transactionPrice: 25651.23,
                description:
                    'hello man so how are you man what is happninig today man',
              ),
              OrderAndDiscountWidget.transaction(
                isDone: true,
                date: DateTime(2019),
                title: '#D-51574',
                transactionPrice: 25651.23,
                description:
                    'hello man so how are you man what is happninig today man',
              ),
              OrderAndDiscountWidget.transaction(
                isDone: false,
                date: DateTime(2019),
                // inputTransaction: true,
                title: '#D-51574',
                transactionPrice: 25651.23,
                description:
                    'hello man so how are you man what is happninig today man',
              ),
              OrderAndDiscountWidget.transaction(
                isDone: true,
                date: DateTime(2019),
                title: '#D-51574',
                transactionPrice: 25651.23,
                description:
                    'hello man so how are you man what is happninig today man',
              ),
              //
              //
              //
              //
              //
            ],
          ),
        )),
      ),
    );
  }
}
