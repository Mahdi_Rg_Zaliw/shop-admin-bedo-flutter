import 'package:bedo_shop_admin/presentation/widgets/button_grid_widget.dart';
import 'package:flutter/material.dart';

class BtnGridShowCase extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
            child: SingleChildScrollView(
          child: Column(
            children: [
              ButtonGrid(
                title: 'time needed',
                options: [
                  '10 min',
                  '100 min',
                  '50 min',
                  '101 min',
                  '200 min',
                  '53 min'
                ],
                crossAxisCount: 3,
                  onChanged: (){},
              ),
              SizedBox(
                height: 20,
              ),
              ButtonGrid(
                title: 'time needed',
                options: [
                  '10 min',
                  '100 min',
                  '50 min',
                  '101 min',
                ],
                crossAxisCount: 2,
                onChanged: (){},
              ),
            ],
          ),
        )),
      ),
    );
  }
}
