import 'package:bedo_shop_admin/bloc/shop_bloc/shop_bloc.dart';
import 'package:bedo_shop_admin/bloc/transactions_bloc/transactions_bloc.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/presentation/widgets/filtered_list.dart';
import 'package:bedo_shop_admin/presentation/widgets/order_and_discount_widget.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RestaurantWalletTransactionScreen extends StatefulWidget {
  @override
  _RestaurantWalletTransactionScreenState createState() =>
      _RestaurantWalletTransactionScreenState();
}

class _RestaurantWalletTransactionScreenState
    extends State<RestaurantWalletTransactionScreen> {
  @override
  void initState() {
    super.initState();
    //TransactionsVisited
    BlocProvider.of<TransactionsBloc>(context).add(TransactionsVisited());
  }

  @override
  Widget build(BuildContext context) {
    //todo All Strings in this file should be replaced after connected to api
    return FilteredList.fix(
      appBarTitlesList: [
        Text(
          S.of(context).restaurantWalletTransaction,
          style: grey14medium,
        )
      ],
      onSwipeTo: (ix) {
        printPink('$ix');
      },
      appBarBottomList: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              BlocBuilder<ShopBloc, ShopState>(
                builder: (context, state) {
                  String adress = '';
                  String shopName = '';
                  if (state is ShopEditableDataLoaded) {
                    adress = state.data['address'];
                    shopName = state.data['name'];
                  }
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5),
                        child: Text(
                          shopName,
                          style: black16semibold,
                        ),
                      ),
                      Text(
                        adress,
                        style: grey12regular,
                      ),
                    ],
                  );
                },
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: BlocBuilder<ShopBloc, ShopState>(
                  builder: (context, state) {
                    num walletPrice = 0;
                    if (state is ShopEditableDataLoaded) {
                      walletPrice =
                          state.data['getTotalTransactionsAmountByShopAdmin'] ??
                              0;
                      printWhiteBg(walletPrice);
                    }
                    return Text(
                      ' $walletPrice',
                      style: green18medium.copyWith(
                        color: walletPrice > 0 ? primaryMaterialColor : red,
                      ),
                    );
                  },
                ),
              ),
              // Padding(
              //   padding: const EdgeInsets.symmetric(horizontal: 20),
              //   child: Text(
              //     "+982\$",
              //   ),
              // )
            ],
          ),
        )
      ],
      optionsList: [
        S.of(context).total,
        S.of(context).input,
        S.of(context).output
      ],
      showBackButton: true,
      totalAmount: 20,
      pagesList: [
        BlocBuilder<TransactionsBloc, TransactionsState>(
          builder: (context, state) {
            var d;
            if (state is TransactionsLoadSuccess) {
              d = state.totals;
            }
            return TransactionList(TxType.Total, d);
          },
        ),
        BlocBuilder<TransactionsBloc, TransactionsState>(
          builder: (context, state) {
            var d;
            if (state is TransactionsLoadSuccess) {
              d = state.inputs;
            }
            return TransactionList(TxType.Input, d);
          },
        ),
        // TransactionList(TxType.Input),
        BlocBuilder<TransactionsBloc, TransactionsState>(
          builder: (context, state) {
            var d;
            if (state is TransactionsLoadSuccess) {
              d = state.outputs;
            }
            return TransactionList(TxType.Output, d);
          },
        ),
        // TransactionList(TxType.Output),
      ],
    );
  }
}

enum TxType { Input, Output, Total }

class TransactionList extends StatelessWidget {
  final TxType type;
  final List data;

  bool parsePaymentType(String type) {
    if (type == 'PAY_FROM_USER_TO_SHOP' || type == 'PAY_FROM_BEDO_TO_SHOP') {
      return true;
    }
    if (type == 'PAY_FROM_SHOP_TO_DRIVER' || type == 'PAY_FROM_SHOP_TO_BEDO') {
      return false;
    }
    return false;
  }

  TransactionList(this.type, this.data);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 15),
      child: (data != null && data.isNotEmpty)
          ? ListView.builder(
              itemBuilder: (ctx, index) {
                var d = data[index];
                return OrderAndDiscountWidget.order(
                    date: DateTime.parse(d['createdAt']) ?? 'no time data',
                    title: d['transactionMethod'].toString().toLowerCase(),
                    orderPrice: double.parse(d['amount']?.toString()) ?? 0.0,
                    description:
                        d['type'].toString().replaceAll('_', ' ').toLowerCase(),
                    isExpanded: true,
                    inputTransaction: false,
                    isDone: parsePaymentType(d['type'].toString())
                    // type == TxType.Output ? false : true,
                    );
              },
              itemCount: data.length,
            )
          : Center(
              child: Text('no data received' + '\n\n\n\n\n\n\n\n\n'),
            ),
    );
  }
}
