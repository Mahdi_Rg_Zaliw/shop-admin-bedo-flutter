import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/presentation/screens/signup_verfication_builder.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:bedo_shop_admin/design/icons_f.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/presentation/screens/login.dart';
import 'package:bedo_shop_admin/bloc/auth_bloc/auth_bloc.dart';
import 'package:bedo_shop_admin/bloc/auth_bloc/auth_events.dart';
import 'package:bedo_shop_admin/bloc/auth_bloc/auth_states.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SignUp extends StatefulWidget {
  static String routeName = 'sign_up';
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  String _errMsg = '';
  bool _err = false;
  String phoneNumberUser = '';
  String initialCountry = 'AZ';
  PhoneNumber number = PhoneNumber(isoCode: 'AZ');
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      // key: formKey,
      body: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Container(
            width: double.infinity,
            height: height,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: height * 0.15),
                      child: Icon(
                        Iconsf.i32_logo,
                        color: Theme.of(context).primaryColor,
                        size: 76,
                      ),
                    ),
                    SizedBox(height: height * 0.05),
                    Column(
                      children: [
                        const Padding(
                          padding: const EdgeInsets.only(bottom: 4.0),
                          child: Text(
                            "Sign Up",
                            style: grey14regular,
                            textAlign: TextAlign.center,
                          ),
                        ),
                        const Padding(
                          padding: const EdgeInsets.only(top: 4.0),
                          child: Text(
                            "Manage Store Orders",
                            style: black24semibold,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Flexible(child: SizedBox()),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.end,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Padding(
                          padding: const EdgeInsets.only(left: 25, bottom: 15),
                          child: Text('Phone Number', style: grey14regular),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 25),
                          child: InternationalPhoneNumberInput(
                            onInputValidated: (bool value) {
                              print(value);
                            },
                            validator: (v) {
                              if (_err) {
                                return '$_errMsg';
                              }
                              return null;
                            },
                            ignoreBlank: false,
                            formatInput: false,
                            keyboardType: TextInputType.numberWithOptions(
                                signed: true, decimal: true),
                            inputBorder: OutlineInputBorder(),
                            onInputChanged: (PhoneNumber number) {
                              setState(() {
                                phoneNumberUser = number.phoneNumber;
                              });
                              print(phoneNumberUser);
                            },
                            selectorConfig: SelectorConfig(
                              showFlags: false,
                              setSelectorButtonAsPrefixIcon: true,
                              selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
                            ),
                            textStyle: black12regular,
                            autoValidateMode: AutovalidateMode.disabled,
                            selectorTextStyle: TextStyle(
                              color: Color(0xFF161616),
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                            ),

                            initialValue: number,
                            // textFieldController: _phoneNumberController,
                            spaceBetweenSelectorAndTextField: 0,
                            //!#####################################
                            //!#####################################
                            // inputDecoration: InputDecoration(
                            //   focusedBorder: OutlineInputBorder(
                            //     borderRadius: BorderRadius.circular(15),
                            //     borderSide: BorderSide(
                            //       color: _err == false
                            //           ? primaryMaterialColor
                            //           : red,
                            //       width: 1,
                            //     ),
                            //   ),
                            //   hintText: "Enter Your Phone Number",
                            // ),
                            //!#####################################
                            //!#####################################
                            onSaved: (PhoneNumber number) {
                              print('On Saved: $number');
                            },
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: height * 0.05),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 25, vertical: height * 0.01),
                      child: BlocConsumer<AuthBloc, AuthState>(
                        listener: (context, state) {
                          if (state is SendingCodeVerification) {
                            setState(() {
                              _err = false;
                              formKey.currentState.validate();
                            });

                            Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => VerificationBuilder(
                                phonNumber: phoneNumberUser,
                                testcode: state.code,
                                // c: state.code,
                              ),
                            ));
                            String c = state.code;

                            showToastWidget(
                              Container(
                                padding: EdgeInsets.symmetric(horizontal: 18.0),
                                margin: EdgeInsets.symmetric(horizontal: 50.0),
                                decoration: ShapeDecoration(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  color: primaryMaterialColor,
                                ),
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(24.0),
                                      child: Text(
                                        'sms code sent was $c ',
                                        style: white14medium,
                                      ),
                                    ),
                                    // IconButton(
                                    //   onPressed: () {

                                    //   },
                                    //   icon: Icon(
                                    //     Icons.add_circle_outline_outlined,
                                    //     color: Colors.white,
                                    //   ),
                                    // ),
                                  ],
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                ),
                              ),
                              context: context,
                              isIgnoring: false,
                              alignment: Alignment.topCenter,
                              position: StyledToastPosition.top,
                              duration: Duration(seconds: 4),
                            );
                          }
                          if (state is SignUpErrors) {
                            setState(
                              () {
                                _err = true;
                                _errMsg = state.clientErrors.toString();
                                formKey.currentState.validate();
                              },
                            );
                          }
                        },
                        builder: (context, state) {
                          print(state);
                          return RaisedButton(
                            onPressed: () async {
                              context.read<AuthBloc>().add(AuthBlocReset());
                              printOrange('hi now');
                              printPink('bye');
                              if (formKey.currentState.validate() == false) {
                                setState(() {
                                  _err = false;
                                });
                              } else {
                                setState(() {
                                  _err = true;
                                  _errMsg = '';
                                });
                                context.read<AuthBloc>().add(
                                      SignUpEvents(
                                        phoneNumber: phoneNumberUser,
                                      ),
                                    );
                              }
                            },
                            child: const Text(
                              "Continue",
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w400),
                            ),
                            elevation: 0,
                            highlightElevation: 1,
                          );
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pushReplacement(context,
                              MaterialPageRoute(builder: (context) => Login()));
                        },
                        child: Center(
                          child: RichText(
                              text: TextSpan(children: <TextSpan>[
                            const TextSpan(
                                text: "Already have an account? ",
                                style: grey14medium),
                            const TextSpan(
                                text: "Sign in", style: black14medium),
                          ])),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
