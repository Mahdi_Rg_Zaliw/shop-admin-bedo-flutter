import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/presentation/screens/mohammads_show_case/menu.dart';
import 'package:bedo_shop_admin/presentation/screens/sina_showcase/menu.dart';
import 'package:bedo_shop_admin/presentation/screens/splash.dart';
import 'package:bedo_shop_admin/presentation/screens/vanils_showcases/menu.dart';
import 'package:bedo_shop_admin/presentation/widgets/round_botton.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'mahdis_showcase/menu.dart';

class Debug extends StatelessWidget {
  static const routeName = 'home';

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: whiteAllButtonsColor,
      child: Scaffold(
        body: Center(
          child: IntrinsicWidth(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.all(4),
                  child: Buttons.red(
                    onPressed: () {
                      Navigator.of(context).pushNamed(Splash.routeName);
                    },
                    child: Text('MAIN'),
                  ),
                ),
                //
                //
                //
                //
                //
                //
                //
                //
                //
                Padding(
                  padding: const EdgeInsets.all(4),
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => VanilsMenu(),
                        ),
                      );
                    },
                    child: Text('vanils widgetss'),
                  ),
                ),
                //
                //
                //
                //
                //
                //
                Padding(
                  padding: const EdgeInsets.all(4),
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => MohammadsMenu(),
                        ),
                      );
                    },
                    child: Text('mohammads widgets'),
                  ),
                ),
                //
                //
                //
                //
                //
                //
                Padding(
                  padding: const EdgeInsets.all(4),
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => MehdisMenu(),
                        ),
                      );
                    },
                    child: Text('mehdi widgets'),
                  ),
                ),
                //
                //
                //
                //
                //
                //
                //
                //
                //
                //
                //
                //
                //
                Builder(
                  builder: (BuildContext ctx) => Padding(
                    padding: const EdgeInsets.all(4),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => SinasMenu(),
                          ),
                        );
                      },
                      child: Text('sinas widgets'),
                    ),
                  ),
                ),
                //
                //
                //
                //
                //
                //
                //
                //
              ],
            ),
          ),
        ),
      ),
    );
  }
}
