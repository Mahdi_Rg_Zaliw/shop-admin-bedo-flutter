import 'package:bedo_shop_admin/bloc/conversations_bloc/conversations_bloc.dart';
import 'package:bedo_shop_admin/bloc/conversations_bloc/conversations_events.dart';
import 'package:bedo_shop_admin/bloc/conversations_bloc/conversations_state.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/model/message_model.dart';
import 'package:bedo_shop_admin/presentation/screens/chat_screen.dart';
import 'package:bedo_shop_admin/presentation/widgets/filtered_list.dart';
import 'package:bedo_shop_admin/utils/time_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SupportPages extends StatefulWidget {
  static const routeName = 'support_page';
  @override
  _SupportPagesState createState() => _SupportPagesState();
}

class _SupportPagesState extends State<SupportPages> {
  @override
  void initState() {
    BlocProvider.of<ConversationBloc>(context).add(LoadConversations());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FilteredList.fix(
      appBarTitlesList: [
        Text(
          S.of(context).supportTeam,
          style: grey14medium,
        )
      ],
      showBackButton: true,
      optionsList: [S.of(context).inbox, S.of(context).messages],
      pagesList: [SupportInboxPage(), SupportMessages()],
    );
  }
}

class SupportInboxPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ConversationBloc, ConversationsState>(
        builder: (context, state) {
      if (state is ConversationsLoaded) {
        var conversations = state.repliedConversations;
        return ListView.separated(
          itemCount: conversations.length,
          separatorBuilder: (context, index) => Divider(
            height: 2,
          ),
          itemBuilder: (context, index) {
            return ListTile(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ChatScreen(conversations[index])));
              },
              isThreeLine: false,
              title: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      conversations[index].title,
                      style: black14medium,
                    ),
                    Text(
                      relativeTime(conversations[index].updatedAt, context),
                      style: grey14medium,
                    ),
                  ],
                ),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  conversations[index].lastMessage.type == MessageType.Text
                      ? conversations[index].lastMessage.text
                      : S.of(context).File + ':',
                  style: grey14regular,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            );
          },
        );
      }
      if (state is ConversationsLoading) {
        return Center(
          child: CupertinoActivityIndicator(),
        );
      }
      if (state is ConversationsLoadFailed) {
        return Center(
          child: Padding(
            padding: const EdgeInsets.all(30),
            child: Text(
              S.of(context).conversationsLoadFailed,
              style: red14medium,
            ),
          ),
        );
      }
      return Container();
    });
  }
}

class SupportMessages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ConversationBloc, ConversationsState>(
        builder: (context, state) {
      if (state is ConversationsLoaded) {
        var conversations = state.notRepliedConversations;
        return ListView.separated(
          itemCount: conversations.length,
          separatorBuilder: (context, index) => Divider(
            height: 2,
          ),
          itemBuilder: (context, index) {
            return ListTile(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ChatScreen(conversations[index])));
              },
              isThreeLine: false,
              title: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      conversations[index].title,
                      style: black14medium,
                    ),
                    Text(
                      relativeTime(conversations[index].updatedAt, context),
                      style: grey14medium,
                    ),
                  ],
                ),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  conversations[index].lastMessage.text,
                  style: grey14regular,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            );
          },
        );
      }
      if (state is ConversationsLoading) {
        return Center(
          child: CupertinoActivityIndicator(),
        );
      }
      if (state is ConversationsLoadFailed) {
        return Center(
          child: Padding(
            padding: const EdgeInsets.all(30),
            child: Text(
              S.of(context).conversationsLoadFailed,
              style: red14medium,
            ),
          ),
        );
      }
      return Container();
    });
  }
}
