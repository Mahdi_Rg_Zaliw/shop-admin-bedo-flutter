import 'dart:io';
import 'package:bedo_shop_admin/presentation/screens/homePage.dart';
import 'package:bedo_shop_admin/presentation/screens/splash.dart';
import 'package:flutter/services.dart';

import './edit_shop.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/presentation/widgets/form_input_custom_location_field.dart';
import 'package:bedo_shop_admin/presentation/widgets/form_input_select_shop_func.dart';
import 'package:bedo_shop_admin/presentation/widgets/form_input_select_working_day.dart';
import 'package:bedo_shop_admin/utils/device_dimensions.dart';
import 'package:bedo_shop_admin/presentation/widgets/pick_picture_button.dart';
import 'package:bedo_shop_admin/presentation/widgets/custom_text_field.dart';
import 'package:flutter/material.dart';
import 'package:bedo_shop_admin/generated/l10n.dart';
import 'package:bedo_shop_admin/bloc/shop_bloc/shop_bloc.dart';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'custom_expansion_tile.dart';

class AddShopPage extends StatefulWidget {
  static const routeName = 'add shop';
  AddShopPage({Key key}) : super(key: key);

  @override
  _AddShopPageState createState() => _AddShopPageState();
}

class _AddShopPageState extends State<AddShopPage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<ShopBloc>(context).add(AddShopVisited());
  }

  Location _location;

  @override
  void dispose() {
    super.dispose();
    _name.dispose();
    _phone.dispose();
    _tax.dispose();
    _cnum.dispose();
    _prep.dispose();
  }

  final _name = TextEditingController();
  final _phone = TextEditingController();
  final _tax = TextEditingController();
  final _cnum = TextEditingController();
  final _prep = TextEditingController();

  // Location _location;
  // String imagePath;
  File uploadIMG;

  List<DayWorkingTime> week;

  // List categories;

  List get weekFixed {
    List ans = [];
    week?.forEach((element) {
      if (element.check == true) {
        ans.add(element.toMap());
      }
    });
    return ans;
  }

  List<CategoryDataModel> categories;

  List get categoryDataFixed {
    List ans = [];
    categories?.forEach((element) {
      if (element.check == true) {
        ans.add(element.id);
      }
    });
    return ans;
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 70,
        centerTitle: true,
        title: Text(
          'create shop',
          style: grey14regular,
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
            key: _formKey,
            child: Column(
              children: [
                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(vertical: 30),
                  child: PickPictureButton(
                    // pickerType: ,
                    // previewImg: d['logoUrl'],
                    onChanged: (file) {
                      uploadIMG = file;
                    },
                  ),
                ),
                //
                //
                //
                //
                // name
                //
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: CustomTextField(
                    controller: _name,
                    title: S.of(context).restaurantName,
                    hintText: S.of(context).enterTheRestaurantName,
                    // controller: textFieldController[0],
                    validator: (value) {
                      if (value.length == 0) {
                        return 'this field is required'; //todo translate
                      }
                      return null;
                    },
                  ),
                ),
                //
                //
                //
                //
                // location
                //
                CustomLocationField(
                  context: context,
                  // initial: Location(x: x, y: y),
                  validator: (selectedLocation) {
                    if (selectedLocation?.x == null ||
                        selectedLocation?.y == null) {
                      return ' please select a location on map'; //todo translate
                    }
                    return null;
                  },
                  onSaved: (val) {
                    _location = val;
                  },
                ),
                //
                //
                //
                //
                // phone number
                //
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: CustomTextField(
                    controller: _phone,
                    formatters: [FilteringTextInputFormatter.digitsOnly],

                    validator: (value) {
                      if (value.length == 0) {
                        return 'thins field is required'; //todo translate
                      }
                      if (!value.isValidMobile()) {
                        return 'plese enter a valid phone number'; // todo translate
                      }
                      return null;
                    },
                    keyboardType: TextInputType.phone,
                    title: S.of(context).restaurantPhoneNumber,
                    hintText: S.of(context).enterTheRestaurantPhoneNumber,
                    // controller: textFieldController[1],
                  ),
                ),
                //
                //
                //
                //  week
                //
                //
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: WorkingWeekDays(
                      // data: d['workingHoursInMinutes'],
                      onChange: (d) {
                    week = d;
                    printBlueBg(weekFixed);
                    printYellowBg(week);
                  }),
                ),

                ///
                ///
                ///
                ///
                ///
                ///
                ///
                /// category
                ///
                ///
                ///
                ///
                ///
                ///
                BlocBuilder<ShopBloc, ShopState>(
                  builder: (context, state) {
                    if (state is ShopCategoriesLoaded) {
                      printColor(Colors.lime, 'we build this');

                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 24),
                        child: ShopFunction(
                            onCahange: (v) {
                              printYellowBg(v);
                              categories = v;
                              printGreenBg(categoryDataFixed);
                            },
                            data: state.data),
                      );
                    }
                    // return CircularProgressIndicator();
                    return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 24),
                        child: CustomExpansionTile(
                          // onExpansionChanged: (value) => ,
                          title: Row(
                            children: [
                              Text(
                                'select shop function' + '    ',
                                style: black14medium,
                              ),
                            ],
                          ),
                          children: [
                            SizedBox(
                              height: 70,
                              child: Center(
                                child: Text('could noth categories'),
                              ),
                            )
                          ],
                        ));
                  },
                ),

                ///
                ///
                ///
                ///
                ///
                ///
                ///
                /// avrage preparing time
                ///
                ///
                ///
                ///
                ///
                ///

                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: CustomTextField(
                    controller: _prep,
                    formatters: [FilteringTextInputFormatter.digitsOnly],

                    validator: (value) {
                      if (value.length == 0) {
                        return 'thins field is required'; //todo translate
                      }

                      return null;
                    },
                    keyboardType: TextInputType.number,
                    title: 'avrage prepairing time ', //todo translate
                    hintText: 'enter time in minutes', //todo translate
                    // controller: textFieldController[1],
                  ),
                ),

                ///
                ///
                ///
                ///
                ///
                ///
                ///
                /// tax
                ///
                ///
                ///
                ///
                ///
                ///
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: CustomTextField(
                    controller: _tax,
                    // validator: (value) {
                    //   if (value.length == 0) {
                    //     return 'thins field is required'; //todo translate
                    //   }

                    //   return null;
                    // },
                    keyboardType: TextInputType.text,
                    title: 'tax code ', //todo translate
                    hintText: 'enter tax code', //todo translate
                    // controller: textFieldController[1],
                  ),
                ),
                //
                //
                //
                // card number
                //
                //
                //
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: CustomTextField(
                    controller: _cnum,
                    formatters: [FilteringTextInputFormatter.digitsOnly],

                    validator: (value) {
                      if (value.length == 0) {
                        return 'thins field is required'; //todo translate
                      }

                      return null;
                    },
                    keyboardType: TextInputType.number,
                    title: ' card number', //todo translate
                    hintText: 'enter card number', //todo translate
                    // controller: textFieldController[1],
                  ),
                ),
                //
                //
                //
                // submit button
                //
                //
                Container(
                  width: DeviceDimensions.widthSize(context),
                  padding:
                      EdgeInsets.only(left: 24, right: 24, bottom: 24, top: 10),
                  child: BlocListener<ShopBloc, ShopState>(
                    listener: (context, state) {
                      if (state is SuccessShopOperation) {
                        showDialog(
                          context: context,
                          builder: (context) => CustomDialogueSuccess(
                            title: 'successfully created shop',
                          ),
                        ).then(
                          (value) => Navigator.of(context)
                              .pushReplacementNamed(HomePage.routeName),
                        );
                      }
                      if (state is FailedShopDataOperation) {
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            title: FailureText(
                              title:
                                  'Something went wrong! \ncould not handle your request at this moment\n' +
                                      '${state.clientErr}',
                            ),
                          ),
                        );
                      }
                    },
                    child: RaisedButton(
                      elevation: 0,
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();
                          final data = {
                            'phoneNumbers': [_phone.text],
                            'coordinates': [_location.x, _location.y],
                            'name': _name.text,
                            'taxCode': _tax.text,
                            'cardNumber': _cnum.text,
                            'workingHoursInMinutes': weekFixed,
                            'file': uploadIMG,
                            'categories': categoryDataFixed,
                            'preparingTime': int.parse(_prep.text)
                          };

                          printCyanBg(data);

                          BlocProvider.of<ShopBloc>(context)
                              .add(ShopCreating(data));
                        }
                      },
                      child: Text(S.of(context).confirm),
                    ),
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
