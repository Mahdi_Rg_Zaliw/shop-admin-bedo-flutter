import 'dart:async';
import 'package:bedo_shop_admin/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:bedo_shop_admin/design/styles.dart';
import 'package:bedo_shop_admin/presentation/screens/complete_profile_screen.dart';
import 'package:bedo_shop_admin/bloc/auth_bloc/auth_bloc.dart';
import 'package:bedo_shop_admin/bloc/auth_bloc/auth_events.dart';
import 'package:bedo_shop_admin/bloc/auth_bloc/auth_states.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bedo_shop_admin/utils/device_dimensions.dart';
import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/model/user_model.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';

enum EditingState { editing, waiting, accepted, denied }

class VerificationCodeScreen extends StatefulWidget {
  final bool signUp;
  final String phoneNumber;

  VerificationCodeScreen({this.signUp = true, this.phoneNumber});

  @override
  _VerificationCodeScreenState createState() => _VerificationCodeScreenState();
}

class _VerificationCodeScreenState extends State<VerificationCodeScreen> {
  Timer timer;

  int _currentTime;

  String phoneNumber;

  EditingState _editingState = EditingState.editing;

  final List<TextEditingController> verificationTextList = [
    TextEditingController(),
    TextEditingController(),
    TextEditingController(),
    TextEditingController(),
  ];

  final List<FocusNode> focusNodeList = [
    FocusNode(),
    FocusNode(),
    FocusNode(),
    FocusNode(),
  ];

  deactivate() {
    timer.cancel();

    super.deactivate();
  }

  List<String> verificationCode;

  User user = User();

  @override
  void initState() {
    // Toast.show("Toast plugin app", context, duration: 1, gravity: Toast.TOP);
    // Toast.

    focusNodeList.first.requestFocus();
    _currentTime = 59;
    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      if (_currentTime == 0) {
        timer.cancel();
        setState(() {
          focusNodeList.forEach((element) {
            element.unfocus();
          });
          _editingState = EditingState.denied;
        });
      } else {
        setState(() {
          _currentTime -= 1;
        });
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => AuthBloc(),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    widget.signUp ? "Create account" : "Verification",
                    style: grey14regular,
                    textAlign: TextAlign.start,
                  ),
                  Text(
                    widget.signUp ? "Phone verification" : "Verify Your Number",
                    style: black24semibold,
                  ),
                ],
              ),
              const Spacer(
                flex: 1,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.max,
                children: [
                  BlocConsumer<AuthBloc, AuthState>(
                    listener: (context, state) {
                      if (state is AuthLoading) {
                        setState(() {
                          _editingState = EditingState.waiting;
                        });
                      }
                      if (state is ComplateInformation) {
                        setState(() {
                          _editingState = EditingState.accepted;
                        });
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (contex) => CompleteProfileScreen(
                              phoneNumber: widget.phoneNumber,
                              phoneNumberSignUpCode: state.phoneSignUpCode,
                            ),
                          ),
                        );
                      }
                      if (state is CodeVerifyErrors) {
                        setState(() {
                          _editingState = EditingState.denied;
                        });

                        print('Verification Code Is Wrong');
                      }
                    },
                    builder: (context, state) {
                      return AbsorbPointer(
                        absorbing: false,
                        child: PhoneVerificationWidget(
                          verificationTextList: verificationTextList,
                          editingState: _editingState,
                          focusInputList: focusNodeList,
                          onChanged: [
                            (String a) {
                              String allCodeVerificationText =
                                  '${verificationTextList[0].text}${verificationTextList[1].text}${verificationTextList[2].text}${verificationTextList[3].text}';
                              printGreenBg(allCodeVerificationText.length);
                              printGreenBg(
                                  verificationTextList.map((e) => e.text));
                              if (allCodeVerificationText.length == 4) {
                                context.read<AuthBloc>().add(
                                      CheckVerificationCode(
                                        phoneVerificationCode:
                                            allCodeVerificationText,
                                        phoneNumber: widget.phoneNumber,
                                      ),
                                    );
                              }
                              // *==============
                              print(a);
                              if (a != '') {
                                FocusScope.of(context)
                                    .requestFocus(focusNodeList[1]);
                                setState(() {
                                  verificationCode.add(a);
                                });
                              } else {
                                FocusScope.of(context).unfocus();
                              }
                            },
                            (String a) {
                              String allCodeVerificationText =
                                  '${verificationTextList[0].text}${verificationTextList[1].text}${verificationTextList[2].text}${verificationTextList[3].text}';
                              if (allCodeVerificationText.length == 4) {
                                context.read<AuthBloc>().add(
                                      CheckVerificationCode(
                                        phoneVerificationCode:
                                            allCodeVerificationText,
                                        phoneNumber: widget.phoneNumber,
                                      ),
                                    );
                              }
                              // *==============
                              print(a);
                              printGreenBg(allCodeVerificationText.length);
                              printGreenBg(
                                  verificationTextList.map((e) => e.text));
                              if (a != '') {
                                FocusScope.of(context)
                                    .requestFocus(focusNodeList[2]);

                                setState(() {
                                  verificationCode.add(a);
                                });
                              } else {
                                FocusScope.of(context)
                                    .requestFocus(focusNodeList[0]);
                              }
                            },
                            (String a) {
                              String allCodeVerificationText =
                                  '${verificationTextList[0].text}${verificationTextList[1].text}${verificationTextList[2].text}${verificationTextList[3].text}';
                              printGreenBg(allCodeVerificationText.length);
                              printGreenBg(
                                  verificationTextList.map((e) => e.text));
                              if (allCodeVerificationText.length == 4) {
                                context.read<AuthBloc>().add(
                                      CheckVerificationCode(
                                        phoneVerificationCode:
                                            allCodeVerificationText,
                                        phoneNumber: widget.phoneNumber,
                                      ),
                                    );
                              }
                              // *==============
                              print(a);
                              if (a != '') {
                                FocusScope.of(context)
                                    .requestFocus(focusNodeList[3]);

                                setState(() {
                                  verificationCode.add(a);
                                });
                              } else {
                                FocusScope.of(context)
                                    .requestFocus(focusNodeList[1]);
                              }
                            },
                            // (String a) {
                            //   printYellowBg(a.length);
                            //   printYellowBg(a);

                            //   if (a.length == 4) {
                            //     printYellowBg(a.length);
                            //   }
                            // },
                            (String a) {
                              String allCodeVerificationText =
                                  '${verificationTextList[0].text}${verificationTextList[1].text}${verificationTextList[2].text}${verificationTextList[3].text}';
                              print(allCodeVerificationText.length);
                              if (allCodeVerificationText.length == 4) {
                                context.read<AuthBloc>().add(
                                      CheckVerificationCode(
                                        phoneVerificationCode:
                                            allCodeVerificationText,
                                        phoneNumber: widget.phoneNumber,
                                      ),
                                    );
                              }
                              // *==============
                              print(state);

                              // context.read<AuthBloc>().add(
                              //       CheckVerificationCode(
                              //         phoneVerificationCode:
                              //             allCodeVerificationText,
                              //         phoneNumber: widget.phoneNumber,
                              //       ),
                              //     );

                              print(a);

                              if (a != '') {
                                FocusScope.of(context).unfocus();

                                setState(() {
                                  verificationCode.add(a);
                                });
                              } else {
                                FocusScope.of(context)
                                    .requestFocus(focusNodeList[2]);
                              }
                            },
                          ],
                        ),
                      );
                    },
                  ),
                  _editingState == EditingState.accepted
                      ? Text('your enter verification code is true',
                          style: green14regular, textAlign: TextAlign.center)
                      : _editingState == EditingState.denied
                          ? Container(
                              width: MediaQuery.of(context).size.width * 0.5,
                              child: FittedBox(
                                child: Text(
                                  'your enter verification code is wrong or has been expired',
                                  style: red14regular,
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            )
                          : Container(),
                  if (_editingState != EditingState.accepted)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const Text(
                          "Didn't receive a code?",
                          style: grey14medium,
                        ),
                        FlatButton(
                          onPressed: () {
                            ResendVerificationCodeRequest
                                resendVerificationCode =
                                ResendVerificationCodeRequest(
                              phoneNumber: widget.phoneNumber,
                            );
                            context
                                .read<AuthBloc>()
                                .add(resendVerificationCode);
                            clear();
                            focusNodeList.first.requestFocus();
                          },
                          child: Text(
                            "RESEND",
                            style: green14medium,
                          ),
                        ),
                        Expanded(child: Container()),
                        Padding(
                          padding: const EdgeInsets.all(2),
                          child: Text(
                            "00:" + _currentTime.toString().padLeft(2, '0'),
                            style:
                                _currentTime < 10 ? red14medium : grey14medium,
                          ),
                        ),
                      ],
                    ),
                ],
              ),
              Expanded(
                flex: 8,
                child: _editingState == EditingState.waiting
                    ? CupertinoActivityIndicator(
                        radius: 15,
                      )
                    : Container(),
              ),
              // Padding(
              //   padding: const EdgeInsets.only(bottom: 30),
              //   child: Buttons.grey(
              //     onPressed: () {
              //       if (_currentTime != 0) {
              //         clear();
              //         focusNodeList.first.requestFocus();
              //       }
              //     },
              //     child: Text(
              //       "Clear",
              //       style: grey18semibold,
              //     ),
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }

  void clear() {
    verificationTextList[0].clear();
    verificationTextList[1].clear();
    verificationTextList[2].clear();
    verificationTextList[3].clear();
    setState(() {
      _editingState = EditingState.editing;
    });
  }
}

class PhoneVerificationWidget extends StatelessWidget {
  final List<ValueChanged<String>> onChanged;
  final EditingState editingState;
  final List<TextEditingController> verificationTextList;
  final List<FocusNode> focusInputList;

  PhoneVerificationWidget({
    this.editingState,
    this.verificationTextList,
    this.focusInputList,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: DeviceDimensions.widthSize(context) / 0.5,
      height: DeviceDimensions.widthSize(context) / 4,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ...verificationTextList
              .map(
                (e) => Container(
                  alignment: Alignment.center,
                  width: DeviceDimensions.widthSize(context) / 5,
                  height: DeviceDimensions.widthSize(context) / 5,
                  decoration: BoxDecoration(
                    color: lightGreyBg,
                    border: Border.all(
                      color: editingState == EditingState.accepted
                          ? primaryMaterialColor
                          : editingState == EditingState.denied
                              ? red
                              : Colors.transparent,
                      width: 1,
                    ),
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: TextField(
                    cursorColor: primaryMaterialColor,
                    autocorrect: false,
                    maxLength: 1,
                    showCursor: true,
                    maxLengthEnforced: true,
                    textAlign: TextAlign.center,
                    controller: e,
                    style: black35medium,
                    focusNode: focusInputList[verificationTextList.indexOf(e)],
                    onChanged: onChanged[verificationTextList.indexOf(e)],
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.number,
                    //     inputDecoration: InputDecoration(
                    //   focusedBorder: OutlineInputBorder(
                    //     borderRadius: BorderRadius.circular(15),
                    //     borderSide: BorderSide(
                    //       color: _err == false
                    //           ? primaryMaterialColor
                    //           : red,
                    //       width: 1,
                    //     ),
                    //   ),
                    //   hintText: "Enter Your Phone Number",
                    // ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      focusColor: Colors.transparent,
                      focusedBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      enabledBorder: (editingState == EditingState.denied)
                          ? InputBorder.none
                          : null,
                      // OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(15),
                      //     borderSide: BorderSide(
                      //         color: primaryMaterialColor, width: 1)),
                      errorMaxLines: 1,
                      counterText: "",
                    ),
                  ),
                ),
              )
              .toList(),
        ],
      ),
    );
  }
}
