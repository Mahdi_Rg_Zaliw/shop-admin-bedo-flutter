import 'package:bedo_shop_admin/design/theme.dart';
import 'package:bedo_shop_admin/presentation/screens/home.dart';
import 'package:bedo_shop_admin/presentation/screens/splash.dart';
import 'package:bedo_shop_admin/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'bloc/lang_bloc/lang_bloc.dart';
import 'generated/l10n.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LangBloc, LangState>(
      builder: (context, state) {
        return MaterialApp(
          title: 'bedo',
          locale: Locale.fromSubtags(languageCode: state.languageCode),
          // locale: context.select<LangBloc, Locale>((langBloc) => Locale.fromSubtags(languageCode: langBloc.state.languageCode)),
          localizationsDelegates: [
            S.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: S.delegate.supportedLocales,
          theme: theme,
          debugShowCheckedModeBanner: false,
          initialRoute: Splash.routeName,
          // key:,
          // navigatorKey: ,
          routes: routes,
        );
      },
    );
  }
}
