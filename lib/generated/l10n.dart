// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `this field is required`
  String get thisFieldIsRequired {
    return Intl.message(
      'this field is required',
      name: 'thisFieldIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `Your responce`
  String get yourResponce {
    return Intl.message(
      'Your responce',
      name: 'yourResponce',
      desc: '',
      args: [],
    );
  }

  /// `days`
  String get days {
    return Intl.message(
      'days',
      name: 'days',
      desc: '',
      args: [],
    );
  }

  /// `day`
  String get day {
    return Intl.message(
      'day',
      name: 'day',
      desc: '',
      args: [],
    );
  }

  /// `ago`
  String get ago {
    return Intl.message(
      'ago',
      name: 'ago',
      desc: '',
      args: [],
    );
  }

  /// `weeks`
  String get weeks {
    return Intl.message(
      'weeks',
      name: 'weeks',
      desc: '',
      args: [],
    );
  }

  /// `week`
  String get week {
    return Intl.message(
      'week',
      name: 'week',
      desc: '',
      args: [],
    );
  }

  /// `minutes`
  String get minutes {
    return Intl.message(
      'minutes',
      name: 'minutes',
      desc: '',
      args: [],
    );
  }

  /// `minute`
  String get minute {
    return Intl.message(
      'minute',
      name: 'minute',
      desc: '',
      args: [],
    );
  }

  /// `hours`
  String get hours {
    return Intl.message(
      'hours',
      name: 'hours',
      desc: '',
      args: [],
    );
  }

  /// `hour`
  String get hour {
    return Intl.message(
      'hour',
      name: 'hour',
      desc: '',
      args: [],
    );
  }

  /// `years`
  String get years {
    return Intl.message(
      'years',
      name: 'years',
      desc: '',
      args: [],
    );
  }

  /// `year`
  String get year {
    return Intl.message(
      'year',
      name: 'year',
      desc: '',
      args: [],
    );
  }

  /// `months`
  String get months {
    return Intl.message(
      'months',
      name: 'months',
      desc: '',
      args: [],
    );
  }

  /// `just now`
  String get justNow {
    return Intl.message(
      'just now',
      name: 'justNow',
      desc: '',
      args: [],
    );
  }

  /// `Select language`
  String get selectLanguage {
    return Intl.message(
      'Select language',
      name: 'selectLanguage',
      desc: '',
      args: [],
    );
  }

  /// `submit`
  String get submit {
    return Intl.message(
      'submit',
      name: 'submit',
      desc: '',
      args: [],
    );
  }

  /// `settings`
  String get settings {
    return Intl.message(
      'settings',
      name: 'settings',
      desc: '',
      args: [],
    );
  }

  /// `sign in`
  String get signIn {
    return Intl.message(
      'sign in',
      name: 'signIn',
      desc: '',
      args: [],
    );
  }

  /// `manage store orders`
  String get manageStoreOrders {
    return Intl.message(
      'manage store orders',
      name: 'manageStoreOrders',
      desc: '',
      args: [],
    );
  }

  /// `email address`
  String get emailAddress {
    return Intl.message(
      'email address',
      name: 'emailAddress',
      desc: '',
      args: [],
    );
  }

  /// `enter the email`
  String get enterTheEmail {
    return Intl.message(
      'enter the email',
      name: 'enterTheEmail',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get password {
    return Intl.message(
      'Password',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `enter the password`
  String get enterThePassword {
    return Intl.message(
      'enter the password',
      name: 'enterThePassword',
      desc: '',
      args: [],
    );
  }

  /// `create a new account`
  String get createNewAccount {
    return Intl.message(
      'create a new account',
      name: 'createNewAccount',
      desc: '',
      args: [],
    );
  }

  /// `Logout`
  String get logout {
    return Intl.message(
      'Logout',
      name: 'logout',
      desc: '',
      args: [],
    );
  }

  /// `change`
  String get change {
    return Intl.message(
      'change',
      name: 'change',
      desc: '',
      args: [],
    );
  }

  /// `Enter credit card Number`
  String get enterCreditCardNumber {
    return Intl.message(
      'Enter credit card Number',
      name: 'enterCreditCardNumber',
      desc: '',
      args: [],
    );
  }

  /// `Language`
  String get language {
    return Intl.message(
      'Language',
      name: 'language',
      desc: '',
      args: [],
    );
  }

  /// `Notification`
  String get notification {
    return Intl.message(
      'Notification',
      name: 'notification',
      desc: '',
      args: [],
    );
  }

  /// `FAQ`
  String get faq {
    return Intl.message(
      'FAQ',
      name: 'faq',
      desc: '',
      args: [],
    );
  }

  /// `Create account`
  String get createAccount {
    return Intl.message(
      'Create account',
      name: 'createAccount',
      desc: '',
      args: [],
    );
  }

  /// `Verification`
  String get verification {
    return Intl.message(
      'Verification',
      name: 'verification',
      desc: '',
      args: [],
    );
  }

  /// `Phone verification`
  String get phoneVerification {
    return Intl.message(
      'Phone verification',
      name: 'phoneVerification',
      desc: '',
      args: [],
    );
  }

  /// `Verify Your Number`
  String get verifyYourNumber {
    return Intl.message(
      'Verify Your Number',
      name: 'verifyYourNumber',
      desc: '',
      args: [],
    );
  }

  /// `your enter verification code is true`
  String get yourEnterVerificationCodeIsTrue {
    return Intl.message(
      'your enter verification code is true',
      name: 'yourEnterVerificationCodeIsTrue',
      desc: '',
      args: [],
    );
  }

  /// `your enter verification code is wrong or has been expired`
  String get yourEnterVerificationCodeIsWrongOrHasBeenExpired {
    return Intl.message(
      'your enter verification code is wrong or has been expired',
      name: 'yourEnterVerificationCodeIsWrongOrHasBeenExpired',
      desc: '',
      args: [],
    );
  }

  /// `Didn't receive a code?`
  String get didntReceiveACode {
    return Intl.message(
      'Didn\'t receive a code?',
      name: 'didntReceiveACode',
      desc: '',
      args: [],
    );
  }

  /// `RESEND`
  String get resend {
    return Intl.message(
      'RESEND',
      name: 'resend',
      desc: '',
      args: [],
    );
  }

  /// `Clear`
  String get clear {
    return Intl.message(
      'Clear',
      name: 'clear',
      desc: '',
      args: [],
    );
  }

  /// `Sign Up`
  String get signUp {
    return Intl.message(
      'Sign Up',
      name: 'signUp',
      desc: '',
      args: [],
    );
  }

  /// `Phone Number`
  String get phoneNumber {
    return Intl.message(
      'Phone Number',
      name: 'phoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `enter phone number`
  String get enterPhoneNumber {
    return Intl.message(
      'enter phone number',
      name: 'enterPhoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `Continue`
  String get continueto {
    return Intl.message(
      'Continue',
      name: 'continueto',
      desc: '',
      args: [],
    );
  }

  /// `Already have an account?`
  String get alreadyHaveAnAccount {
    return Intl.message(
      'Already have an account?',
      name: 'alreadyHaveAnAccount',
      desc: '',
      args: [],
    );
  }

  /// `Accept`
  String get accept {
    return Intl.message(
      'Accept',
      name: 'accept',
      desc: '',
      args: [],
    );
  }

  /// `Reject`
  String get reject {
    return Intl.message(
      'Reject',
      name: 'reject',
      desc: '',
      args: [],
    );
  }

  /// `Invoice amount`
  String get invoiceAmount {
    return Intl.message(
      'Invoice amount',
      name: 'invoiceAmount',
      desc: '',
      args: [],
    );
  }

  /// `Cash`
  String get cash {
    return Intl.message(
      'Cash',
      name: 'cash',
      desc: '',
      args: [],
    );
  }

  /// `Order number`
  String get orderNumber {
    return Intl.message(
      'Order number',
      name: 'orderNumber',
      desc: '',
      args: [],
    );
  }

  /// `status`
  String get status {
    return Intl.message(
      'status',
      name: 'status',
      desc: '',
      args: [],
    );
  }

  /// `online order`
  String get onlineOrder {
    return Intl.message(
      'online order',
      name: 'onlineOrder',
      desc: '',
      args: [],
    );
  }

  /// `current order`
  String get currentOrder {
    return Intl.message(
      'current order',
      name: 'currentOrder',
      desc: '',
      args: [],
    );
  }

  /// `in service`
  String get inService {
    return Intl.message(
      'in service',
      name: 'inService',
      desc: '',
      args: [],
    );
  }

  /// `Available`
  String get available {
    return Intl.message(
      'Available',
      name: 'available',
      desc: '',
      args: [],
    );
  }

  /// `menu management`
  String get menuManagement {
    return Intl.message(
      'menu management',
      name: 'menuManagement',
      desc: '',
      args: [],
    );
  }

  /// `product management`
  String get productManagement {
    return Intl.message(
      'product management',
      name: 'productManagement',
      desc: '',
      args: [],
    );
  }

  /// `number of menus`
  String get numberOfMenus {
    return Intl.message(
      'number of menus',
      name: 'numberOfMenus',
      desc: '',
      args: [],
    );
  }

  /// `number of products`
  String get numberOfProducts {
    return Intl.message(
      'number of products',
      name: 'numberOfProducts',
      desc: '',
      args: [],
    );
  }

  /// `in stock`
  String get inStock {
    return Intl.message(
      'in stock',
      name: 'inStock',
      desc: '',
      args: [],
    );
  }

  /// `add new menu item`
  String get addNewMenuItem {
    return Intl.message(
      'add new menu item',
      name: 'addNewMenuItem',
      desc: '',
      args: [],
    );
  }

  /// `add new meal item`
  String get addNewMealItem {
    return Intl.message(
      'add new meal item',
      name: 'addNewMealItem',
      desc: '',
      args: [],
    );
  }

  /// `view products menu`
  String get viewProductsMenu {
    return Intl.message(
      'view products menu',
      name: 'viewProductsMenu',
      desc: '',
      args: [],
    );
  }

  /// `Edit menu`
  String get editMenu {
    return Intl.message(
      'Edit menu',
      name: 'editMenu',
      desc: '',
      args: [],
    );
  }

  /// `Delete menu`
  String get deleteMenu {
    return Intl.message(
      'Delete menu',
      name: 'deleteMenu',
      desc: '',
      args: [],
    );
  }

  /// `Yes, Delete it`
  String get yesDeleteIt {
    return Intl.message(
      'Yes, Delete it',
      name: 'yesDeleteIt',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get cancel {
    return Intl.message(
      'Cancel',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `are you sure you want to delete menu`
  String get areYouSureYouWantToDeleteMenu {
    return Intl.message(
      'are you sure you want to delete menu',
      name: 'areYouSureYouWantToDeleteMenu',
      desc: '',
      args: [],
    );
  }

  /// `Fruits menu`
  String get fruitsMenu {
    return Intl.message(
      'Fruits menu',
      name: 'fruitsMenu',
      desc: '',
      args: [],
    );
  }

  /// `menu`
  String get menu {
    return Intl.message(
      'menu',
      name: 'menu',
      desc: '',
      args: [],
    );
  }

  /// `Edit`
  String get edit {
    return Intl.message(
      'Edit',
      name: 'edit',
      desc: '',
      args: [],
    );
  }

  /// `Add To A menu`
  String get addToAMenu {
    return Intl.message(
      'Add To A menu',
      name: 'addToAMenu',
      desc: '',
      args: [],
    );
  }

  /// `delete`
  String get delete {
    return Intl.message(
      'delete',
      name: 'delete',
      desc: '',
      args: [],
    );
  }

  /// `close`
  String get close {
    return Intl.message(
      'close',
      name: 'close',
      desc: '',
      args: [],
    );
  }

  /// `you have`
  String get youHave {
    return Intl.message(
      'you have',
      name: 'youHave',
      desc: '',
      args: [],
    );
  }

  /// `menus`
  String get menus {
    return Intl.message(
      'menus',
      name: 'menus',
      desc: '',
      args: [],
    );
  }

  /// `Add New Product`
  String get addNewProduct {
    return Intl.message(
      'Add New Product',
      name: 'addNewProduct',
      desc: '',
      args: [],
    );
  }

  /// `Fill Product Information`
  String get fillProductInformation {
    return Intl.message(
      'Fill Product Information',
      name: 'fillProductInformation',
      desc: '',
      args: [],
    );
  }

  /// `Enter Product Name`
  String get enterProductName {
    return Intl.message(
      'Enter Product Name',
      name: 'enterProductName',
      desc: '',
      args: [],
    );
  }

  /// `Product Name`
  String get productName {
    return Intl.message(
      'Product Name',
      name: 'productName',
      desc: '',
      args: [],
    );
  }

  /// `select a menu`
  String get selectAMenu {
    return Intl.message(
      'select a menu',
      name: 'selectAMenu',
      desc: '',
      args: [],
    );
  }

  /// `required field`
  String get requiredField {
    return Intl.message(
      'required field',
      name: 'requiredField',
      desc: '',
      args: [],
    );
  }

  /// `Deserts`
  String get deserts {
    return Intl.message(
      'Deserts',
      name: 'deserts',
      desc: '',
      args: [],
    );
  }

  /// `Italian food`
  String get italianFood {
    return Intl.message(
      'Italian food',
      name: 'italianFood',
      desc: '',
      args: [],
    );
  }

  /// `turkish food`
  String get turkishFood {
    return Intl.message(
      'turkish food',
      name: 'turkishFood',
      desc: '',
      args: [],
    );
  }

  /// `indian food`
  String get indianFood {
    return Intl.message(
      'indian food',
      name: 'indianFood',
      desc: '',
      args: [],
    );
  }

  /// `Enter price here`
  String get enterPriceHere {
    return Intl.message(
      'Enter price here',
      name: 'enterPriceHere',
      desc: '',
      args: [],
    );
  }

  /// `other widget`
  String get otherWidget {
    return Intl.message(
      'other widget',
      name: 'otherWidget',
      desc: '',
      args: [],
    );
  }

  /// `Product Size`
  String get productSize {
    return Intl.message(
      'Product Size',
      name: 'productSize',
      desc: '',
      args: [],
    );
  }

  /// `small`
  String get small {
    return Intl.message(
      'small',
      name: 'small',
      desc: '',
      args: [],
    );
  }

  /// `medium`
  String get medium {
    return Intl.message(
      'medium',
      name: 'medium',
      desc: '',
      args: [],
    );
  }

  /// `large`
  String get large {
    return Intl.message(
      'large',
      name: 'large',
      desc: '',
      args: [],
    );
  }

  /// `Enter Stock`
  String get enterStock {
    return Intl.message(
      'Enter Stock',
      name: 'enterStock',
      desc: '',
      args: [],
    );
  }

  /// `product stock`
  String get productStock {
    return Intl.message(
      'product stock',
      name: 'productStock',
      desc: '',
      args: [],
    );
  }

  /// `Write product information`
  String get writeProductInformation {
    return Intl.message(
      'Write product information',
      name: 'writeProductInformation',
      desc: '',
      args: [],
    );
  }

  /// `description about you product`
  String get descriptionAboutYouProduct {
    return Intl.message(
      'description about you product',
      name: 'descriptionAboutYouProduct',
      desc: '',
      args: [],
    );
  }

  /// `Add`
  String get add {
    return Intl.message(
      'Add',
      name: 'add',
      desc: '',
      args: [],
    );
  }

  /// `Comments`
  String get comments {
    return Intl.message(
      'Comments',
      name: 'comments',
      desc: '',
      args: [],
    );
  }

  /// `confirmed`
  String get confirmed {
    return Intl.message(
      'confirmed',
      name: 'confirmed',
      desc: '',
      args: [],
    );
  }

  /// `waiting`
  String get waiting {
    return Intl.message(
      'waiting',
      name: 'waiting',
      desc: '',
      args: [],
    );
  }

  /// `Confirm and reply`
  String get confirmAndReply {
    return Intl.message(
      'Confirm and reply',
      name: 'confirmAndReply',
      desc: '',
      args: [],
    );
  }

  /// `Reply`
  String get reply {
    return Intl.message(
      'Reply',
      name: 'reply',
      desc: '',
      args: [],
    );
  }

  /// `replying to`
  String get replyingTo {
    return Intl.message(
      'replying to',
      name: 'replyingTo',
      desc: '',
      args: [],
    );
  }

  /// `write your reply here`
  String get writeYourReplyHere {
    return Intl.message(
      'write your reply here',
      name: 'writeYourReplyHere',
      desc: '',
      args: [],
    );
  }

  /// `Submit reply`
  String get submitReply {
    return Intl.message(
      'Submit reply',
      name: 'submitReply',
      desc: '',
      args: [],
    );
  }

  /// `Support team`
  String get supportTeam {
    return Intl.message(
      'Support team',
      name: 'supportTeam',
      desc: '',
      args: [],
    );
  }

  /// `inbox`
  String get inbox {
    return Intl.message(
      'inbox',
      name: 'inbox',
      desc: '',
      args: [],
    );
  }

  /// `messages`
  String get messages {
    return Intl.message(
      'messages',
      name: 'messages',
      desc: '',
      args: [],
    );
  }

  /// `Enter your reply`
  String get enterYourReply {
    return Intl.message(
      'Enter your reply',
      name: 'enterYourReply',
      desc: '',
      args: [],
    );
  }

  /// `Order payment`
  String get orderPayment {
    return Intl.message(
      'Order payment',
      name: 'orderPayment',
      desc: '',
      args: [],
    );
  }

  /// `message subject`
  String get messageSubject {
    return Intl.message(
      'message subject',
      name: 'messageSubject',
      desc: '',
      args: [],
    );
  }

  /// `Edit Credit Card Number`
  String get editCreditCardNumber {
    return Intl.message(
      'Edit Credit Card Number',
      name: 'editCreditCardNumber',
      desc: '',
      args: [],
    );
  }

  /// `credit card number`
  String get creditCardNumber {
    return Intl.message(
      'credit card number',
      name: 'creditCardNumber',
      desc: '',
      args: [],
    );
  }

  /// `enter your credit card number here`
  String get enterYourCreditCardNumberHere {
    return Intl.message(
      'enter your credit card number here',
      name: 'enterYourCreditCardNumberHere',
      desc: '',
      args: [],
    );
  }

  /// `Change your Password`
  String get changeYourPassword {
    return Intl.message(
      'Change your Password',
      name: 'changeYourPassword',
      desc: '',
      args: [],
    );
  }

  /// `Current Password`
  String get currentPassword {
    return Intl.message(
      'Current Password',
      name: 'currentPassword',
      desc: '',
      args: [],
    );
  }

  /// `enter Current Password`
  String get enterCurrentPassword {
    return Intl.message(
      'enter Current Password',
      name: 'enterCurrentPassword',
      desc: '',
      args: [],
    );
  }

  /// `must be more than 8 characters`
  String get mustBeMoreThan8Characters {
    return Intl.message(
      'must be more than 8 characters',
      name: 'mustBeMoreThan8Characters',
      desc: '',
      args: [],
    );
  }

  /// `New Password`
  String get newPassword {
    return Intl.message(
      'New Password',
      name: 'newPassword',
      desc: '',
      args: [],
    );
  }

  /// `enter New Password`
  String get enterNewPassword {
    return Intl.message(
      'enter New Password',
      name: 'enterNewPassword',
      desc: '',
      args: [],
    );
  }

  /// `Confirm Password`
  String get confirmPassword {
    return Intl.message(
      'Confirm Password',
      name: 'confirmPassword',
      desc: '',
      args: [],
    );
  }

  /// `enter Confirm Password`
  String get enterConfirmPassword {
    return Intl.message(
      'enter Confirm Password',
      name: 'enterConfirmPassword',
      desc: '',
      args: [],
    );
  }

  /// `password confirm must match new password`
  String get passwordConfirmMustMatchNewPassword {
    return Intl.message(
      'password confirm must match new password',
      name: 'passwordConfirmMustMatchNewPassword',
      desc: '',
      args: [],
    );
  }

  /// `Change Password`
  String get changePassword {
    return Intl.message(
      'Change Password',
      name: 'changePassword',
      desc: '',
      args: [],
    );
  }

  /// `Notification Settings`
  String get notificationSettings {
    return Intl.message(
      'Notification Settings',
      name: 'notificationSettings',
      desc: '',
      args: [],
    );
  }

  /// `New Order Notification`
  String get newOrderNotification {
    return Intl.message(
      'New Order Notification',
      name: 'newOrderNotification',
      desc: '',
      args: [],
    );
  }

  /// `Order Status Notification`
  String get orderStatusNotification {
    return Intl.message(
      'Order Status Notification',
      name: 'orderStatusNotification',
      desc: '',
      args: [],
    );
  }

  /// `Payment Notification`
  String get paymentNotification {
    return Intl.message(
      'Payment Notification',
      name: 'paymentNotification',
      desc: '',
      args: [],
    );
  }

  /// `Delivery Notification`
  String get deliveryNotification {
    return Intl.message(
      'Delivery Notification',
      name: 'deliveryNotification',
      desc: '',
      args: [],
    );
  }

  /// `Message Notification`
  String get messageNotification {
    return Intl.message(
      'Message Notification',
      name: 'messageNotification',
      desc: '',
      args: [],
    );
  }

  /// `Number of sales`
  String get numberOfSales {
    return Intl.message(
      'Number of sales',
      name: 'numberOfSales',
      desc: '',
      args: [],
    );
  }

  /// `Number of orders received`
  String get numberOfOrdersReceived {
    return Intl.message(
      'Number of orders received',
      name: 'numberOfOrdersReceived',
      desc: '',
      args: [],
    );
  }

  /// `Number of successful orders`
  String get numberOfSuccessfulOrders {
    return Intl.message(
      'Number of successful orders',
      name: 'numberOfSuccessfulOrders',
      desc: '',
      args: [],
    );
  }

  /// `enter the code we just send to you`
  String get enterTheCodeWeJustSendToYou {
    return Intl.message(
      'enter the code we just send to you',
      name: 'enterTheCodeWeJustSendToYou',
      desc: '',
      args: [],
    );
  }

  /// `Fill out the following fileds to complete your profile`
  String get fillOutTheFollowingFiledsToCompleteYourProfile {
    return Intl.message(
      'Fill out the following fileds to complete your profile',
      name: 'fillOutTheFollowingFiledsToCompleteYourProfile',
      desc: '',
      args: [],
    );
  }

  /// `Full name`
  String get fullName {
    return Intl.message(
      'Full name',
      name: 'fullName',
      desc: '',
      args: [],
    );
  }

  /// `Enter the name`
  String get enterTheName {
    return Intl.message(
      'Enter the name',
      name: 'enterTheName',
      desc: '',
      args: [],
    );
  }

  /// `Entered name is too short`
  String get enteredNameIsTooShort {
    return Intl.message(
      'Entered name is too short',
      name: 'enteredNameIsTooShort',
      desc: '',
      args: [],
    );
  }

  /// `Entered email is invalid`
  String get enteredEmailIsInvalid {
    return Intl.message(
      'Entered email is invalid',
      name: 'enteredEmailIsInvalid',
      desc: '',
      args: [],
    );
  }

  /// `Entered password is too short`
  String get enteredPasswordIsTooShort {
    return Intl.message(
      'Entered password is too short',
      name: 'enteredPasswordIsTooShort',
      desc: '',
      args: [],
    );
  }

  /// `Add Courier`
  String get addCourier {
    return Intl.message(
      'Add Courier',
      name: 'addCourier',
      desc: '',
      args: [],
    );
  }

  /// `Courier full name`
  String get courierFullName {
    return Intl.message(
      'Courier full name',
      name: 'courierFullName',
      desc: '',
      args: [],
    );
  }

  /// `Enter the Courier name`
  String get enterTheCourierName {
    return Intl.message(
      'Enter the Courier name',
      name: 'enterTheCourierName',
      desc: '',
      args: [],
    );
  }

  /// `Courier phone number`
  String get courierPhoneNumber {
    return Intl.message(
      'Courier phone number',
      name: 'courierPhoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `Enter the Courier phone number`
  String get enterTheCourierPhoneNumber {
    return Intl.message(
      'Enter the Courier phone number',
      name: 'enterTheCourierPhoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `Courier email address`
  String get courierEmailAddress {
    return Intl.message(
      'Courier email address',
      name: 'courierEmailAddress',
      desc: '',
      args: [],
    );
  }

  /// `Enter the Courier email address`
  String get enterTheCourierEmailAddress {
    return Intl.message(
      'Enter the Courier email address',
      name: 'enterTheCourierEmailAddress',
      desc: '',
      args: [],
    );
  }

  /// `Courier Password`
  String get courierPassword {
    return Intl.message(
      'Courier Password',
      name: 'courierPassword',
      desc: '',
      args: [],
    );
  }

  /// `Enter the Courier Password`
  String get enterTheCourierPassword {
    return Intl.message(
      'Enter the Courier Password',
      name: 'enterTheCourierPassword',
      desc: '',
      args: [],
    );
  }

  /// `Male`
  String get male {
    return Intl.message(
      'Male',
      name: 'male',
      desc: '',
      args: [],
    );
  }

  /// `Female`
  String get female {
    return Intl.message(
      'Female',
      name: 'female',
      desc: '',
      args: [],
    );
  }

  /// `Driver Features`
  String get driverFeatures {
    return Intl.message(
      'Driver Features',
      name: 'driverFeatures',
      desc: '',
      args: [],
    );
  }

  /// `Enter the Driver Features`
  String get enterTheDriverFeatures {
    return Intl.message(
      'Enter the Driver Features',
      name: 'enterTheDriverFeatures',
      desc: '',
      args: [],
    );
  }

  /// `Vehicle name`
  String get vehicleName {
    return Intl.message(
      'Vehicle name',
      name: 'vehicleName',
      desc: '',
      args: [],
    );
  }

  /// `Enter the Vehicle name`
  String get enterTheVehicleName {
    return Intl.message(
      'Enter the Vehicle name',
      name: 'enterTheVehicleName',
      desc: '',
      args: [],
    );
  }

  /// `Restaurant Wallet transaction`
  String get restaurantWalletTransaction {
    return Intl.message(
      'Restaurant Wallet transaction',
      name: 'restaurantWalletTransaction',
      desc: '',
      args: [],
    );
  }

  /// `Total`
  String get total {
    return Intl.message(
      'Total',
      name: 'total',
      desc: '',
      args: [],
    );
  }

  /// `Input`
  String get input {
    return Intl.message(
      'Input',
      name: 'input',
      desc: '',
      args: [],
    );
  }

  /// `Output`
  String get output {
    return Intl.message(
      'Output',
      name: 'output',
      desc: '',
      args: [],
    );
  }

  /// `Add Discount code`
  String get addDiscountCode {
    return Intl.message(
      'Add Discount code',
      name: 'addDiscountCode',
      desc: '',
      args: [],
    );
  }

  /// `Add Discount`
  String get addDiscount {
    return Intl.message(
      'Add Discount',
      name: 'addDiscount',
      desc: '',
      args: [],
    );
  }

  /// `Add new discount code`
  String get addNewDiscountCode {
    return Intl.message(
      'Add new discount code',
      name: 'addNewDiscountCode',
      desc: '',
      args: [],
    );
  }

  /// `Discount Title`
  String get discountTitle {
    return Intl.message(
      'Discount Title',
      name: 'discountTitle',
      desc: '',
      args: [],
    );
  }

  /// `write a name for the promotion`
  String get writeANameForThePromotion {
    return Intl.message(
      'write a name for the promotion',
      name: 'writeANameForThePromotion',
      desc: '',
      args: [],
    );
  }

  /// `Please enter name for the discount`
  String get pleaseEnterNameForTheDiscount {
    return Intl.message(
      'Please enter name for the discount',
      name: 'pleaseEnterNameForTheDiscount',
      desc: '',
      args: [],
    );
  }

  /// `Discount Amount`
  String get discountAmount {
    return Intl.message(
      'Discount Amount',
      name: 'discountAmount',
      desc: '',
      args: [],
    );
  }

  /// `Enter the Discount amount`
  String get enterTheDiscountAmount {
    return Intl.message(
      'Enter the Discount amount',
      name: 'enterTheDiscountAmount',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a discount amount`
  String get pleaseEnterADiscountAmount {
    return Intl.message(
      'Please enter a discount amount',
      name: 'pleaseEnterADiscountAmount',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a number with two decimal points`
  String get pleaseEnterANumberWithTwoDecimalPoints {
    return Intl.message(
      'Please enter a number with two decimal points',
      name: 'pleaseEnterANumberWithTwoDecimalPoints',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a number in between 100 and 0`
  String get pleaseEnterANumberInBetween100And0 {
    return Intl.message(
      'Please enter a number in between 100 and 0',
      name: 'pleaseEnterANumberInBetween100And0',
      desc: '',
      args: [],
    );
  }

  /// `Discount`
  String get discount {
    return Intl.message(
      'Discount',
      name: 'discount',
      desc: '',
      args: [],
    );
  }

  /// `Driver Details`
  String get driverDetails {
    return Intl.message(
      'Driver Details',
      name: 'driverDetails',
      desc: '',
      args: [],
    );
  }

  /// `Call Passenger`
  String get callPassenger {
    return Intl.message(
      'Call Passenger',
      name: 'callPassenger',
      desc: '',
      args: [],
    );
  }

  /// `Order ID`
  String get orderId {
    return Intl.message(
      'Order ID',
      name: 'orderId',
      desc: '',
      args: [],
    );
  }

  /// `min`
  String get min {
    return Intl.message(
      'min',
      name: 'min',
      desc: '',
      args: [],
    );
  }

  /// `Restaurant Wallet`
  String get restaurantWallet {
    return Intl.message(
      'Restaurant Wallet',
      name: 'restaurantWallet',
      desc: '',
      args: [],
    );
  }

  /// `Detail`
  String get detail {
    return Intl.message(
      'Detail',
      name: 'detail',
      desc: '',
      args: [],
    );
  }

  /// `Edit Information`
  String get editInformation {
    return Intl.message(
      'Edit Information',
      name: 'editInformation',
      desc: '',
      args: [],
    );
  }

  /// `Add Products`
  String get addProducts {
    return Intl.message(
      'Add Products',
      name: 'addProducts',
      desc: '',
      args: [],
    );
  }

  /// `Add Menus`
  String get addMenus {
    return Intl.message(
      'Add Menus',
      name: 'addMenus',
      desc: '',
      args: [],
    );
  }

  /// `Select`
  String get select {
    return Intl.message(
      'Select',
      name: 'select',
      desc: '',
      args: [],
    );
  }

  /// `Selected`
  String get selected {
    return Intl.message(
      'Selected',
      name: 'selected',
      desc: '',
      args: [],
    );
  }

  /// `Courier`
  String get courier {
    return Intl.message(
      'Courier',
      name: 'courier',
      desc: '',
      args: [],
    );
  }

  /// `Select Courier`
  String get selectCourier {
    return Intl.message(
      'Select Courier',
      name: 'selectCourier',
      desc: '',
      args: [],
    );
  }

  /// `Fund Status`
  String get fundStatus {
    return Intl.message(
      'Fund Status',
      name: 'fundStatus',
      desc: '',
      args: [],
    );
  }

  /// `Full Report`
  String get fullReport {
    return Intl.message(
      'Full Report',
      name: 'fullReport',
      desc: '',
      args: [],
    );
  }

  /// `Total Amount`
  String get totalAmount {
    return Intl.message(
      'Total Amount',
      name: 'totalAmount',
      desc: '',
      args: [],
    );
  }

  /// `Today`
  String get today {
    return Intl.message(
      'Today',
      name: 'today',
      desc: '',
      args: [],
    );
  }

  /// `Select time range`
  String get selectTimeRange {
    return Intl.message(
      'Select time range',
      name: 'selectTimeRange',
      desc: '',
      args: [],
    );
  }

  /// `Last 3 days`
  String get last3Days {
    return Intl.message(
      'Last 3 days',
      name: 'last3Days',
      desc: '',
      args: [],
    );
  }

  /// `Last 7 days`
  String get last7Days {
    return Intl.message(
      'Last 7 days',
      name: 'last7Days',
      desc: '',
      args: [],
    );
  }

  /// `Done`
  String get done {
    return Intl.message(
      'Done',
      name: 'done',
      desc: '',
      args: [],
    );
  }

  /// `Date Promotion Period`
  String get datePromotionPeriod {
    return Intl.message(
      'Date Promotion Period',
      name: 'datePromotionPeriod',
      desc: '',
      args: [],
    );
  }

  /// `Set Time`
  String get setTime {
    return Intl.message(
      'Set Time',
      name: 'setTime',
      desc: '',
      args: [],
    );
  }

  /// `Total amount today`
  String get totalAmountToday {
    return Intl.message(
      'Total amount today',
      name: 'totalAmountToday',
      desc: '',
      args: [],
    );
  }

  /// `Tracking`
  String get tracking {
    return Intl.message(
      'Tracking',
      name: 'tracking',
      desc: '',
      args: [],
    );
  }

  /// `Restaurant name`
  String get restaurantName {
    return Intl.message(
      'Restaurant name',
      name: 'restaurantName',
      desc: '',
      args: [],
    );
  }

  /// `Enter the restaurant name`
  String get enterTheRestaurantName {
    return Intl.message(
      'Enter the restaurant name',
      name: 'enterTheRestaurantName',
      desc: '',
      args: [],
    );
  }

  /// `latitude`
  String get latitude {
    return Intl.message(
      'latitude',
      name: 'latitude',
      desc: '',
      args: [],
    );
  }

  /// `longitude`
  String get longitude {
    return Intl.message(
      'longitude',
      name: 'longitude',
      desc: '',
      args: [],
    );
  }

  /// `Restaurant location`
  String get restaurantLocation {
    return Intl.message(
      'Restaurant location',
      name: 'restaurantLocation',
      desc: '',
      args: [],
    );
  }

  /// `Restaurant Phone Number`
  String get restaurantPhoneNumber {
    return Intl.message(
      'Restaurant Phone Number',
      name: 'restaurantPhoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `Enter the restaurant phone number`
  String get enterTheRestaurantPhoneNumber {
    return Intl.message(
      'Enter the restaurant phone number',
      name: 'enterTheRestaurantPhoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `Set time restaurant`
  String get setTimeRestaurant {
    return Intl.message(
      'Set time restaurant',
      name: 'setTimeRestaurant',
      desc: '',
      args: [],
    );
  }

  /// `Open time`
  String get openTime {
    return Intl.message(
      'Open time',
      name: 'openTime',
      desc: '',
      args: [],
    );
  }

  /// `Close time`
  String get closeTime {
    return Intl.message(
      'Close time',
      name: 'closeTime',
      desc: '',
      args: [],
    );
  }

  /// `Confirm`
  String get confirm {
    return Intl.message(
      'Confirm',
      name: 'confirm',
      desc: '',
      args: [],
    );
  }

  /// `Expire Discount`
  String get ExpireDiscount {
    return Intl.message(
      'Expire Discount',
      name: 'ExpireDiscount',
      desc: '',
      args: [],
    );
  }

  /// `expire Discount`
  String get expireDiscount {
    return Intl.message(
      'expire Discount',
      name: 'expireDiscount',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to expire this discount? This operation cannot be returned.`
  String get expireDiscountConfirm {
    return Intl.message(
      'Are you sure you want to expire this discount? This operation cannot be returned.',
      name: 'expireDiscountConfirm',
      desc: '',
      args: [],
    );
  }

  /// `Edit Discount`
  String get editDiscount {
    return Intl.message(
      'Edit Discount',
      name: 'editDiscount',
      desc: '',
      args: [],
    );
  }

  /// `Edit this discount`
  String get editThisDiscount {
    return Intl.message(
      'Edit this discount',
      name: 'editThisDiscount',
      desc: '',
      args: [],
    );
  }

  /// `Christmas2021`
  String get discountTitleHint {
    return Intl.message(
      'Christmas2021',
      name: 'discountTitleHint',
      desc: '',
      args: [],
    );
  }

  /// `You have not created any discounts yet. Try adding some discounts`
  String get noDiscountsMessage {
    return Intl.message(
      'You have not created any discounts yet. Try adding some discounts',
      name: 'noDiscountsMessage',
      desc: '',
      args: [],
    );
  }

  /// `An error has  occurred while getting discounts, Please check your connection and try again`
  String get errorWhileFetchingDiscounts {
    return Intl.message(
      'An error has  occurred while getting discounts, Please check your connection and try again',
      name: 'errorWhileFetchingDiscounts',
      desc: '',
      args: [],
    );
  }

  /// `Start date:`
  String get startDate {
    return Intl.message(
      'Start date:',
      name: 'startDate',
      desc: '',
      args: [],
    );
  }

  /// `End date:`
  String get endDate {
    return Intl.message(
      'End date:',
      name: 'endDate',
      desc: '',
      args: [],
    );
  }

  /// `Yes, Expire it`
  String get yesExpireIt {
    return Intl.message(
      'Yes, Expire it',
      name: 'yesExpireIt',
      desc: '',
      args: [],
    );
  }

  /// `Discount title should be longer than 3 characters`
  String get tooShortDiscountTitle {
    return Intl.message(
      'Discount title should be longer than 3 characters',
      name: 'tooShortDiscountTitle',
      desc: '',
      args: [],
    );
  }

  /// `Discount title should be shorter than 16 characters`
  String get tooLongDiscountTitle {
    return Intl.message(
      'Discount title should be shorter than 16 characters',
      name: 'tooLongDiscountTitle',
      desc: '',
      args: [],
    );
  }

  /// `select start date`
  String get selectStartDate {
    return Intl.message(
      'select start date',
      name: 'selectStartDate',
      desc: '',
      args: [],
    );
  }

  /// `select end date`
  String get selectEndDate {
    return Intl.message(
      'select end date',
      name: 'selectEndDate',
      desc: '',
      args: [],
    );
  }

  /// `Something went wrong when getting online orders. Check your connection and try again later.`
  String get errorWhileFetchingOnlineOrders {
    return Intl.message(
      'Something went wrong when getting online orders. Check your connection and try again later.',
      name: 'errorWhileFetchingOnlineOrders',
      desc: '',
      args: [],
    );
  }

  /// `You have no online orders.`
  String get noOnlineOrders {
    return Intl.message(
      'You have no online orders.',
      name: 'noOnlineOrders',
      desc: '',
      args: [],
    );
  }

  /// `Failed, Try again`
  String get tryAcceptOrderAgain {
    return Intl.message(
      'Failed, Try again',
      name: 'tryAcceptOrderAgain',
      desc: '',
      args: [],
    );
  }

  /// `Total Orders today`
  String get totalOrdersToday {
    return Intl.message(
      'Total Orders today',
      name: 'totalOrdersToday',
      desc: '',
      args: [],
    );
  }

  /// `preparation time`
  String get preparationTime {
    return Intl.message(
      'preparation time',
      name: 'preparationTime',
      desc: '',
      args: [],
    );
  }

  /// `Type of courier`
  String get typeOfCourier {
    return Intl.message(
      'Type of courier',
      name: 'typeOfCourier',
      desc: '',
      args: [],
    );
  }

  /// `write your messages here`
  String get chatHint {
    return Intl.message(
      'write your messages here',
      name: 'chatHint',
      desc: '',
      args: [],
    );
  }

  /// `An error occurred while loading conversations. Please check your connection and try again.`
  String get conversationsLoadFailed {
    return Intl.message(
      'An error occurred while loading conversations. Please check your connection and try again.',
      name: 'conversationsLoadFailed',
      desc: '',
      args: [],
    );
  }

  /// `File`
  String get File {
    return Intl.message(
      'File',
      name: 'File',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to reject this order?`
  String get rejectOrderConfirmation {
    return Intl.message(
      'Are you sure you want to reject this order?',
      name: 'rejectOrderConfirmation',
      desc: '',
      args: [],
    );
  }

  /// `yes, Reject it`
  String get yesRejectIt {
    return Intl.message(
      'yes, Reject it',
      name: 'yesRejectIt',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'az'),
      Locale.fromSubtags(languageCode: 'ru'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}