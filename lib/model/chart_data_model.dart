class ChartDataModel {
  List<int> numberOfSales;
  List<int> receivedOrders;
  List<int> successfulOrders;
  List<int> unSuccessfulOrders;
  List<int> returnedOrders;
  List<double> cashDaySales;
  List<double> cardDaySales;
  List<double> companyCommission;

  List<DateTime> dates;

  ChartDataModel({
    this.numberOfSales,
    this.receivedOrders,
    this.successfulOrders,
    this.unSuccessfulOrders,
    this.returnedOrders,
    this.cashDaySales,
    this.cardDaySales,
    this.companyCommission,
    this.dates,
  });
}
