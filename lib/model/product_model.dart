class Product {
  String id;
  String titleAZ;
  String titleEN;
  String titleRU;

  Product.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson['_id'];
    titleAZ = parsedJson['title'][0]['value'];
    titleEN = parsedJson['title'][1]['value'];
    titleRU = parsedJson['title'][2]['value'];
  }
}
