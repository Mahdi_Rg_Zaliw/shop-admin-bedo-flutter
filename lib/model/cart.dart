import 'package:bedo_shop_admin/model/cart_product.dart';
import 'package:meta/meta.dart';

class Cart {
  String id;
  String currency;
  double productsPrice;
  double shipmentCost;
  double finalPrice;
  double discount;
  double afterDiscountPrice;
  List<CartProduct> products;

  Cart({
    @required this.id,
    this.currency,
    this.productsPrice,
    this.shipmentCost,
    this.finalPrice,
    this.discount,
    this.afterDiscountPrice,
    @required this.products,
  });
}
