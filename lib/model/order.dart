import 'package:meta/meta.dart';
import 'package:bedo_shop_admin/model/cart.dart';

enum ShipmentModel { BEDO, SHOP }
enum OrderStatus {
  preparing,
  shipping,
  pending,
  delivered,
  rejected,
  delivery_not_accepted,
  finished_due_to_not_paying
}

class Order {
  String id;
  Cart cart;
  String description;
  String trackID;
  DateTime estimatedDelivery;
  DateTime createdAt;
  double totalPrice;
  double finalPrice;
  double tax;
  double courierFee;
  bool inputOrder;

  Order({
    @required this.id,
    @required this.cart,
    // this.description,
    // this.trackID,
    // this.estimatedDelivery,
    this.createdAt,
    this.finalPrice,
    this.totalPrice,
    this.tax,
    this.courierFee,
    this.inputOrder,
    this.description,
    this.trackID,
    this.estimatedDelivery,
    this.userLocation,
    this.status,
  });
  Map userLocation;
  OrderStatus status;

  static OrderStatus parseOrderStatus(String status) {
    print(status);
    switch (status) {
      case 'ACCEPTED':
        return OrderStatus.preparing;
        break;
      case 'PREPARING':
        return OrderStatus.preparing;
        break;
      case 'SHIPPING':
        return OrderStatus.shipping;
        break;
      case 'PENDING':
        return OrderStatus.pending;
        break;
      case 'DELIVERED':
        return OrderStatus.delivered;
        break;
      case 'REJECTED':
        return OrderStatus.rejected;
        break;
      case 'DELIVERY_NOT_ACCEPTED':
        return OrderStatus.delivery_not_accepted;
        break;
      case 'FINISHED_DUE_TO_NOT_PAYING':
        return OrderStatus.finished_due_to_not_paying;
        break;
      default:
        return OrderStatus.pending;
    }
  }
}

class AcceptOrderModel {
  String id;
  ShipmentModel shipmentModel;
  int preparationTime;
  String driverID;

  AcceptOrderModel(this.id, this.shipmentModel, this.preparationTime,
      [this.driverID])
      : assert((shipmentModel == ShipmentModel.SHOP) ^ (driverID == null));
}
