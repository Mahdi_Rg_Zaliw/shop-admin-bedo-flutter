import 'package:flutter/foundation.dart';

enum UserType { User, Driver }

class ChatUser {
  final String ID;
  final String name;
  final UserType userType;

  const ChatUser({
    @required this.ID,
    @required this.name,
    @required this.userType,
  });
}
