class Menu {
  String id;
  String titleEN;
  String titleRU;
  String titleAZ;

  Menu.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson['_id'];
    titleEN = parsedJson['name'][0]['value'];
    titleRU = parsedJson['name'][1]['value'];
    titleAZ = parsedJson['name'][2]['value'];
  }
}
