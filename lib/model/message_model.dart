import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

enum MessageSenderType { User, Driver, Admin }
enum MessageType { Text, Upload }

class Message extends Equatable {
  final String id;
  final String text;
  final DateTime time;
  final MessageType type;
  final MessageSenderType senderType;
  final String fileName;
  final String fileNameToShow;

  // final String driverID;
  // final String userID;

  @override
  List<Object> get props => [id, text, time, type, senderType];

  const Message({
    @required this.id,
    @required this.text,
    @required this.time,
    @required this.type,
    @required this.senderType,
    this.fileName,
    this.fileNameToShow,
  });
// this.driverID,
// this.userID})
// : assert(driverID != null || userID != null);
}
