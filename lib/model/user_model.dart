class User {
  String emailOrPhoneNumber;
  String phoneNumber;
  String phoneSignUpCode;

  User({
    this.emailOrPhoneNumber = '',
    this.phoneNumber = '',
    this.phoneSignUpCode,
  });

  User.fromJson(Map<String, dynamic> json) {
    emailOrPhoneNumber = json['emailOrPhoneNumber'];
    phoneNumber = json['phoneNumber'];
    phoneSignUpCode = json['phoneSignUpCode'];
  }
}
