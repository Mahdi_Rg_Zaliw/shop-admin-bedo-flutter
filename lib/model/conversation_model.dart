import 'package:bedo_shop_admin/model/chat_user.dart';
import 'package:bedo_shop_admin/model/message_model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class Conversation extends Equatable {
  final String ID;
  final String title;
  final ChatUser user;
  Message lastMessage;
  List<Message> messages;
  final DateTime updatedAt;
  final bool repliedByAdmin;

  Conversation({
    @required this.ID,
    @required this.title,
    @required this.user,
    @required this.lastMessage,
    @required this.messages,
    @required this.updatedAt,
    @required this.repliedByAdmin,
  });

  @override
  List<Object> get props =>
      [messages, lastMessage, updatedAt, repliedByAdmin, user, title, ID];

  void setMessages(List<Message> messages) {
    this.messages = messages;
    this.lastMessage = messages.first;
  }
}
