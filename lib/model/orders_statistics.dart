class OrdersStatistics {
  final int numberOfSales;
  final int receivedOrders;
  final int successfulOrders;
  final int unSuccessfulOrders;
  final int returnedOrders;
  final double cashDaySales;
  final double cardDaySales;
  final double companyCommission;

  OrdersStatistics({
    this.numberOfSales = 0,
    this.receivedOrders = 0,
    this.successfulOrders = 0,
    this.unSuccessfulOrders = 0,
    this.returnedOrders = 0,
    this.cashDaySales = 0,
    this.cardDaySales = 0,
    this.companyCommission = 0,
  });

  Map<String, num> toMap() {
    final result = {
      'Number of sales': this.numberOfSales,
      'Number of orders received': this.receivedOrders,
      'Number of successful orders': this.successfulOrders,
      'Number of unsuccessful orders': this.unSuccessfulOrders,
      'Number of returned orders': this.returnedOrders,
      'Cash sales': this.cashDaySales,
      'Card sales': this.cardDaySales,
      'Company commission': this.companyCommission,
    };
    return result;
  }
}
