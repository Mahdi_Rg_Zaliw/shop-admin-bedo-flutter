import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

class Comment extends Equatable {
  final String fullUserName;
  final List<dynamic> rank;
  final String commentsText;
  final DateTime createdAt;
  final String id;
  final AdminReply adminReply;
  final String pfp;

  Comment.accepted({
    @required this.pfp,
    @required this.fullUserName,
    @required this.rank,
    @required this.commentsText,
    @required this.createdAt,
    @required this.id,
    @required this.adminReply,
  });

  String get rate {
    List<dynamic> z = rank.map((e) => e['rate']).toList();
    var s = z.fold(0, (p, e) => (p + e)) / z.length;
    s = (s > 0) ? s : 'not rated';
    return s.toString();
  }

  Comment.pending({
    @required this.pfp,
    @required this.fullUserName,
    @required this.rank,
    @required this.commentsText,
    @required this.createdAt,
    @required this.id,
  }) : this.adminReply = null;
  @override
  String toString() {
    var timeFormat = DateFormat.yMMMd();
    var c = timeFormat.format(createdAt);
    // fullUserName:  $fullUserName createdAt:  $createdAt rate:
    return ' << fullUserName:$fullUserName -- createdAt:$c -- rate:$rate >>';
  }

  @override
  List<Object> get props => [fullUserName, rank, commentsText, createdAt];
}

class AdminReply extends Equatable {
  final Admin admin;
  final String replyText;

  AdminReply({this.admin, this.replyText});

  @override
  List<Object> get props => [admin, replyText];
}

class Admin extends Equatable {
  final String name;
  final String id;

  Admin({this.name, this.id});

  @override
  List<Object> get props => [name, id];
}
