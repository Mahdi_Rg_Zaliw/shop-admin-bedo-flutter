import 'package:bedo_shop_admin/model/product.dart';

class CartProduct{
  Product product;
  int quantity;

  CartProduct(this.product, this.quantity);
}