import 'package:meta/meta.dart';

class Product {
  String id;
  String title;
  double price;

  Product({
    @required this.id,
    @required this.title,
    @required this.price,
  });

  @override
  String toString() {
    return this.id + ' ' + this.title + ' ' + this.price.toString();
  }
}
