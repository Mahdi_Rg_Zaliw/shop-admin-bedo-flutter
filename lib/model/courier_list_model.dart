class CourierList {
  String id;
  String fullName;
  String email;
  String phoneNumber;
  String gender;
  String profileImageUrl;

  CourierList.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson['_id'];
    fullName = parsedJson['fullName'];
    email = parsedJson['email'];
    phoneNumber = parsedJson['phoneNumber'];
    gender = parsedJson['gender'];
    profileImageUrl = parsedJson['profileImageUrl'];
  }
}
